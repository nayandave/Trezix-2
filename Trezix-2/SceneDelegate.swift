//
//  SceneDelegate.swift
//  Trezix-2
//
//  Created by Amar Panchal on 20/06/22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    var savedShortCutItem: UIApplicationShortcutItem?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        if appDelegate.isNetworkAvailable {
            if let shortcutItem = connectionOptions.shortcutItem {
                // Save it off for later when we become active.
                savedShortCutItem = shortcutItem
            }
            guard let _ = (scene as? UIWindowScene) else { return }
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
        save_BadgeCount(0)
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    func windowScene(_ windowScene: UIWindowScene,
                     performActionFor shortcutItem: UIApplicationShortcutItem,
                     completionHandler: @escaping (Bool) -> Void) {
        print("SHORTCUT Item = \(shortcutItem.type)")
        let handeled = handleShortCutItem(shortcut: shortcutItem)
        completionHandler(handeled)
    }
}

// MARK: - Redirection To VCs Functions
extension SceneDelegate {
    public func redirect_HomeStoryboard() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let initController = homeStoryboard.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
            initController.modalPresentationStyle = .overCurrentContext
            
            // Assign it to rootViewController
            self.window?.rootViewController = nil
            
            let options: UIView.AnimationOptions = .transitionCurlDown
            let duration = 0.75
            UIView.transition(with: self.window!, duration: duration, options: options, animations: {}, completion: nil)
            self.window?.rootViewController = initController
            self.window?.makeKeyAndVisible()
        }
    }

    public func redirect_LoginStoryboard() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            // Get initial viewController
            
            let initController = loginStoryboard.instantiateViewController(withIdentifier: loginNavIdentifier) as! UINavigationController
            initController.modalPresentationStyle = .overCurrentContext
            
            // Assign it to rootViewController
            strongSelf.window?.rootViewController = nil
    //        self.window?.makeKeyAndVisible()
            let options: UIView.AnimationOptions = .transitionCurlUp
            let duration = 0.75
            UIView.transition(with: strongSelf.window!, duration: duration, options: options, animations: {}, completion: nil)
            strongSelf.window?.rootViewController = initController
            strongSelf.window?.makeKeyAndVisible()
        }
    }
    
    /// Handles Quick Shortcut and Redirects to respective ShortCut related View Controller
    /// - Parameter shortcut : called from `willConnectTo` Scene Delegate Method
    /// - Returns:Boolean value that indicates if shortcut is handled.
    public func handleShortCutItem(shortcut: UIApplicationShortcutItem) -> Bool {
        if let topVC = UIApplication.topViewController() {
            if let mainNavController = topVC.children[0] as? UINavigationController {
                if shortcut.type == AllQuickShortcutTypes.report.rawValue {
                    mainNavController.sideMenuController?.revealMenu()
                    NotificationCenter.default.post(name: .select_Report_Notificaiton, object: nil)
//                    if !mainNavController.children.last!.isKind(of: FormOrderReportVC.self) {
//                        let vc = allReportStoryboard.instantiateViewController(withIdentifier: "FormOrderReportVC")
//                        vc.modalPresentationStyle = .fullScreen
//                        mainNavController.present(vc, animated: true)
//                        return true
//                    }
                } else if shortcut.type == AllQuickShortcutTypes.notification.rawValue {
                    if !mainNavController.children.last!.isKind(of: HomepageVC.self) {
                        mainNavController.popToRootViewController(animated: true)
                        return true
                    }
                } else if shortcut.type == AllQuickShortcutTypes.notification.rawValue {
                    if !mainNavController.children.last!.isKind(of: Notification_List_VC.self) {
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "Notification_List_VC")
                        vc.modalPresentationStyle = .fullScreen
                        mainNavController.present(vc, animated: true)
                        return true
                    }
                }
            }
        }
        return false
    }
}
