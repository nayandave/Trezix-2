//
//  Constants.swift
//  HappyRide_Partner
//
//  Created by Amar Panchal on 13/10/21.
//
import UIKit
import Alamofire

//MARK: ----------- API Path Constants ----------

///*Changable BaseURL According To Signup API Response*
public var API_BaseURL: String {
    let domain = userDefault.string(forKey: "savedUserDomain") ?? "dev"
    //let url = "https://\(domain).tre-zix.com/"
    let url = "https://\(domain).trezix.io/"
    return url
}

///*Will Return Main Base URL for specific User if user is Authenticated*
//let generalURL = "https://singup.tre-zix.com/api/"
let generalURL = "https://signup.trezix.io/api/"

public var default_app_header: HTTPHeader {
    let header: HTTPHeader = .authorization(username: unique_device_id, password: mainUserData.appToken)
    debugPrint("USERNAME = \(unique_device_id) & PASSWORD = \(mainUserData.appToken)")
    return header
}

/// **Contains All Keyword For All APIs**
///  - Must Be Used with **Internal "fullPath" Function**
///    # USAGE EXAMPLE #
///  ```
///  NetworkClient
///  .networkRequest(apiName: APIConstants.VALIDATE_MAIL.fullPath(),..)
///  ```
public enum APIConstants : String {
    
    // LogIn Page APIs
    case VALIDATE_MAIL = "user_domain"
    case LOGIN = "user_login"
    case LOGOUT = "user_logout"
    case CHANGE_PASSWORD = "changePassword"
      
    // Home - Dashboard Page APIs
    case DASHBOARD_DATA = "dashboard"
    case GET_NOTIFICATION = "notification"
    
    // New Dashboard
    
    // Import Dashboard
    case IMPORT_DASHBOARD = "dashboard-import"
    case EXPORT_DASHBOARD = "dashboard-export-new"
    
    // More Details Import APIs
    case IMPORT_INVOICE_MORE_DETAILS = "dashboard-invoice-detail"
    case IMPORT_ATBANK_MORE_DETAILS = "dashboard-at-bank-detail"
    case IMPORT_INCLEARING_MORE_DETAILS = "dashboard-clearing-detail"
    case IMPORT_EXPECTED_MORE_DETAILS = "dashboard-except-arrive-detail"
    
    case IMPORT_PENDING_ORDER_MORE_DETAILS = "dashboard-pending-order-detail"
    
    case IMPORT_CLOSED_ORDER_MORE_DETAILS = "dashboard-close-order-detail"
    
    case IMPORT_LICENCE_INFO_MORE_DETAILS = "dashboard-licence-detail"
    
    case EVENT_LIST_IMPORT = "dashboard-event-detail"
    
    case IMPORT_SHIPMENT_TRACKING_MORE_DETAILS = "dashboard-shipment-tracking-detail"
    
    
    // More Details Export APIs
    case EXPORT_OPEN_ORDER_DETAILS = "dashboard-export-open-order-detail"
    case EXPORT_SHIPMENT_DETAILS = "dashboard-export-shipment-detail"
    case EXPORT_COMPLETE_ORDER_DETAILS = "dashboard-export-complete-order-detail"
    case EXPORT_DISPATCH_DETAILS = "dashboard-export-dispatch-planing-detail"
    
    case EVENT_LIST_EXPORT = "dashboard-export-event-detail"
    
    case EXPORT_SHIPMENT_TRACKING_MORE_DETAILS = "dashboard-export-shipment-tracking-detail"
    
    
    // Report Pages APIs
    case GET_ORDER_REPORT = "order-report"
    case SHIPMENT_REPORT = "shipment-report"
    case GET_MATERIAL_DROPDOWN = "material"
    case GET_MATERIAL_GROUP_DROPDOWN = "material-group"
    case GET_SALES_REPORT = "salesreport"
    case GET_SALES_ORDER_REPORT = "salesorder"
    case GET_INVOICE_LIST = "getInvoices"
    case LANDING_COST_INVOICE = "LandingCostNew"
    case LANDING_COST_REPORT = "LandingCost"
    case DISPATCH_PLANNING_REPORT = "dispatchplanning"
    case GET_PSI_LIST = "pre-shipment-invoice"
    case SCRIPT_DRAWBACK_REPORT = "script-drawback-report"
    
    
    // Approval Mechanism APIs
    case GET_APPROVAL_LIST = "approval-list"
    case GET_PO_APPROVAL_DETAIL = "poView"
    case GET_SO_APPROVAL_DETAIL = "soView"
    case GET_PSI_APPROVAL_DETAIL = "psiView"
    case GET_DUTY_CLEARING_APPROVAL_DETAIL = "dutyClearingView"
    case APPROVE_REJCT_APPROVAL = "approve-process"
    
    
    // New Landing Report APIs
    case GET_ALL_DROPDOWNS_LANDING_COST = "getLandingCostFilter"
    case GET_PLANT_LIST_FROM_COMPANY = "getCompanyPlant"
    
    
    // PO TimeLine APIs
    case FETCH_PO_LIST = "getPurchaseOrders"
    case PO_TIMELINE_DETAILS = "poTimeline"
    
    // EximGPT
    case ASK_GPT = "http://3.108.230.4:8083/ask_question"
    
    // Vessel Tracking
    case VESSEL_LIST = "vessel-list"
    case VESSEL_INFO = "vessel-detail"
    
    // Document Upload
    case FETCH_DOC_LIST = "doc-list"
    case DELETE_DOC = "doc-delete"
    case UPLOAD_DOC = "upload-doc"
    
    // Quotation System
    case QUOTATION_LIST = "quotation-list"
    case QUOTATION_DETAIL = "quotation-get"
    
    
    /// **Merges API Constant With Full API**
    /// - Returns: API BaseURL merged with selected Enum Value in String Type
    func fullPath() -> String {
        let fullAPIPath = (self == .VALIDATE_MAIL ? generalURL : API_BaseURL) + self.rawValue
        return fullAPIPath
    }
}




let api = """
https://domain.trezix.io/user_domain
https://domain.trezix.io/user_login
https://domain.trezix.io/user_logout
https://domain.trezix.io/changePassword
https://domain.trezix.io/dashboard
https://domain.trezix.io/notification
https://domain.trezix.io/dashboard-import
https://domain.trezix.io/dashboard-export-new
https://domain.trezix.io/dashboard-invoice-detail
https://domain.trezix.io/dashboard-at-bank-detail
https://domain.trezix.io/dashboard-clearing-detail
https://domain.trezix.io/dashboard-except-arrive-detail
https://domain.trezix.io/dashboard-pending-order-detail
https://domain.trezix.io/dashboard-close-order-detail
https://domain.trezix.io/dashboard-licence-detail
https://domain.trezix.io/dashboard-event-detail
https://domain.trezix.io/dashboard-shipment-tracking-detail
https://domain.trezix.io/dashboard-export-open-order-detail
https://domain.trezix.io/dashboard-export-shipment-detail
https://domain.trezix.io/dashboard-export-complete-order-detail
https://domain.trezix.io/dashboard-export-dispatch-planing-detail
https://domain.trezix.io/dashboard-export-event-detail
https://domain.trezix.io/dashboard-export-shipment-tracking-detail
https://domain.trezix.io/order-report
https://domain.trezix.io/shipment-report
https://domain.trezix.io/material
https://domain.trezix.io/material-group
https://domain.trezix.io/salesreport
https://domain.trezix.io/salesorder
https://domain.trezix.io/getInvoices
https://domain.trezix.io/LandingCostNew
https://domain.trezix.io/LandingCost
https://domain.trezix.io/dispatchplanning
https://domain.trezix.io/pre-shipment-invoice
https://domain.trezix.io/script-drawback-report
https://domain.trezix.io/approval-list
https://domain.trezix.io/poView
https://domain.trezix.io/soView
https://domain.trezix.io/psiView
https://domain.trezix.io/dutyClearingView
https://domain.trezix.io/approve-process
https://domain.trezix.io/getLandingCostFilter
https://domain.trezix.io/getCompanyPlant
https://domain.trezix.io/getPurchaseOrders
https://domain.trezix.io/poTimeline
https://domain.trezix.io/vessel-list
https://domain.trezix.io/vessel-detail
https://domain.trezix.io/doc-list
https://domain.trezix.io/doc-delete
https://domain.trezix.io/upload-doc
https://domain.trezix.io/quotation-list
https://domain.trezix.io/quotation-get
http://3.108.230.4:8083/ask_question
"""
