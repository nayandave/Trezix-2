//
//  Approval_PO_Detail_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 21/10/22.
//

import Foundation

// MARK: - Approval PO Detail Response Modal
public struct Approval_PO_Response_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: PO_Detail_Modal?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Approval PO Item Detail
public struct PO_Detail_Modal: Codable {
    public var poID: Int?
    public var company: String?
    public var companyAddress: String?
    public var companyAddress2: String?
    public var mobileNumber: String?
    public var email: String?
    public var poNo: String?
    public var poDate: String?
    public var vendor: String?
    public var vendorAddress1: String?
    public var vendorAddress2: String?
    public var vendorAddress3: String?
    public var vendorAddress4: String?
    public var vendorAddressRegion: String?
    public var vendorCountry: String?
    public var vendorMobileNumber: String?
    public var bank: [Bank_Detail_Modal]?
    public var shipBy: String?
    public var countryOfOriginGoods: String?
    public var incoTerm: String?
    public var incoTerm2: String?
    public var finalDestination: String?
    public var finalDestinationCountry: String?
    public var paymentTerm: String?
    public var shippingInstruction: String?
    public var shippingSchedule: String?
    public var remark: String?
    public var packingInstruction: String?
    public var productLineItem: [POProductLineItem]?
    public var total: String?

    enum CodingKeys: String, CodingKey {
        case poID = "po_id"
        case company = "company"
        case companyAddress = "company_address"
        case companyAddress2 = "company_address2"
        case mobileNumber = "mobile_number"
        case email = "email"
        case poNo = "po_no"
        case poDate = "po_date"
        case vendor = "vendor"
        case vendorAddress1 = "vendor_address1"
        case vendorAddress2 = "vendor_address2"
        case vendorAddress3 = "vendor_address3"
        case vendorAddress4 = "vendor_address4"
        case vendorAddressRegion = "vendor_address_region"
        case vendorCountry = "vendor_country"
        case vendorMobileNumber = "vendor_mobile_number"
        case bank = "bank"
        case shipBy = "ship_by"
        case countryOfOriginGoods = "country_of_origin_goods"
        case incoTerm = "inco_term"
        case incoTerm2 = "inco_term_2"
        case finalDestination = "final_destination"
        case finalDestinationCountry = "final_destination_country"
        case paymentTerm = "payment_term"
        case shippingInstruction = "shipping_instruction"
        case shippingSchedule = "shipping_schedule"
        case remark = "remark"
        case packingInstruction = "packing_instruction"
        case productLineItem = "product_line_item"
        case total = "total"
    }

    public init(poID: Int?, company: String?, companyAddress: String?, companyAddress2: String?, mobileNumber: String?, email: String?, poNo: String?, poDate: String?, vendor: String?, vendorAddress1: String?, vendorAddress2: String?, vendorAddress3: String?, vendorAddress4: String?, vendorAddressRegion: String?, vendorCountry: String?, vendorMobileNumber: String?, bank: [Bank_Detail_Modal]?, shipBy: String?, countryOfOriginGoods: String?, incoTerm: String?, incoTerm2: String?, finalDestination: String?, finalDestinationCountry: String?, paymentTerm: String?, shippingInstruction: String?, shippingSchedule: String?, remark: String?, packingInstruction: String?, productLineItem: [POProductLineItem]?, total: String?) {
        self.poID = poID
        self.company = company
        self.companyAddress = companyAddress
        self.companyAddress2 = companyAddress2
        self.mobileNumber = mobileNumber
        self.email = email
        self.poNo = poNo
        self.poDate = poDate
        self.vendor = vendor
        self.vendorAddress1 = vendorAddress1
        self.vendorAddress2 = vendorAddress2
        self.vendorAddress3 = vendorAddress3
        self.vendorAddress4 = vendorAddress4
        self.vendorAddressRegion = vendorAddressRegion
        self.vendorCountry = vendorCountry
        self.vendorMobileNumber = vendorMobileNumber
        self.bank = bank
        self.shipBy = shipBy
        self.countryOfOriginGoods = countryOfOriginGoods
        self.incoTerm = incoTerm
        self.incoTerm2 = incoTerm2
        self.finalDestination = finalDestination
        self.finalDestinationCountry = finalDestinationCountry
        self.paymentTerm = paymentTerm
        self.shippingInstruction = shippingInstruction
        self.shippingSchedule = shippingSchedule
        self.remark = remark
        self.packingInstruction = packingInstruction
        self.productLineItem = productLineItem
        self.total = total
    }
}

// MARK: - Bank Detail Modal
public struct Bank_Detail_Modal: Codable {
    public var bankName: String?
    public var bankAddress: String?
    public var contactPersonNum: String?
    public var acNo: String?
    public var branch: String?
    public var swiftCode: String?

    enum CodingKeys: String, CodingKey {
        case bankName = "bank_name"
        case bankAddress = "bank_address"
        case contactPersonNum = "contact_person_num"
        case acNo = "ac_no"
        case branch = "branch"
        case swiftCode = "swift_code"
    }

    public init(bankName: String?, bankAddress: String?, contactPersonNum: String?, acNo: String?, branch: String?, swiftCode: String?) {
        self.bankName = bankName
        self.bankAddress = bankAddress
        self.contactPersonNum = contactPersonNum
        self.acNo = acNo
        self.branch = branch
        self.swiftCode = swiftCode
    }
}

// MARK: - PO Product LineItem
public struct POProductLineItem: Codable {
    public var id: Int?
    public var product: String?
    public var productCode: String?
    public var hsnCode: String?
    public var heirarchy: String?
    public var quantity: String?
    public var remainingQty: Double?
    public var receivedQty: Double?
    public var unitOfMeasurement: String?
    public var price: String?
    public var currency: String?
    public var amount: Double?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case product = "product"
        case productCode = "product_code"
        case hsnCode = "hsn_code"
        case heirarchy = "heirarchy"
        case quantity = "quantity"
        case remainingQty = "remaining_qty"
        case receivedQty = "received_qty"
        case unitOfMeasurement = "unit_of_measurement"
        case price = "price"
        case currency = "currency"
        case amount = "amount"
    }

    public init(id: Int?, product: String?, productCode: String?, hsnCode: String?, heirarchy: String?, quantity: String?, remainingQty: Double?, receivedQty: Double?, unitOfMeasurement: String?, price: String?, currency: String?, amount: Double?) {
        self.id = id
        self.product = product
        self.productCode = productCode
        self.hsnCode = hsnCode
        self.heirarchy = heirarchy
        self.quantity = quantity
        self.remainingQty = remainingQty
        self.receivedQty = receivedQty
        self.unitOfMeasurement = unitOfMeasurement
        self.price = price
        self.currency = currency
        self.amount = amount
    }
}
