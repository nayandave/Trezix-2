//
//  Approval_PSI_Response_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 12/11/22.
//

import Foundation

// MARK: - Approval PSI Response Modal
public struct Approval_PSI_Response_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var data: PSI_Detail_Modal?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - PSI Detail Modal
public struct PSI_Detail_Modal: Codable {
    public var invoiceID: Int?
    public var billingType: String?
    public var invoiceType: String?
    public var company: String?
    public var companyAddress: String?
    public var companyAddress2: String?
    public var countryOfOriginGoods: String?
    public var mobileNumber: String?
    public var email: String?
    public var invoiceNo: String?
    public var invoiceDate: String?
    public var taxInvNo: String?
    public var taxInvDate: String?
    public var buyerOrderNo: String?
    public var buyerOrderDate: String?
    public var deliveryNo: String?
    public var otherReference: String?
    public var consigneeName: String?
    public var consigneeCountry: String?
    public var buyerName: String?
    public var buyerCountry: String?
    public var buyerCountryName: String?
    public var placeOfReceipt: String?
    public var preCarriageBy: String?
    public var portOfLoading: String?
    public var portOfLoadingText: String?
    public var portOfDischarge: String?
    public var finalDestinationCountry: String?
    public var paymentTerm: String?
    public var paymentTermTextMode: String?
    public var finalDestination: String?
    public var vesselFlightNo: String?
    public var invoiceItemList: [PSIItemList]?
    public var total: String?

    enum CodingKeys: String, CodingKey {
        case invoiceID = "invoice_id"
        case billingType = "billing_type"
        case invoiceType = "invoice_type"
        case company = "company"
        case companyAddress = "company_address"
        case companyAddress2 = "company_address2"
        case countryOfOriginGoods = "country_of_origin_goods"
        case mobileNumber = "mobile_number"
        case email = "email"
        case invoiceNo = "invoice_no"
        case invoiceDate = "invoice_date"
        case taxInvNo = "tax_inv_no"
        case taxInvDate = "tax_inv_date"
        case buyerOrderNo = "buyer_order_no"
        case buyerOrderDate = "buyer_order_date"
        case deliveryNo = "delivery_no"
        case otherReference = "other_reference"
        case consigneeName = "consignee_name"
        case consigneeCountry = "consignee_country"
        case buyerName = "buyer_name"
        case buyerCountry = "buyer_country"
        case buyerCountryName = "buyer_country_name"
        case placeOfReceipt = "place_of_receipt"
        case preCarriageBy = "pre_carriage_by"
        case portOfLoading = "port_of_loading"
        case portOfLoadingText = "port_of_loading_text"
        case portOfDischarge = "port_of_discharge"
        case finalDestinationCountry = "final_destination_country"
        case paymentTerm = "payment_term"
        case paymentTermTextMode = "payment_term_text_mode"
        case finalDestination = "final_destination"
        case vesselFlightNo = "vessel_flight_no"
        case invoiceItemList = "invoiceItemList"
        case total = "total"
    }

    public init(invoiceID: Int?, billingType: String?, invoiceType: String?, company: String?, companyAddress: String?, companyAddress2: String?, countryOfOriginGoods: String?, mobileNumber: String?, email: String?, invoiceNo: String?, invoiceDate: String?, taxInvNo: String?, taxInvDate: String?, buyerOrderNo: String?, buyerOrderDate: String?, deliveryNo: String?, otherReference: String?, consigneeName: String?, consigneeCountry: String?, buyerName: String?, buyerCountry: String?, buyerCountryName: String?, placeOfReceipt: String?, preCarriageBy: String?, portOfLoading: String?, portOfLoadingText: String?, portOfDischarge: String?, finalDestinationCountry: String?, paymentTerm: String?, paymentTermTextMode: String?, finalDestination: String?, vesselFlightNo: String?, invoiceItemList: [PSIItemList]?, total: String?) {
        self.invoiceID = invoiceID
        self.billingType = billingType
        self.invoiceType = invoiceType
        self.company = company
        self.companyAddress = companyAddress
        self.companyAddress2 = companyAddress2
        self.countryOfOriginGoods = countryOfOriginGoods
        self.mobileNumber = mobileNumber
        self.email = email
        self.invoiceNo = invoiceNo
        self.invoiceDate = invoiceDate
        self.taxInvNo = taxInvNo
        self.taxInvDate = taxInvDate
        self.buyerOrderNo = buyerOrderNo
        self.buyerOrderDate = buyerOrderDate
        self.deliveryNo = deliveryNo
        self.otherReference = otherReference
        self.consigneeName = consigneeName
        self.consigneeCountry = consigneeCountry
        self.buyerName = buyerName
        self.buyerCountry = buyerCountry
        self.buyerCountryName = buyerCountryName
        self.placeOfReceipt = placeOfReceipt
        self.preCarriageBy = preCarriageBy
        self.portOfLoading = portOfLoading
        self.portOfLoadingText = portOfLoadingText
        self.portOfDischarge = portOfDischarge
        self.finalDestinationCountry = finalDestinationCountry
        self.paymentTerm = paymentTerm
        self.paymentTermTextMode = paymentTermTextMode
        self.finalDestination = finalDestination
        self.vesselFlightNo = vesselFlightNo
        self.invoiceItemList = invoiceItemList
        self.total = total
    }
}

// MARK: - PSI Item List
public struct PSIItemList: Codable {
    public var id: Int?
    public var product: String?
    public var hsnCode: String?
    public var quantity: String?
    public var unitOfMeasurement: String?
    public var price: String?
    public var amount: String?
    public var currency: String?
    public var marks: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case product = "product"
        case hsnCode = "hsn_code"
        case quantity = "quantity"
        case unitOfMeasurement = "unit_of_measurement"
        case price = "price"
        case amount = "amount"
        case currency = "currency"
        case marks = "marks"
    }

    public init(id: Int?, product: String?, hsnCode: String?, quantity: String?, unitOfMeasurement: String?, price: String?, amount: String?, currency: String?, marks: String?) {
        self.id = id
        self.product = product
        self.hsnCode = hsnCode
        self.quantity = quantity
        self.unitOfMeasurement = unitOfMeasurement
        self.price = price
        self.amount = amount
        self.currency = currency
        self.marks = marks
    }
}
