//
//  Approval_SO_Detail_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 11/11/22.
//

import Foundation

// MARK: - Approval PO Detail Response Modal
public struct Approval_SO_Response_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: SO_Detail_Modal?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Approval SO Item Detail
public struct SO_Detail_Modal: Codable {
    public var soID: Int?
    public var company: String?
    public var mfgPlant: String?
    public var companyAddress: String?
    public var companyAddress2: String?
    public var mobileNumber: String?
    public var email: String?
    public var soNo: String?
    public var soDate: String?
    public var buyerOrderNo: String?
    public var buyerOrderDate: String?
    public var iecNo: String?
    public var iso: String?
    public var otherReference: String?
    public var buyerName: String?
    public var buyerCountry: String?
    public var buyerCountryName: String?
    public var preCarriageBy: String?
    public var placeOfReceipt: String?
    public var vesselFlightNo: String?
    public var portOfLoading: String?
    public var portOfLoadingText: String?
    public var countryOfOriginGoods: String?
    public var incoTerm1: String?
    public var incoTerm2: String?
    public var portOfDischarge: String?
    public var finalDestinationCountry: String?
    public var paymentTerm: String?
    public var termsOfPaymentText: String?
    public var finalDestination: String?
    public var lineItem: [SOProductLineItem]?
    public var total: String?

    enum CodingKeys: String, CodingKey {
        case soID = "so_id"
        case company = "company"
        case mfgPlant = "mfg_plant"
        case companyAddress = "company_address"
        case companyAddress2 = "company_address2"
        case mobileNumber = "mobile_number"
        case email = "email"
        case soNo = "so_no"
        case soDate = "so_date"
        case buyerOrderNo = "buyer_order_no"
        case buyerOrderDate = "buyer_order_date"
        case iecNo = "iec_no"
        case iso = "iso"
        case otherReference = "other_reference"
        case buyerName = "buyer_name"
        case buyerCountry = "buyer_country"
        case buyerCountryName = "buyer_country_name"
        case preCarriageBy = "pre_carriage_by"
        case placeOfReceipt = "place_of_receipt"
        case vesselFlightNo = "vessel_flight_no"
        case portOfLoading = "port_of_loading"
        case portOfLoadingText = "port_of_loading_text"
        case countryOfOriginGoods = "country_of_origin_goods"
        case incoTerm1 = "inco_term_1"
        case incoTerm2 = "inco_term_2"
        case portOfDischarge = "port_of_Discharge"
        case finalDestinationCountry = "final_destination_country"
        case paymentTerm = "payment_term"
        case termsOfPaymentText = "terms_of_payment_text"
        case finalDestination = "final_destination"
        case lineItem = "line_item"
        case total = "total"
    }

    public init(soID: Int?, company: String?, mfgPlant: String?, companyAddress: String?, companyAddress2: String?, mobileNumber: String?, email: String?, soNo: String?, soDate: String?, buyerOrderNo: String?, buyerOrderDate: String?, iecNo: String?, iso: String?, otherReference: String?, buyerName: String?, buyerCountry: String?, buyerCountryName: String?, preCarriageBy: String?, placeOfReceipt: String?, vesselFlightNo: String?, portOfLoading: String?, portOfLoadingText: String?, countryOfOriginGoods: String?, incoTerm1: String?, incoTerm2: String?, portOfDischarge: String?, finalDestinationCountry: String?, paymentTerm: String?, termsOfPaymentText: String?, finalDestination: String?, lineItem: [SOProductLineItem]?, total: String?) {
        self.soID = soID
        self.company = company
        self.mfgPlant = mfgPlant
        self.companyAddress = companyAddress
        self.companyAddress2 = companyAddress2
        self.mobileNumber = mobileNumber
        self.email = email
        self.soNo = soNo
        self.soDate = soDate
        self.buyerOrderNo = buyerOrderNo
        self.buyerOrderDate = buyerOrderDate
        self.iecNo = iecNo
        self.iso = iso
        self.otherReference = otherReference
        self.buyerName = buyerName
        self.buyerCountry = buyerCountry
        self.buyerCountryName = buyerCountryName
        self.preCarriageBy = preCarriageBy
        self.placeOfReceipt = placeOfReceipt
        self.vesselFlightNo = vesselFlightNo
        self.portOfLoading = portOfLoading
        self.portOfLoadingText = portOfLoadingText
        self.countryOfOriginGoods = countryOfOriginGoods
        self.incoTerm1 = incoTerm1
        self.incoTerm2 = incoTerm2
        self.portOfDischarge = portOfDischarge
        self.finalDestinationCountry = finalDestinationCountry
        self.paymentTerm = paymentTerm
        self.termsOfPaymentText = termsOfPaymentText
        self.finalDestination = finalDestination
        self.lineItem = lineItem
        self.total = total
    }
}

// MARK: - SO Product LineItem
public struct SOProductLineItem: Codable {
    public var id: Int?
    public var product: String?
    public var hsnCode: String?
    public var quantity: String?
    public var unitOfMeasurement: String?
    public var price: String?
    public var amount: String?
    public var currency: String?
    public var marks: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case product = "product"
        case hsnCode = "hsn_code"
        case quantity = "quantity"
        case unitOfMeasurement = "unit_of_measurement"
        case price = "price"
        case amount = "amount"
        case currency = "currency"
        case marks = "marks"
    }

    public init(id: Int?, product: String?, hsnCode: String?, quantity: String?, unitOfMeasurement: String?, price: String?, amount: String?, currency: String?, marks: String?) {
        self.id = id
        self.product = product
        self.hsnCode = hsnCode
        self.quantity = quantity
        self.unitOfMeasurement = unitOfMeasurement
        self.price = price
        self.amount = amount
        self.currency = currency
        self.marks = marks
    }
}
