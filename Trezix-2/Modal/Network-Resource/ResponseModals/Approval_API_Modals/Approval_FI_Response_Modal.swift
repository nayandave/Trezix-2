//
//  Approval_FI_Response_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 06/05/23.
//

import Foundation

// MARK: - Approval_FI_Response_Modal
public struct Approval_FI_Response_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var data: FI_Detail_Modal?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - FI_Detail_Modal
public struct FI_Detail_Modal: Codable {
    public var invoiceID: Int?
    public var billingType: String?
    public var invoiceType: String?
    public var company: String?
    public var companyAddress: String?
    public var companyAddress2: String?
    public var countryOfOriginGoods: String?
    public var mobileNumber: String?
    public var email: String?
    public var invoiceNo: String?
    public var invoiceDate: String?
    public var preshipmentInvoiceNo: String?
    public var preshipmentInvoiceNoDate: String?
    public var buyerOrderNo: String?
    public var buyerOrderDate: String?
    public var deliveryNo: String?
    public var otherReference: String?
    public var consigneeName: String?
    public var consigneeCountry: String?
    public var buyerName: String?
    public var buyerCountry: String?
    public var buyerCountryName: String?
    public var placeOfReceipt: String?
    public var preCarriageBy: String?
    public var portOfLoading: String?
    public var portOfLoadingText: String?
    public var portOfDischarge: String?
    public var finalDestinationCountry: String?
    public var paymentTerm: String?
    public var paymentTermTextMode: String?
    public var finalDestination: String?
    public var vesselFlightNo: String?
    public var invoiceItemList: [FIItemList]?
    public var total: String?

    enum CodingKeys: String, CodingKey {
        case invoiceID = "invoice_id"
        case billingType = "billing_type"
        case invoiceType = "invoice_type"
        case company = "company"
        case companyAddress = "company_address"
        case companyAddress2 = "company_address2"
        case countryOfOriginGoods = "country_of_origin_goods"
        case mobileNumber = "mobile_number"
        case email = "email"
        case invoiceNo = "invoice_no"
        case invoiceDate = "invoice_date"
        case preshipmentInvoiceNo = "preshipment_invoice_no"
        case preshipmentInvoiceNoDate = "preshipment_invoice_no_date"
        case buyerOrderNo = "buyer_order_no"
        case buyerOrderDate = "buyer_order_date"
        case deliveryNo = "delivery_no"
        case otherReference = "other_reference"
        case consigneeName = "consignee_name"
        case consigneeCountry = "consignee_country"
        case buyerName = "buyer_name"
        case buyerCountry = "buyer_country"
        case buyerCountryName = "buyer_country_name"
        case placeOfReceipt = "place_of_receipt"
        case preCarriageBy = "pre_carriage_by"
        case portOfLoading = "port_of_loading"
        case portOfLoadingText = "port_of_loading_text"
        case portOfDischarge = "port_of_discharge"
        case finalDestinationCountry = "final_destination_country"
        case paymentTerm = "payment_term"
        case paymentTermTextMode = "payment_term_text_mode"
        case finalDestination = "final_destination"
        case vesselFlightNo = "vessel_flight_no"
        case invoiceItemList = "invoiceItemList"
        case total = "total"
    }
}

// MARK: - FIItemList
public struct FIItemList: Codable {
    public var id: Int?
    public var product: String?
    public var hsnCode: String?
    public var quantity: String?
    public var unitOfMeasurement: String?
    public var price: String?
    public var amount: String?
    public var currency: String?
    public var marks: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case product = "product"
        case hsnCode = "hsn_code"
        case quantity = "quantity"
        case unitOfMeasurement = "unit_of_measurement"
        case price = "price"
        case amount = "amount"
        case currency = "currency"
        case marks = "marks"
    }

    public init(id: Int?, product: String?, hsnCode: String?, quantity: String?, unitOfMeasurement: String?, price: String?, amount: String?, currency: String?, marks: String?) {
        self.id = id
        self.product = product
        self.hsnCode = hsnCode
        self.quantity = quantity
        self.unitOfMeasurement = unitOfMeasurement
        self.price = price
        self.amount = amount
        self.currency = currency
        self.marks = marks
    }
}
