//
//  Approval_Duty_Clearing_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 21/10/22.
//

import Foundation

// MARK: - Approval Duty Clearing Response Modal
public struct Approval_DC_Response_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: DC_Detail_Modal?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Duty Clearing Detail Modal
public struct DC_Detail_Modal: Codable {
    public var blNo: String?
    public var invoiceNum: String?
    public var exchangeRate: String?
    public var addFreight: String?
    public var productLineItem: [DCProductLineItem]?

    enum CodingKeys: String, CodingKey {
        case blNo = "bl_no"
        case invoiceNum = "invoice_num"
        case exchangeRate = "exchange_rate"
        case addFreight = "add_freight"
        case productLineItem = "product_line_item"
    }

    public init(blNo: String?, invoiceNum: String?, exchangeRate: String?, addFreight: String?, productLineItem: [DCProductLineItem]?) {
        self.blNo = blNo
        self.invoiceNum = invoiceNum
        self.exchangeRate = exchangeRate
        self.addFreight = addFreight
        self.productLineItem = productLineItem
    }
}

// MARK: - Duty Clearing Product LineItem
public struct DCProductLineItem: Codable {
    public var id: Int?
    public var adminID: Int?
    public var blNo: String?
    public var bcd: Int?
    public var cvd: Int?
    public var sad: Int?
    public var cessOfCvd: Int?
    public var secHEducCessOnCvd: Int?
    public var antiDumping: Int?
    public var penalty: Int?
    public var eduCess: Int?
    public var socialWelfare: String?
    public var igst: String?
    public var firstName: String?
    public var lastName: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case adminID = "admin_id"
        case blNo = "bl_no"
        case bcd = "bcd"
        case cvd = "cvd"//CAIDC
        case sad = "sad"//SAFD
        case cessOfCvd = "cess_of_cvd"//CC
        case secHEducCessOnCvd = "sec_h_educ_cess_on_cvd"//CHCESS
        case antiDumping = "anti_dumping"//ADD
        case penalty = "penalty"//PREFID
        case eduCess = "edu_cess"//EAIDC
        case socialWelfare = "social_welfare"//SWC
        case igst = "igst"
        case firstName = "first_name"
        case lastName = "last_name"
    }

    public init(id: Int?, adminID: Int?, blNo: String?, bcd: Int?, cvd: Int?, sad: Int?, cessOfCvd: Int?, secHEducCessOnCvd: Int?, antiDumping: Int?, penalty: Int?, eduCess: Int?, socialWelfare: String?, igst: String?, firstName: String?, lastName: String?) {
        self.id = id
        self.adminID = adminID
        self.blNo = blNo
        self.bcd = bcd
        self.cvd = cvd
        self.sad = sad
        self.cessOfCvd = cessOfCvd
        self.secHEducCessOnCvd = secHEducCessOnCvd
        self.antiDumping = antiDumping
        self.penalty = penalty
        self.eduCess = eduCess
        self.socialWelfare = socialWelfare
        self.igst = igst
        self.firstName = firstName
        self.lastName = lastName
    }
}
