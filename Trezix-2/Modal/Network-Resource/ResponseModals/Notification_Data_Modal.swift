//
//  Notification_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 29/07/22.
//

import Foundation

// MARK: - Welcome
///*Handles Data for*  `Notification API Calling Response`
public class Notification_Response_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: Main_Notification_Data?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Main Notification Data Modal
/// has Actual Notification Data
/// - Subset of ``Notification_Response_Modal`` which is Response Modal for Notification API Calling
public class Main_Notification_Data: Codable {
    public var unreadTotalNotification: Int
    public var notifications: [Notification_Modal]?

    enum CodingKeys: String, CodingKey {
        case unreadTotalNotification = "unread_total_notification"
        case notifications = "notifications"
    }

    public init(unreadTotalNotification: Int, notifications: [Notification_Modal]) {
        self.unreadTotalNotification = unreadTotalNotification
        self.notifications = notifications
    }
}


// MARK: - Notification
public class Notification_Modal: Codable {
    public var id: Int
    public var adminID: Int
    public var notificationType: String
    public var message: String
    public var isRead: String
    public var createdAt: String
    public var updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case adminID = "admin_id"
        case notificationType = "notification_type"
        case message = "message"
        case isRead = "is_read"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    public init(id: Int, adminID: Int, notificationType: String, message: String, isRead: String, createdAt: String, updatedAt: String) {
        self.id = id
        self.adminID = adminID
        self.notificationType = notificationType
        self.message = message
        self.isRead = isRead
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
