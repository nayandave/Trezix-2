//
//  Import_Dashboard_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 10/01/23.
//

import Foundation

public struct Import_Dashboard_Data_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var data: Import_Dashboard_Data?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Import_Dashboard_Data
public struct Import_Dashboard_Data: Codable {
    public var monthly: String?
    public var invoice30D: [Intransit_Import_Data?]?
    public var invoice60D: [Intransit_Import_Data?]?
    public var invoice60DPlus: [Intransit_Import_Data?]?
    public var atBank30D: [Intransit_Import_Data?]?
    public var atBank60D: [Intransit_Import_Data?]?
    public var atBank60DPlus: [Intransit_Import_Data?]?
    public var inClearing30D: [Intransit_Import_Data?]?
    public var inClearing60D: [Intransit_Import_Data?]?
    public var inClearing60DPlus: [Intransit_Import_Data?]?
    public var expectedToArrive30D: [Intransit_Import_Data?]?
    public var expectedToArrive60D: [Intransit_Import_Data?]?
    public var expectedToArrive60DPlus: [Intransit_Import_Data?]?
    public var invoiceCount: Int?
    public var invoice30DCount: Int?
    public var invoice60DCount: Int?
    public var invoice60DPlusCount: Int?
    public var atBank: Int?
    public var atBank30DCount: Int?
    public var atBank60DCount: Int?
    public var atBank60DPlusCount: Int?
    public var inClearing: Int?
    public var inClearing30DCount: Int?
    public var inClearing60DCount: Int?
    public var inClearing60DPlusCount: Int?
    public var expectedToArrive: Int?
    public var expectedToArrive30DCount: Int?
    public var expectedToArrive60DCount: Int?
    public var expectedToArrive60DPlusCount: Int?
    public var totalPendingOrder: Int?
    public var pendingOrderFullyNotDispatch: Int?
    public var pendingOrderPartiallyDispatch: Int?
    public var pendingOrderFullyNotDispatchData: [Pending_Order_Import_Data?]?
    public var pendingOrderPartiallyDispatchData: [Pending_Order_Import_Data?]?
    public var licenceInformation: [Licence_Info_Import_Data?]?
    public var closeOrder: [Closed_Order_Import_Data?]?
    public var totalOrders: Int?
    public var eventList: [Event_List_Import_Data?]?
    public var pieChart: [PieChart_Data?]?
    public var unreadTotalNotification: Int?
    public var totalPendingApprovalNotification: Int?
    public var shipmentTrackingCount: Int?
    public var shipmentTracking: [ShipmentTracking_Import_Data?]?


    enum CodingKeys: String, CodingKey {
        case monthly = "monthly"
        case invoice30D = "invoice_30d"
        case invoice60D = "invoice_60d"
        case invoice60DPlus = "invoice_60d_plus"
        case atBank30D = "at_bank_30d"
        case atBank60D = "at_bank_60d"
        case atBank60DPlus = "at_bank_60d_plus"
        case inClearing30D = "in_clearing_30d"
        case inClearing60D = "in_clearing_60d"
        case inClearing60DPlus = "in_clearing_60d_plus"
        case expectedToArrive30D = "expected_to_arrive_30d"
        case expectedToArrive60D = "expected_to_arrive_60d"
        case expectedToArrive60DPlus = "expected_to_arrive_60d_plus"
        case invoiceCount = "invoice_count"
        case invoice30DCount = "invoice_30d_count"
        case invoice60DCount = "invoice_60d_count"
        case invoice60DPlusCount = "invoice_60d_plus_count"
        case atBank = "at_bank"
        case atBank30DCount = "at_bank_30d_count"
        case atBank60DCount = "at_bank_60d_count"
        case atBank60DPlusCount = "at_bank_60d_plus_count"
        case inClearing = "in_clearing"
        case inClearing30DCount = "in_clearing_30d_count"
        case inClearing60DCount = "in_clearing_60d_count"
        case inClearing60DPlusCount = "in_clearing_60d_plus_count"
        case expectedToArrive = "expected_to_arrive"
        case expectedToArrive30DCount = "expected_to_arrive_30d_count"
        case expectedToArrive60DCount = "expected_to_arrive_60d_count"
        case expectedToArrive60DPlusCount = "expected_to_arrive_60d_plus_count"
        case totalPendingOrder = "total_pending_order"
        case pendingOrderFullyNotDispatch = "pending_order_fully_not_dispatch"
        case pendingOrderPartiallyDispatch = "pending_order_partially_dispatch"
        case pendingOrderFullyNotDispatchData = "pending_order_fully_not_dispatch_data"
        case pendingOrderPartiallyDispatchData = "pending_order_partially_dispatch_data"
        case licenceInformation = "licenceInformation"
        case closeOrder = "closeOrder"
        case totalOrders = "total_orders"
        case eventList = "eventList"
        case pieChart = "pie_chart"
        case unreadTotalNotification = "unread_total_notification"
        case totalPendingApprovalNotification = "total_pending_approval_notification"
        case shipmentTrackingCount = "shipmentTrackingCount"
        case shipmentTracking = "shipmentTracking"
    }
}

// MARK: - Closed_Order_Import_Data
public struct Closed_Order_Import_Data: Codable {
    public var poNo: String?
    public var date: String?
    public var qty: Double?
    public var price: Double?
    public var currency: String?
    public var measurement: String?

    enum CodingKeys: String, CodingKey {
        case poNo = "po_no"
        case date = "date"
        case qty = "qty"
        case price = "price"
        case currency = "currency"
        case measurement = "measurement"
    }
}

// MARK: - Intransit_Import_Data
public struct Intransit_Import_Data: Codable {
    public var id: Int?
    public var invoice: String?
    public var invoiceDate: String?
    public var total: Double?
    public var qty: Double?
    public var currency: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case invoice = "invoice"
        case invoiceDate = "invoice_date"
        case total = "total"
        case qty = "qty"
        case currency = "currency"
    }
}

// MARK: - Licence_Info_Import_Data
public struct Licence_Info_Import_Data: Codable {
    public var licenceNo: String?
    public var licenceDate: String?
    public var licenseValue: Double?
    public var balance: Double?

    enum CodingKeys: String, CodingKey {
        case licenceNo = "licence_no"
        case licenceDate = "licence_date"
        case licenseValue = "license_value"
        case balance = "balance"
    }
}

// MARK: - Pending_Order_Import_Data
public struct Pending_Order_Import_Data: Codable {
    public var id: Int?
    public var poNo: String?
    public var poDate: String?
    public var poQty: Double?
    public var total: Double?
    public var currency: String?
    public var uom: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case poNo = "po_no"
        case poDate = "po_date"
        case poQty = "po_qty"
        case total = "total"
        case currency = "currency"
        case uom = "uom"
    }
}

// MARK: - Event_List_Import_Data
public struct Event_List_Import_Data: Codable {
    public var title: String?
    public var start: String?

    enum CodingKeys: String, CodingKey {
        case title = "title"
        case start = "start"
    }
}

// MARK: - PieChart_Data
///**Also  in Export Dashboard Data Modal**
public struct PieChart_Data: Codable {
    public var newDate: String
    public var total: Double
    
    enum CodingKeys: String, CodingKey {
        case newDate = "new_date"
        case total = "total"
    }
}

// MARK: - ShipmentTracking_Import_Data
public struct ShipmentTracking_Import_Data: Codable {
    public var id: Int?
    public var vendorName: String?
    public var invoiceNumber: String?
    public var blNumber: String?
    public var vesselName: String?
    public var etaByTracking: String?
    public var status: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case vendorName = "vendor_name"
        case invoiceNumber = "invoice_number"
        case blNumber = "bl_number"
        case vesselName = "vessel_name"
        case etaByTracking = "eta_by_tracking"
        case status = "status"
    }

    public init(id: Int?, vendorName: String?, invoiceNumber: String?, blNumber: String?, vesselName: String?, etaByTracking: String?, status: String?) {
        self.id = id
        self.vendorName = vendorName
        self.invoiceNumber = invoiceNumber
        self.blNumber = blNumber
        self.vesselName = vesselName
        self.etaByTracking = etaByTracking
        self.status = status
    }
}
