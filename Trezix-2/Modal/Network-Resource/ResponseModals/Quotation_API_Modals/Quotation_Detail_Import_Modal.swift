//
//  Quotation_Detail_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 24/08/23.
//

import Foundation

// MARK: - Quotation Detail Response Modal
public struct Quotation_Detail_Modal: Codable {
    public var success: Bool
    public var message: String
    public var data: Quotation_Detail_Data?

    enum CodingKeys: String, CodingKey {
        case success = "success"
        case message = "message"
        case data = "data"
    }
}

// MARK: - Quotation Detail Data
public struct Quotation_Detail_Data: Codable {
    public var id: Int
    public var title: String?
    public var poNo: String?
    public var soNo: String?
    public var vendor: String?
    public var vendorInvoice: String?
    public var bidDate: String?
    public var dueDate: String?
    public var partnerType: String?
    public var allPartners: [Quotation_Partner_Name]?
    public var attributes: [Quotation_Attribute]?
    public var selectWinner: Bool
    public var notes: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case poNo = "po_no"
        case soNo = "so_no"
        case vendor = "vendor"
        case vendorInvoice = "vendor_invoice"
        case bidDate = "bid_date"
        case dueDate = "due_date"
        case partnerType = "partner_type"
        case allPartners = "all_partners"
        case attributes = "attributes"
        case notes = "notes"
        case selectWinner = "selectWinner"
    }
}

// MARK: - Quotation Partner Name
public struct Quotation_Partner_Name: Codable {
    public var partnerName: String

    enum CodingKeys: String, CodingKey {
        case partnerName = "partner_name"
    }
}

// MARK: - Quotation Attributes
public struct Quotation_Attribute: Codable {
    public var partner: String
    public var partnerID: Int?
    public var winner: String
    public var attributeClass: [Quotation_Additional_Attribute]?

    enum CodingKeys: String, CodingKey {
        case partner = "partner"
        case partnerID = "partner_id"
        case winner = "winner"
        case attributeClass = "attribute_class"
    }
}

// MARK: - Quotation Additional Attributes
public struct Quotation_Additional_Attribute: Codable {
    public var heading: String
    public var value: String
    enum CodingKeys: String, CodingKey {
        case heading = "heading"
        case value = "value"
    }
}
