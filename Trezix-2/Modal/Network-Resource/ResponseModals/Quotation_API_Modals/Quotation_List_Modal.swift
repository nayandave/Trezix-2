//
//  Quotation_List_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 21/08/23.
//

import Foundation
// MARK: - Quotation List API Response Modal
public struct Quotation_List_Modal: Codable {
    public var success: Bool
    public var message: String
    public var data: [Quotation_List_Data]?

    enum CodingKeys: String, CodingKey {
        case success = "success"
        case message = "message"
        case data = "data"
    }
}

// MARK: - Quotation List Data
public struct Quotation_List_Data: Codable {
    public var id: Int
    public var module: Quotation_List_Data_Module
    public var title: String?
    public var bidDate: String?
    public var dueDate: String?
    public var bidCount: String?
    public var status: Quotation_List_Data_Status

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case module = "module"
        case title = "title"
        case bidDate = "bid_date"
        case dueDate = "due_date"
        case bidCount = "bid_count"
        case status = "status"
    }
}

public enum Quotation_List_Data_Module: String, Codable {
    case empty = ""     // Temporary Solcution
    case modExport = "Export"
    case modImport = "Import"
}

public enum Quotation_List_Data_Status: String, Codable {
    case statusClose = "Close"
    case statusOpen = "Open"
}
