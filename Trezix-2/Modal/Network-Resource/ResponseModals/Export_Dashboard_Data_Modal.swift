//
//  Export_Dashboard_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 10/01/23.
//

import Foundation

public struct Export_Dashboard_Data_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var data: Export_Dashboard_Data?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Export_Dashboard_Data
public struct Export_Dashboard_Data: Codable {
    public var monthly: String?
    public var openOrderPartiallyDeliveryCount: Int?
    public var openOrderPartiallyDelivery: [OpenShipmentCompleted_Export_Data?]?
    public var openFullPendingOrderCount: Int?
    public var openFullPendingOrder: [OpenShipmentCompleted_Export_Data?]?
    public var shipmentDispatchPlanningCount: Int?
    public var shipmentDispatchPlanning: [OpenShipmentCompleted_Export_Data?]?
    public var shipmentBillingPlanningCount: Int?
    public var shipmentBillingPlanning: [OpenShipmentCompleted_Export_Data?]?
    public var completedOrder: [OpenShipmentCompleted_Export_Data?]?
    public var dispatchPlanning: [DispatchPlanning_Export_Data?]?
    public var eventList: [String: EventList_Export_Data?]?
    public var pieChart: [PieChart_Data?]?
    public var unreadTotalNotification: Int?
    public var totalPendingApprovalNotification: Int?
    public var shipmentDetailCount: Int?
    public var shipmentDetail: [ShipmentTracking_Export_Data?]?


    enum CodingKeys: String, CodingKey {
        case monthly = "monthly"
        case openOrderPartiallyDeliveryCount = "openOrderPartiallyDeliveryCount"
        case openOrderPartiallyDelivery = "openOrderPartiallyDelivery"
        case openFullPendingOrderCount = "openFullPendingOrderCount"
        case openFullPendingOrder = "openFullPendingOrder"
        case shipmentDispatchPlanningCount = "shipmentDispetchPlanningCount"
        case shipmentDispatchPlanning = "shipmentDispetchPlanning"
        case shipmentBillingPlanningCount = "shipmentBillingPlanningCount"
        case shipmentBillingPlanning = "shipmentBillingPlanning"
        case completedOrder = "completedOrder"
        case dispatchPlanning = "dispatchPlanning"
        case eventList = "eventList"
        case pieChart = "pieChart"
        case unreadTotalNotification = "unread_total_notification"
        case totalPendingApprovalNotification = "total_pending_approval_notification"
        case shipmentDetailCount = "shipmentDetailCount"
        case shipmentDetail = "shipmentDetail"
    }
}

// MARK: - DispatchPlanning_Export_Data
public struct DispatchPlanning_Export_Data: Codable {
    public var dispatchDate: String?
    public var netWeight: Double?
    public var noOfPi: Int?
    public var uom: String?

    enum CodingKeys: String, CodingKey {
        case dispatchDate = "dispatch_date"
        case netWeight = "net_weight"
        case noOfPi = "no_of_pi"
        case uom = "uom"
    }

    public init(dispatchDate: String?, netWeight: Double?, noOfPi: Int?, uom: String?) {
        self.dispatchDate = dispatchDate
        self.netWeight = netWeight
        self.noOfPi = noOfPi
        self.uom = uom
    }
}

// MARK: - EventList_Export_Data
public struct EventList_Export_Data: Codable {
    public var title: String?
    public var start: String?
    public var brcNo: String?

    enum CodingKeys: String, CodingKey {
        case title = "title"
        case start = "start"
        case brcNo = "brc_no"
    }

    public init(title: String?, start: String?, brcNo: String?) {
        self.title = title
        self.start = start
        self.brcNo = brcNo
    }
}

// MARK: - Open_Shipment_Export_Data
public struct OpenShipmentCompleted_Export_Data: Codable {
    public var invoiceNo: String?
    public var date: String?
    public var quantity: Double?
    public var amount: Double?
    public var currency: String?
    public var uom: String?

    enum CodingKeys: String, CodingKey {
        case invoiceNo = "invoice_no"
        case date = "date"
        case quantity = "quantity"
        case amount = "amount"
        case currency = "currency"
        case uom = "uom"
    }

    public init(invoiceNo: String?, date: String?, quantity: Double?, amount: Double?, currency: String?, uom: String?) {
        self.invoiceNo = invoiceNo
        self.date = date
        self.quantity = quantity
        self.amount = amount
        self.currency = currency
        self.uom = uom
    }
}

// MARK: - ShipmentTracking_Export_Data
public struct ShipmentTracking_Export_Data: Codable {
    public var id: Int?
    public var blNo: String?
    public var etaByTracking: String?
    public var delayedBy: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case blNo = "bl_no"
        case etaByTracking = "eta_by_tracking"
        case delayedBy = "delayed_by"
    }

    public init(id: Int?, blNo: String?, etaByTracking: String?, delayedBy: String?) {
        self.id = id
        self.blNo = blNo
        self.etaByTracking = etaByTracking
        self.delayedBy = delayedBy
    }
}



//public struct Welcome: Codable {
//    public var status: Int?
//    public var msg: String?
//    public var data: DataClass?
//
//    public init(status: Int?, msg: String?, data: DataClass?) {
//        self.status = status
//        self.msg = msg
//        self.data = data
//    }
//}
//
//// MARK: - DataClass
//public struct DataClass: Codable {
//    public var monthly: String?
//    public var openOrderPartiallyDeliveryCount: Int?
//    public var openOrderPartiallyDelivery: [CompletedOrder]?
//    public var openFullPendingOrderCount: Int?
//    public var openFullPendingOrder: [CompletedOrder]?
//    public var shipmentDispetchPlanningCount: Int?
//    public var shipmentDispetchPlanning: [CompletedOrder]?
//    public var shipmentBillingPlanningCount: Int?
//    public var shipmentBillingPlanning, completedOrder: [CompletedOrder]?
//    public var dispatchPlanning: [DispatchPlanning]?
//    public var eventList: [String: EventList]?
//    public var pieChart: [PieChart]?
//    public var unreadTotalNotification: Int?
//    public var totalPendingApprovalNotification: Int?
//    public var shipmentDetailCount: Int?
//    public var shipmentDetail: [ShipmentDetail]?
//
//    enum CodingKeys: String, CodingKey {
//        case monthly, openOrderPartiallyDeliveryCount, openOrderPartiallyDelivery, openFullPendingOrderCount, openFullPendingOrder, shipmentDispetchPlanningCount, shipmentDispetchPlanning, shipmentBillingPlanningCount, shipmentBillingPlanning, completedOrder, dispatchPlanning, eventList, pieChart
//        case unreadTotalNotification = "unread_total_notification"
//        case totalPendingApprovalNotification = "total_pending_approval_notification"
//        case shipmentDetailCount, shipmentDetail
//    }
//
//    public init(monthly: String?, openOrderPartiallyDeliveryCount: Int?, openOrderPartiallyDelivery: [CompletedOrder]?, openFullPendingOrderCount: Int?, openFullPendingOrder: [CompletedOrder]?, shipmentDispetchPlanningCount: Int?, shipmentDispetchPlanning: [CompletedOrder]?, shipmentBillingPlanningCount: Int?, shipmentBillingPlanning: [CompletedOrder]?, completedOrder: [CompletedOrder]?, dispatchPlanning: [DispatchPlanning]?, eventList: [String: EventList]?, pieChart: [PieChart]?, unreadTotalNotification: Int?, totalPendingApprovalNotification: Int?, shipmentDetailCount: Int?, shipmentDetail: [ShipmentDetail]?) {
//        self.monthly = monthly
//        self.openOrderPartiallyDeliveryCount = openOrderPartiallyDeliveryCount
//        self.openOrderPartiallyDelivery = openOrderPartiallyDelivery
//        self.openFullPendingOrderCount = openFullPendingOrderCount
//        self.openFullPendingOrder = openFullPendingOrder
//        self.shipmentDispetchPlanningCount = shipmentDispetchPlanningCount
//        self.shipmentDispetchPlanning = shipmentDispetchPlanning
//        self.shipmentBillingPlanningCount = shipmentBillingPlanningCount
//        self.shipmentBillingPlanning = shipmentBillingPlanning
//        self.completedOrder = completedOrder
//        self.dispatchPlanning = dispatchPlanning
//        self.eventList = eventList
//        self.pieChart = pieChart
//        self.unreadTotalNotification = unreadTotalNotification
//        self.totalPendingApprovalNotification = totalPendingApprovalNotification
//        self.shipmentDetailCount = shipmentDetailCount
//        self.shipmentDetail = shipmentDetail
//    }
//}
//
//// MARK: - CompletedOrder
//public struct CompletedOrder: Codable {
//    public var invoiceNo, date: String?
//    public var quantity, amount: Double?
//    public var currency: Currency?
//    public var uom: String?
//
//    enum CodingKeys: String, CodingKey {
//        case invoiceNo = "invoice_no"
//        case date, quantity, amount, currency, uom
//    }
//
//    public init(invoiceNo: String?, date: String?, quantity: Double?, amount: Double?, currency: Currency?, uom: String?) {
//        self.invoiceNo = invoiceNo
//        self.date = date
//        self.quantity = quantity
//        self.amount = amount
//        self.currency = currency
//        self.uom = uom
//    }
//}
//
//public enum Currency: String, Codable {
//    case cad = "CAD"
//    case eur = "EUR"
//    case gbp = "GBP"
//    case usd = "USD"
//}
//
//// MARK: - DispatchPlanning
//public struct DispatchPlanning: Codable {
//    public var dispatchDate: String?
//    public var netWeight, noOfPi: Int?
//    public var uom: String?
//
//    enum CodingKeys: String, CodingKey {
//        case dispatchDate = "dispatch_date"
//        case netWeight = "net_weight"
//        case noOfPi = "no_of_pi"
//        case uom
//    }
//
//    public init(dispatchDate: String?, netWeight: Int?, noOfPi: Int?, uom: String?) {
//        self.dispatchDate = dispatchDate
//        self.netWeight = netWeight
//        self.noOfPi = noOfPi
//        self.uom = uom
//    }
//}
//
//// MARK: - EventList
//public struct EventList: Codable {
//    public var title, start, brcNo: String?
//
//    enum CodingKeys: String, CodingKey {
//        case title, start
//        case brcNo = "brc_no"
//    }
//
//    public init(title: String?, start: String?, brcNo: String?) {
//        self.title = title
//        self.start = start
//        self.brcNo = brcNo
//    }
//}
//
//// MARK: - PieChart
//public struct PieChart: Codable {
//    public var newDate: String?
//    public var total: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case newDate = "new_date"
//        case total
//    }
//
//    public init(newDate: String?, total: Int?) {
//        self.newDate = newDate
//        self.total = total
//    }
//}
//
//// MARK: - ShipmentDetail
//public struct ShipmentDetail: Codable {
//    public var id: Int?
//    public var blNo, etaByTracking, delayedBy: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case blNo = "bl_no"
//        case etaByTracking = "eta_by_tracking"
//        case delayedBy = "delayed_by"
//    }
//
//    public init(id: Int?, blNo: String?, etaByTracking: String?, delayedBy: String?) {
//        self.id = id
//        self.blNo = blNo
//        self.etaByTracking = etaByTracking
//        self.delayedBy = delayedBy
//    }
//}
