//
//  Validate_Mail.swift
//  Trezix-2
//
//  Created by Amar Panchal on 22/06/22.
//

import Foundation
// MARK: - Basic Response Modal
/// **It is technically used in every API response**
public struct BasicResponseModal: Codable {
    public let status: Int
    public let msg: String

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
    }

    public init(status: Int, msg: String) {
        self.status = status
        self.msg = msg
    }
}


// MARK: - Basic Response Modal For Approval API
public struct BasicResponseModalForApproval: Codable {
    public var status: Int?
    public var message: String?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
    }

    public init(status: Int?, message: String?) {
        self.status = status
        self.message = message
    }
}
