//
//  More_Details_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 24/01/23.
//

import Foundation

// MARK: -------------------- Import More Details Data Modals --------------------
// MARK: - Import In-Transit More Details Data Modal
public struct Import_InTransit_MoreDetail_Modal: Codable {
    public let status: Int
    public let msg: String
    public let data: [Intransit_Import_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Import Pending Order More Details Data Modal
public struct Import_PendingOrder_MoreDetail_Modal: Codable {
    public let status: Int
    public let msg: String
    public let data: [Pending_Order_Import_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Import Closed Orders More Details Data Modal
public struct Import_ClosedOrder_MoreDetail_Modal: Codable {
    public let status: Int
    public let msg: String
    public let data: [Closed_Order_Import_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Import Licence Info More Details Data Modal
public struct Import_LicenceInfo_MoreDetail_Modal: Codable {
    public let status: Int
    public let msg: String
    public let data: [Licence_Info_Import_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}


// MARK: - Import Event List Data Modal
public struct Event_List_Import_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [Event_List_Import_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Shipment Tracking More Detail Import Modal
public struct ShipmentTracking_MoreDetail_Import_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var data: [ShipmentTracking_Import_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}


// MARK: -------------------- Export More Details Data Modals --------------------

// MARK: - Export Open_Order / Shipment_Planning / Complete_Order More Details Data Modal
public struct Export_OpenOrder_Shipment_CompleteOrder_MoreDetail_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [OpenShipmentCompleted_Export_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Export Dispatch_Planning More Details Data Modal
public struct Export_DispatchPlanning_MoreDetail_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [DispatchPlanning_Export_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Export Event List More Details Data Modal
public struct Event_List_Export_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [EventList_Export_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}


// MARK: - Shipment Tracking More Detail Export Modal
public struct ShipmentTracking_MoreDetail_Export_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var data: [ShipmentTracking_Export_Data?]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}
