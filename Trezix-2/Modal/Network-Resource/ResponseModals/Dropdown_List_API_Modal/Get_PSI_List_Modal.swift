//
//  Get_PSI_List_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 02/01/23.
//

import Foundation
// MARK: - Get_PSI_List_Modal
public struct Get_PSI_List_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var data: [PSI_List_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - PSI_List_Data
public struct PSI_List_Data: Codable {
    public var deliveryNo: String?
    public var preshipmentInvoiceNo: String?

    enum CodingKeys: String, CodingKey {
        case deliveryNo = "delivery_no"
        case preshipmentInvoiceNo = "preshipment_invoice_no"
    }
}
