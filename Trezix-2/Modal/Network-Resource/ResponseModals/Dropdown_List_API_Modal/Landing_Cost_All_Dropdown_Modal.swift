//
//  Landing_Cost_All_Dropdown_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 25/03/23.
//

import Foundation

public struct Landing_Cost_All_Dropdown_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: Landing_All_Dropdown_Data?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Landing_All_Dropdown_Data
public struct Landing_All_Dropdown_Data: Codable {
    public var invoices: [Landing_Invoice_Data]?
    public var vendor: [Landing_Vendor_Data]?
    public var companysetup: [Landing_Companysetup_Data]?
    public var material: [Landing_Material_Data]?

    enum CodingKeys: String, CodingKey {
        case invoices = "invoices"
        case vendor = "vendor"
        case companysetup = "companysetup"
        case material = "material"
    }
}

// MARK: - Landing_Companysetup_Data
public struct Landing_Companysetup_Data: Codable {
    public var id: Int
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}

// MARK: - Landing_Invoice_Data
public struct Landing_Invoice_Data: Codable {
    public var id: Int
    public var invoiceNum: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case invoiceNum = "invoice_num"
    }
}

// MARK: - Landing_Material_Data
public struct Landing_Material_Data: Codable {
    public var id: Int
    public var materialDescription: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case materialDescription = "material_description"
    }
}

// MARK: - Landing_Vendor_Data
public struct Landing_Vendor_Data: Codable {
    public var id: Int
    public var companyName: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case companyName = "company_name"
    }
}
