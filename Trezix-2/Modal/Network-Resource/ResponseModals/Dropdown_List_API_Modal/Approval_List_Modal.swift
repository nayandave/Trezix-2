//
//  Approval_List_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 21/10/22.
//

import Foundation

// MARK: - Approval List Response Modal
public struct Approval_List_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [Approval_Item_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

#warning("Check & change all response modals, if it has any int params except for ids..")
// MARK: - Approval Item Data Modal
public struct Approval_Item_Data: Codable, Equatable {
    public var id: Int?
    public var tyoeOfVerification: String?
    public var moduleName: String?
    public var userID: String?
    public var moduleID: String?
    public var approveBy1: String?
    public var approveBy2: String?
    public var comment: String?
    public var comment2: String?
    public var aP1: String?
    public var aP2: String?
    public var firstName: String?
    public var lastName: String?
    public var step1_ApprovalStatus: String?
    public var step2_ApprovalStatus: String?
    public var moduleOriginalID: String?
    public var createdAt: String?
    public var date: String?
    public var tyoeOfVerificationSlug: String?
    public var moduleNameSlug: String?
    public var username: String?
    public var status: String?
    public var step1_ApprovalStatusSlug: String?
    public var step2_ApprovalStatusSlug: String?
    public var quantity: Int?
    public var amount: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case tyoeOfVerification = "tyoe_of_verification"
        case moduleName = "module_name"
        case userID = "user_id"
        case moduleID = "module_id"
        case approveBy1 = "approve_by_1"
        case approveBy2 = "approve_by_2"
        case comment = "comment"
        case comment2 = "comment_2"
        case aP1 = "a_p_1"
        case aP2 = "a_p_2"
        case firstName = "first_name"
        case lastName = "last_name"
        case step1_ApprovalStatus = "step_1_approval_status"
        case step2_ApprovalStatus = "step_2_approval_status"
        case moduleOriginalID = "module_original_id"
        case createdAt = "created_at"
        case date = "date"
        case tyoeOfVerificationSlug = "tyoe_of_verification_slug"
        case moduleNameSlug = "module_name_slug"
        case username = "username"
        case status = "status"
        case step1_ApprovalStatusSlug = "step_1_approval_status_slug"
        case step2_ApprovalStatusSlug = "step_2_approval_status_slug"
        case quantity = "quantity"
        case amount = "amount"
    }

    public init(id: Int?, tyoeOfVerification: String?, moduleName: String?, userID: String?, moduleID: String?, approveBy1: String?, approveBy2: String?, comment: String?, comment2: String?, aP1: String?, aP2: String?, firstName: String?, lastName: String?, status: String?, step1_ApprovalStatus: String?, step2_ApprovalStatus: String?, moduleOriginalID: String?, createdAt: String?, date: String?, tyoeOfVerificationSlug: String?, moduleNameSlug: String?, username: String?, step1_ApprovalStatusSlug: String?, step2_ApprovalStatusSlug: String?, quantity: Int?, amount: String?) {
        self.id = id
        self.tyoeOfVerification = tyoeOfVerification
        self.moduleName = moduleName
        self.userID = userID
        self.moduleID = moduleID
        self.approveBy1 = approveBy1
        self.approveBy2 = approveBy2
        self.comment = comment
        self.comment2 = comment2
        self.aP1 = aP1
        self.aP2 = aP2
        self.firstName = firstName
        self.lastName = lastName
        self.step1_ApprovalStatus = step1_ApprovalStatus
        self.step2_ApprovalStatus = step2_ApprovalStatus
        self.moduleOriginalID = moduleOriginalID
        self.createdAt = createdAt
        self.date = date
        self.tyoeOfVerificationSlug = tyoeOfVerificationSlug
        self.moduleNameSlug = moduleNameSlug
        self.username = username
        self.status = status
        self.step1_ApprovalStatusSlug = step1_ApprovalStatusSlug
        self.step2_ApprovalStatusSlug = step2_ApprovalStatusSlug
        self.quantity = quantity
        self.amount = amount
    }
}

extension String {
    public func convertToJSONObject() -> [String : AnyObject]? {
        do {
            guard let json = self.data(using: String.Encoding.utf8, allowLossyConversion: false) else { return nil }
            guard let jsonObject = try JSONSerialization.jsonObject(with: json, options: .mutableContainers) as? [String : AnyObject] else { return nil}
            return jsonObject
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
