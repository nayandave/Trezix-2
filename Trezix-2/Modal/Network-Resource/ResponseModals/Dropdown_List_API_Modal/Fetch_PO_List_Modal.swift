//
//  Fetch_PO_List_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 28/11/22.
//

import Foundation

// MARK: - Fetch PO List Response Modal
public struct Fetch_PO_List_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var result: [PO_List_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case result = "result"
    }
}

// MARK: - PO List Data Modal
public struct PO_List_Data: Codable {
    public var id: Int
    public var adminID: Int?
    public var poNo: String?
    public var company: String?
    public var vendor: String?
    public var shipToAddress: String?
    public var nominatedCha: String?
    public var deliveryPort: String?
    public var currencyID: String?
    public var paymentTerm: String?
    public var incoTerm: String?
    public var incoTerm2: String?
    public var modeOfShipment: String?
    public var ourBank: String?
    public var specialInstruction: String?
    public var shippingMark: String?
    public var packingInstruction: String?
    public var shippingSchedule: String?
    public var shippingInstruction: String?
    public var remark: String?
    public var invoiceTemplate: String?
    public var existingPo: String?
    public var otherDocument: String?
    public var documentName: String?
    public var documentUpload: String?
    public var digitalSignature: String?
    public var status: Int?
    public var createdAt: String?
    public var updatedAt: String?
    public var poID: String?
    public var deletedAt: String?
    public var patnerType: String?
    public var patnerName: String?
    public var packing: String?
    public var packQty: String?
    public var currencyExchangeRate: String?
    public var approvalStatus: String?
    public var otherDocumentsTag: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case adminID = "admin_id"
        case poNo = "po_no"
        case company = "company"
        case vendor = "vendor"
        case shipToAddress = "ship_to_address"
        case nominatedCha = "Nominated_cha"
        case deliveryPort = "delivery_port"
        case currencyID = "currency_id"
        case paymentTerm = "payment_term"
        case incoTerm = "inco_term"
        case incoTerm2 = "inco_term_2"
        case modeOfShipment = "mode_of_shipment"
        case ourBank = "our_bank"
        case specialInstruction = "special_instruction"
        case shippingMark = "shipping_mark"
        case packingInstruction = "packing_instruction"
        case shippingSchedule = "shipping_schedule"
        case shippingInstruction = "shipping_instruction"
        case remark = "remark"
        case invoiceTemplate = "invoice_template"
        case existingPo = "existing_po"
        case otherDocument = "other_document"
        case documentName = "document_name"
        case documentUpload = "document_upload"
        case digitalSignature = "digital_signature"
        case status = "status"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case poID = "po_id"
        case deletedAt = "deleted_at"
        case patnerType = "patner_type"
        case patnerName = "patner_name"
        case packing = "packing"
        case packQty = "pack_qty"
        case currencyExchangeRate = "currency_exchange_rate"
        case approvalStatus = "approval_status"
        case otherDocumentsTag = "other_documents_tag"
    }

    public init(id: Int, adminID: Int?, poNo: String?, company: String?, vendor: String?, shipToAddress: String?, nominatedCha: String?, deliveryPort: String?, currencyID: String?, paymentTerm: String?, incoTerm: String?, incoTerm2: String?, modeOfShipment: String?, ourBank: String?, specialInstruction: String?, shippingMark: String?, packingInstruction: String?, shippingSchedule: String?, shippingInstruction: String?, remark: String?, invoiceTemplate: String?, existingPo: String?, otherDocument: String?, documentName: String?, documentUpload: String?, digitalSignature: String?, status: Int?, createdAt: String?, updatedAt: String?, poID: String?, deletedAt: String?, patnerType: String?, patnerName: String?, packing: String?, packQty: String?, currencyExchangeRate: String?, approvalStatus: String?, otherDocumentsTag: String?) {
        self.id = id
        self.adminID = adminID
        self.poNo = poNo
        self.company = company
        self.vendor = vendor
        self.shipToAddress = shipToAddress
        self.nominatedCha = nominatedCha
        self.deliveryPort = deliveryPort
        self.currencyID = currencyID
        self.paymentTerm = paymentTerm
        self.incoTerm = incoTerm
        self.incoTerm2 = incoTerm2
        self.modeOfShipment = modeOfShipment
        self.ourBank = ourBank
        self.specialInstruction = specialInstruction
        self.shippingMark = shippingMark
        self.packingInstruction = packingInstruction
        self.shippingSchedule = shippingSchedule
        self.shippingInstruction = shippingInstruction
        self.remark = remark
        self.invoiceTemplate = invoiceTemplate
        self.existingPo = existingPo
        self.otherDocument = otherDocument
        self.documentName = documentName
        self.documentUpload = documentUpload
        self.digitalSignature = digitalSignature
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.poID = poID
        self.deletedAt = deletedAt
        self.patnerType = patnerType
        self.patnerName = patnerName
        self.packing = packing
        self.packQty = packQty
        self.currencyExchangeRate = currencyExchangeRate
        self.approvalStatus = approvalStatus
        self.otherDocumentsTag = otherDocumentsTag
    }
}
