//
//  Get_Invoice_List_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 24/12/22.
//

import Foundation

// MARK: - Get_Invoice_List_Modal
public struct Get_Invoice_List_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var result: [Invoice_List_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case result = "result"
    }
}

// MARK: - Invoice_Data_Modal
public struct Invoice_List_Data: Codable {
    public var id: String
    public var invoiceNum: String?
    public var invoiceDate: String?
    public var adminID: String?
    public var poID: String?
    public var invoicingPartyID: String?
    public var attachment: String?
    public var patnerType: String?
    public var patnerName: String?
    public var status: String?
    public var mainStatus: String?
    public var otherDocument: String?
    public var otherDocumentsTag: String?
    public var advancePayment: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case invoiceNum = "invoice_num"
        case invoiceDate = "invoice_date"
        case adminID = "admin_id"
        case poID = "po_id"
        case invoicingPartyID = "invoicing_party_id"
        case attachment = "attachment"
        case patnerType = "patner_type"
        case patnerName = "patner_name"
        case status = "status"
        case mainStatus = "main_status"
        case otherDocument = "other_document"
        case otherDocumentsTag = "other_documents_tag"
        case advancePayment = "advance_payment"
    }

    public init(id: String, invoiceNum: String?, invoiceDate: String?, adminID: String?, poID: String?, invoicingPartyID: String?, attachment: String?, patnerType: String?, patnerName: String?, status: String?, mainStatus: String?, otherDocument: String?, otherDocumentsTag: String?, advancePayment: String?) {
        self.id = id
        self.invoiceNum = invoiceNum
        self.invoiceDate = invoiceDate
        self.adminID = adminID
        self.poID = poID
        self.invoicingPartyID = invoicingPartyID
        self.attachment = attachment
        self.patnerType = patnerType
        self.patnerName = patnerName
        self.status = status
        self.mainStatus = mainStatus
        self.otherDocument = otherDocument
        self.otherDocumentsTag = otherDocumentsTag
        self.advancePayment = advancePayment
    }
}
