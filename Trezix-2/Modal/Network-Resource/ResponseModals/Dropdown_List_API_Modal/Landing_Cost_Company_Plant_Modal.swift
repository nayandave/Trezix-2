//
//  Landing_Cost_Company_Plant_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 27/03/23.
//

import Foundation

// MARK: - CompanyPlantListModal
public struct Landing_Cost_Company_Plant_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [Landing_Cost_Comapny_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Datum
public struct Landing_Cost_Comapny_Data: Codable {
    public var id: Int
    public var label: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "label"
    }
}
