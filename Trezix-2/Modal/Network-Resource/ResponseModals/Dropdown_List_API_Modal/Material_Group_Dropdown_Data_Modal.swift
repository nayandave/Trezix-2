//
//  Material_Group_Dropdown_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 06/08/22.
//

import Foundation

// MARK: - Material Group Dropdown Data Response Modal
/// `Material Group Dropdown Data Modal`
///  - Fetched From ``APIConstants/GET_MATERIAL_GROUP_DROPDOWN`` API
public class Material_Group_Dropdown_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [Material_Group_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Material Group Data Modal
/// `Structure of Material Group Data`
/// - Fetched From ``APIConstants/GET_MATERIAL_GROUP_DROPDOWN`` in Array
public class Material_Group_Data: Codable {
    public var id: Int
    public var adminID: Int
    public var name: String
    public var value: String
    public var createdAt: String?
    public var updatedAt: String?
    public var deletedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case adminID = "admin_id"
        case name = "name"
        case value = "value"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
    }

    public init(id: Int, adminID: Int, name: String, value: String, createdAt: String?, updatedAt: String?, deletedAt: String?) {
        self.id = id
        self.adminID = adminID
        self.name = name
        self.value = value
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
    }
}
