//
//  Material_Dropdown_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 06/08/22.
//

import Foundation

// MARK: - Material Dropdown Data Response Modal
/// `Material Dropdown Data Modal`
/// - Fetched From ``APIConstants/GET_MATERIAL_DROPDOWN`` API
public class Material_Dropdown_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [Material_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Material Data Modal
/// `Structure of Material Data`
/// - Fetched From ``APIConstants/GET_MATERIAL_DROPDOWN`` in Array
public class Material_Data: Codable {
    public var id: Int
    public var adminID: Int?
    public var materialCode: String?
    public var materialDescription: String?
    public var oldMaterialCode: String?
    public var materialGrpName: String?
    public var materialGrpValue: String?
    public var heirarchy: String?
    public var hsnCode: String?
    public var taxPricingGroup: String?
    public var minimumSalesPrice: Double?
    public var minimumQtyForSale: Int?
    public var materialCost: Double?
    public var measurement: String?
    public var manufacturerDate: String?
    public var selfLife: String?
    public var uom: String?
    public var createdAt: String?
    public var updatedAt: String?
    public var deletedAt: String?
    public var tolerance: Int?
    public var flag: String?
    public var bassNOS: String?
     public var bassUom: String?
     public var convertedNOS: String?
     public var convertedUom: String?
     public var isCompleted: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
         case adminID = "admin_id"
         case materialCode = "material_code"
         case materialDescription = "material_description"
         case oldMaterialCode = "old_material_code"
         case materialGrpName = "material_grp_name"
         case materialGrpValue = "material_grp_value"
         case heirarchy = "heirarchy"
         case hsnCode = "hsn_code"
         case taxPricingGroup = "tax_pricing_group"
         case minimumSalesPrice = "minimum_sales_price"
         case minimumQtyForSale = "minimum_qty_for_sale"
         case materialCost = "material_cost"
         case measurement = "measurement"
         case manufacturerDate = "manufacturer_date"
         case selfLife = "self_life"
         case uom = "uom"
         case createdAt = "created_at"
         case updatedAt = "updated_at"
         case deletedAt = "deleted_at"
         case tolerance = "tolerance"
         case flag = "flag"
         case bassNOS = "bass_nos"
         case bassUom = "bass_uom"
         case convertedNOS = "converted_nos"
         case convertedUom = "converted_uom"
         case isCompleted = "is_completed"
    }
}
