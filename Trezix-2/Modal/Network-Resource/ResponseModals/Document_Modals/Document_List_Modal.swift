//
//  Document_List_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 30/05/23.
//

import Foundation

// MARK: - Document List Response Modal
public struct Document_List_Response_Modal: Codable {
    public var status: Int?
    public var url: String?
    public var data: [Document_List_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case url = "url"
        case data = "data"
    }
}

// MARK: - Datum
public struct Document_List_Data: Codable {
    public var id: Int?
    public var adminID: Int?
    public var fileName: String?
    public var ipAddress: String?
    public var createdAt: String?
    public var updatedAt: String?
    public var fileType: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case adminID = "admin_id"
        case fileName = "file_name"
        case ipAddress = "ip_address"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case fileType = "file_type"
    }
}
