//
//  Ask_GPT_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 05/04/23.
//
import Alamofire
import Foundation

class GPT_Helper {
    static func ask_EximGPT(withQuestion: String,
                            showHud: Bool = true,
                            senderView: UIView? = nil,
                            completionHandler:
                            @escaping ((_ isSuccess: Bool, _ response: (GPT_ResponseModel)?, _ errorMessage: String?) -> Void)) {
        let param = ["question" : withQuestion] //try make_Askable_JSON_GPT(question: withQuestion)
        let apiName = APIConstants.ASK_GPT.rawValue
        
        if appDelegate.isNetworkAvailable  {
            print("\n\n -- Ask GPT API ---- \(apiName) ---\n\n")
            
            if showHud {
                Loader.showLoader()
            }
            senderView?.animateLoader()
            
            AF.request(apiName,
                       method: .post,
                       parameters: param,
                       encoding: JSONEncoding.default,
                       headers: [HTTPHeader.contentType("application/json"), HTTPHeader.accept("*/*")]) { $0.timeoutInterval = 10 }
                .validate(statusCode: 200...299)
                .responseData { response in
                    //debugPrint("Response Time == \(response.metrics?.taskInterval.duration)")
                    switch response.result {
                    case .success(let responseData):
                        if let data = try? JSONDecoder().decode(GPT_ResponseModel.self, from: responseData) {
                            senderView?.remove_Activity_Animator()
                            completionHandler(true, data, nil)
                        } else {
                            senderView?.remove_Activity_Animator()
                            completionHandler(false, nil, "GPT Response is not passing through Codable Response Modal")
                        }
                    case .failure(let error):
                        senderView?.remove_Activity_Animator()
                        completionHandler(false, nil, error.localizedDescription)
                    }
                }
        } else {
            //            NoInternet.showInternetView(noInternetMsg)
        }
    }
}


// MARK: - EximGPT Response Modal
public struct GPT_ResponseModel: Codable {
    public var status: Int
    public var answer: String?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case answer = "answer"
    }
}
