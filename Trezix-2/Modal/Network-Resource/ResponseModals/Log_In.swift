//
//  ResponseModals.swift
//  Trezix-Demo1
//
//  Created by Amar Panchal on 10/06/22.
//

import Foundation

// MARK: - LOGIN RESPONSE MODAL
public struct LoginResponseModal: Codable {
    public let status: Int
    public let msg: String
    public let data: UserDataModal?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

///**USER DATA FROM LOGIN RESPONSE MODAL**
public struct UserDataModal: Codable {
    public var id: Int
    public var adminID: Int
    public var companyName: String?
    public var domain: String?
    public var companyDesignation: String?
    public var firstName: String?
    public var lastName: String?
    public var dob: String?
    public var email: String?
    public var mobileNumber: String?
    public var salesPersonName: String?
    public var salesPersonPhone: String?
    public var salesPersonEmail: String?
    public var emailVerifiedAt: String?
    public var password: String?
    public var rememberToken: String?
    public var createdAt: String?
    public var updatedAt: String?
    public var companyCode: String?
    public var country: String?
    public var bucket: String?
    public var appToken: String
    public var token: String?
    public var logst: String?
    public var lastLoginAt: String?
    public var lastLoginIP: String?
    public var status: String?
    public var companyLimit: String?
    public var flag: String?
    public var permission: [String]?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case adminID = "admin_id"
        case companyName = "company_name"
        case domain = "domain"
        case companyDesignation = "company_designation"
        case firstName = "first_name"
        case lastName = "last_name"
        case dob = "dob"
        case email = "email"
        case mobileNumber = "mobile_number"
        case salesPersonName = "sales_person_name"
        case salesPersonPhone = "sales_person_phone"
        case salesPersonEmail = "sales_person_email"
        case emailVerifiedAt = "email_verified_at"
        case password = "password"
        case rememberToken = "remember_token"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case companyCode = "company_code"
        case country = "country"
        case bucket = "bucket"
        case appToken = "app_token"
        case token = "token"
        case logst = "logst"
        case lastLoginAt = "last_login_at"
        case lastLoginIP = "last_login_ip"
        case status = "status"
        case companyLimit = "company_limit"
        case flag = "flag"
        case permission = "permission"
    }
}
