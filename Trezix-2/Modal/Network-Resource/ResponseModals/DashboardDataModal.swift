// MARK: - Pending Import Quantity Modal
public class Pending_Import_Quantity_Modal: Codable {
    public var name: String?
    public var measurementUnit: String?
    public var pendingOrderQuantity: Int?
    public var status: Int?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case measurementUnit = "measurement_unit"
        case pendingOrderQuantity = "pending_order_quantity"
        case status = "status"
        case createdAt = "created_at"
    }

    public init(name: String?, measurementUnit: String?, pendingOrderQuantity: Int?, status: Int?, createdAt: String?) {
        self.name = name
        self.measurementUnit = measurementUnit
        self.pendingOrderQuantity = pendingOrderQuantity
        self.status = status
        self.createdAt = createdAt
    }
}

// MARK: - Close Import Quantity Modal
public class Close_Import_Quantity_Modal: Codable {
    public var name: String?
    public var proLineItemID: Int?
    public var status: Int?
    public var poQuantity: Int?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case proLineItemID = "pro_line_item_id"
        case status = "status"
        case poQuantity = "po_quantity"
        case createdAt = "created_at"
    }

    public init(name: String?, proLineItemID: Int?, status: Int?, poQuantity: Int?, createdAt: String?) {
        self.name = name
        self.proLineItemID = proLineItemID
        self.status = status
        self.poQuantity = poQuantity
        self.createdAt = createdAt
    }
}

// MARK: - MeasurementUnit
public class MeasurementUnit: Codable {
    public var id: Int
    public var adminID: Int
    public var code: String
    public var name: String
    public var createdAt: String
    public var updatedAt: String
    public var deletedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case adminID = "admin_id"
        case code = "code"
        case name = "name"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
    }

    public init(id: Int, adminID: Int, code: String, name: String, createdAt: String, updatedAt: String, deletedAt: String?) {
        self.id = id
        self.adminID = adminID
        self.code = code
        self.name = name
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
    }
}

// MARK: - Close Import Amount Modal
public class Close_Import_Amount_Modal: Codable {
    public var currencyID: Int?
    public var status: Int?
    public var currency: Currency?
    public var poPrice: Double?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case currencyID = "currency_id"
        case status = "status"
        case currency = "currency"
        case poPrice = "po_price"
        case createdAt = "created_at"
    }

    public init(currencyID: Int?, status: Int?, currency: Currency?, poPrice: Double?, createdAt: String?) {
        self.currencyID = currencyID
        self.status = status
        self.currency = currency
        self.poPrice = poPrice
        self.createdAt = createdAt
    }
}

// MARK: - Pending Import Amount Modal
public class Pending_Import_Amount_Modal: Codable {
    public var currencyID: Int?
    public var status: Int?
    public var currency: Currency?
    public var pendingOrderPrice: Double?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case currencyID = "currency_id"
        case status = "status"
        case currency = "currency"
        case pendingOrderPrice = "pending_order_price"
        case createdAt = "created_at"
    }

    public init(currencyID: Int?, status: Int?, currency: Currency?, pendingOrderPrice: Double?, createdAt: String?) {
        self.currencyID = currencyID
        self.status = status
        self.currency = currency
        self.pendingOrderPrice = pendingOrderPrice
        self.createdAt = createdAt
    }
}

// MARK: - Currency
public class Currency: Codable {
    public var currencyName: String?
    public var currency: String?
    public var iso4217_Code: Int?
    public var updatedAt: String?
    public var deletedAt: String?
    public var predecimal: String?
    public var createdAt: String?
    public var currencyShortCode: String?
    public var currencySymbole: String?
    public var adminID: Int?
    public var id: Int?
    public var print: String?

    enum CodingKeys: String, CodingKey {
        case currencyName = "currency_name"
        case currency = "currency"
        case iso4217_Code = "iso_4217_code"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case predecimal = "predecimal"
        case createdAt = "created_at"
        case currencyShortCode = "currency_short_code"
        case currencySymbole = "currency_symbole"
        case adminID = "admin_id"
        case id = "id"
        case print = "print"
    }

    public init(currencyName: String?, currency: String?, iso4217_Code: Int?, updatedAt: String?, deletedAt: String?, predecimal: String?, createdAt: String?, currencyShortCode: String?, currencySymbole: String?, adminID: Int?, id: Int?, print: String?) {
        self.currencyName = currencyName
        self.currency = currency
        self.iso4217_Code = iso4217_Code
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.predecimal = predecimal
        self.createdAt = createdAt
        self.currencyShortCode = currencyShortCode
        self.currencySymbole = currencySymbole
        self.adminID = adminID
        self.id = id
        self.print = print
    }
}

// MARK: - Upcoming Shipment Data Modal
public class Upcoming_Shipment_Modal: Codable {
    public var invoiceNo: String
    public var blNo: String
    public var vesselName: String
    public var date: String

    enum CodingKeys: String, CodingKey {
        case invoiceNo = "invoice_no"
        case blNo = "bl_no"
        case vesselName = "vessel_name"
        case date = "date"
    }

    public init(invoiceNo: String, blNo: String, vesselName: String, date: String) {
        self.invoiceNo = invoiceNo
        self.blNo = blNo
        self.vesselName = vesselName
        self.date = date
    }
}


// MARK: - Pie Chart Modal
public class PieChartModal: Codable {
    public var newDate: String
    public var total: Double

    enum CodingKeys: String, CodingKey {
        case newDate = "new_date"
        case total = "total"
    }

    public init(newDate: String, total: Double) {
        self.newDate = newDate
        self.total = total
    }
}

