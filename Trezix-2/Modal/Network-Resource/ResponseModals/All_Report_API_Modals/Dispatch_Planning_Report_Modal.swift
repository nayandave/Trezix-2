//
//  Dispatch_Planning_Report_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 26/12/22.
//

import Foundation

// MARK: - Dispatch_Planning_Report_Modal
public struct Dispatch_Planning_Report_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var data: [Dispatch_Planning_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Dispatch_Planning_Data
public struct Dispatch_Planning_Data: Codable {
    public var shipType: String?
    public var buyer: String?
    public var shipToParty: String?
    public var unit: String?
    public var shipNetWeight: Double?
    public var shipGrossWeight: Double?
    public var noOfPallet: Double?
    public var loose: String?
    public var stuffingInstruction: String?
    public var cartingPoint: String?
    public var blSpecificRemark: String?
    public var remark: String?
    public var containerNo: String?
    public var vehicalNo: String?
    public var contactPersonName: String?
    public var mobileNo: String?
    public var preshipmentlineitem: [Dispatch_Planning_LineItem]?
    public var total: Double?

    enum CodingKeys: String, CodingKey {
        case shipType = "ship_type"
        case buyer = "buyer"
        case shipToParty = "ship_to_party"
        case unit = "unit"
        case shipNetWeight = "ship_net_weight"
        case shipGrossWeight = "ship_gross_weight"
        case noOfPallet = "no_of_pallet"
        case loose = "loose"
        case stuffingInstruction = "stuffing_instruction"
        case cartingPoint = "carting_point"
        case blSpecificRemark = "bl_specific_remark"
        case remark = "remark"
        case containerNo = "container_no"
        case vehicalNo = "vehical_no"
        case contactPersonName = "contact_person_name"
        case mobileNo = "mobile_no"
        case preshipmentlineitem = "preshipmentlineitem"
        case total = "total"
    }
}

// MARK: - Dispatch_Planning_LineItem
public struct Dispatch_Planning_LineItem: Codable {
    public var preshipmentInvoiceNo: String?
    public var materialDescription: String?
    public var quantity: String?

    enum CodingKeys: String, CodingKey {
        case preshipmentInvoiceNo = "preshipment_invoice_no"
        case materialDescription = "material_description"
        case quantity = "quantity"
    }

    public init(preshipmentInvoiceNo: String?, materialDescription: String?, quantity: String?) {
        self.preshipmentInvoiceNo = preshipmentInvoiceNo
        self.materialDescription = materialDescription
        self.quantity = quantity
    }
}
