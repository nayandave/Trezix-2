//
//  PO_TimeLine_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 28/11/22.
//

import Foundation

// MARK: - PO TimeLine Data Modal
public struct PO_TimeLine_Data_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: TimeLine_Data?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - TimeLine_Data
public struct TimeLine_Data: Codable {
    public var pochange: [Pochange?]?
    public var popdf: [Popdf?]?
    public var salescontract: [Salescontract?]?
    public var lc: [Lc?]?
    public var pss: [Pss?]?
    public var invoice: [Invoice?]?
    public var shipment: [Shipment?]?
    public var payment: [Payment?]?
    public var grn: [Grn?]?
    public var docbank: [Docbank?]?
    public var boe: [Boe?]?
    public var duty: [Duty?]?
    public var url: DurlClass?
    public var durl: DurlClass?

    enum CodingKeys: String, CodingKey {
        case pochange = "pochange"
        case popdf = "popdf"
        case salescontract = "salescontract"
        case lc = "lc"
        case pss = "pss"
        case invoice = "invoice"
        case shipment = "shipment"
        case payment = "payment"
        case grn = "grn"
        case docbank = "docbank"
        case boe = "boe"
        case duty = "duty"
        case url = "url"
        case durl = "durl"
    }

    public init(pochange: [Pochange?], popdf: [Popdf?], salescontract: [Salescontract?], lc: [Lc?], pss: [Pss?], invoice: [Invoice?], shipment: [Shipment?], payment: [Payment?], grn: [Grn?], docbank: [Docbank?], boe: [Boe?], duty: [Duty?], url: DurlClass?, durl: DurlClass?) {
        self.pochange = pochange
        self.popdf = popdf
        self.salescontract = salescontract
        self.lc = lc
        self.pss = pss
        self.invoice = invoice
        self.shipment = shipment
        self.payment = payment
        self.grn = grn
        self.docbank = docbank
        self.boe = boe
        self.duty = duty
        self.url = url
        self.durl = durl
    }
}

// MARK: - Boe
public struct Boe: Codable {
    public var id: Int?
    public var beNo: String?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case beNo = "be_no"
        case createdAt = "created_at"
    }

    public init(id: Int?, beNo: String?, createdAt: String?) {
        self.id = id
        self.beNo = beNo
        self.createdAt = createdAt
    }
}

// MARK: - Docbank
public struct Docbank: Codable {
    public var id: Int
    public var intimationNumber: String?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case intimationNumber = "intimation_number"
        case createdAt = "created_at"
    }

    public init(id: Int, intimationNumber: String?, createdAt: String?) {
        self.id = id
        self.intimationNumber = intimationNumber
        self.createdAt = createdAt
    }
}

// MARK: - DurlClass
public struct DurlClass: Codable {
    public var po: String?
    public var poDocument: String?
    public var salesContract: String?
    public var other: String?
    public var invoice: String?

    enum CodingKeys: String, CodingKey {
        case po = "PO"
        case poDocument = "PO_Document"
        case salesContract = "sales_contract"
        case other = "Other"
        case invoice = "invoice"
    }

    public init(po: String?, poDocument: String?, salesContract: String?, other: String?, invoice: String?) {
        self.po = po
        self.poDocument = poDocument
        self.salesContract = salesContract
        self.other = other
        self.invoice = invoice
    }
}

// MARK: - Duty
public struct Duty: Codable {
    public var id: Int
    public var invoiceNum: String?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case invoiceNum = "invoice_num"
        case createdAt = "created_at"
    }

    public init(id: Int, invoiceNum: String?, createdAt: String?) {
        self.id = id
        self.invoiceNum = invoiceNum
        self.createdAt = createdAt
    }
}

// MARK: - Grn
public struct Grn: Codable {
    public var id: Int
    public var grnReference: String?
    public var grnDate: String?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case grnReference = "grn_reference"
        case grnDate = "grn_date"
        case createdAt = "created_at"
    }

    public init(id: Int, grnReference: String?, grnDate: String?, createdAt: String?) {
        self.id = id
        self.grnReference = grnReference
        self.grnDate = grnDate
        self.createdAt = createdAt
    }
}

// MARK: - Invoice
public struct Invoice: Codable {
    public var id: Int
    public var invoiceNum: String?
    public var attachment: String?
    public var invoiceDate: String?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case invoiceNum = "invoice_num"
        case attachment = "attachment"
        case invoiceDate = "invoice_date"
        case createdAt = "created_at"
    }

    public init(id: Int, invoiceNum: String?, attachment: String?, invoiceDate: String?, createdAt: String?) {
        self.id = id
        self.invoiceNum = invoiceNum
        self.attachment = attachment
        self.invoiceDate = invoiceDate
        self.createdAt = createdAt
    }
}

// MARK: - Lc
public struct Lc: Codable {
    public var id: Int
    public var lcNo: String?
    public var lcDate: String?
    public var lcExpiryDate: String?
    public var lcAmount: Int?
    public var letterofcredit: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case lcNo = "lc_no"
        case lcDate = "lc_date"
        case lcExpiryDate = "lc_expiry_date"
        case lcAmount = "lc_amount"
        case letterofcredit = "letterofcredit"
    }

    public init(id: Int, lcNo: String?, lcDate: String?, lcExpiryDate: String?, lcAmount: Int?, letterofcredit: String?) {
        self.id = id
        self.lcNo = lcNo
        self.lcDate = lcDate
        self.lcExpiryDate = lcExpiryDate
        self.lcAmount = lcAmount
        self.letterofcredit = letterofcredit
    }
}

// MARK: - Payment
public struct Payment: Codable {
    public var id: Int
    public var paymentDate: String?
    public var createdAt: String?
    public var paymentModeSlug: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case paymentDate = "payment_date"
        case createdAt = "created_at"
        case paymentModeSlug = "payment_mode_slug"
    }

    public init(id: Int, paymentDate: String?, createdAt: String?, paymentModeSlug: String?) {
        self.id = id
        self.paymentDate = paymentDate
        self.createdAt = createdAt
        self.paymentModeSlug = paymentModeSlug
    }
}

// MARK: - Pochange
public struct Pochange: Codable {
    public var id: Int
    public var poNo: String?
    public var createdAt: String?
    public var status: Int?
    public var documentUpload: String?
    public var otherDocument: String?
    public var existingPo: String?
    public var statusSlug: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case poNo = "po_no"
        case createdAt = "created_at"
        case status = "status"
        case documentUpload = "document_upload"
        case otherDocument = "other_document"
        case existingPo = "existing_po"
        case statusSlug = "status_slug"
    }

    public init(id: Int, poNo: String?, createdAt: String?, status: Int?, documentUpload: String?, otherDocument: String?, existingPo: String?, statusSlug: String?) {
        self.id = id
        self.poNo = poNo
        self.createdAt = createdAt
        self.status = status
        self.documentUpload = documentUpload
        self.otherDocument = otherDocument
        self.existingPo = existingPo
        self.statusSlug = statusSlug
    }
}

// MARK: - Popdf
public struct Popdf: Codable {
    public var id: Int
    public var adminID: Int?
    public var poID: String?
    public var poNo: String?
    public var poPDF: String?
    public var createdAt: String?
    public var updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case adminID = "admin_id"
        case poID = "po_id"
        case poNo = "po_no"
        case poPDF = "po_pdf"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    public init(id: Int, adminID: Int?, poID: String?, poNo: String?, poPDF: String?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.adminID = adminID
        self.poID = poID
        self.poNo = poNo
        self.poPDF = poPDF
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

// MARK: - Pss
public struct Pss: Codable {
    public var id: Int
    public var sampleNo: String?
    public var recvdDate: String?
    public var createdAt: String?
    public var statusSlug: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case sampleNo = "sample_no"
        case recvdDate = "recvd_date"
        case createdAt = "created_at"
        case statusSlug = "status_slug"
    }

    public init(id: Int, sampleNo: String?, recvdDate: String?, createdAt: String?, statusSlug: String?) {
        self.id = id
        self.sampleNo = sampleNo
        self.recvdDate = recvdDate
        self.createdAt = createdAt
        self.statusSlug = statusSlug
    }
}

// MARK: - Salescontract
public struct Salescontract: Codable {
    public var id: Int
    public var contractNo: String?
    public var date: String?
    public var createdAt: String?
    public var salescontract: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case contractNo = "contract_no"
        case date = "date"
        case createdAt = "created_at"
        case salescontract = "salescontract"
    }

    public init(id: Int, contractNo: String?, date: String?, createdAt: String?, salescontract: String?) {
        self.id = id
        self.contractNo = contractNo
        self.date = date
        self.createdAt = createdAt
        self.salescontract = salescontract
    }
}

// MARK: - Shipment
public struct Shipment: Codable {
    public var id: Int
    public var blNo: String?
    public var etd: String?
    public var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case blNo = "bl_no"
        case etd = "etd"
        case createdAt = "created_at"
    }

    public init(id: Int, blNo: String?, etd: String?, createdAt: String?) {
        self.id = id
        self.blNo = blNo
        self.etd = etd
        self.createdAt = createdAt
    }
}
