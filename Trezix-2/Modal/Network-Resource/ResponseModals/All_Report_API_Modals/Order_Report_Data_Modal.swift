//
//  Order_Report_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 21/07/22.
//

import Foundation

// MARK: - Order Report Response Modal
public class OrderReportReponse: Codable {
    public var status: Int
     public var msg: String
     public var data: [mainOrderReportData]?

     enum CodingKeys: String, CodingKey {
         case status = "status"
         case msg = "msg"
         case data = "data"
     }
}

// MARK: - Main Order Report Data
public class mainOrderReportData: Codable {
    public var poNo: String
    public var createdAt: String
    public var companyPlant: String?
    public var vendorName: String
    public var group1: String?
    public var group2: String?
    public var advancePayment: String?
    public var materialCode: String?
    public var materialName: String?
    public var quility: String?
    public var unit: String?
    public var rate: String?
    public var currency: String?
    public var fcValue: Double?
    public var inrvalue: Double?
    public var shippedQty: Int?
    public var pendingQty: Int?
    public var packingInstruction: String?
    public var materialGrpValue: String?
    public var paymentTerm: String?
    public var incoterm: String?

    enum CodingKeys: String, CodingKey {
        case poNo = "po_no"
        case createdAt = "created_at"
        case companyPlant = "company_plant"
        case vendorName = "vendor_name"
        case group1 = "group1"
        case group2 = "group2"
        case advancePayment = "advance_payment"
        case materialCode = "material_code"
        case materialName = "material_name"
        case quility = "quility"
        case unit = "unit"
        case rate = "rate"
        case currency = "currency"
        case fcValue = "fc_value"
        case inrvalue = "inrvalue"
        case shippedQty = "shipped_qty"
        case pendingQty = "pending_qty"
        case packingInstruction = "packing_instruction"
        case materialGrpValue = "material_grp_value"
        case paymentTerm = "payment_term"
        case incoterm = "incoterm"
    }

    public init(poNo: String, createdAt: String, companyPlant: String?, vendorName: String, group1: String?, group2: String?, advancePayment: String?,materialCode: String?, materialName: String?, quility: String?, unit: String?, rate: String?, currency: String?, fcValue: Double?, inrvalue: Double?, shippedQty: Int?, pendingQty: Int?, packingInstruction: String?, materialGrpValue: String?, paymentTerm: String?, incoterm: String?) {
        self.poNo = poNo
        self.createdAt = createdAt
        self.companyPlant = companyPlant
        self.vendorName = vendorName
        self.group1 = group1
        self.group2 = group2
        self.advancePayment = advancePayment
        self.materialCode = materialCode
        self.materialName = materialName
        self.quility = quility
        self.unit = unit
        self.rate = rate
        self.currency = currency
        self.fcValue = fcValue
        self.inrvalue = inrvalue
        self.shippedQty = shippedQty
        self.pendingQty = pendingQty
        self.packingInstruction = packingInstruction
        self.materialGrpValue = materialGrpValue
        self.paymentTerm = paymentTerm
        self.incoterm = incoterm
    }
}
