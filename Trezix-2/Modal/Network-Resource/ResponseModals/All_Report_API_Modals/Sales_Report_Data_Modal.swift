//
//  Sales_Report_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 19/12/22.
//

import Foundation

// MARK: - Sales Report Data Modal
public struct Sales_Report_Data_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [Sales_Report_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Sales_Report_Data
public struct Sales_Report_Data: Codable {
    public var srNo: Int?
    public var piNo: String?
    public var piDate: String?
    public var fiNo: String?
    public var fiDate: String?
    public var commInvoiceNo: String?
    public var commInvoiceDate: String?
    public var soldToCountry: String?
    public var shipToCountry: String?
    public var paymentTerms: String?
    public var incotermDescription: String?
    public var incoTerm2: String?
    public var netWeight: Int?
    public var currencyName: String?
    public var valueTotal: String?
    public var exchangeRate: String?
    public var pdfPreshipmentInvoiceInr: String?
    public var shipmentType: String?
    public var scheme1: Int?
    public var license1: String?
    public var scheme2: Int?
    public var license2: String?
    public var scheme3: Int?
    public var license3: String?
    public var scheme4: Int?
    public var license4: String?
    public var insPolicyNo: String?
    public var insCERTNo: String?
    public var insCERTDate: String?
    public var insuredAmount: String?
    public var insuranceCharges: String?
    public var vesselFlightNo: String?
    public var containerNo: String?
    public var portOfLoading: String?
    public var portOfDischarge: String?
    public var finalDestination: String?
    public var countryFinalDestination: String?
    public var portCode: String?
    public var portName: String?
    public var placeOfReceipt: String?
    public var fiExchangeRate: String?
    public var fiInr: String?
    public var letExportDate: String?
    public var blNo: String?
    public var blDate: String?
    public var sbNo: String?
    public var sbDate: String?
    public var shippingBillExchangeRate: String?
    public var grNo: String?
    public var grDate: String?
    public var brcNo: String?
    public var brcDate: String?
    public var brcExchangeRate: String?
    public var pdfFobInvoice: String?
    public var currencyID: Int?
    public var payerID: String?
    public var foreignBankCharges: String?
    public var bankCharges: String?
    public var shortPayment: String?
    public var dueDate: String?
    public var orderNo: String?
    public var deliveryNo: String?
    public var plant: String?
    public var customerName: String?
    public var customerGroup2: String?
    public var customerGroup3: String?
    public var customerGroup4: String?
    public var customerGroup5: String?
    public var customerGroup6: String?
    public var customerGroup7: String?
    public var customerGroup8: String?
    public var customerGroup9: String?
    public var customerGroup10: String?
    public var notify: String?
    public var localBank: String?
    public var customerBank: String?
    public var cha: String?
    public var shippingAgent: String?
    public var insuranceCompany: String?
    public var materialCode: String?
    public var materialDescription: String?
    public var customerProductDescription: String?
    public var batchNo: String?
    public var perPack: String?
    public var noPacking: String?
    public var itemQty: String?
    public var storageLocation: String?
    public var price: String?
    public var itemValue: String?
    public var inrValue: String?
    public var poNo: String?
    public var poDate: String?
    public var materialGroup1: String?
    public var materialGroup2: String?
    public var materialGroup3: String?
    public var materialGroup4: String?
    public var materialGroup5: String?
    public var materialGroup6: String?
    public var materialGroup7: String?
    public var materialGroup8: String?
    public var materialGroup9: String?
    public var materialGroup10: String?

    enum CodingKeys: String, CodingKey {
        case srNo = "sr_no"
        case piNo = "pi_no"
        case piDate = "pi_date"
        case fiNo = "fi_no"
        case fiDate = "fi_date"
        case commInvoiceNo = "comm_invoice_no"
        case commInvoiceDate = "comm_invoice_date"
        case soldToCountry = "sold_to_country"
        case shipToCountry = "ship_to_country"
        case paymentTerms = "payment_terms"
        case incotermDescription = "incoterm_description"
        case incoTerm2 = "inco_term_2"
        case netWeight = "net_weight"
        case currencyName = "currency_name"
        case valueTotal = "value_total"
        case exchangeRate = "exchange_rate"
        case pdfPreshipmentInvoiceInr = "pdf_preshipment_invoice_inr"
        case shipmentType = "shipment_type"
        case scheme1 = "scheme1"
        case license1 = "license1"
        case scheme2 = "scheme2"
        case license2 = "license2"
        case scheme3 = "scheme3"
        case license3 = "license3"
        case scheme4 = "scheme4"
        case license4 = "license4"
        case insPolicyNo = "ins_policy_no"
        case insCERTNo = "ins_cert_no"
        case insCERTDate = "ins_cert_date"
        case insuredAmount = "insured_amount"
        case insuranceCharges = "insurance_charges"
        case vesselFlightNo = "vessel_flight_no"
        case containerNo = "container_no"
        case portOfLoading = "port_of_loading"
        case portOfDischarge = "port_of_discharge"
        case finalDestination = "final_destination"
        case countryFinalDestination = "country_final_destination"
        case portCode = "port_code"
        case portName = "port_name"
        case placeOfReceipt = "place_of_receipt"
        case fiExchangeRate = "fi_exchange_rate"
        case fiInr = "fi_inr"
        case letExportDate = "let_export_date"
        case blNo = "bl_no"
        case blDate = "bl_date"
        case sbNo = "sb_no"
        case sbDate = "sb_date"
        case shippingBillExchangeRate = "shipping_bill_exchange_rate"
        case grNo = "gr_no"
        case grDate = "gr_date"
        case brcNo = "brc_no"
        case brcDate = "brc_date"
        case brcExchangeRate = "brc_exchange_rate"
        case pdfFobInvoice = "pdf_fob_invoice"
        case currencyID = "currency_id"
        case payerID = "payer_id"
        case foreignBankCharges = "foreign_bank_charges"
        case bankCharges = "bank_charges"
        case shortPayment = "short_payment"
        case dueDate = "due_date"
        case orderNo = "order_no"
        case deliveryNo = "delivery_no"
        case plant = "plant"
        case customerName = "customer_name"
        case customerGroup2 = "customer_group2"
        case customerGroup3 = "customer_group3"
        case customerGroup4 = "customer_group4"
        case customerGroup5 = "customer_group5"
        case customerGroup6 = "customer_group6"
        case customerGroup7 = "customer_group7"
        case customerGroup8 = "customer_group8"
        case customerGroup9 = "customer_group9"
        case customerGroup10 = "customer_group10"
        case notify = "notify"
        case localBank = "local_bank"
        case customerBank = "customer_bank"
        case cha = "cha"
        case shippingAgent = "shipping_agent"
        case insuranceCompany = "insurance_company"
        case materialCode = "material_code"
        case materialDescription = "material_description"
        case customerProductDescription = "customer_product_description"
        case batchNo = "batch_no"
        case perPack = "per_pack"
        case noPacking = "no_packing"
        case itemQty = "item_qty"
        case storageLocation = "storage_location"
        case price = "price"
        case itemValue = "item_value"
        case inrValue = "inr_value"
        case poNo = "po_no"
        case poDate = "po_date"
        case materialGroup1 = "material_group_1"
        case materialGroup2 = "material_group_2"
        case materialGroup3 = "material_group_3"
        case materialGroup4 = "material_group_4"
        case materialGroup5 = "material_group_5"
        case materialGroup6 = "material_group_6"
        case materialGroup7 = "material_group_7"
        case materialGroup8 = "material_group_8"
        case materialGroup9 = "material_group_9"
        case materialGroup10 = "material_group_10"
    }

    public init(srNo: Int?, piNo: String?, piDate: String?, fiNo: String?, fiDate: String?, commInvoiceNo: String?, commInvoiceDate: String?, soldToCountry: String?, shipToCountry: String?, paymentTerms: String?, incotermDescription: String?, incoTerm2: String?, netWeight: Int?, currencyName: String?, valueTotal: String?, exchangeRate: String?, pdfPreshipmentInvoiceInr: String?, shipmentType: String?, scheme1: Int?, license1: String?, scheme2: Int?, license2: String?, scheme3: Int?, license3: String?, scheme4: Int?, license4: String?, insPolicyNo: String?, insCERTNo: String?, insCERTDate: String?, insuredAmount: String?, insuranceCharges: String?, vesselFlightNo: String?, containerNo: String?, portOfLoading: String?, portOfDischarge: String?, finalDestination: String?, countryFinalDestination: String?, portCode: String?, portName: String?, placeOfReceipt: String?, fiExchangeRate: String?, fiInr: String?, letExportDate: String?, blNo: String?, blDate: String?, sbNo: String?, sbDate: String?, shippingBillExchangeRate: String?, grNo: String?, grDate: String?, brcNo: String?, brcDate: String?, brcExchangeRate: String?, pdfFobInvoice: String?, currencyID: Int?, payerID: String?, foreignBankCharges: String?, bankCharges: String?, shortPayment: String?, dueDate: String?, orderNo: String?, deliveryNo: String?, plant: String?, customerName: String?, customerGroup2: String?, customerGroup3: String?, customerGroup4: String?, customerGroup5: String?, customerGroup6: String?, customerGroup7: String?, customerGroup8: String?, customerGroup9: String?, customerGroup10: String?, notify: String?, localBank: String?, customerBank: String?, cha: String?, shippingAgent: String?, insuranceCompany: String?, materialCode: String?, materialDescription: String?, customerProductDescription: String?, batchNo: String?, perPack: String?, noPacking: String?, itemQty: String?, storageLocation: String?, price: String?, itemValue: String?, inrValue: String?, poNo: String?, poDate: String?, materialGroup1: String?, materialGroup2: String?, materialGroup3: String?, materialGroup4: String?, materialGroup5: String?, materialGroup6: String?, materialGroup7: String?, materialGroup8: String?, materialGroup9: String?, materialGroup10: String?) {
        self.srNo = srNo
        self.piNo = piNo
        self.piDate = piDate
        self.fiNo = fiNo
        self.fiDate = fiDate
        self.commInvoiceNo = commInvoiceNo
        self.commInvoiceDate = commInvoiceDate
        self.soldToCountry = soldToCountry
        self.shipToCountry = shipToCountry
        self.paymentTerms = paymentTerms
        self.incotermDescription = incotermDescription
        self.incoTerm2 = incoTerm2
        self.netWeight = netWeight
        self.currencyName = currencyName
        self.valueTotal = valueTotal
        self.exchangeRate = exchangeRate
        self.pdfPreshipmentInvoiceInr = pdfPreshipmentInvoiceInr
        self.shipmentType = shipmentType
        self.scheme1 = scheme1
        self.license1 = license1
        self.scheme2 = scheme2
        self.license2 = license2
        self.scheme3 = scheme3
        self.license3 = license3
        self.scheme4 = scheme4
        self.license4 = license4
        self.insPolicyNo = insPolicyNo
        self.insCERTNo = insCERTNo
        self.insCERTDate = insCERTDate
        self.insuredAmount = insuredAmount
        self.insuranceCharges = insuranceCharges
        self.vesselFlightNo = vesselFlightNo
        self.containerNo = containerNo
        self.portOfLoading = portOfLoading
        self.portOfDischarge = portOfDischarge
        self.finalDestination = finalDestination
        self.countryFinalDestination = countryFinalDestination
        self.portCode = portCode
        self.portName = portName
        self.placeOfReceipt = placeOfReceipt
        self.fiExchangeRate = fiExchangeRate
        self.fiInr = fiInr
        self.letExportDate = letExportDate
        self.blNo = blNo
        self.blDate = blDate
        self.sbNo = sbNo
        self.sbDate = sbDate
        self.shippingBillExchangeRate = shippingBillExchangeRate
        self.grNo = grNo
        self.grDate = grDate
        self.brcNo = brcNo
        self.brcDate = brcDate
        self.brcExchangeRate = brcExchangeRate
        self.pdfFobInvoice = pdfFobInvoice
        self.currencyID = currencyID
        self.payerID = payerID
        self.foreignBankCharges = foreignBankCharges
        self.bankCharges = bankCharges
        self.shortPayment = shortPayment
        self.dueDate = dueDate
        self.orderNo = orderNo
        self.deliveryNo = deliveryNo
        self.plant = plant
        self.customerName = customerName
        self.customerGroup2 = customerGroup2
        self.customerGroup3 = customerGroup3
        self.customerGroup4 = customerGroup4
        self.customerGroup5 = customerGroup5
        self.customerGroup6 = customerGroup6
        self.customerGroup7 = customerGroup7
        self.customerGroup8 = customerGroup8
        self.customerGroup9 = customerGroup9
        self.customerGroup10 = customerGroup10
        self.notify = notify
        self.localBank = localBank
        self.customerBank = customerBank
        self.cha = cha
        self.shippingAgent = shippingAgent
        self.insuranceCompany = insuranceCompany
        self.materialCode = materialCode
        self.materialDescription = materialDescription
        self.customerProductDescription = customerProductDescription
        self.batchNo = batchNo
        self.perPack = perPack
        self.noPacking = noPacking
        self.itemQty = itemQty
        self.storageLocation = storageLocation
        self.price = price
        self.itemValue = itemValue
        self.inrValue = inrValue
        self.poNo = poNo
        self.poDate = poDate
        self.materialGroup1 = materialGroup1
        self.materialGroup2 = materialGroup2
        self.materialGroup3 = materialGroup3
        self.materialGroup4 = materialGroup4
        self.materialGroup5 = materialGroup5
        self.materialGroup6 = materialGroup6
        self.materialGroup7 = materialGroup7
        self.materialGroup8 = materialGroup8
        self.materialGroup9 = materialGroup9
        self.materialGroup10 = materialGroup10
    }
}
