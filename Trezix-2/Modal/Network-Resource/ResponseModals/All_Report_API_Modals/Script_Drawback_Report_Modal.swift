//
//  Script_Drawback_Report_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 02/01/23.
//

import Foundation

// MARK: - Script Drawback Report Modal
public struct Script_Drawback_Report_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var data: [Script_Drawback_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Script Drawback Report Data
public struct Script_Drawback_Data: Codable {
    public var piNo: String?
    public var piDate: String?
    public var taxInvoiceNo: String?
    public var taxInvoiceDate: String?
    public var scheme1: String?
    public var license1: String?
    public var scheme2: String?
    public var license2: String?
    public var scheme3: String?
    public var license3: String?
    public var scheme4: String?
    public var license4: String?
    public var scriptNo: String?
    public var scriptDate: String?
    public var expiryDate: String?
    public var receivedDate: String?
    public var bankID: String?
    public var amount: String?
    public var fiNo: String?
    public var fiDate: String?
    public var customer: String?
    public var incoterm: String?
    public var paymentTerm: String?
    public var dueDate: String?
    public var realizationDate: String?
    public var totalValue: String?
    public var invCurrency: String?
    public var blNo: String?
    public var blDate: String?
    public var sbNo: String?
    public var sbDate: String?
    public var sbExchangeRate: String?
    public var letExportDate: String?
    public var sbFob: String?
    public var portCode: String?
    public var portName: Int?
    public var scheme1_SbValue: String?
    public var scheme1_Received: String?
    public var scheme2_SbValue: String?
    public var scheme2_Received: String?
    public var applied1_Date: String?
    public var applied2_Date: String?
    public var balance: String?

    enum CodingKeys: String, CodingKey {
        case piNo = "pi_no"
        case piDate = "pi_date"
        case taxInvoiceNo = "tax_invoice_no"
        case taxInvoiceDate = "tax_invoice_date"
        case scheme1 = "scheme1"
        case license1 = "license1"
        case scheme2 = "scheme2"
        case license2 = "license2"
        case scheme3 = "scheme3"
        case license3 = "license3"
        case scheme4 = "scheme4"
        case license4 = "license4"
        case scriptNo = "script_no"
        case scriptDate = "script_date"
        case expiryDate = "expiry_date"
        case receivedDate = "received_date"
        case bankID = "bank_id"
        case amount = "amount"
        case fiNo = "fi_no"
        case fiDate = "fi_date"
        case customer = "customer"
        case incoterm = "incoterm"
        case paymentTerm = "payment_term"
        case dueDate = "due_date"
        case realizationDate = "realization_date"
        case totalValue = "total_value"
        case invCurrency = "inv_currency"
        case blNo = "bl_no"
        case blDate = "bl_date"
        case sbNo = "sb_no"
        case sbDate = "sb_date"
        case sbExchangeRate = "sb_exchange_rate"
        case letExportDate = "let_export_date"
        case sbFob = "sb_fob"
        case portCode = "port_code"
        case portName = "port_name"
        case scheme1_SbValue = "scheme_1_sb_value"
        case scheme1_Received = "scheme_1_received"
        case scheme2_SbValue = "scheme_2_sb_value"
        case scheme2_Received = "scheme_2_received"
        case applied1_Date = "applied_1_date"
        case applied2_Date = "applied_2_date"
        case balance = "balance"
    }
}
