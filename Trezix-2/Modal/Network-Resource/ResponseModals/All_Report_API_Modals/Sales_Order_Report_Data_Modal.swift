//
//  Sales_Order_Report_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 19/12/22.
//

import Foundation
// MARK: - Sales_Order_Report_Data_Modal
public struct Sales_Order_Report_Data_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [Sales_Order_Report_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Sales_Order_Report_Data
public struct Sales_Order_Report_Data: Codable {
    public var srNo: Int?
    public var soNo: String?
    public var soDate: String?
    public var customerOrderNo: String?
    public var customerOrderDate: String?
    public var customerName: String?
    public var shipToName: String?
    public var mot: String?
    public var incoTerm1: String?
    public var incoTerm2: String?
    public var portOfDischarge: String?
    public var lcNo: String?
    public var lcDate: String?
    public var material: String?
    public var materialDescription: String?
    public var rate: String?
    public var totalValue: String?
    public var currency: String?
    public var scheduleDate: String?
    public var orderQuantity: String?
    public var salesQuantity: String?
    public var uom: String?
    public var balance: String?
    public var pss: String?
    public var mfgPlant: String?
    public var packing: String?
    public var itemStatus: String?
    public var orderStatus: String?

    enum CodingKeys: String, CodingKey, CaseIterable {
        case srNo = "sr_no"
        case soNo = "so_no"
        case soDate = "so_date"
        case customerOrderNo = "customer_order_no"
        case customerOrderDate = "customer_order_date"
        case customerName = "customer_name"
        case shipToName = "ship_to_name"
        case mot = "mot"
        case incoTerm1 = "inco_term_1"
        case incoTerm2 = "inco_term_2"
        case portOfDischarge = "port_of_discharge"
        case lcNo = "lc_no"
        case lcDate = "lc_date"
        case material = "material"
        case materialDescription = "material_description"
        case rate = "rate"
        case totalValue = "total_value"
        case currency = "currency"
        case scheduleDate = "schedule_date"
        case orderQuantity = "order_quantity"
        case salesQuantity = "sales_quantity"
        case uom = "uom"
        case balance = "balance"
        case pss = "pss"
        case mfgPlant = "mfg_plant"
        case packing = "packing"
        case itemStatus = "item_status"
        case orderStatus = "order_status"
    }
    
    static var allCases: [String] {
      return CodingKeys.allCases.map { $0.rawValue }
    }
    
    public init(srNo: Int?, soNo: String?, soDate: String?, customerOrderNo: String?, customerOrderDate: String?, customerName: String?, shipToName: String?, mot: String?, incoTerm1: String?, incoTerm2: String?, portOfDischarge: String?, lcNo: String?, lcDate: String?, material: String?, materialDescription: String?, rate: String?, totalValue: String?, currency: String?, scheduleDate: String?, orderQuantity: String?, salesQuantity: String?, uom: String?, balance: String?, pss: String?, mfgPlant: String?, packing: String?, itemStatus: String?, orderStatus: String?) {
        self.srNo = srNo
        self.soNo = soNo
        self.soDate = soDate
        self.customerOrderNo = customerOrderNo
        self.customerOrderDate = customerOrderDate
        self.customerName = customerName
        self.shipToName = shipToName
        self.mot = mot
        self.incoTerm1 = incoTerm1
        self.incoTerm2 = incoTerm2
        self.portOfDischarge = portOfDischarge
        self.lcNo = lcNo
        self.lcDate = lcDate
        self.material = material
        self.materialDescription = materialDescription
        self.rate = rate
        self.totalValue = totalValue
        self.currency = currency
        self.scheduleDate = scheduleDate
        self.orderQuantity = orderQuantity
        self.salesQuantity = salesQuantity
        self.uom = uom
        self.balance = balance
        self.pss = pss
        self.mfgPlant = mfgPlant
        self.packing = packing
        self.itemStatus = itemStatus
        self.orderStatus = orderStatus
    }
}
