//
//  Landing_Cost_Invoice_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 24/12/22.
//

import Foundation

// MARK: - Landing Cost Invoice Report Modal
public struct Landing_Cost_Invoice_Modal: Codable {
    public var status: Int?
    public var msg: String?
    public var result: [Landing_Cost_Invoice_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case result = "result"
    }
}

// MARK: - Landing Cost Invoice Data Modal
public struct Landing_Cost_Invoice_Data: Codable {
    public var productCode: String?
    public var productName: String?
    public var qty: String?
    public var price: String?
    public var total: String?
    public var currncy: String?
    public var beExchangeRate: String?
    public var bankCharges: String?
    public var deec: String?
    public var bcd: String?
    public var cvd: String?
    public var sad: String?
    public var igst: String?
    public var cessOfCvd: String?
    public var edvCess: String?
    public var antiDumping: String?
    public var penalty: String?
    public var other: String?
    public var secEducCess: String?
    public var socialWelfare: String?
    public var finalTotal: String?
    public var finalTotalWithGst: String?
    public var totalInr: String?
    public var totalInrWithGst: String?
    public var otherCharges: [Landing_Cost_OtherCharges_Data]?
    public var quantityInr: String?
    public var quantityOther: String?
    public var quantityInrGst: String?
    public var quantityOtherGst: String?

    enum CodingKeys: String, CodingKey {
        case productCode = "product_code"
        case productName = "product_name"
        case qty = "qty"
        case price = "price"
        case total = "total"
        case currncy = "currncy"
        case beExchangeRate = "be_exchange_rate"
        case bankCharges = "bank_charges"
        case deec = "deec"
        case bcd = "bcd"
        case cvd = "cvd"
        case sad = "sad"
        case igst = "igst"
        case cessOfCvd = "cess_of_cvd"
        case edvCess = "edv_cess"
        case antiDumping = "anti_dumping"
        case penalty = "penalty"
        case other = "other"
        case secEducCess = "sec_educ_cess"
        case socialWelfare = "social_welfare"
        case finalTotal = "final_total"
        case finalTotalWithGst = "final_total_with_gst"
        case totalInr = "total_inr"
        case totalInrWithGst = "total_inr_with_gst"
        case otherCharges = "other_charges"
        case quantityInr = "quantity_inr"
        case quantityOther = "quantity_usd"
        case quantityInrGst = "quantity_inr_gst"
        case quantityOtherGst = "quantity_usd_gst"
    }
}

// MARK: - Landing Cost OtherCharges Data
public struct Landing_Cost_OtherCharges_Data: Codable {
    public var ptype: String?
    public var pvalue: String?

    enum CodingKeys: String, CodingKey {
        case ptype = "ptype"
        case pvalue = "pvalue"
    }
}


