//
//  Shipment_Report_Data_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 02/11/22.
//

import Foundation

public struct ShipmentReportDataModal: Codable {
    public var cName: String?
    public var status: Int?
    public var msg: String?
    public var data: [MainShipmentData]?

    enum CodingKeys: String, CodingKey {
        case cName = "c_name"
        case status = "status"
        case msg = "msg"
        case data = "data"
    }

    public init(cName: String?, status: Int?, msg: String?, data: [MainShipmentData]?) {
        self.cName = cName
        self.status = status
        self.msg = msg
        self.data = data
    }
}

// MARK: - Main Shipment Data
public struct MainShipmentData: Codable {
    public var id: Int?
    public var poNo: String
    public var poDate: String?
    public var lcNo: String?
    public var lcDate: String?
    public var lcExpiryDate: String?
    public var lastDateOfShipment: String?
    public var companyPlant: String?
    public var vendorName: String?
    public var vendorGroup1: String?
    public var vendorGroup2: String?
    public var materialCode: String?
    public var materialName: String?
    public var poQuantity: String?
    public var shippedQuantity: Int?
    public var pendingQuantity: Int?
    public var unit: String?
    public var rate: String?
    public var currency: String?
    public var fcValue: Double?
    public var inrValue: Double?
    public var packingInstruction: String?
    public var materialGrpValue: String?
    public var paymentTerm: String?
    public var incoTerm1: String?
    public var incoTerm2: String?
    public var invoicingParty: String?
    public var invoiceNum: String?
    public var invoiceDate: String?
    public var deliveryPort: String?
    public var blNo: String?
    public var blDate: String?
    public var etaDate: String?
    public var intimationNumber: String?
    public var intimationDate: String?
    public var documentClearingDate: String?
    public var nominatedCha: String?
    public var beNo: String?
    public var beDate: String?
    public var igmNo: String?
    public var igmDate: String?
    public var status: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case poNo = "po_no"
        case poDate = "po_date"
        case lcNo = "lc_no"
        case lcDate = "lc_date"
        case lcExpiryDate = "lc_expiry_date"
        case lastDateOfShipment = "last_date_of_shipment"
        case companyPlant = "company_plant"
        case vendorName = "vendor_name"
        case vendorGroup1 = "vendor_group1"
        case vendorGroup2 = "vendor_group2"
        case materialCode = "material_code"
        case materialName = "material_name"
        case poQuantity = "po_quantity"
        case shippedQuantity = "shipped_quantity"
        case pendingQuantity = "pending_quantity"
        case unit = "unit"
        case rate = "rate"
        case currency = "currency"
        case fcValue = "fc_value"
        case inrValue = "inr_value"
        case packingInstruction = "packing_instruction"
        case materialGrpValue = "material_grp_value"
        case paymentTerm = "payment_term"
        case incoTerm1 = "inco_term_1"
        case incoTerm2 = "inco_term_2"
        case invoicingParty = "invoicing_party"
        case invoiceNum = "invoice_num"
        case invoiceDate = "invoice_date"
        case deliveryPort = "delivery_port"
        case blNo = "bl_no"
        case blDate = "bl_date"
        case etaDate = "eta_date"
        case intimationNumber = "intimation_number"
        case intimationDate = "intimation_date"
        case documentClearingDate = "document_clearing_date"
        case nominatedCha = "nominated_cha"
        case beNo = "be_no"
        case beDate = "be_date"
        case igmNo = "igm_no"
        case igmDate = "igm_date"
        case status = "status"
    }

    public init(id: Int?, poNo: String, poDate: String?, lcNo: String?, lcDate: String?, lcExpiryDate: String?, lastDateOfShipment: String?, companyPlant: String?, vendorName: String?, vendorGroup1: String?, vendorGroup2: String?, materialCode: String?, materialName: String?, poQuantity: String?, shippedQuantity: Int?, pendingQuantity: Int?, unit: String?, rate: String?, currency: String?, fcValue: Double?, inrValue: Double?, packingInstruction: String?, materialGrpValue: String?, paymentTerm: String?, incoTerm1: String?, incoTerm2: String?, invoicingParty: String?, invoiceNum: String?, invoiceDate: String?, deliveryPort: String?, blNo: String?, blDate: String?, etaDate: String?, intimationNumber: String?, intimationDate: String?, documentClearingDate: String?, nominatedCha: String?, beNo: String?, beDate: String?, igmNo: String?, igmDate: String?, status: String?) {
        self.id = id
        self.poNo = poNo
        self.poDate = poDate
        self.lcNo = lcNo
        self.lcDate = lcDate
        self.lcExpiryDate = lcExpiryDate
        self.lastDateOfShipment = lastDateOfShipment
        self.companyPlant = companyPlant
        self.vendorName = vendorName
        self.vendorGroup1 = vendorGroup1
        self.vendorGroup2 = vendorGroup2
        self.materialCode = materialCode
        self.materialName = materialName
        self.poQuantity = poQuantity
        self.shippedQuantity = shippedQuantity
        self.pendingQuantity = pendingQuantity
        self.unit = unit
        self.rate = rate
        self.currency = currency
        self.fcValue = fcValue
        self.inrValue = inrValue
        self.packingInstruction = packingInstruction
        self.materialGrpValue = materialGrpValue
        self.paymentTerm = paymentTerm
        self.incoTerm1 = incoTerm1
        self.incoTerm2 = incoTerm2
        self.invoicingParty = invoicingParty
        self.invoiceNum = invoiceNum
        self.invoiceDate = invoiceDate
        self.deliveryPort = deliveryPort
        self.blNo = blNo
        self.blDate = blDate
        self.etaDate = etaDate
        self.intimationNumber = intimationNumber
        self.intimationDate = intimationDate
        self.documentClearingDate = documentClearingDate
        self.nominatedCha = nominatedCha
        self.beNo = beNo
        self.beDate = beDate
        self.igmNo = igmNo
        self.igmDate = igmDate
        self.status = status
    }
}
