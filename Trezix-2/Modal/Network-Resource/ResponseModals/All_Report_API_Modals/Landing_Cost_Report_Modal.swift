//
//  Landing_Cost_Report_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 28/03/23.
//

import Foundation

// MARK: - Landing Cost Report API Response Modal
public struct Landing_Cost_Report_Modal: Codable {
    public var status: Int
    public var msg: String
    public var data: [Landing_Cost_Report_Data]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - Landing Cost Report Data
public struct Landing_Cost_Report_Data: Codable {
    public var partnercharges: String?
    public var sr: String?
    public var invoiceNo: String?
    public var creationDate: String?
    public var companyname: String?
    public var plant: String?
    public var poNo: String?
    public var vendor: String?
    public var material: String?
    public var unitOM: String?
    public var productCode: String?
    public var productName: String?
    public var quantity: String?
    public var price: String?
    public var total: String?
    public var currency: String?
    public var beExRate: String?
    public var bankCharges: String?
    public var deec: String?
    public var bcd: String?
    public var cvd: String?
    public var sad: String?
    public var igst: String?
    public var cessOfCVD: String?
    public var edvCess: String?
    public var antiDumping: String?
    public var penalty: String?
    public var other: String?
    public var secEducCess: String?
    public var socialWelfare: String?
    public var totalINR: String?
    public var totalForegin: String?
    public var totalINRGST: String?
    public var totalForeginGST: String?
    public var oneQtyINR: String?
    public var oneQtyForegin: String?
    public var oneQtyINRGST: String?
    public var oneQtyForeginGST: String?
    public var keyvalue: String?

    enum CodingKeys: String, CodingKey {
        case partnercharges = "partnercharges"
        case sr = "Sr"
        case invoiceNo = "invoice_no"
        case creationDate = "creation_date"
        case companyname = "Companyname"
        case plant = "Plant"
        case poNo = "po_no"
        case vendor = "vendor"
        case material = "Material"
        case unitOM = "Unit_O_M"
        case productCode = "Product_Code"
        case productName = "Product_Name"
        case quantity = "Quantity"
        case price = "Price"
        case total = "Total"
        case currency = "Currency"
        case beExRate = "BE_Ex_Rate"
        case bankCharges = "Bank_Charges"
        case deec = "DEEC"
        case bcd = "BCD"
        case cvd = "CVD"
        case sad = "SAD"
        case igst = "IGST"
        case cessOfCVD = "Cess_of_CVD"
        case edvCess = "Edv_cess"
        case antiDumping = "Anti_Dumping"
        case penalty = "Penalty"
        case other = "Other"
        case secEducCess = "Sec_Educ_Cess"
        case socialWelfare = "Social_Welfare"
        case totalINR = "Total_INR"
        case totalForegin = "Total_Foregin"
        case totalINRGST = "Total_INR_GST"
        case totalForeginGST = "Total_Foregin_GST"
        case oneQtyINR = "one_Qty_INR"
        case oneQtyForegin = "one_Qty_Foregin"
        case oneQtyINRGST = "one_Qty_INR_GST"
        case oneQtyForeginGST = "one_Qty_Foregin_GST"
        case keyvalue = "keyvalue"
    }

    public init(partnercharges: String?, sr: String?, invoiceNo: String?, creationDate: String?, companyname: String?, plant: String?, poNo: String?, vendor: String?, material: String?, unitOM: String?, productCode: String?, productName: String?, quantity: String?, price: String?, total: String?, currency: String?, beExRate: String?, bankCharges: String?, deec: String?, bcd: String?, cvd: String?, sad: String?, igst: String?, cessOfCVD: String?, edvCess: String?, antiDumping: String?, penalty: String?, other: String?, secEducCess: String?, socialWelfare: String?, totalINR: String?, totalForegin: String?, totalINRGST: String?, totalForeginGST: String?, oneQtyINR: String?, oneQtyForegin: String?, oneQtyINRGST: String?, oneQtyForeginGST: String?, keyvalue: String?) {
        self.partnercharges = partnercharges
        self.sr = sr
        self.invoiceNo = invoiceNo
        self.creationDate = creationDate
        self.companyname = companyname
        self.plant = plant
        self.poNo = poNo
        self.vendor = vendor
        self.material = material
        self.unitOM = unitOM
        self.productCode = productCode
        self.productName = productName
        self.quantity = quantity
        self.price = price
        self.total = total
        self.currency = currency
        self.beExRate = beExRate
        self.bankCharges = bankCharges
        self.deec = deec
        self.bcd = bcd
        self.cvd = cvd
        self.sad = sad
        self.igst = igst
        self.cessOfCVD = cessOfCVD
        self.edvCess = edvCess
        self.antiDumping = antiDumping
        self.penalty = penalty
        self.other = other
        self.secEducCess = secEducCess
        self.socialWelfare = socialWelfare
        self.totalINR = totalINR
        self.totalForegin = totalForegin
        self.totalINRGST = totalINRGST
        self.totalForeginGST = totalForeginGST
        self.oneQtyINR = oneQtyINR
        self.oneQtyForegin = oneQtyForegin
        self.oneQtyINRGST = oneQtyINRGST
        self.oneQtyForeginGST = oneQtyForeginGST
        self.keyvalue = keyvalue
    }
}
