//
//  Vessel_Detail_Response_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 17/04/23.
//

import Foundation

public struct Vessel_Detail_Response_Modal: Codable {
    public var success: Bool
    public var message: String
    public var data: Vessel_Detail_Data?

    enum CodingKeys: String, CodingKey {
        case success = "success"
        case message = "message"
        case data = "data"
    }
}

// MARK: - Vessel_Detail_Data
public struct Vessel_Detail_Data: Codable {
    public var shipmentNumber: String?
    public var containerNumber: String?
    public var containerSealNumber: String?
    public var containerSize: String?
    public var containerType: String?
    public var containerISO: String?
    public var status: String?
    public var subStatus1: String?
    public var subStatus2: String?
    public var mblNumber: String?
    public var carrierScac: String?
    public var originToPort: Vessel_OriginToPort_Data?
    public var portToPort: Vessel_PortToPort_Data?
    public var latestLocation: Vessel_LatestLocation_Data?
    public var shipmentEvent: [Vessel_Shipment_Event_Data]?

    enum CodingKeys: String, CodingKey {
        case shipmentNumber = "shipment_number"
        case containerNumber = "container_number"
        case containerSealNumber = "container_seal_number"
        case containerSize = "container_size"
        case containerType = "container_type "
        case containerISO = "container_iso "
        case status = "status"
        case subStatus1 = "sub_status1"
        case subStatus2 = "sub_status2"
        case mblNumber = "mbl_number"
        case carrierScac = "carrier_scac"
        case originToPort = "origin_to_port"
        case portToPort = "port_to_port"
        case latestLocation = "latest_location"
        case shipmentEvent = "shipment_event"
    }
}

// MARK: - Vessel_LatestLocation_Data
public struct Vessel_LatestLocation_Data: Codable {
    public var latitude: Double?
    public var longitude: Double?

    enum CodingKeys: String, CodingKey {
        case latitude = "latitude"
        case longitude = "longitude"
    }
}

// MARK: - Vessel_OriginToPort_Data
public struct Vessel_OriginToPort_Data: Codable {
    public var carrier: String?

    enum CodingKeys: String, CodingKey {
        case carrier = "carrier"
    }
}

// MARK: - Vessel_PortToPort_Data
public struct Vessel_PortToPort_Data: Codable {
    public var firstPort: String?
    public var firstPortAtd: String?
    public var dischargePort: String?
    public var dischargePortATA: String?

    enum CodingKeys: String, CodingKey {
        case firstPort = "first_port"
        case firstPortAtd = "first_port_atd"
        case dischargePort = "discharge_port"
        case dischargePortATA = "discharge_port_ata"
    }
}

// MARK: - Vessel_Shipment_Event_Data
public struct Vessel_Shipment_Event_Data: Codable {
    public var name: String?
    public var estimateTime: String?
    public var actualTime: String?
    public var location: String?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case estimateTime = "estimate_time"
        case actualTime = "actual_time"
        case location = "location"
    }
}
