//
//  Vessel_List_Response_Modal.swift
//  Trezix-2
//
//  Created by Amar Panchal on 13/04/23.
//

import Foundation

// MARK: - Vessel_List_Response_Modal
public struct Vessel_List_Response_Modal: Codable {
    public var success: Bool
    public var message: String
    public var data: [Vessel_List_Data]?

    enum CodingKeys: String, CodingKey {
        case success = "success"
        case message = "message"
        case data = "data"
    }
}

// MARK: - Vessel_List_Data
public struct Vessel_List_Data: Codable {
    public var id: Int
    public var containerNumber: String?
    public var vesselName: String?
    public var seaLine: String?
    public var etaDateAccordingUser: String?
    public var etaDateAccordingTracking: String?
    public var trackStatus: String?
    public var shipmentStatus: String?
    public var blNumber: String?
    public var vendorName: String?
    public var invoice: String?
    public var timeStamp: String?
    public var inShipmentPlanning: Bool?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case containerNumber = "container_number"
        case vesselName = "vessel_name"
        case seaLine = "sea_line"
        case etaDateAccordingUser = "eta_date_according_user"
        case etaDateAccordingTracking = "eta_date_according_tracking"
        case trackStatus = "track_status"
        case shipmentStatus = "shipment_status"
        case blNumber = "bl_number"
        case vendorName = "vendor_name"
        case invoice = "invoice"
        case timeStamp = "time_stamp"
        case inShipmentPlanning = "in_shipment_planning"
    }
}
