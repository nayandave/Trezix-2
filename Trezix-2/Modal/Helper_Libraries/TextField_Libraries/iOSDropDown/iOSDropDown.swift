
//
//  iOSDropDown.swift
//
//
//  Created by Jishnu Raj T on 26/04/18.
//  Copyright © 2018 JRiOSdev. All rights reserved.
//
import UIKit

@IBDesignable
open class DropDown : UITextField {

    var arrow : Arrow!
    var table : UITableView!
    var shadow : UIView!
    public var selectedIndex: Int?


    //MARK: IBInspectable
    @IBInspectable public var rowHeight: CGFloat = getDynamicHeight(size: 40)
    @IBInspectable public var rowBackgroundColor: UIColor = .systemBackground
    @IBInspectable public var selectedRowColor: UIColor = .cyan
    @IBInspectable public var hideOptionsWhenSelect = true
    @IBInspectable public var isSearchEnable: Bool = true {
        didSet{
            addGesture()
        }
    }
    @IBInspectable public var isMultipleSelectionEnabled: Bool = false {
        didSet {
            hideOptionsWhenSelect = false
        }
    }
    private var arrSelectedEntities: ([String], [Int], [Int])! = nil
    private var footerDoneButton: UIButton! = nil
    private var footerCancelButton: UIButton! = nil
    
    @IBInspectable public var selectedTextColor: UIColor = .white
    
    @IBInspectable public var isCustomTouchEnabled: Bool = false {
        didSet {
            CustomTouchAction = { debugPrint("CUSTOM TOUCH SET ->"); }
        }
    }
    fileprivate var CustomTouchAction: (() -> ())? = { debugPrint("CUSTOM TOUCH SET"); }
    
    /// `Outside TextField touch hides the dropdown`
    /// Should be `FALSE` for all dropDowns for the same ViewController
    public var isOutsideTouchAllowed: Bool = true
//    {
//        didSet {
//            if !isOutsideTouchAllowed {
//                if let allGest = self.backgroundView.gestureRecognizers {
//                    for gest in allGest { self.backgroundView.removeGestureRecognizer(gest) }
//                }
//            }
//        }
//    }
    
    @IBInspectable public var borderColor: UIColor =  UIColor.lightGray {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    //prvious value was : getDynamicHeight(size: UIScreen.main.bounds.height*0.5)
    @IBInspectable public var listHeight: CGFloat = getDynamicHeight(size: SCREEN_HEIGHT*0.45) {
        didSet {

        }
    }
    @IBInspectable public var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

    @IBInspectable public var cornerRadius: CGFloat = 5.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    // Variables
    fileprivate var tableheightX: CGFloat = getDynamicHeight(size: 150)
    fileprivate var dataArray = [String]()
    fileprivate var imageArray = [String]()
    fileprivate weak var parentController:UIViewController?
    fileprivate var pointToParent = CGPoint(x: 0, y: 0)
    fileprivate var backgroundView = UIView()
    fileprivate var keyboardHeight:CGFloat = 0 {
        didSet {
            self.reSizeTable()
        }
    }

    public var optionArray = [String]() {
        didSet{
            self.dataArray = self.optionArray
        }
    }
    public var optionImageArray = [String]() {
        didSet{
            self.imageArray = self.optionImageArray
        }
    }
    public var optionIds : [Int]?
    var searchText = String() {
        didSet{
            if searchText == "" {
                self.dataArray = self.optionArray
            }else{
                self.dataArray = optionArray.filter {
                    return $0.range(of: searchText, options: .caseInsensitive) != nil
                }
            }
            reSizeTable()
            selectedIndex = nil
            self.table.reloadData()
        }
    }
    @IBInspectable public var arrowSize: CGFloat = getDynamicHeight(size: 15) {
        didSet{
            let center =  arrow.superview!.center
            arrow.frame = CGRect(x: center.x - arrowSize/2, y: center.y - arrowSize/2, width: arrowSize, height: arrowSize)
        }
    }
    @IBInspectable public var arrowColor: UIColor = .black {
        didSet{
            arrow.arrowColor = arrowColor
        }
    }
    @IBInspectable public var checkMarkEnabled: Bool = true {
        didSet{
            
        }
    }
    @IBInspectable public var handleKeyboard: Bool = true {
        didSet{
            
        }
    }

    // Init
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        self.delegate = self
    }

    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupUI()
        self.delegate = self
    }


    //MARK: Closures
    fileprivate var didSelectCompletion: (String, Int ,Int) -> () = { selectedText, index , id  in }
    fileprivate var didMultiSelectCompletion: ([String], [Int] ,[Int], Bool) -> () = { selectedTexts, indexes , ids, isCanceled  in }
    fileprivate var TableWillAppearCompletion: () -> () = { }
    fileprivate var TableDidAppearCompletion: () -> () = { }
    fileprivate var TableWillDisappearCompletion: () -> () = { }
    fileprivate var TableDidDisappearCompletion: () -> () = { }

    func setupUI () {
        rowHeight = self.frame.height   // Static RowHeight
        let size = self.frame.height
        let rightView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: size, height: size))
        self.rightView = rightView
        self.rightViewMode = .always
        let arrowContainerView = UIView(frame: rightView.frame)
        self.rightView?.addSubview(arrowContainerView)
        let center = arrowContainerView.center
        
        let constant: CGFloat = (self.traitCollection.horizontalSizeClass == .regular && self.traitCollection.verticalSizeClass == .regular) ? 10 : 0
        
        arrow = Arrow(origin: CGPoint(x: center.x - arrowSize/2-constant,y: center.y - arrowSize/2),size: arrowSize  )
        arrowContainerView.addSubview(arrow)

        self.backgroundView = UIView(frame: .zero)
        self.backgroundView.backgroundColor = .clear
        addGesture()
        if isSearchEnable || handleKeyboard {
            NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { (notification) in
                if self.isFirstResponder{
                let userInfo:NSDictionary = notification.userInfo! as NSDictionary
                    let keyboardFrame:NSValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
                let keyboardRectangle = keyboardFrame.cgRectValue
                self.keyboardHeight = keyboardRectangle.height
//                    if self.isSelected{
//                        self.showList()
//                    }
                }
              
            }
            NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { (notification) in
                if self.isFirstResponder{
                self.keyboardHeight = 0
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    fileprivate func addGesture (){
        let gesture =  UITapGestureRecognizer(target: self, action:  #selector(touchAction))
        if isSearchEnable{
            self.rightView?.addGestureRecognizer(gesture)
        }else{
            self.addGestureRecognizer(gesture)
        }
        let gesture2 = UITapGestureRecognizer(target: self, action:  #selector(touchAction))
        self.backgroundView.addGestureRecognizer(gesture2)
    }
    func getConvertedPoint(_ targetView: UIView, baseView: UIView?)->CGPoint{
        var pnt = targetView.frame.origin
        if nil == targetView.superview{
            return pnt
        }
        var superView = targetView.superview
        while superView != baseView{
            pnt = superView!.convert(pnt, to: superView!.superview)
            if nil == superView!.superview{
                break
            }else{
                superView = superView!.superview
            }
        }
        return superView!.convert(pnt, to: baseView)
    }
    public func showList() {
        if parentController == nil{
            parentController = self.parentViewController
        }
        backgroundView.frame = parentController?.view.frame ?? backgroundView.frame
        pointToParent = getConvertedPoint(self, baseView: parentController?.view)
        parentController?.view.insertSubview(backgroundView, aboveSubview: self)
        TableWillAppearCompletion()
//        debugPrint("LIST HEIGHT = \(listHeight), & rowHeight \(rowHeight) & Array Count = \(dataArray.count)")
        if listHeight > rowHeight * CGFloat(dataArray.count) {
            self.tableheightX = (rowHeight * CGFloat(dataArray.count)) + 5
        } else {
            self.tableheightX = listHeight
        }
//        debugPrint("TableHeight = \(self.tableheightX)")
        table = UITableView(frame: CGRect(x: pointToParent.x ,
                                          y: pointToParent.y + self.frame.height ,
                                          width: self.frame.width,
                                          height: self.frame.height))
        shadow = UIView(frame: table.frame)
        shadow.backgroundColor = .clear

        table.dataSource = self
        table.delegate = self
        table.alpha = 0
        table.separatorStyle = .singleLine
        table.layer.cornerRadius = 3
        table.backgroundColor = rowBackgroundColor
        table.rowHeight = rowHeight
        parentController?.view.addSubview(shadow)
        parentController?.view.addSubview(table)
        self.isSelected = true
        let height = (self.parentController?.view.frame.height ?? 0) - (self.pointToParent.y + self.frame.height + 5)
        var y = self.pointToParent.y+self.frame.height+5
        if height < (keyboardHeight+tableheightX){
            y = self.pointToParent.y - tableheightX
        }
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.4,
                       initialSpringVelocity: 0.1,
                       options: .curveEaseInOut,
                       animations: { () -> Void in

                        self.table.frame = CGRect(x: self.pointToParent.x,
                                                  y: y,
                                                  width: self.frame.width,
                                                  height: self.tableheightX)
                        self.table.alpha = 1
                        self.shadow.frame = self.table.frame
                        self.shadow.dropShadow()
                        self.arrow.position = .up
        },
                       completion: { (finish) -> Void in
                        self.layoutIfNeeded()

        })
    }


    public func hideList() {
        TableWillDisappearCompletion()
        if table != nil {
            UIView.animate(withDuration: 0.25,
                           delay: 0.15,
                           usingSpringWithDamping: 0.9,
                           initialSpringVelocity: 0.1,
                           options: .curveEaseInOut,
                           animations: { () -> Void in

                    self.table.frame = CGRect(x: self.pointToParent.x,
                                              y: self.pointToParent.y + self.frame.height,
                                              width: self.frame.width,
                                              height: 0)
                    self.shadow.frame = self.table.frame
                    self.shadow.alpha = 0
                self.arrow.position = .down
            },
                           completion: { (didFinish) -> Void in
                
                self.shadow.removeFromSuperview()
                self.table.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
                self.isSelected = false
                self.TableDidDisappearCompletion()
            })
        }
    }

    @objc public func touchAction(_ tapGesture: UITapGestureRecognizer? = nil) {
        if isCustomTouchEnabled {
            CustomTouchAction?()
        } else {
            if isSelected {
                if let tap = tapGesture {
                    let location = tap.location(in: self)
                    let exist = self.bounds.contains(location)
                    if isOutsideTouchAllowed || exist {
                        hideList()
                    }
                } else {
                    hideList()
                }
            } else {
                showList()
            }
//            isSelected ?  hideList() : showList()
        }
    }
    func reSizeTable() {
        if listHeight > rowHeight * CGFloat( dataArray.count) {
            self.tableheightX = rowHeight * CGFloat(dataArray.count)
        }else{
            self.tableheightX = listHeight
        }
        let height = (self.parentController?.view.frame.height ?? 0) - (self.pointToParent.y + self.frame.height + 5)
        var y = self.pointToParent.y+self.frame.height+5
        if height < (keyboardHeight+tableheightX){
            y = self.pointToParent.y - tableheightX
        }
        UIView.animate(withDuration: 0.2,
                       delay: 0.1,
                       usingSpringWithDamping: 0.9,
                       initialSpringVelocity: 0.1,
                       options: .curveEaseInOut,
                       animations: { () -> Void in
                        self.table.frame = CGRect(x: self.pointToParent.x,
                                                  y: y,
                                                  width: self.frame.width,
                                                  height: self.tableheightX)
                        self.shadow.frame = self.table.frame
                        self.shadow.dropShadow()

        },
                       completion: { (didFinish) -> Void in
                      //  self.shadow.layer.shadowPath = UIBezierPath(rect: self.table.bounds).cgPath
                        self.layoutIfNeeded()

        })
    }

    //MARK: Actions Methods
    public func didSelect(completion: @escaping (_ selectedText: String, _ index: Int , _ id:Int ) -> ()) {
        didSelectCompletion = completion
    }

    public func listWillAppear(completion: @escaping () -> ()) {
        TableWillAppearCompletion = completion
    }

    public func listDidAppear(completion: @escaping () -> ()) {
        TableDidAppearCompletion = completion
    }

    public func listWillDisappear(completion: @escaping () -> ()) {
        TableWillDisappearCompletion = completion
    }

    public func listDidDisappear(completion: @escaping () -> ()) {
        TableDidDisappearCompletion = completion
    }
    
    public func setCustomTouchAction(completion: @escaping () -> ()) {
        isCustomTouchEnabled = true
        CustomTouchAction = completion
    }
    
    public func didMultiSelect(_ completion: @escaping (_ selectedTexts: [String], _ indexes: [Int], _ ids: [Int], _ isCanceled: Bool) -> ()) {
        didMultiSelectCompletion = completion
    }
}

//MARK: UITextFieldDelegate
extension DropDown : UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        superview?.endEditing(true)
        return false
    }
    public func  textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        //self.selectedIndex = nil
        self.dataArray = self.optionArray
        touchAction()
    }
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return isSearchEnable
    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string != "" {
            self.searchText = self.text! + string
        }else{
            let subText = self.text?.dropLast()
            self.searchText = String(subText!)
        }
        if !isSelected {
            showList()
        }
        return true;
    }

}
///MARK: UITableViewDataSource
extension DropDown: UITableViewDataSource {

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "DropDownCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
            cell!.separatorInset = UIEdgeInsets(top: 2.5, left: 10, bottom: 2.5, right: 10)
        }
        if isMultipleSelectionEnabled && arrSelectedEntities != nil {
            if (!arrSelectedEntities.1.contains(indexPath.row)) {
                cell!.backgroundColor = rowBackgroundColor
                cell!.textLabel?.textColor = .ourAppThemeColor
            } else {
                cell?.backgroundColor = selectedRowColor
                cell!.textLabel?.textColor = selectedTextColor
            }
        } else {
            if (indexPath.row != selectedIndex) {
                cell!.backgroundColor = rowBackgroundColor
                cell!.textLabel?.textColor = .ourAppThemeColor
            }else {
                cell?.backgroundColor = selectedRowColor
                cell!.textLabel?.textColor = selectedTextColor
            }
        }

        if self.imageArray.count > indexPath.row {
            cell!.imageView!.image = UIImage(named: imageArray[indexPath.row])
        }
        
        cell!.textLabel!.text = "\(dataArray[indexPath.row])"
        cell!.accessoryType = indexPath.row == selectedIndex && checkMarkEnabled  ? .checkmark : .none
        cell!.selectionStyle = .none
        cell?.textLabel?.minimumScaleFactor = 0.75
        cell?.textLabel?.font = self.font
        cell?.textLabel?.textAlignment = self.textAlignment
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.lineBreakMode = .byTruncatingMiddle
        cell?.textLabel?.adjustsFontSizeToFitWidth = true
        cell?.textLabel?.sizeToFit()
        return cell!
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return isMultipleSelectionEnabled ? 40 : 0
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if !isMultipleSelectionEnabled { return nil }
        else {
            let footerView = UIView()
            
            footerView.frame = CGRect(origin: .zero, size: CGSize(width: tableView.bounds.width, height: 40))
            footerView.backgroundColor = rowBackgroundColor.withAlphaComponent(0.875)
            
            let sepView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 0.5))
            sepView.backgroundColor = .systemGray
            footerView.addSubview(sepView)
            
            
            // Done Button
            footerDoneButton = UIButton()
            footerView.addSubview(footerDoneButton)
            footerDoneButton.layer.masksToBounds = false
            footerDoneButton.backgroundColor = .ourAppThemeColor
            
            footerDoneButton.translatesAutoresizingMaskIntoConstraints = false
            
            footerDoneButton.widthAnchor.constraint(equalTo: footerView.widthAnchor, multiplier: 0.3).isActive = true
            footerDoneButton.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: -10).isActive = true
            footerDoneButton.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 5).isActive = true
            footerDoneButton.bottomAnchor.constraint(equalTo: footerView.bottomAnchor, constant: -5).isActive = true
            
            footerDoneButton.titleLabel?.textColor = .white
            footerDoneButton.titleLabel?.changeFontStyle(style: .Bold, fontSize: getDynamicHeight(size: 18))
            footerDoneButton.titleLabel?.minimumScaleFactor = 0.5
            footerDoneButton.setTitle("Done", for: .normal)
            footerDoneButton.layer.layoutIfNeeded()
            footerDoneButton.layer.borderWidth = 0.25
            footerDoneButton.layer.borderColor = UIColor.ourAppThemeColor.cgColor
            footerDoneButton.layer.applyShadowAndRadius()
            footerDoneButton.addTarget(self, action: #selector(done_MultiSelect_Action(_:)), for: .touchUpInside)
            
            
            // Cancel Button
            footerCancelButton = UIButton()
            footerView.addSubview(footerCancelButton)
            footerCancelButton.layer.masksToBounds = false
            footerCancelButton.backgroundColor = .gray
            
            footerCancelButton.translatesAutoresizingMaskIntoConstraints = false
            
            footerCancelButton.widthAnchor.constraint(equalTo: footerView.widthAnchor, multiplier: 0.35).isActive = true
            footerCancelButton.trailingAnchor.constraint(equalTo: footerDoneButton.leadingAnchor, constant: -10).isActive = true
            footerCancelButton.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 5).isActive = true
            footerCancelButton.bottomAnchor.constraint(equalTo: footerView.bottomAnchor, constant: -5).isActive = true
            
            footerCancelButton.titleLabel?.textColor = .ourAppThemeColor
            footerCancelButton.titleLabel?.changeFontStyle(style: .Bold, fontSize: getDynamicHeight(size: 18))
            footerCancelButton.titleLabel?.minimumScaleFactor = 0.5
            footerCancelButton.setTitle("Cancel", for: .normal)
            footerCancelButton.layer.layoutIfNeeded()
            footerCancelButton.layer.borderWidth = 0.25
            footerCancelButton.layer.borderColor = UIColor.systemBackground.cgColor
            footerCancelButton.addTarget(self, action: #selector(cancel_MultiSelection_Action(_:)), for: .touchUpInside)
            footerCancelButton.layer.applyShadowAndRadius()
            
            return footerView
        }
    }
    
    @objc private func cancel_MultiSelection_Action(_ sender: UIButton) {
        debugPrint("Cancel Multiple Selection..")
        if arrSelectedEntities != nil {
            arrSelectedEntities.0.removeAll()
            arrSelectedEntities.1.removeAll()
            arrSelectedEntities.2.removeAll()
        }
        self.text = dataArray[0]
        selectedIndex = 0
        didMultiSelectCompletion([dataArray[0]], [0], [0], true)
        touchAction()
        self.endEditing(true)
    }
    
    @objc private func done_MultiSelect_Action(_ sender: UIButton) {
        if arrSelectedEntities == nil || arrSelectedEntities.0.count == 0 {
            appView.makeToast("Please Select Atleast One Option!")
        } else {
            didMultiSelectCompletion(arrSelectedEntities.0, arrSelectedEntities.1, arrSelectedEntities.2, false)
            touchAction()
        }
        self.endEditing(true)
    }
}
//MARK: UITableViewDelegate
extension DropDown: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = (indexPath as NSIndexPath).row
        let selectedText = self.dataArray[self.selectedIndex!]
        tableView.cellForRow(at: indexPath)?.alpha = 0
        UIView.animate(withDuration: 0.25,
                       animations: { () -> Void in
            tableView.cellForRow(at: indexPath)?.alpha = 1.0
            tableView.cellForRow(at: indexPath)?.backgroundColor = self.selectedRowColor
        } ,
                       completion: { (didFinish) -> Void in
            if self.isMultipleSelectionEnabled && self.arrSelectedEntities != nil {
                self.text = self.arrSelectedEntities.0.joined(separator: ", ")
            } else {
                self.text = "\(selectedText)"
            }
            
            tableView.reloadData()
        })
        if hideOptionsWhenSelect {
            touchAction()
            self.endEditing(true)
        }
        if let selected = optionArray.firstIndex(where: {$0 == selectedText}) {
            if isMultipleSelectionEnabled && indexPath.row != 0 {   // First Row will be placeholder like "Select Material or Select Vendro" etc.
                var currID: Int = 0
                if let id = optionIds?[selected-1] {
                    currID = id
                }
                HapticFeedbackGenerator.simpleFeedback(type: .light)    // Custom Class Implemented , can be removed for general use
                if arrSelectedEntities == nil {
                    arrSelectedEntities = ([selectedText], [selected], [currID])
                } else {
                    if arrSelectedEntities.0.contains(selectedText) && arrSelectedEntities.1.contains(indexPath.row) {   //Already Selected
                        arrSelectedEntities.0 = arrSelectedEntities.0.filter { $0 != selectedText }
                        arrSelectedEntities.1 = arrSelectedEntities.1.filter { $0 != indexPath.row }
                        arrSelectedEntities.2 = arrSelectedEntities.2.filter { $0 != currID }
                        if arrSelectedEntities.0.count == 0 {       // Deselected All
                            //self.text = dataArray[0]
                            selectedIndex = nil
                        }
                    } else {
                        arrSelectedEntities.0.append(selectedText)
                        arrSelectedEntities.1.append(indexPath.row)
                        arrSelectedEntities.2.append(currID)
                    }
                }
            } else {
                if let id = optionIds?[selected] {
                    didSelectCompletion(selectedText, selected , id )
                }else{
                    didSelectCompletion(selectedText, selected , 0)
                }
            }
        }
    }
}



//MARK: Arrow
enum Position {
    case left
    case down
    case right
    case up
}

class Arrow: UIView {
    let shapeLayer = CAShapeLayer()
    var arrowColor:UIColor = .black {
        didSet{
            shapeLayer.fillColor = arrowColor.cgColor
        }
    }
    
    var position: Position = .down {
        didSet{
            switch position {
            case .left:
                self.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
                break

            case .down:
                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi*2)
                break

            case .right:
                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
                break

            case .up:
                self.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                break
            }
        }
    }

    init(origin: CGPoint, size: CGFloat ) {
        super.init(frame: CGRect(x: origin.x, y: origin.y, width: size, height: size))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {

        // Get size
        let size = self.layer.frame.width

        // Create path
        let bezierPath = UIBezierPath()

        // Draw points
        let qSize = size/4

        bezierPath.move(to: CGPoint(x: 0, y: qSize))
        bezierPath.addLine(to: CGPoint(x: size, y: qSize))
        bezierPath.addLine(to: CGPoint(x: size/2, y: qSize*3))
        bezierPath.addLine(to: CGPoint(x: 0, y: qSize))
        bezierPath.close()

        // Mask to path
        shapeLayer.path = bezierPath.cgPath
      //  shapeLayer.fillColor = arrowColor.cgColor
       
        if #available(iOS 12.0, *) {
            self.layer.addSublayer (shapeLayer)
        } else {
            self.layer.mask = shapeLayer
        }
    }
}

extension UIView {

    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowRadius = 2
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func viewBorder(borderColor : UIColor, borderWidth : CGFloat?) {
        self.layer.borderColor = borderColor.cgColor
        if let borderWidth_ = borderWidth {
            self.layer.borderWidth = borderWidth_
        } else {
            self.layer.borderWidth = 1.0
        }
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
