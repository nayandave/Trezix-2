//
//  Typing_Loader_View.swift
//  Trezix-2
//
//  Created by Amar Panchal on 10/04/23.
//

import UIKit


func showAnimatingDotsInImageView(superView: UIView) {
    let lay = CAReplicatorLayer()
    lay.name = "TypingLoader_Layer"
    lay.frame = CGRect(x: 10, y: 10, width: 60, height: 30) //yPos == 12
    let circle = CALayer()
    circle.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
    circle.cornerRadius = circle.frame.width / 2
    circle.backgroundColor = UIColor.ourAppThemeColor.cgColor //UIColor(red: 110/255.0, green: 110/255.0, blue: 110/255.0, alpha: 1).cgColor//lightGray.cgColor //UIColor.black.cgColor
    circle.name = "TypingLoader_Circle"
    lay.addSublayer(circle)
    lay.instanceCount = 3
    lay.instanceTransform = CATransform3DMakeTranslation(25, 0, 0)
    let anim = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
    anim.fromValue = 1.0
    anim.toValue = 0.2
    anim.duration = 1
    anim.repeatCount = .infinity
    circle.add(anim, forKey: "Circular_Dot_Animation")
    lay.instanceDelay = anim.duration / Double(lay.instanceCount)
    
    superView.layer.addSublayer(lay)
}

func removeDotAnimation(superView: UIView) {
    guard let subLayers = superView.layer.sublayers else { debugPrint("No Sublayers"); return }
    subLayers.filter { $0.name == "TypingLoader_Layer" }.forEach { layer in
        layer.removeAllAnimations()
        layer.removeFromSuperlayer()
    }
    
//    if let dotLayer = subLayers.filter({ $0.name == "TypingLoaderAnimation" }).first {
//        superView.layer.removeAllAnimations()
//        dotLayer.removeAllAnimations()
//        dotLayer.removeFromSuperlayer()
//    }
}
