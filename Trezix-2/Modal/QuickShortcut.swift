//
//  QuickShortcut.swift
//  Trezix-2
//
//  Created by Amar Panchal on 25/08/22.
//

import UIKit

/// `Array of all Quick Actions Shortcuts`
public let AllQuickShortcuts : [QuickShortcut] = [//QuickShortcut(title: "Import Dashboard", type: .importDashboard, icon: .home),
                                                  QuickShortcut(title: "Reports", type: .report, icon: .bookmark),
                                                  QuickShortcut(title: "Ask EximGTP", type: .eximGPT, icon: .search),
                                                  //QuickShortcut(title: "Shipment Report", type: .shipmentReport, icon: .bookmark),
                                                  QuickShortcut(title: "New Notifications", type: .notification, icon: .mail),
                                                  QuickShortcut(title: "My Approvals", type: .approval, icon: .confirmation),
                                                  QuickShortcut(title: "Track My Vessel", type: .vesselTrack, icon: .location)]

/// `Types of all Quick Action Shortcuts that we use`
/// * It's used to verify which Quick Action is triggered
public enum AllQuickShortcutTypes: String {
    case report = "All Report"
    case notification = "All Notifications"
    case eximGPT = "EximGPT"
    case vesselTrack = "Vessel Tracking"
    case approval = "Approval"
}

/// `Quick Action Shortcut Structure`
public struct QuickShortcut {
    public let title: String
    public let type: AllQuickShortcutTypes
    public let subTitle: String?
    public let icon: UIApplicationShortcutIcon.IconType
    
    init(title: String, type: AllQuickShortcutTypes, subTitle: String? = nil, icon: UIApplicationShortcutIcon.IconType) {
        self.title = title
        self.type = type
        self.subTitle = subTitle
        self.icon = icon
    }
}

/// `Adds all quick action shortcuts for our app`
///  * It's Called when user successfully logs in to our app, only then he/she should be able to see All Quick Actions
public func Add_Quick_Shortcuts() {
    let application = UIApplication.shared
    if application.shortcutItems == nil {
        var arrShortcuts = [UIApplicationShortcutItem]()
        for shortcut in AllQuickShortcuts {
            arrShortcuts.append(UIApplicationShortcutItem(type: shortcut.type.rawValue,
                                                          localizedTitle: shortcut.title,
                                                          localizedSubtitle: shortcut.subTitle,
                                                          icon: UIApplicationShortcutIcon(type: shortcut.icon)))
        }
        application.shortcutItems = arrShortcuts
    }
}

/// `Removes All Quick Action Shortcuts`
///  * Called when user is logging out
public func Remove_All_Quick_Shortcuts() {
    UIApplication.shared.shortcutItems = nil
}
