//
//  Utils.swift
//  Savapp
//
//  Created by Amar Panchal on 02/11/20.
//  Copyright © 2020 Amar Panchal. All rights reserved.
//

import Foundation

import UIKit
//import MBProgressHUD
import Photos

// MARK: -------- App Constants --------
let APPNAME = "Trezix"
let userDefault = UserDefaults.standard

let allReportPath = mainFilePath.appendingPathComponent("Reports")
let poTimelineReportPath = mainFilePath.appendingPathComponent("PO Timeline Report")
let mainFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
let tempFolderPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0].appendingPathComponent("Trezix_Cache")

let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

let loginStoryboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
let loginNavIdentifier = "LoginPageNavController"

let homeStoryboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
let homeNavIdentifier = "HomePageNavController"

let allReportStoryboard = UIStoryboard(name: "AllReportStoryboard", bundle: nil)

let approvalStoryboard = UIStoryboard(name: "ApprovalPageStoryboard", bundle: nil)

let documentStoryboard = UIStoryboard(name: "Document_Upload_Storyboard", bundle: nil)

let eximStoryboard = UIStoryboard(name: "EximGPTStoryboard", bundle: nil)

let vesselStoryboard = UIStoryboard(name: "Vessel_Tracking_Storyboard", bundle: nil)

let quotationStoryboard = UIStoryboard(name: "QuotationStoryboard", bundle: nil)

let noInternetMsg = "No Internet Connection Found.\nCheck your Internet Connection or Try Again"

let SCREEN_WIDTH = UIScreen.main.bounds.width
let SCREEN_HEIGHT = UIScreen.main.bounds.height

let SCREEN_WIDTH_DESIGN = 428
let SCREEN_HEIGHT_DESIGN = 926

let isiPhone =  UIDevice.current.userInterfaceIdiom.rawValue == 0 ? true : false

/// `Global App-Delegate Variable`
/// * To get access of `AppDelegate Class` anywhere in the project
let appDelegate = UIApplication.shared.delegate as! AppDelegate

let sceneDelegate = UIApplication.shared.connectedScenes.first!.delegate as! SceneDelegate

///**Global AppDelegate Window to use as UIView**
let appView = sceneDelegate.window!


/// **Current View-Controller From AppDelegate**
var appViewController: UIViewController? {
    if let vc = appView.rootViewController { return vc }
    else if let vc2 = sceneDelegate.window?.rootViewController { return vc2 }
    else { debugPrint("AppViewController is nil!"); return nil }
}

///**Unique Device ID that does not change even after reinstalling app**
let unique_device_id = UIDevice.current.identifierForVendor!.uuidString

///`Device Name`
let device_name = UIDevice.current.name

/// `Used as Key which has Integer Value of selected row from SideMenu ViewController`
let sideMenuSelectionIdentifier = "selectedOption"

let customModuleTblCellHeight = 40

let KC_AppToken_Key = "AppToken"
let KC_UserID_Key = "UserID"

//MARK:- Font

public let AppFontRobotoName = "Roboto"

public enum AppRobotoFont: String {
    case Regular = "-Regular"
    case Medium = "-Medium"
    case Bold = "-Bold"
    case Italic = "-Italic"
    case Light = "-Light"
    case Thin = "-Thin"
    case Black = "-Black"
    
    func Name() -> String {
        let finalName = AppFontRobotoName + self.rawValue
        return finalName
    }
}

public let AppFontCenturyName = "CenturyGothic"
public enum AppCenturyFont: String {
    case Regular
    case Bold = "-Bold"
    case Italic = "-Italic"
    case BoldItalic = "-BoldItalic"
    
    func Name() -> String {
        switch self {
        case .Regular:
            return AppFontCenturyName
        case .Bold, .BoldItalic, .Italic:
            return AppFontCenturyName + self.rawValue
        }
    }
}

public enum FontStyle {
    case Bold
    case Regular
    case Medium
    case Thin
    case Light
}

let Font_Regular = "Montserrat-Regular"
let Font_Bold = "Montserrat-Bold"
let Font_Medium = "Montserrat-Medium"
let Font_Semibold = "Montserrat-SemiBold"
let Font_Light = "Montserrat-Light"

//MARK:- Color
var color_blue = UIColor(red: 10/255, green: 29/255, blue: 93/255, alpha: 1)

var color_Gray_seperator = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)


// MARK: - Design related common methods

enum TypesOfHeight {
    case bottomNotch
    case topNotch
    case safeAreaHeight
    case safeAreaWidth
}

func getEdgeNotchHeight(of: TypesOfHeight) -> CGFloat {
    switch of {
    case .bottomNotch:
        return appView.safeAreaInsets.bottom
    case .topNotch:
        return appView.safeAreaInsets.top
    case .safeAreaHeight:
        return appView.safeAreaLayoutGuide.layoutFrame.height
    case .safeAreaWidth:
        return appView.safeAreaLayoutGuide.layoutFrame.width
    }
}



//func setRoundedCornerRadius(_ view: UIView) {
//    view.layer.cornerRadius = view.frame.size.height/2
//    view.clipsToBounds = true
//    //    view.layer.masksToBounds = true
//}
//func setRoundedCornerRadiusWithBorder(_ view: UIView,borderWidth:CGFloat, color:UIColor) {
//    view.layer.cornerRadius = view.frame.size.height/2
//    view.layer.borderColor = color.cgColor
//    view.layer.borderWidth = borderWidth
//    view.clipsToBounds = true
//    view.layer.masksToBounds = true
//    //    view.layer.masksToBounds = true
//}
//func setRoundedCornerRadiusWithBorderGray(_ view: UIView,borderWidth:CGFloat) {
//    view.layer.cornerRadius = view.frame.size.height/2
//    view.layer.borderColor = color_Gray_seperator.cgColor
//    view.layer.borderWidth = borderWidth
//    view.clipsToBounds = true
//    view.layer.masksToBounds = true
//    //    view.layer.masksToBounds = true
//}
//
//func setCornerRadius(_ view: UIView, cornerRadius:CGFloat) {
//    view.layer.cornerRadius = cornerRadius
//    view.clipsToBounds = true
//}
//
//func setBorderToView(_ view: UIView, borderWidth:CGFloat, cornerRadius:CGFloat, color:UIColor) {
//    view.layer.cornerRadius = cornerRadius
//    view.layer.borderColor = color.cgColor
//    view.layer.borderWidth = borderWidth
//    view.clipsToBounds = true
//    view.layer.masksToBounds = true
//}

func getDynamicHeight(size:CGFloat) -> CGFloat {
    return (SCREEN_HEIGHT * size)/CGFloat(SCREEN_HEIGHT_DESIGN)
}

func getDynamicWidth(size:CGFloat) -> CGFloat {
    return (SCREEN_WIDTH * size)/CGFloat(SCREEN_WIDTH_DESIGN)
}

func setSystemFontWithSize(_ label: UILabel, size: CGFloat, weight: UIFont.Weight) {
    let fontSize = getDynamicWidth(size: size)
    label.font = UIFont.systemFont(ofSize: fontSize, weight: weight)
}

func setRegularFontToLabel(_ label:UILabel, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    label.font = UIFont(name: Font_Regular, size: fontSize)
}

func setBoldFontToLabel(_ label:UILabel, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    label.font = UIFont(name: Font_Bold, size: fontSize)
}

func setLightFontToLabel(_ label:UILabel, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    label.font = UIFont(name: Font_Light, size: fontSize)
}

func setMediumFontToLabel(_ label:UILabel, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    label.font = UIFont(name: Font_Medium, size: fontSize)
}
func setSemiboldFontToLabel(_ label:UILabel, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    label.font = UIFont(name: Font_Semibold, size: fontSize)
}


func setRegularFontToTextField(_ txtfeild:UITextField, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Regular, size: fontSize)
}

func setBoldFontToTextField(_ txtfeild:UITextField, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Bold, size: fontSize)
}

func setLightFontToTextField(_ txtfeild:UITextField, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Light, size: fontSize)
}

func setMediumFontToTextField(_ txtfeild:UITextField, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Medium, size: fontSize)
}

func setSemiboldFontToTextField(_ txtfeild:UITextField, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Semibold, size: fontSize)
}

func setRegularFontToButton(_ btn:UIButton, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    btn.titleLabel?.font = UIFont(name: Font_Regular, size: fontSize)
}
func setBoldFontToButton(_ btn:UIButton, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    btn.titleLabel?.font = UIFont(name: Font_Bold, size: fontSize)
}
func setLightFontToButton(_ btn:UIButton, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    btn.titleLabel?.font = UIFont(name: Font_Light, size: fontSize)
}
func setMediumFontToButton(_ btn:UIButton, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    btn.titleLabel?.font = UIFont(name: Font_Medium, size: fontSize)
}
func setSemiboldFontToButton(_ btn:UIButton, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    btn.titleLabel?.font = UIFont(name: Font_Semibold, size: fontSize)
}

func changePlaceholderColorOfTextfiled(_ textfield:UITextField, color:UIColor) {
    let iVar = class_getInstanceVariable(UITextField.self, "_placeholderLabel")!
    let placeholderLabel = object_getIvar(textfield, iVar) as! UILabel
    placeholderLabel.textColor = color.withAlphaComponent(0.6)
}


func setRegularFontToUITextView(_ txtfeild:UITextView, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Regular, size: fontSize)
}
func setBoldFontToUITextView(_ txtfeild:UITextView, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Bold, size: fontSize)
}
func setLightFontToUITextView(_ txtfeild:UITextView, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Light, size: fontSize)
}
func setMediumFontToUITextView(_ txtfeild:UITextView, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Medium, size: fontSize)
}
func setSemiboldFontToUITextView(_ txtfeild:UITextView, size:CGFloat) {
    let fontSize = getDynamicWidth(size: size)
    txtfeild.font = UIFont(name: Font_Semibold, size: fontSize)
}

func roundCorners(view:UIView, corners: UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    view.layer.mask = mask
}

//MARK:- alert with handler
func showAlertWithCallback(_ title: String?, message: String?, isWithCancel: Bool, handler: (() -> Void)? = nil) {
    if UIApplication.topViewController() != nil {
        if (UIApplication.topViewController()!.isKind(of: UIAlertController.self)) {
            return
        }
    }
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    if isWithCancel {
        alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
    }
    
    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
        handler?()
    }))
    
   // alertController.view.tintColor = UIColor.red
    UIApplication.currentViewController()?.present(alertController, animated: true, completion:nil)
}

func openAlert(msg:String, viewcontroller:UIViewController) {
    let alert = UIAlertController(title: APPNAME, message: msg, preferredStyle: UIAlertController.Style.alert)
    
    // add an action (button)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
    
    // show the alert
    viewcontroller.present(alert, animated: true, completion: nil)
}

func presentAlertWithTitle(title: String,viewcontroller:UIViewController, message: String, options: String..., completion: @escaping (Int) -> Void) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    for (index, option) in options.enumerated() {
        alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
            completion(index)
        }))
    }
    viewcontroller.present(alertController, animated: true, completion: nil)
}

func isValidMovileNumberWithCode(mobile:String) -> Bool {
    let PHONE_REGEX = "^([0]|\\+91|91|091)?[6-9][0-9]{9}$";
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    let result =  phoneTest.evaluate(with: mobile)
    print("valida number or not \(result)")
    
    return result
    
}
func removeWhiteSpaceFromString(strString:String?) -> String {
    if (strString != nil)
    {
        let trimmedString = strString!.trimmingCharacters(in: .whitespaces)
        return trimmedString
    }
    return ""
}

func generateRandomReqID() -> String {
    let currDate = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "ddMMyyHHmmss"
    let finalStr = formatter.string(from: currDate) + "\(Int.random(in: 1...10000))"
    return finalStr
}

//func showProgressHub(view:UIView)
//{
//    let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
//    loadingNotification.mode = MBProgressHUDMode.indeterminate
//    //    loadingNotification.label.text = "Loading"
//}

//func hideProgressHub(view:UIView)
//{
//    MBProgressHUD.hide(for: view, animated: true)// hideAllHUDs(for: view, animated: true)
//}


// MARK: - Notification
public let notification_Category = "Notification_Category"

public var all_Gen_Noti_Actions: [UNNotificationAction] {
    return [UNNotificationAction(identifier: "MARK_AS_READ",
                                 title: "Mark As Read",
                                 options: .authenticationRequired),
            UNNotificationAction(identifier: "REMIND_LATER",
                                 title: "Remind in 1 hour",
                                 options: .foreground),
            UNNotificationAction(identifier: "DISMISS",
                                 title: "Dismiss",
                                 options: .destructive)]
}

func scheduleNotification(title: String = "New Message",
                          message: String = "Scheduled Notification",
                          data: [String:Any]? = nil) {
    
    let content = UNMutableNotificationContent()
    let userActions = notification_Category
    let notificationCenter = UNUserNotificationCenter.current()
    
    let count = get_BadgeCount() + 1
    save_BadgeCount(count)
    
    content.title = title
    content.body = "Long long String as Subtitle, Long long String as Subtitle, Long long String as Subtitle."
    content.subtitle = message
//    content.sound = UNNotificationSound.default
    
    content.badge = NSNumber(value: count)
    content.categoryIdentifier = userActions
    
    
    if let data { content.userInfo = data }
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    let identifier = "Local Notification"
    let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
    
    notificationCenter.add(request) { (error) in
        if let error = error {
            print("Error \(error.localizedDescription)")
        }
    }
}

extension UIImageView {
    static func fromGif(frame: CGRect) -> UIImageView? {
        guard let path = Bundle.main.path(forResource: "download", ofType: "gif") else {
            print("Gif does not exist at that path")
            return nil
        }
        let url = URL(fileURLWithPath: path)
        guard let gifData = try? Data(contentsOf: url),
            let source =  CGImageSourceCreateWithData(gifData as CFData, nil) else { return nil }
        var images = [UIImage]()
        let imageCount = CGImageSourceGetCount(source)
        for i in 0 ..< imageCount {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(UIImage(cgImage: image))
            }
        }
        let gifImageView = UIImageView(frame: frame)
        gifImageView.animationImages = images
        return gifImageView
    }
}

extension UITableView {
    func indexPathForView(_ view: UIView) -> IndexPath? {
        let center = view.center
        let viewCenter = self.convert(center, from: view.superview)
        let indexPath = self.indexPathForRow(at: viewCenter)
        return indexPath
    }
}

extension UICollectionView {
    func indexCollectionPathForView(_ view: UIView) -> IndexPath? {
        let center = view.center
        let viewCenter = self.convert(center, from: view.superview)
        let indexPath = self.indexPathForItem(at: viewCenter)
        return indexPath
    }
}


extension UINavigationController {
    static public func navBarHeight() -> CGFloat {
        let nVc = UINavigationController(rootViewController: UIViewController(nibName: nil, bundle: nil))
        let navBarHeight = nVc.navigationBar.frame.size.height
        return navBarHeight
    }
}


extension UIImage {
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    
    func compress(to kb: Int, allowedMargin: CGFloat = 0.2) -> Data {
        let bytes = kb * 1024
        var compression: CGFloat = 1.0
        let step: CGFloat = 0.05
        var holderImage = self
        var complete = false
        while(!complete) {
            if let data = holderImage.jpegData(compressionQuality: 1.0) {
                let ratio = data.count / bytes
                if data.count < Int(CGFloat(bytes) * (1 + allowedMargin)) {
                    complete = true
                    return data
                } else {
                    let multiplier:CGFloat = CGFloat((ratio / 5) + 1)
                    compression -= (step * multiplier)
                }
            }
            
            guard let newImage = holderImage.resized(withPercentage: compression) else { break }
            holderImage = newImage
        }
        return Data()
    }
}

//MARK:- GET DOCUMENT DIRECTORY
func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    let documentsDirectory = paths[0]
    return documentsDirectory
}
func removeOldFileIfExist(strName:String) {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    if paths.count > 0 {
        let dirPath = paths[0]
        
        let filePath = dirPath.appendingPathComponent("\(strName)")
        if FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.removeItem(atPath: filePath.path)
                print("old data has been removed")
            } catch {
                print("an error during a removing")
            }
        }
    }
}

// MARK: - UserDefaults Storage
func set_Device_Token_UD(token:String) {
    userDefault.set(token, forKey: "token")
}
func get_Device_Token_UD() -> String {
    return userDefault.value(forKey: "token") as? String ?? ""
}

func user_Is_Logged_In(is_set:Bool) {
    userDefault.set(is_set, forKey: "is_login_UD")
}
func is_User_Logged_In() -> Bool {
    return userDefault.value(forKey: "is_login_UD") as? Bool ?? false
}

func save_BadgeCount(_ notiCount: Int) {
    userDefault.set(notiCount, forKey: "Application_Notification_Count")
}

func get_BadgeCount() -> Int {
    return userDefault.integer(forKey: "Application_Notification_Count")
}


//func setIsSignupUD(is_set:Bool) {
//    userDefault.set(is_set, forKey: "signup_UD")
//}
//func getIsSignupUD() -> Bool {
//    return userDefault.value(forKey: "signup_UD") as? Bool ?? false
//}


func saveUserData(userData: UserDataModal) {
    userDefault.set(try? PropertyListEncoder().encode(userData), forKey: "UserData")
}

func getUserData() -> UserDataModal? {
    if let data = userDefault.value(forKey: "UserData") as? Data {
        return try? PropertyListDecoder().decode(UserDataModal.self, from: data)
    }
    return nil
}

func save_Granted_Permissions() {
    let allStr = all_Permissions.map { $0.rawValue }
    userDefault.set(allStr, forKey: "GrantedPermission")
}

func get_Granted_Permissions() -> [All_Grantable_Permission] {
    guard let arrPermission = userDefault.object(forKey: "GrantedPermission") as? [String] else {
        return All_Grantable_Permission.allCases
    }
    
    return All_Grantable_Permission.allCases.filter { arrPermission.contains($0.rawValue) }
}

func saveUserDomain(domain: String?) {
    userDefault.set(domain, forKey: "savedUserDomain")
}

func getUserDomain() -> String? {
    userDefault.string(forKey: "savedUserDomain")
}
//
//
//func getAdminData() -> AdminData? {
//    if let data = userDefault.value(forKey: "AdminData") as? Data {
//        return try? PropertyListDecoder().decode(AdminData.self, from: data)
//    }
//    return nil
//}
//
//func getCurrentCustForBroker() -> UserData? {
//    if let data = userDefault.value(forKey: "CurrentCustomer") as? Data {
//        return try? PropertyListDecoder().decode(UserData.self, from: data)
//    }
//    return nil
//}


func ShadowOnView(view:UIView) {
    view.layer.cornerRadius = view.frame.height/2
    view.clipsToBounds = true
    view.layer.masksToBounds = false
    view.layer.shadowRadius = 0.2
    view.layer.shadowOpacity = 0.1
    view.layer.shadowOffset = CGSize(width: -1, height:0)
    view.layer.shadowColor = UIColor.black.cgColor

}

//func showAlertWithCallback(_ title: String?, message: String?, isWithCancel: Bool, handler: (() -> Void)? = nil) {
//    if UIApplication.topViewController() != nil {
//        if (UIApplication.topViewController()!.isKind(of: UIAlertController.self)) {
//            return
//        }
//    }
//    
//    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//    
//    if isWithCancel {
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
//    }
//    
//    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
//        handler?()
//    }))
//    
//   // alertController.view.tintColor = UIColor.red
//    
//    UIApplication.currentViewController()?.present(alertController, animated: true, completion:nil)
//}


extension String {
    /// `String is valid email address or not`
    /// - Returns `true` if valid mail address
    /// - Returns `false` if mail address is not valid
    var isValidEmail: Bool {
        let regularExpressionForEmail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let testEmail = NSPredicate(format:"SELF MATCHES %@", regularExpressionForEmail)
        return testEmail.evaluate(with: self)
    }
    /// `Boolean value`  that indicates if String is valid `Indian phone number or not`
    var isValidPhoneNumber: Bool {
        let PHONE_REGEX = "^([0]|\\+91|91|091)?[6-9][0-9]{9}$";
        let testPhoneNumber = NSPredicate(format:"SELF MATCHES %@", PHONE_REGEX)
        return testPhoneNumber.evaluate(with: self)
    }
}


extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.connectedScenes
        .compactMap { $0 as? UIWindowScene }
        .flatMap { $0.windows }
        .first(where: { $0.isKeyWindow })?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }

    /// `Returns current Top Most ViewController in hierarchy of Window.`
    class func topMostWindowController()->UIViewController? {
        var topController = UIApplication.shared.connectedScenes
            .compactMap { $0 as? UIWindowScene }
            .flatMap { $0.windows }
            .first(where: { $0.isKeyWindow })?.rootViewController
        
        while let presentedController = topController?.presentedViewController {
            topController = presentedController
        }
        
        return topController
    }
    
    // Returns the topViewController from stack of topMostWindowController (if in navigation).
    class func currentViewController()->UIViewController? {
        
        var currentViewController = UIApplication.topMostWindowController()
        
        while currentViewController != nil && currentViewController is UINavigationController && (currentViewController as! UINavigationController).topViewController != nil {
            currentViewController = (currentViewController as! UINavigationController).topViewController
        }
        
        return currentViewController
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
extension UIContentSizeCategoryAdjusting {
    /// Changes `FontWeight` and `FontSize` of `UILabel`, `UITextView` and `UITextField` to Default App Font `Century Gothic`
    /// - Parameter style: `FontStyle` enum value, default value is `.Regular`
    /// - Parameter fontSize: `CGFloat` font size value, default value is `16`
    func changeFontStyle(style: AppCenturyFont = .Regular, fontSize: CGFloat = 16) {
        var finalFont = UIFont(name: AppCenturyFont.Regular.Name(), size: getDynamicHeight(size: fontSize))
        switch style {
        case .Bold:
            finalFont = UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: fontSize))
        case .Regular:
            finalFont = UIFont(name: AppCenturyFont.Regular.Name(), size: getDynamicHeight(size: fontSize))
        case .Italic:
            finalFont = UIFont(name: AppCenturyFont.Italic.Name(), size: getDynamicHeight(size: fontSize))
        case .BoldItalic:
            finalFont = UIFont(name: AppCenturyFont.BoldItalic.Name(), size: getDynamicHeight(size: fontSize))
        }
        
        switch self {
        case let self as UILabel:
            self.font = finalFont
        case let self as UITextView:
            self.font = finalFont
        case let self as UITextField:
            self.font = finalFont
        default:
            break
        }
        adjustsFontForContentSizeCategory = true
    }
    
    
//    /// Changes `FontWeight` and `FontSize` of `UILabel`, `UITextView` and `UITextField` to `Roboto` Font
//    /// - Parameter style: `FontStyle` enum value, default value is `.Regular`
//    /// - Parameter fontSize: `CGFloat` font size value, default value is `16`
//    func changeFontStyle(style: FontStyle = .Regular, fontSize: CGFloat = 16) {
//        var finalFont = UIFont(name: AppRobotoFont.Regular.Name(), size: getDynamicHeight(size: fontSize))
//        switch style {
//        case .Bold:
//            finalFont = UIFont(name: AppRobotoFont.Bold.Name(), size: getDynamicHeight(size: fontSize))
//        case .Regular:
//            finalFont = UIFont(name: AppRobotoFont.Regular.Name(), size: getDynamicHeight(size: fontSize))
//        case .Medium:
//            finalFont = UIFont(name: AppRobotoFont.Medium.Name(), size: getDynamicHeight(size: fontSize))
//        case .Thin:
//            finalFont = UIFont(name: AppRobotoFont.Thin.Name(), size: getDynamicHeight(size: fontSize))
//        case .Light:
//            finalFont = UIFont(name: AppRobotoFont.Light.Name(), size: getDynamicHeight(size: fontSize))
//        }
//
//        switch self {
//        case let self as UILabel:
//            self.font = finalFont
//        case let self as UITextView:
//            self.font = finalFont
//        case let self as UITextField:
//            self.font = finalFont
//        default:
//            break
//        }
//        adjustsFontForContentSizeCategory = true
//    }
}

extension Optional<String> {
    /// `Custom Unwrapper which returns "N/A" if self is nil or empty`
    func non_NilEmpty(_ withoutNA: Bool = false) -> String {
        if self.isNilOrEmpty { return withoutNA ? "" : "N/A" }
        else { return self! }
    }
    
    var isNilOrEmpty: Bool {
        if self == nil || self == "" { return true }
        return false
    }
}
/// `Opens in iOS Files App`
/// - Parameter fileURL: Local path of that specific File
func openInFilesApp(_ fileURL: URL?) {
    guard let fileURL else { debugPrint("NIL URL"); return }
    let openPath = fileURL.absoluteString.replacingOccurrences(of: "file://", with: "shareddocuments://")
    let url = URL(string: openPath)!
    UIApplication.shared.open(url)
}
