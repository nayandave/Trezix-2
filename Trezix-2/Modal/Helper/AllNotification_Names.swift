//
//  AllNotification_Names.swift
//  Trezix-2
//
//  Created by Amar Panchal on 30/05/23.
//

import Foundation

extension Notification.Name {
    // Side Menu Notifications
    static let sideMenuSelectionNotification = Notification.Name("sideMenuSelection")
    static let reloadSideMenuSelectionNotification = Notification.Name("ReloadSideMenuSelection")
    static let select_Report_Notificaiton = Notification.Name("Select_Report_Section")
    
    
    // Import/Export Dashboard Notifications
    static let dismissingMonthPickerNotification = Notification.Name("Dismiss_MonthPicker_Notification")
    
    // Approval Refresh Notification
    static let refreshApprovalListNotification = Notification.Name("Refresh_Approval_List")
    
    // Document List Refresh Notification
    static let refreshDocumentListNotification = Notification.Name("Refresh_Document_List")
}
