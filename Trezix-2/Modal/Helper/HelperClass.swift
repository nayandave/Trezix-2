//
//  HelperClass.swift
//  HappyRide_Partner
//
//  Created by Amar Panchal on 18/10/21.
//

import UIKit
import Photos

extension CALayer {
    // MARK: --- CornerRadius & Shadow to UIView ---
    // TODO: - Needs a method to handle superview corner radius and shadow { subviews should be clipped },
    // Approach will be adding two container view
    // 1. with masksToBound `TRUE` for corner radius.
    // 2. with masksToBound `FALSE` for shadow purpose.
    // Refere: https://www.advancedswift.com/corners-borders-shadows/#:~:text=Solutions%20To%20Common%20Problems,-Shadows%20And%20Rounded&text=Shadows%20are%20drawn%20outside%20of,the%20shadow%20to%20be%20visible
    
    ///**Applies CornerRadius and Shadow to UIView Layer**
    ///*It Does not work with UIImage, CollectionView Cell and UITableView Cell*
    func applyShadowAndRadius(corners: UIRectCorner? = nil,
                              cornerRadius: CGFloat = 8,
                              opacity: Float = 0.5,
                              offset: CGSize = .zero,
                              shadowRadius: CGFloat = 2.5,
                              shadowColor: UIColor = UIColor(named: "ShadowLabelColor")!) {
        if let corners = corners {
            var arr: CACornerMask = []

            let allCorners: [UIRectCorner] = [.topLeft, .topRight, .bottomLeft, .bottomRight, .allCorners]

            for corn in allCorners {
                if(corners.contains(corn)){
                    switch corn {
                    case .topLeft:
                        arr.insert(.layerMinXMinYCorner)
                    case .topRight:
                        arr.insert(.layerMaxXMinYCorner)
                    case .bottomLeft:
                        arr.insert(.layerMinXMaxYCorner)
                    case .bottomRight:
                        arr.insert(.layerMaxXMaxYCorner)
                    case .allCorners:
                        arr.insert(.layerMinXMinYCorner)
                        arr.insert(.layerMaxXMinYCorner)
                        arr.insert(.layerMinXMaxYCorner)
                        arr.insert(.layerMaxXMaxYCorner)
                    default: break
                    }
                }
            }
            self.maskedCorners = arr
        }
        self.masksToBounds = false
        self.cornerRadius = cornerRadius
        self.shadowColor = shadowColor.cgColor
        self.shadowOffset = offset
        self.shadowOpacity = opacity
        self.shadowRadius = shadowRadius
        self.shadowPath = corners == nil ? UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath : UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners!, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        self.shouldRasterize = true
        self.rasterizationScale = UIScreen.main.scale
    }
    
    ///**Applies Shadow And CornerRadius to CollectionView Cell And TableView Cell**
    ///
    ///**USAGE EXAMPLE :**
    ///
    ///In Cell File, And In LayoutSubViews Method
    ///```
    ///override func layoutSubviews() {
    ///    super.layoutSubviews()
    ///    layer.shadowToCell(contentView: contentView)
    ///}
    ///```
    func shadowToCell(radius: CGFloat = 8,
                      shadowColor: UIColor = UIColor(named: "ShadowLabelColor")!,
                        shadowOffset: CGSize = .zero,
                      shadowRadius: CGFloat = 2.5,
                      shadowOpacity: Float = 0.65,
                        contentView: UIView? = nil) {
        if let conView = contentView {
            conView.layer.cornerRadius = radius
//            conView.layer.borderWidth = 1
//            conView.layer.borderColor = UIColor.clear.cgColor
            conView.layer.masksToBounds = true
        }

        self.shadowColor = shadowColor.cgColor
        self.shadowOffset = shadowOffset
        self.shadowRadius = shadowRadius
        self.shadowOpacity = shadowOpacity
        self.masksToBounds = false
        self.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius).cgPath
        self.cornerRadius = radius
    }
}


extension UIViewController {
    /// `Boolean value that indicates if current device is `**iPAD**` or not`
    /// - Returns `true if current device is IPad`
    /// - Returns `false if current device is not IPad`
    public var isRunningOnIPAD : Bool {
        return (self.traitCollection.horizontalSizeClass == .regular && self.traitCollection.verticalSizeClass == .regular)
    }
    
    // Animation to create paging animation (Turn to Next View)
    /// - Parameter fromView: Currently visible page View which will be hidden (ie pageNumber 2)
    /// - Parameter toView: Next Page View which will be visible (i.e. pageNumber 3)
    func turnNextPage(fromView: UIView, toView: UIView) {
        toView.transform = toView.transform.translatedBy(x: self.view.frame.width, y: 0)
        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 5, options: .transitionFlipFromLeft) {
            fromView.transform = fromView.transform.translatedBy(x: -self.view.frame.width, y: 0)
            toView.isHidden = false
            toView.transform = toView.transform.translatedBy(x: -self.view.frame.width, y: 0)
            self.view.layoutIfNeeded()
        } completion: { succ in
            fromView.isHidden = true
            fromView.transform = fromView.transform.translatedBy(x: self.view.frame.width, y: 0)
            self.view.layoutIfNeeded()
        }
    }
    
    // Animation to create paging animation (Turn to previous image)
    /// - Parameter fromView: Currently visible page View which will be hidden (ie pageNumber 3)
    /// - Parameter toView: Previous Page View which will be visible (i.e. pageNumber 2)
    func turnPreviousPage(fromView: UIView, toView: UIView) {
        toView.transform = toView.transform.translatedBy(x: -self.view.frame.width, y: 0)
        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 5, options: .transitionFlipFromLeft) {
            fromView.transform = fromView.transform.translatedBy(x: self.view.frame.width, y: 0)
            toView.isHidden = false
            toView.transform = toView.transform.translatedBy(x: self.view.frame.width, y: 0)
            self.view.layoutIfNeeded()
        } completion: { succ in
            fromView.isHidden = true
            fromView.transform = fromView.transform.translatedBy(x: -self.view.frame.width, y: 0)
            self.view.layoutIfNeeded()
        }
    }
    
    func showViewFromBottomAnimation(view: UIView) {
        view.transform = view.transform.translatedBy(x: 0, y: self.view.frame.height)
        view.isHidden = false
        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveLinear, animations: {
            view.transform = view.transform.translatedBy(x: 0, y: -self.view.frame.height)
        }, completion: nil)
    }
    
    func hideViewFromBottomAnimation(view: UIView) {
        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            view.transform = view.transform.translatedBy(x: 0, y: self.view.frame.height)
        }) { succ in
            view.isHidden = true
            view.transform = view.transform.translatedBy(x: 0, y: -self.view.frame.height)
        }
    }
}

///// **Hides BATabBar WIth Bottom Smooth Animation**
//func hideTabBarWithAnimation() {
//    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveLinear) {
//        mainTabBarController.tabBar?.transform = CGAffineTransform(translationX: 0, y: mainTabBarController.tabBar?.frame.height ?? 250)
//    } completion: { SUC in
//        mainTabBarController.tabBar?.isHidden = true
//    }
//}
//
///// **Shows BATabBar WIth Bottom Smooth Animation**
//func showTabBarWithAnimation() {
//    mainTabBarController.tabBar?.isHidden = false
//    UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.5, options: .curveLinear, animations: {
//        mainTabBarController.tabBar?.transform = CGAffineTransform(translationX: 0, y: 0)
//    }, completion: nil)
//}

///// **Creates Default Home, Vehicle, Driver Folders if it does not exist**
//func createDefaultFolders() {
//    if !FileManager.default.fileExists(atPath: homePath.path) {
//        do {
//            for path in [homePath, driverPath, vehiclePath] {
//                try FileManager.default.createDirectory(at: path, withIntermediateDirectories: true, attributes: nil)
//            }
//        } catch let erro as NSError {
//            debugPrint("Unable to create Default Folders \(erro.debugDescription)")
//        }
//    }
//}


func getSettingAlert(status: PHAuthorizationStatus? = nil, mediaAccess: AVAuthorizationStatus? = nil) {
    var err: String? = nil
    if let mediaAccess {
        switch mediaAccess {
        case .notDetermined: debugPrint("Not Determined Yet!!!!")
        case .restricted: err = "Restricted"
        case .denied: err = "Denied"
        case .authorized: debugPrint("Authorization Successful")
        @unknown default: debugPrint("Unknown Camera Authorization Status in Setting Alert!")
        }
        if err != nil {
            let alert = UIAlertController(title: "Oops", message: "You have \(err!) access of Camera\nYou can change it in Settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { act in
                if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive))
            DispatchQueue.main.async {
                appViewController?.present(alert, animated: true)
            }
        }
    } else if let status {
        switch status {
        case .notDetermined: debugPrint("Not Determined Yet")
        case .authorized: debugPrint("Authorization Successful")
        case .restricted: err = "Restricted"
        case .denied: err = "Denied"
        case .limited: err = "Limited"
        @unknown default: debugPrint("Unknown Authorization Status In Setting Alert")
        }
        if err != nil {
            let alert = UIAlertController(title: "Oops", message: "You have \(err!) access of Photo Library\nYou can change it in Settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { act in
                if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive))
            DispatchQueue.main.async {
                appViewController?.present(alert, animated: true)
            }
        }
    }
}

// MARK: ----- PhotoLibrary Extension For Authorization -----
public extension PHPhotoLibrary {
    static func handle_Authorization_CompletionHandler(mediaType: UIImagePickerController.SourceType = .photoLibrary,
                                                       succHandler: @escaping (() -> Void)) {
        if mediaType == .camera {
            if !UIImagePickerController.isSourceTypeAvailable(.camera) {
                getSettingAlert(mediaAccess: .notDetermined)
                return
            } else {
                let camStatus = AVCaptureDevice.authorizationStatus(for: .video)
                switch camStatus {
                case .notDetermined:
                    AVCaptureDevice.requestAccess(for: .video) { access in
                        guard access == true else { getSettingAlert(mediaAccess: .denied); return }
                        getSettingAlert(mediaAccess: .authorized)
                        return
                    }
                case .authorized:
                    DispatchQueue.main.async {
                        succHandler()
                        return
                    }
                case .restricted: getSettingAlert(mediaAccess: .restricted); return
                case .denied: getSettingAlert(mediaAccess: .denied); return
                @unknown default: debugPrint("Unknown Camera Authorization Status"); return
                }
            }
        }
        var status: PHAuthorizationStatus
        if #available(iOS 14, *) {
            status = self.authorizationStatus(for: .readWrite)
            if status == .limited {
                getSettingAlert(status: .limited)
                return
            }
        } else {
            status = self.authorizationStatus()
        }
        switch status {
        case .notDetermined:
            if #available(iOS 14, *) {
                self.requestAuthorization(for: .readWrite) { status in
                    switch status {
                    case .notDetermined:
                        debugPrint("2 times not determined")
                    case .restricted: getSettingAlert(status: .restricted)
                    case .denied: getSettingAlert(status: .denied)
                    case .authorized:
                        debugPrint("Selected Authorization in ios >= 14")
                        DispatchQueue.main.async {
                            succHandler()
                        }
                    case .limited: getSettingAlert(status: .limited)
                    @unknown default:
                        break
                    }
                }
            } else {
                self.requestAuthorization { status in
                    switch status {
                    case .denied, .restricted: getSettingAlert(status: .denied)
                    case .authorized :
                        debugPrint("Selcted Authorization in ios < 14")
                        succHandler()
                    default: break
                    }
                }
            }
        case .restricted, .denied: getSettingAlert(status: .restricted)
        case .authorized:
            debugPrint("Authorized Photo Library")
            succHandler()
        case .limited:
            debugPrint("Limited Functionality !!!")
        @unknown default: debugPrint("Unknown Default in Checking Authorization")
        }
    }
}

extension URL {
    func gettingPath() -> String {
        if #available(iOS 16, *) { return self.path() } else { return self.path }
    }
}

extension UIViewController {
    enum CurrentSorting {
        case firstSecAscending
        case secondSecAscending
        case thirdSecAscending
        case firstSecDescending
        case secondSecDescending
        case thirdSecDescending
    }
}
