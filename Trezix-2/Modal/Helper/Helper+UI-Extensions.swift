//
//  Helper+UI-Extensions.swift
//  BATSONS
//
//  Created by Amar Panchal on 10/12/21.

import UIKit

extension UISegmentedControl {
    func ios13SelectionTint(selectedTintColor: UIColor) {
        if #available(iOS 13.0, *) {
            self.selectedSegmentTintColor = selectedTintColor
            self.backgroundColor = .white
            
            self.layer.borderColor = UIColor.white.cgColor
            self.layer.borderWidth = 1

            let selectedText = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 17))]
            self.setTitleTextAttributes(selectedText as [NSAttributedString.Key : Any], for: .selected)
            
            let unselectedText = [NSAttributedString.Key.foregroundColor : selectedTintColor, NSAttributedString.Key.font : UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 17))]
            self.setTitleTextAttributes(unselectedText as [NSAttributedString.Key : Any], for: .normal)
        } else {
            // Fallback on earlier versions
            self.tintColor = selectedTintColor
        }
    }
}

//Changed from UITextField to UIView to see if it works for Buttons and other components too. in OrderReportVC...
extension UIView {
    // MARK: --- ERROR SHAKE TO THE TEXTFIELD ---
    func errorShake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 5, y: self.center.y))
        
        self.layer.add(animation, forKey: "position")
    }
    
    enum DistanceType {
        case upperDistanceFromSafeArea
        case lowerDistanceFromSafeArea
    }
    
    func distanceFromSafeArea(distanceType: DistanceType) -> CGFloat {
        let view2 = appView.rootViewController?.view
        let frame = self.superview!.convert(view2!.safeAreaLayoutGuide.layoutFrame, from: view2!)
        
//        let realOrigin = self.superview!.convert(self.frame.origin, to: view2!)
//        debugPrint("BOTTOM = \((frame.maxY - self.bounds.maxY - self.frame.height - getEdgeNotchHeight(of: .bottomNotch)))")
        return distanceType == .upperDistanceFromSafeArea ? (self.bounds.minY-frame.minY) : (frame.maxY - (self.bounds.maxY/2) - getEdgeNotchHeight(of: .bottomNotch) - 10)
    }
}

//func distanceBetween(bottomOf view1: UIView, andTopOf view2: UIView) -> CGFloat {
//    let frame2 = view1.convert(view2.bounds, from: view2)
//    return frame2.minY - view1.bounds.maxY
//}

extension UIColor {
    // MARK: ----- Converts Hex code to UIColor -----
    static func hexStringToUIColor(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static var randomColor: UIColor {
        return UIColor(
            hue: .random(in: 0...1),
            saturation: .random(in: 0.5...1),
            brightness: .random(in: 0.5...1),
            alpha: 1.0)
        
//        return UIColor(
//            red: .random(in: 0...1),
//            green: .random(in: 0...1),
//            blue: .random(in: 0...1),
//            alpha: 1.0
//        )
    }
    
    ///**App Theme Default Color (Dark Green For Trezix)**
    static var ourAppThemeColor: UIColor {
        return UIColor(red: 0.031, green: 0.357, blue: 0.408, alpha: 1)
    }
    
    ///**App Theme Yellow Color**
    static var themeYellowColor: UIColor {
        return UIColor.hexStringToUIColor(hex: "FFB847")
    }
    
    ///**Lightens the given UIColor by passed percentage**
    /// - Parameter percentage: Float value by which percentage UIColor will be lighten
    /// - Returns Optional Lightened UIColor
    func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    ///**Darkens the given UIColor by passed percentage**
    /// - Parameter percentage: Float value by which percentage UIColor will be darken
    /// - Returns Optional Darkened UIColor
    func darker(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }

    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + percentage/100, 1.0),
                           green: min(green + percentage/100, 1.0),
                           blue: min(blue + percentage/100, 1.0),
                           alpha: alpha)
        } else {
            return nil
        }
    }
    
}


extension UIView {
    func show_With_Popup_Effect() {
        self.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        UIViewPropertyAnimator(duration: 0.3, curve: .easeInOut) { [weak self] in
            guard let self = self else { return }
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }.startAnimation()
    }
    
    func dismiss_With_Popup_Effect() {
        UIViewPropertyAnimator(duration: 0.3, curve: .easeInOut) { [weak self] in
            guard let self = self else { return }
            self.alpha = 0
            self.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        }.startAnimation()
    }
    
    
    func drawCurver() {
        let curveHeight : CGFloat = 50.0
        let curvedLayer = CAShapeLayer()
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        path.addArc(withCenter: CGPoint(x: CGFloat(self.frame.width) - curveHeight, y: self.frame.height), radius: curveHeight, startAngle: 0, endAngle: 1.5 * CGFloat.pi, clockwise: false)
        path.addLine(to: CGPoint(x: curveHeight, y: self.frame.height - curveHeight))
        path.addArc(withCenter: CGPoint(x: curveHeight, y: self.frame.height - (curveHeight * 2.0)), radius: curveHeight, startAngle: 0, endAngle:  CGFloat.pi, clockwise: true)
        
        path.close()
        
        curvedLayer.path = path.cgPath
        curvedLayer.fillColor = UIColor(red: 8/255, green: 95/255, blue: 189/255, alpha: 1.0).cgColor
        curvedLayer.frame = self.frame
        
        self.layer.insertSublayer(curvedLayer, at: 0)
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 10.0
        self.layer.shadowOpacity = 0.7
    }
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath

    mask.fillColor = UIColor.white.cgColor //3
    mask.shadowColor = UIColor.black.cgColor //4
    mask.shadowPath = path.cgPath
    mask.shadowOffset = CGSize(width: 2, height:2) //5
    mask.shadowOpacity = 0.1
    mask.shadowRadius = 0.2
    layer.mask = mask

    }
    
    /* Usage Example
     * bgView.addBottomRoundedEdge(desiredCurve: 1.5)
     */
      func addBottomRoundedEdge(desiredCurve: CGFloat?) {
          let offset: CGFloat = self.frame.width / desiredCurve!
          let bounds: CGRect = self.bounds
          
          let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
          let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
          let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset/2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
          let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
          rectPath.append(ovalPath)
          
          // Create the shape layer and set its path
          let maskLayer: CAShapeLayer = CAShapeLayer()
          maskLayer.frame = bounds
          maskLayer.path = rectPath.cgPath
          
          // Set the newly created shape layer as the mask for the view's layer
          self.layer.mask = maskLayer
      }
}


extension String {
    var convertedFloatValue: Double? {
        guard let num = NumberFormatter().number(from: self) else {
            debugPrint("Can't Convert String Value = \(self) to NumberFormat")
            return nil
        }
        let value = Double(truncating: num)
        return value
    }
}

/**
   - Overrides Every Label of App and Changing it's font size to dynamic height accordingly
   - AwakefromNib Method runs automatically
   - IBInspecatble method needs to set in storyboard and does not work automatically ( must set it's value to TRUE otherwise it won't even work with default value)
 */
extension UILabel {
    @IBInspectable var DynamicHeight: Bool {
        get {
            return true
        }
        set {
            if newValue {
                font = font.withSize(getDynamicHeight(size: font.pointSize))
            }
        }
    }
}

extension UIButton {
    @IBInspectable var DynamicHeight: Bool {
        get {
            return true
        }
        set {
            if newValue {
                self.titleLabel?.font = self.titleLabel?.font.withSize(getDynamicHeight(size: (self.titleLabel?.font.pointSize)!))
            }
        }
    }
}


extension UITextField {
    @IBInspectable var DynamicHeight: Bool {
        get {
            return true
        }
        set {
            if newValue {
                font = font!.withSize(getDynamicHeight(size: (font?.pointSize)!))
            }
        }
    }
}

extension NSLayoutConstraint {
    /// **IF TRUE, It calculates and sets the DYNAMIC HEIGHT to the Current Constraint**
    @IBInspectable open var DynamicHeight: Bool {
        get {
            return true
        }
        set {
            if newValue {
                constant = getDynamicHeight(size: constant)
                self.isActive = true
            }
        }
    }
    
    /// **IF TRUE, It calculates and sets the DYNAMIC WIDTH to the Current Constraint**
    @IBInspectable open var DynamicWidth: Bool {
        get {
            return true
        }
        set {
            if newValue {
                constant = getDynamicWidth(size: constant)
                self.isActive = true
            }
        }
    }
}

//protocol Animations {
//    func animateHidden(flag: Bool)
//}
//
//extension Animations {
//    func animateHidden(flag: Bool) {
//        // some code
//    }
//}
//
//extension UILabel: Animations {}
//
//extension UIImageView: Animations {}
//
//extension Animations where Self: UIView {
//    func animateHidden(flag: Bool) {
//        self.hidden = flag
//    }
//}

extension UIView {
//    static var action: ActivityReturnable? = nil
    struct ActivityReturnable {
        var indicator: UIActivityIndicatorView? = nil
        var image: UIImage? = nil
        var title: String? = nil
        var textFieldSpec: (Bool, UIView?, UITextField.ViewMode?) = (false, nil, nil)
    }
    
    func animateLoader(_ spinnerColor: UIColor = .white) {
        var param = ActivityReturnable()
        var superview: UIView!
        if let superV = self.superview {
            superview = superV
        } else {
            superview = self
        }
        
        guard let parentVC = superview.parentViewController else { return }
        
        parentVC.view.isUserInteractionEnabled = false  //Disabling extra touch when loader is in progress
        
        var loader: UIActivityIndicatorView! = nil
        if loader == nil {
            loader = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: self.bounds.height-5, height: self.bounds.height-5))
            loader.style = .medium
            loader.hidesWhenStopped = true
            loader.color = spinnerColor
            loader.tag = 5141
        }
        
        switch self {
        case let self as UIImageView:
            param.image = self.image
            DispatchQueue.main.async {
                self.image = nil
            }
        case let self as UIButton:
            param.image = self.currentImage
            param.title = self.currentTitle
            DispatchQueue.main.async {
                self.setTitle(nil, for: self.state)
                self.setImage(nil, for: self.state)
            }
        case let self as UILabel:
            param.title = self.text
            DispatchQueue.main.async {
                self.text?.removeAll()
            }
        case let self as UITextField:
            param.textFieldSpec = (true, self.rightView, self.rightViewMode)
            let color = UIColor.init { (trait) -> UIColor in
                return trait.userInterfaceStyle == .dark ? .white : .systemGray
            }
            loader.color = color
            DispatchQueue.main.async {
                debugPrint("LOADER WIDTH ------- \(loader.bounds.width) &&&& \(loader.frame.width)")
                let containerView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.height, height: self.bounds.height))
                containerView.tag = 5141
                containerView.backgroundColor = .clear
                containerView.addSubview(loader)
                loader.translatesAutoresizingMaskIntoConstraints = false
                loader.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
                loader.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
                loader.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 5).isActive = true
                loader.topAnchor.constraint(greaterThanOrEqualTo: containerView.topAnchor, constant: 0).isActive = true
                self.rightViewMode = .always
                self.rightView = containerView
            }
        default:
            debugPrint("Unknown Element")
            loader.color = .systemGray
        }
        UIView.animate(withDuration: 0.25, delay: 0, options: [.transitionCrossDissolve, .curveEaseInOut], animations: {
            if !param.textFieldSpec.0 {
                superview.addSubview(loader)
                loader.translatesAutoresizingMaskIntoConstraints = false
                loader.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
                loader.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
                loader.leadingAnchor.constraint(greaterThanOrEqualTo: self.leadingAnchor, constant: 2.5).isActive = true
                loader.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor, constant: 2.5).isActive = true
            }
            
            param.indicator = loader
            loader.startAnimating()
        }, completion: nil)
        saveActivityData(param)
    }
    
    func remove_Activity_Animator(completion: (() -> Void)? = nil) {
        guard let parentVC = self.parentViewController else { debugPrint("No ParentVC Found in Loader!"); return }
        
        parentVC.view.isUserInteractionEnabled = true   //Re-Enabling touch of VC, which was disabled for loader
        
        guard let action = getActivityData() else { debugPrint("No Action Found!"); return }
        guard let loader = action.indicator else { debugPrint("No Indicator Found!"); return }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25, delay: 0, options: [.transitionCrossDissolve, .curveEaseInOut]) {
                debugPrint("ANIMATING...")
                switch self {
                case let self as UITextField:
                    self.rightView = action.textFieldSpec.1
                    self.rightViewMode = action.textFieldSpec.2 ?? .never
                case let self as UIImageView:
                    self.image = action.image
                case let self as UIButton:
                    self.setTitle(action.title, for: self.state)
                    self.setImage(action.image, for: self.state)
                case let self as UILabel:
                    self.text = action.title
                default:
                    let loader = self.subviews.filter { $0.tag == 5141 }.first as? UIActivityIndicatorView
                    loader?.stopAnimating()
                    loader?.removeFromSuperview()
                }
                loader.stopAnimating()
            } completion: { _ in
                debugPrint("COMPLETE")
                completion?()
            }
        }
    }
    
    func show_Tap_Animation(_ completionBlock: @escaping () -> Void) {
        isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.05,
                       delay: 0,
                       options: .curveLinear,
                       animations: { [weak self] in
            self?.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        }) {  (done) in
            UIView.animate(withDuration: 0.05,
                           delay: 0,
                           options: .curveLinear,
                           animations: { [weak self] in
                self?.transform = .identity
            }) { [weak self] (_) in
                self?.isUserInteractionEnabled = true
                completionBlock()
            }
        }
    }
}

extension UIView {
    /// Inserts a `UIView` behind the UIView extension with shadow & corner radius
    /// - Parameter cornerRadius:`CGFloat` value for the corner radius, `Default: 8`
    public func add_ShadowView(cornerRadius: CGFloat = 8) {
        guard let superview = self.superview else { return }
        let isShadowViewExist = superview.subviews.first { $0.accessibilityIdentifier == String(describing: Unmanaged.passUnretained(self).toOpaque()) }
        if isShadowViewExist != nil { return }
        
        let shadowView = UIView()
        shadowView.backgroundColor = .systemBackground //.tertiarySystemBackground
        shadowView.clipsToBounds = false
        shadowView.accessibilityIdentifier = String(describing: Unmanaged.passUnretained(self).toOpaque())
        superview.insertSubview(shadowView, belowSubview: self)
        
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        shadowView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        shadowView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        shadowView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        superview.layoutIfNeeded()
        
        DispatchQueue.main.async {
            shadowView.layer.applyShadowAndRadius(cornerRadius: cornerRadius)
        }
    }
    
    /// Removes the `UIView` with shadow which is behind the UIView extension
    public func remove_ShadowView() {
        guard let superview = self.superview else { return }
        let isShadowViewExist = superview.subviews.first { $0.accessibilityIdentifier == String(describing: Unmanaged.passUnretained(self).toOpaque()) }
        guard let shadowView = isShadowViewExist else { return }
        shadowView.removeFromSuperview()
        superview.layoutIfNeeded()
    }
}

extension UIDatePicker {
    
    /// `Set Date Picker Style according to iOS 14`
    ///  * if iOS is greater than 14, then `preferredDatePickerStyle` is set to  `inline`
    ///  * if iOS is between 13.4 and 14, then `preferredDatePickerStyle` is set to  `wheels`
    func setPickerStyle_iOS14() {
        if #available(iOS 14, *) {
            self.preferredDatePickerStyle = .inline
        } else {
            if #available(iOS 13.4, *) {
                self.preferredDatePickerStyle = .wheels
            }
        }
    }
}

extension UITableView {
    /// when iOS 15+, sets value `0` default Section Top Padding and Blank FooterView
    /// - Parameter withBottomSafety: `TRUE` if want to include bottom safe area height + 10 and `FALSE` if including blank UIView
    func setSectionPadding_iOS_15(withBottomSafety: Bool = false) {
        if #available(iOS 15, *) {
            self.sectionHeaderTopPadding = 0
        }
        self.tableFooterView = withBottomSafety ? UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: getEdgeNotchHeight(of: .bottomNotch)+10)) : UIView()
    }
}

extension UIView {
    func add_Tap_Effect() {
        if let recs = self.gestureRecognizers,
           recs.firstIndex{ $0.name == "Tap Animation Effect" } != nil {
            
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapRecognizer(_:)))
        tapGesture.name = "Tap Animation Effect"
        self.addGestureRecognizer(tapGesture)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) { [weak self] in
            guard let self = self else { return }
            self.tapRecognizer(tapGesture)
        }
    }
    
    @objc private func tapRecognizer(_ sender: UITapGestureRecognizer) {
        if sender.state == .began {
            UIView.animate(withDuration: 0.05, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveLinear) { [weak self] in
                guard let self = self else { return }
                self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                self.layoutIfNeeded()
            } completion: { [weak self] _ in
                self?.layoutIfNeeded()
                self?.setNeedsDisplay()
            }
        } else if sender.state == .ended {
            UIView.animate(withDuration: 0.05, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveLinear) { [weak self] in
                guard let self = self else { return }
                self.transform = .identity
                self.layoutIfNeeded()
            } completion: { [weak self] _ in
                self?.layoutIfNeeded()
                self?.setNeedsDisplay()
            }
        }
    }
}
