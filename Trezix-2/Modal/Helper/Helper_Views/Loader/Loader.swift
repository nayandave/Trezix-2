//
//  Loader.swift
//  DemoWebervice
//
//  Created by MacMini on 03/01/18.
//  Copyright © 2018 Codiant. All rights reserved.
//

import UIKit

class Loader: UIView {

    static var loaderView : Loader?
    
    @IBOutlet weak  var imgView : UIImageView?
    @IBOutlet weak var viewBg : UIView?
    @IBOutlet weak var label: UILabel?
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var imageXCenter: NSLayoutConstraint!
    @IBOutlet weak var imageYCenter: NSLayoutConstraint!
    
    override func awakeFromNib() {
       // imgView?.image = imgView?.image!.withRenderingMode(.alwaysTemplate)
       // imgView?.tintColor = UIColor.blue
    }
    
    class func showLoader(_ title: String? = nil, on: UIView? = nil) {
        if loaderView != nil {
            hideLoader()
        }
        loaderView = UINib(nibName: "Loader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? Loader
        loaderView?.tag = 2000
//        UIApplication.shared.keyWindow?.addSubview(loaderView!)
//        if let on = on {
//            on.view.addSubview(loaderView!)
//        } else {
//            appView.addSubview(loaderView!)
//        }
        loaderView!.frame = UIScreen.main.bounds
        if title != nil {
            loaderView?.label?.text = title
        } else {
            loaderView?.label?.isHidden = true
        }
        
//        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 50, options: [.autoreverse, .repeat]) {
//            loaderView?.imgView?.image = UIImage(cgImage: loaderView!.imgView!.image!.cgImage!, scale: 1.0, orientation: .leftMirrored)
//        } completion: { _ in
//            print("COMPLETION ANIMATIONS")
//        }

        
//        UIView.animate(withDuration: 0.25, delay: 0, options: .curveLinear, animations: { () -> Void in
////            loaderView?.viewBg!.alpha = 0.5
//        }) { (finished) -> Void in
//        }
//        let rotation = CABasicAnimation(keyPath: "transform.rotation")
//        rotation.fromValue = UIImage(cgImage: loaderView!.imgView!.image!.cgImage!, scale: 1.0, orientation: .downMirrored)
////        rotation.fromValue = -Double.pi/2
////        rotation.toValue = Double.pi/2
//        rotation.toValue = UIImage(cgImage: loaderView!.imgView!.image!.cgImage!, scale: 1.0, orientation: .downMirrored)
//        rotation.autoreverses = true
//        rotation.duration = 1.25
//        rotation.repeatCount = Float.infinity
//        loaderView?.imgView!.layer.add(rotation, forKey: "Spin")
        
  // MARK: - Uncomment below lines for blinking scaling animation
        let scaleAnimation:CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 0.75
        scaleAnimation.repeatCount = .infinity
        scaleAnimation.autoreverses = true
        scaleAnimation.fromValue = 1.2;
        scaleAnimation.toValue = 0.8;
        loaderView?.imgView!.layer.add(scaleAnimation, forKey: "scale")
//        loaderView?.imgView?.alpha = 0.75
        if let on = on {
            on.addSubview(loaderView!)
            loaderView?.imgView?.translatesAutoresizingMaskIntoConstraints = false
            loaderView?.imageXCenter.isActive = false
            loaderView?.imageYCenter.isActive = false
            loaderView?.imgView?.centerXAnchor.constraint(equalTo: on.centerXAnchor).isActive = true
            loaderView?.imgView?.centerYAnchor.constraint(equalTo: on.centerYAnchor).isActive = true
//            NSLayoutConstraint(item: loaderView!.imgView!, attribute: .centerX, relatedBy: .equal, toItem: on, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
//            NSLayoutConstraint(item: loaderView!.imgView!, attribute: .centerY, relatedBy: .equal, toItem: on, attribute: .centerY, multiplier: 1, constant: -10).isActive = true
        } else {
            appView.addSubview(loaderView!)
        }
    }
    
    
    //TODO: - Check if progress conversion is working or change it to ineteger progress
    class func changeUploadProgress(progressText: String? = nil, progress: Progress) {
        DispatchQueue.main.async {
            let text = progressText == nil ? "Uploading" : progressText!
            loaderView?.label?.isHidden = false
            debugPrint("PRO - \(progress.fractionCompleted)")
            loaderView?.label?.text = "\(text)..\n\(String(format: "%.0f", progress.fractionCompleted*100)) %"
        }
    }
    
    
    class func hideLoader(on: UIView? = nil) {
        UIView.animate(withDuration: 0.0, animations: {
            //  loaderView?.viewBg!.alpha = 0.1
        }) { (completion) in
            if  loaderView != nil {
                if let on = on {
                    for subview : UIView in (on.subviews) {
                        if subview.tag == 2000 {
                            subview.removeFromSuperview()
                            loaderView = nil
                        }
                    }
                } else {
                    for subView in appView.subviews {
                        if subView == loaderView {
                            subView.removeFromSuperview()
                            loaderView = nil
                        }
                    }
                }
            }
        }
    }
}
