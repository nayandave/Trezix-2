//
//  Activity.swift
//  Trezix-2
//
//  Created by Amar Panchal on 04/07/22.
//

import UIKit

class Activity: UIView {
    
    static let shared = Activity()
    
    public var originalString: String?
    
    public var containerView: UIView?
    public var activityIndi: UIActivityIndicatorView?
    
    private let customTag = 9876543210
    
    private var lblSubText: UILabel! = nil
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    private func createIndicatior(superView: UIView, with subText: String?) -> UIView {
        // Initializing ContentView
        let contView = UIView()
        contView.translatesAutoresizingMaskIntoConstraints = false
        [contView.centerXAnchor.constraint(equalTo: superView.centerXAnchor),
         contView.centerYAnchor.constraint(equalTo: superView.centerYAnchor),
         contView.heightAnchor.constraint(lessThanOrEqualTo: superView.heightAnchor, multiplier: 0.5, constant: 2),
         contView.widthAnchor.constraint(lessThanOrEqualTo: superView.widthAnchor, multiplier: 0.5, constant: 2)].forEach { constraint in
            constraint.isActive = true
        }
        
        let activity = UIActivityIndicatorView()
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.style = .large
        contView.addSubview(activity)
        
        if subText != nil {
            lblSubText = UILabel()
            lblSubText.translatesAutoresizingMaskIntoConstraints = false
            lblSubText.text = subText!
            lblSubText.changeFontStyle(style: .Regular, fontSize: 17)
            lblSubText.DynamicHeight = true
        }
        NSLayoutConstraint(item: activity, attribute: .leading, relatedBy: .equal, toItem: contView, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: activity, attribute: .trailing, relatedBy: .equal, toItem: contView, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        return contView
    }
    
    public func showActivityIndicator(on: UIView, withSubtext: String?) {
        var showText: String?
        if on is UIButton {
            originalString = (on as! UIButton).title(for: .normal)
            showText = nil
        } else if on is UILabel {
            originalString = (on as! UILabel).text
            showText = nil
        } else {
            originalString = nil
            showText = withSubtext
        }
        containerView = createIndicatior(superView: on, with: showText)
        containerView?.tag = customTag
    }
    
    public func changeUploadingProgress(progress: Progress) {
        DispatchQueue.main.async { [weak self] in
            self?.lblSubText.text = "Uploading..\n\(String(format: "%.0f", progress.fractionCompleted*100))"
        }
    }
    
    public func hideActivity(on: UIView) {
        for subView in on.subviews {
            if subView == containerView || subView.tag == customTag {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 50, options: .curveEaseInOut, animations: {
                    subView.transform = CGAffineTransform(scaleX: 0, y: 0)
                }, completion: { _ in
                    subView.removeFromSuperview()
                    subView.transform = .identity
                })
            }
        }
    }
}
