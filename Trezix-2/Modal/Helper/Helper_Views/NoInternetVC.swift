//
//  NoInternetVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 26/08/22.
//

import UIKit
import PopupDialog

class NoInternetVC: UIViewController {
    // MARK: - ALL IB_OUTLETS
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var imageNoNet: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnTryAgain: UIButton!
    
    // MARK: - ALL CONSTANTS & VARIABLES
    public var popUp: PopupDialog? = nil
    public var message: String = "Internet is not available!"
    public var btnName: String = "Reconnect"
    
    // MARK: - VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        basicSetup()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let corner = sceneDelegate.window?.screen.displayCornerRadius ?? 40
            debugPrint("Corner Radius is \(corner)")
            self.mainContentView.layer.applyShadowAndRadius(cornerRadius: corner/1.5)
            self.btnTryAgain.layer.applyShadowAndRadius(cornerRadius: self.btnTryAgain.frame.height/2)
        }
    }
    
    // MARK: - ALL HELPER METHODS
    /// `Setting static or custom Message and/or TryAgain Button Title`
    private func basicSetup() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.imageNoNet.tintColor = .ourAppThemeColor
            self.lblMessage.text = self.message
            self.btnTryAgain.setTitle(self.btnName, for: .normal)
        }
    }
    
    // MARK: - IB_ACTION METHOD
    /// `Try Again Button Action which checks Network Availability`
    ///
    ///  If Network is available then it dismisses the presented popUp or shakes the popup
    @IBAction func Try_Network_Action(_ sender: UIButton) {
        debugPrint("Network refreshed!")
        if appDelegate.isNetworkAvailable {
            self.dismiss(animated: true)
        } else {
            popUp?.shake()
        }
    }
}

///`No Internet ViewController Manager Singleton Class`
class NoInternet {
    ///`Static constant which is used to access and use all singleton methods`
    static let Manager = NoInternet()
    /// `Popup is saved when presented anywhere`
    private var offlinePopup: PopupDialog! = nil
    
    /// `Presents NoInternetVC as PopupDialog from SceneDelegate Window`
    /// - Parameter withMessage: Optional String Which is assigned to the Message Label of NoInternetVC popup
    /// - Parameter buttonName: Optional String Which is assigned to the Title of TryAgain Button of NoInternetVC popup
    public func showNoInternetPopup(withMessage: String? = nil, buttonName: String? = nil) {
        let vc = NoInternetVC(nibName: "NoInternetVC", bundle: nil)
        let popUp = PopupDialog(viewController: vc, transitionStyle: .zoomIn, tapGestureDismissal: false, panGestureDismissal: false)
        popUp.visibleBackground = true
        vc.popUp = popUp
        if let withMessage = withMessage {
            vc.message = withMessage
        }
        if let buttonName = buttonName {
            vc.btnName = buttonName
        }
        offlinePopup = popUp
        appView.rootViewController?.present(popUp, animated: true)
    }
    
    ///`Dismisses PopupDialog of NoInternetVC if it's presented and pops back one VC from navigationStack to reload that VC`
    public func hideOfflinePopup() {
        if offlinePopup != nil {
            offlinePopup.dismiss(animated: true) {
                appView.rootViewController?.navigationController?.popViewController(animated: false)
            }
        }
        offlinePopup = nil
    }
}

extension UIScreen {
    private static let cornerRadiusKey: String = {
        let components = ["Radius", "Corner", "display", "_"]
        return components.reversed().joined()
    }()
    
    /// The corner radius of the display. Uses a private property of `UIScreen`,
    /// and may report 0 if the API changes.
    public var displayCornerRadius: CGFloat {
        guard let cornerRadius = self.value(forKey: Self.cornerRadiusKey) as? CGFloat else {
            assertionFailure("Failed to detect screen corner radius")
            return 0
        }
        
        return cornerRadius
    }
}
