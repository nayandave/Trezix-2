//
//  HapticFeedbackGenerator.swift
//  Trezix-2
//
//  Created by Amar Panchal on 04/11/22.
//

import UIKit

class HapticFeedbackGenerator {
    
    /// **To notify user with haptic that some process has happened**
    ///
    /// `Simple Haptic Feedback Generator`
    /// - Parameter type: `Heavy`, `Light`, `Medium`, `Rigid` and `Soft` Feedback Style
    static func simpleFeedback(type: UIImpactFeedbackGenerator.FeedbackStyle) {
        DispatchQueue.global(qos: .utility).async {
            let gen = UIImpactFeedbackGenerator(style: type)
            gen.impactOccurred()
        }
    }
    
    /// `Notification Feedback Generator Object`
    static private let notiFeedback = UINotificationFeedbackGenerator()
    
    /// **To notify user with haptic that process is finished successfully, unsuccessfully or throws error**
    ///
    /// `Notification Feedback Generator`
    /// - Parameter type: `Error`, `Success` and `Warning` type of *UINotificationFeedbackGenerator.FeedbackType*
    static func notificationFeedback(type: UINotificationFeedbackGenerator.FeedbackType) {
        notiFeedback.notificationOccurred(type)
    }
}
