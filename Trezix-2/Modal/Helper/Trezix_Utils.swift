//
//  Trezix_Utils.swift
//  Trezix-2
//
//  Created by Amar Panchal on 27/02/23.
//

import UIKit

public enum AllSideMenuIdentifier {
    case Dashboard
    case OrderReport
    case ShipmentReport
    case LandingCostReport
    case LandingCostInvoice
    case Approval
    case POTimeLine
    case SalesOrderReport
    case SalesReport
    case ScriptDrawback
    case DispatchPlanningReport
    case DocumentUpload
    case eximgpt
    case vesselTrack
    case Quotation
    case Logout
}

public enum All_Grantable_Permission: String, CaseIterable {
    case ImportDashboard = "import-dashboard.view"
    case ExportDashboard = "export-dashboard.view"
    case Dashboard = "dashboard.view"
    case OrderReport = "pending-order.view"
    case ShipmentReport = "shipment-report.view"
    case LandingCostReport = "landingcost-report.view"
    case LandingCostInvoice = "landing-cost-invoice.view"
    case Approval = "approval-strategy.view"
    case POTimeLine = "po-timeline-report.view"
    case SalesOrderReport = "sales-order-report.view"
    case SalesReport = "export-sales-report.view"
    case ScriptDrawback = "script-drawback-report.view"
    case DispatchPlanningReport = "dispatch-planning-report.vieww"
    case eximgpt = "search.view"
    case vesselTrack = "vessel-tracking.view"
    case quotationView = "quotation-all.view"
//    case quotationEdit = "quotation-create.edit"
}
// TODO: - Notification Permission Checking Pending.
public var all_Permissions = [All_Grantable_Permission]()

public enum arrDocumentTypes: String, CaseIterable {
    case po = "PO"
    case so = "SO"
    case pi = "PI"
    case fi = "FI"
}

/// `Used trace multiple ```ActivityReturnable``` for accordingly UIView components..
var arr_All_Activity_Actions = [String: UIView.ActivityReturnable]()

extension UIView {
    func saveActivityData(_ action: ActivityReturnable) {
        arr_All_Activity_Actions[String(describing: Unmanaged.passUnretained(self).toOpaque())] = action
    }
    
    func getActivityData() -> ActivityReturnable? {
        if let action = arr_All_Activity_Actions[String(describing: Unmanaged.passUnretained(self).toOpaque())] {
            arr_All_Activity_Actions.removeValue(forKey: String(describing: Unmanaged.passUnretained(self).toOpaque()))
            return action
        }
        return nil
    }
}
