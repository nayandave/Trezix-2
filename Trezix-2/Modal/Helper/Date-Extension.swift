//
//  Date-Extension.swift
//  Trezix-2
//
//  Created by Amar Panchal on 05/07/22.
//

import Foundation

struct DateHelper {
    
    static let shared = DateHelper()
    
    private var dateFormat: String = "dd/MM/yyyy"
    
    public mutating func setDateFormat(_ format: String) {
        self.dateFormat = format
    }
    
    public func getStringFrom(date: Date, withFormat: String? = nil) -> String? {
        var str: String? = nil
        let formatter = DateFormatter()
        formatter.dateFormat = withFormat == nil ? dateFormat : withFormat!
        str = formatter.string(from: date)
        return str
    }
    
    /// `Converts String To Date with "dd/MM/yyyy" Standard Format if Format is not provided`
    public func getDateFromString(_ dateStr: String?, withFormat: String? = nil) -> Date? {
        guard let dateStr else { return nil }
        let formatter = DateFormatter()
        formatter.dateFormat = withFormat ?? dateFormat
        return formatter.date(from: dateStr)
    }
    
    func decodeDashboardDate(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
//        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: dateString)!
        return date
    }
    
}


extension Date {
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func startOfLastMonth() -> Date {
        return Calendar.current.date(byAdding: .month, value: -1, to: startOfMonth())!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startOfMonth())!
    }
    
    func endOfLastMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startOfLastMonth())!
    }
    
    func getDayInShort() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        
        return formatter.string(from: self)
    }
    
    func getOnlyDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        
        return formatter.string(from: self)
    }
    
}
