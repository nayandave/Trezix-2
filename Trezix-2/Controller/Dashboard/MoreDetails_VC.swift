//
//  MoreDetails_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 23/01/23.
//

import UIKit

class MoreDetails_VC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var mainTableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var shipmentHeaderView: UIView!
    @IBOutlet weak var shipment_Top_Export_Filter: NSLayoutConstraint!
    @IBOutlet weak var shipment_Top_Import_Filter: NSLayoutConstraint!
    @IBOutlet weak var lbl_Heading_Shipment_1: UILabel!
    @IBOutlet weak var lbl_Heading_Shipment_2: UILabel!
    @IBOutlet weak var lbl_Heading_Shipment_3: UILabel!
    @IBOutlet weak var shipmentTable: UITableView!
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    // Filters
    @IBOutlet weak var btnShowHideFilter: UIButton!
    
    @IBOutlet weak var import_Filter_View: UIView!
    @IBOutlet weak var export_Filter_View: UIView!
    
    @IBOutlet weak var import_FilterView_Height: NSLayoutConstraint!
    @IBOutlet weak var export_FilterView_Height: NSLayoutConstraint!
    
    
    @IBOutlet weak var drop_Import_Vendors: DropDown!
    @IBOutlet weak var drop_Import_BL_Numbers: DropDown!
    @IBOutlet weak var drop_Import_ETA: DropDown!
    
    @IBOutlet weak var drop_Export_BL_Number: DropDown!
    @IBOutlet weak var drop_Export_Delayed: DropDown!
    @IBOutlet weak var drop_Export_ETA: DropDown!
    
    @IBOutlet weak var btn_Import_Apply: UIButton!
    @IBOutlet weak var btn_Export_Apply: UIButton!
    
    // Filter - Custom Date Picker
    @IBOutlet weak var datePickerContentView: UIView!
    @IBOutlet weak var lblPickerTitle: UILabel!
    @IBOutlet weak var btnCancelPicker: UIButton!
    @IBOutlet weak var btnDonePicker: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    // MARK: - All Constants & Variables
    public var isImport_Section_Selected = true
    public var selectedType: DetailType = .Intransit
    public var selectedEventMonth = String()
    
    public var selImportInTransitSubType: Import_Dashboard_VC.All_InTransit_Types? = nil
    public var selImportPendingSubType: Import_Dashboard_VC.All_Pending_Options? = nil
    
    public var selImportInTransitDateType: Import_Dashboard_VC.All_DateRange_Options? = nil
    
    private var import_Intransit_Data = [Intransit_Import_Data]()
    private var import_Pending_Data = [Pending_Order_Import_Data]()
    private var import_Closed_Data = [Closed_Order_Import_Data]()
    private var import_Licence_Data = [Licence_Info_Import_Data]()
    private var import_Upcoming_Data = [Event_List_Import_Data]()
    
    var import_ShipmentTracking_Data = [ShipmentTracking_Import_Data]()
    var filtered_ImportShipment_Data = [ShipmentTracking_Import_Data]()
    
    // Export Section Outlets
    public var selExportOpenOrderType: Export_Dashboard_VC.All_OpenOrder_Type? = nil
    public var selExportShipmentType: Export_Dashboard_VC.All_ShipmentPlanning_Type? = nil
    
    private var export_OpenOrder_Data = [OpenShipmentCompleted_Export_Data]()
    private var export_Shipment_Data = [OpenShipmentCompleted_Export_Data]()
    private var export_CompleteOrder_Data = [OpenShipmentCompleted_Export_Data]()
    private var export_Dispatch_Data = [DispatchPlanning_Export_Data]()
    private var export_Upcoming_Data = [EventList_Export_Data]()
    
    var export_ShipmentTracking_Data = [ShipmentTracking_Export_Data]()
    var filtered_ExportShipment_Data = [ShipmentTracking_Export_Data]()
    
    private var maxTableHeight = CGFloat()
    
    private var arrShowableSections = [Int]()
    
    public enum DetailType: String {
        case Intransit = "In-Transit"
        case PendingOrder = "Pending Orders"
        case ClosedOrder = "Closed Orders"
        case ImportUpcomingEvent = "Upcoming Events"
        case ExportUpcomingEvent = "Upcoming Events "
        case LicenceInfo = "Licence Information"
        case OpenOrder = "Open Orders"
        case ShipmentPlan = "Shipment Planning"
        case CompleteOrder = "Complete Orders"
        case DispatchPlan = "Dispatch Planning"
        case ImportShipmentTracking = "Shipment Tracking"
        case ExportShipmentTracking = "Shipment Tracking "
    }
    
    enum All_ETA_Times: String, CaseIterable {
        case all = "All Times"
        case sevenDays = "7 Days"
        case fifteenDays = "7 to 15 Days"
        case fifteenPlusDays = "15+ Days"
        case custom = "Custom"
    }
    
    var arrImportAllVendors = ["All Vendors"]
    var arrImportAllBLNumbers = ["All BLs"]
    var arrImportAllTimes = ["All Times", "7 Days", "7 to 15 Days", "15+ Days", "Custom"]
    
    var selectedImportVendors = [String]()
    var selectedImportBLs = [String]()
    var selectedImportETA = All_ETA_Times.all
    
    var arrExportAllDelays = ["All Delays"]
    var arrExportAllBLNumbers = ["All BLs"]
    var arrExportAllTimes = ["All Times", "7 Days", "7 to 15 Days", "15+ Days", "Custom"]
    
    var selectedExportDelays = [String]()
    var selectedExportBLs = [String]()
    var selectedExportETA = All_ETA_Times.all
    
    /// Selected From Date From Calendar to use in Filter Process
    var fromDate: Date! = nil
    
    /// Selected To Date From Calendar to use in FIlter Process
    var toDate: Date! = nil
    
    var isFilterVisible = false
    
    lazy var mainBlurr: UIVisualEffectView = {
        var blurrView = UIVisualEffectView()
        blurrView.frame = view.frame
        blurrView.alpha = 0.9
        return blurrView
    }()
    
    public var refrenceToHomeVC: UIViewController! = nil

    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpThisVC()
        registerTableCell()
        DispatchQueue.main.asyncAfter(deadline: .now()+0.25) { [weak self] in
            self?.call_Respective_API()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        noDataView.layer.applyShadowAndRadius()
        if selectedType == .ExportShipmentTracking || selectedType == .ImportShipmentTracking {
            datePickerContentView.layer.applyShadowAndRadius(shadowRadius: 1.5)
            [btnCancelPicker, btnDonePicker, btn_Import_Apply, btn_Export_Apply].forEach { button in
                button?.layer.applyShadowAndRadius()
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if selectedType == .ImportShipmentTracking || selectedType == .ExportShipmentTracking {
            if let touch = touches.first {
                if !datePickerContentView.bounds.contains(touch.location(in: datePickerContentView)) {
                    cancel_Calendar_Action(touch)
                }
            }
            self.view.endEditing(true)
        }
    }
    
    @IBAction func back_Button_Action(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    // MARK: - All Helper Methods
    private func setUpThisVC() {
        [mainTableView, shipmentTable].forEach { tableView in
            tableView?.configure_General_Table()
        }
        datePicker.setPickerStyle_iOS14()
        let isShipmentMode = self.selectedType == .ImportShipmentTracking || self.selectedType == .ExportShipmentTracking
        btnShowHideFilter.isHidden = !isShipmentMode
        if isShipmentMode {
            if selectedType == .ImportShipmentTracking { setUp_All_Import_Dropdowns() }
            else { setUp_All_Export_Dropdowns() }
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            var heading = self.selectedType.rawValue
            self.import_Filter_View.isHidden = true
            self.export_Filter_View.isHidden = true
            if isShipmentMode {
                if self.selectedType == .ImportShipmentTracking {
                    self.shipment_Top_Export_Filter.isActive = false
                    self.import_FilterView_Height.constant = 0
                    self.lbl_Heading_Shipment_1.text = "Vendor"
                    self.lbl_Heading_Shipment_2.text = "BL Number"
                    self.lbl_Heading_Shipment_3.text = "ETA"
                } else {
                    self.shipment_Top_Import_Filter.isActive = false
                    self.export_FilterView_Height.constant = 0
                    self.lbl_Heading_Shipment_1.text = "BL Number"
                    self.lbl_Heading_Shipment_2.text = "ETA"
                    self.lbl_Heading_Shipment_3.text = "Delayed By"
                }
            }
            self.shipmentHeaderView.isHidden = !isShipmentMode
            self.shipmentTable.isHidden = !isShipmentMode
            self.mainTableView.isHidden = isShipmentMode
            if self.isImport_Section_Selected {
                if self.selImportInTransitSubType != nil { heading = self.selImportInTransitSubType!.rawValue }
                else if self.selImportPendingSubType != nil { heading = self.selImportPendingSubType!.rawValue }
            } else {
                if self.selExportOpenOrderType != nil { heading = self.selExportOpenOrderType!.rawValue }
                else if self.selExportShipmentType != nil { heading = self.selExportShipmentType!.rawValue }
            }
            self.btnBack.setTitle(heading, for: .normal)
        }
    }
    
    /// Registering respective table view cell
    private func registerTableCell() {
        if selectedType == .ImportUpcomingEvent || selectedType == .ExportUpcomingEvent {
            mainTableView.register(UINib(nibName: Upcoming_Event_TableCell.identifier, bundle: nil), forCellReuseIdentifier: Upcoming_Event_TableCell.identifier)
        } else {
            mainTableView.register(UINib(nibName: Four_Column_TableCell.identifier, bundle: nil), forCellReuseIdentifier: Four_Column_TableCell.identifier)
            shipmentTable.register(UINib(nibName: Four_Column_TableCell.identifier, bundle: nil), forCellReuseIdentifier: Four_Column_TableCell.identifier)
            if selectedType == .ImportShipmentTracking || selectedType == .ExportShipmentTracking {
                shipmentTable.register(UINib(nibName: POReportExtraInfoCell.identifier, bundle: nil), forCellReuseIdentifier: POReportExtraInfoCell.identifier)
            }
        }
    }
    
    private func call_Respective_API() {
        mainTableView.remove_ShadowView()
        switch selectedType {
        case .Intransit:
            guard let inTransitType = selImportInTransitSubType else { debugPrint("Something Went Wrong!"); return }
            call_Import_IntransitOrder_API(type: inTransitType, dateTange: selImportInTransitDateType)
        case .PendingOrder:
            guard let pendingType = selImportPendingSubType else { debugPrint("Something Went Wrong!"); return }
            call_Import_PendingOrder_API(type: pendingType)
        case .ClosedOrder:
            call_Import_ClosedOrder_API()
        case .ImportUpcomingEvent:
            call_Import_UpcomingEvent_API()
        case .LicenceInfo:
            call_Import_LicenceInfo_API()
        case .OpenOrder, .ShipmentPlan, .CompleteOrder:
            call_Export_OpenOrder_Shipment_CompleteOrder_API()
        case .DispatchPlan:
            call_Export_Dispatch_API()
        case .ExportUpcomingEvent:
            call_Export_UpcomingEvent_API()
        case .ImportShipmentTracking:
            call_Import_ShipmentTracking_API()
        case .ExportShipmentTracking:
            call_Export_ShipmentTracking_API()
        }
    }
    
    // MARK: - All Import More Details API Calling Methods
    private func call_Import_IntransitOrder_API(type: Import_Dashboard_VC.All_InTransit_Types, dateTange: Import_Dashboard_VC.All_DateRange_Options?) {
        var API_Name = String()
        switch type {
        case .invoice: API_Name = APIConstants.IMPORT_INVOICE_MORE_DETAILS.fullPath()
        case .atBank: API_Name = APIConstants.IMPORT_ATBANK_MORE_DETAILS.fullPath()
        case .inClearing: API_Name = APIConstants.IMPORT_INCLEARING_MORE_DETAILS.fullPath()
        case .expected: API_Name = APIConstants.IMPORT_EXPECTED_MORE_DETAILS.fullPath()
        }
        
        var finalDays = "30"
        if let dateRange = selImportInTransitDateType {
            switch dateRange {
            case .thirtyDay: finalDays = "30"
            case .thirtyToSixty: finalDays = "60"
            case .sixtyPlus: finalDays = "601"
            }
        }
        let param = ["days": finalDays]
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: API_Name,
                                            param: param,
                                            headers: [default_app_header], showHud: true, senderView: view)
        { [weak self] (isSuccess, response: Import_InTransit_MoreDetail_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let intransitData = response.data else { return }
                if intransitData.count != 0 {
                    self.import_Intransit_Data = intransitData.compactMap {$0}
                    self.mainTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                    self.calculate_Assign_MaxHeight(self.import_Intransit_Data.count)
                    DispatchQueue.main.async { [weak self] in
                        self?.mainTableView.reloadData()
                    }
                    self.noDataView.isHidden = intransitData.count != 0
                    self.mainTableView.isHidden = intransitData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching Import InTransit More Detail List!")
            }
        }
    }
    
    private func call_Import_PendingOrder_API(type: Import_Dashboard_VC.All_Pending_Options) {
        
        let param = ["order_type" : type == .fullyNotDispatch ? "1" : "2"]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.IMPORT_PENDING_ORDER_MORE_DETAILS.fullPath(),
                                            param: param, headers: [default_app_header], showHud: true)
        { [weak self] (isSuccess, response: Import_PendingOrder_MoreDetail_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let pendingData = response.data else { return }
                if pendingData.count != 0 {
                    self.import_Pending_Data = pendingData.compactMap {$0}
                    self.mainTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                    self.calculate_Assign_MaxHeight(self.import_Pending_Data.count)
                    DispatchQueue.main.async { [weak self] in
                        self?.mainTableView.reloadData()
                    }
                    self.noDataView.isHidden = pendingData.count != 0
                    self.mainTableView.isHidden = pendingData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching Import Pending More Detail List!")
            }
        }
    }
    
    /// Fetches Closed Order More Data From API
    private func call_Import_ClosedOrder_API() {
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.IMPORT_CLOSED_ORDER_MORE_DETAILS.fullPath(),
                                            headers: [default_app_header], showHud: true)
        { [weak self] (isSuccess, response: Import_ClosedOrder_MoreDetail_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let closedData = response.data else { return }
                if closedData.count != 0 {
                    self.import_Closed_Data = closedData.compactMap {$0}
                    self.mainTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                    self.calculate_Assign_MaxHeight(self.import_Closed_Data.count)
                    DispatchQueue.main.async { [weak self] in
                        self?.mainTableView.reloadData()
                    }
                    self.noDataView.isHidden = closedData.count != 0
                    self.mainTableView.isHidden = closedData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching Import Closed Order More Detail List!")
            }
        }
    }
    
    /// Calculating maximum showable height of tableView according to number of rows
    /// - Parameter rowCount: number of rows that tableView will show
    private func calculate_Assign_MaxHeight(_ rowCount: Int) {
        let contentHeight = (CGFloat(rowCount) * getDynamicHeight(size: 35)) + getDynamicHeight(size: 45)
        DispatchQueue.main.async { [weak self] in
            if self?.maxTableHeight == 0 {
                self?.maxTableHeight = self!.view.bounds.height - self!.navContentView.bounds.height - getEdgeNotchHeight(of: .bottomNotch) - 20
            }
            self?.mainTableHeight.constant = contentHeight >= self!.maxTableHeight ? self!.maxTableHeight : contentHeight
            self?.mainTableView.layer.cornerRadius = 8
            self?.mainTableView.layer.masksToBounds = true
            DispatchQueue.main.async { [weak self] in
                self?.mainTableView.add_ShadowView()
            }
        }
    }
    
    private func call_Import_LicenceInfo_API() {
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.IMPORT_LICENCE_INFO_MORE_DETAILS.fullPath(),
                                            headers: [default_app_header], showHud: true)
        { [weak self] (isSuccess, response: Import_LicenceInfo_MoreDetail_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let licenceData = response.data else { return }
                if licenceData.count != 0 {
                    self.import_Licence_Data = licenceData.compactMap {$0}
                    self.mainTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                    self.calculate_Assign_MaxHeight(self.import_Licence_Data.count)
                    DispatchQueue.main.async { [weak self] in
                        self?.mainTableView.reloadData()
                    }
                    self.noDataView.isHidden = licenceData.count != 0
                    self.mainTableView.isHidden = licenceData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching Import Licence Information More Detail List!")
            }
        }
    }
    
    private func call_Import_UpcomingEvent_API() {
        let param = ["month" : selectedEventMonth]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.EVENT_LIST_IMPORT.fullPath(),
                                            param: param,
                                            headers: [default_app_header], showHud: true)
        { [weak self] (isSuccess, response: Event_List_Import_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let eventData = response.data else { return }
                if eventData.count != 0 {
                    self.import_Upcoming_Data = eventData.compactMap{$0}.sorted{ $0.start ?? "N/A" < $1.start ?? "N/A" }
                    self.mainTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                    DispatchQueue.main.async { [weak self] in
                        self?.mainTableView.reloadData()
                    }
                    self.noDataView.isHidden = eventData.count != 0
                    self.mainTableView.isHidden = eventData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching Import Upcoming Event More Detail List!")
            }
        }
    }
    
    //MARK: - All Export More Details API Calling Methods
    private func call_Export_OpenOrder_Shipment_CompleteOrder_API() {
        var API_Name = String()
        var param: [String: Any]? = nil
        if selectedType == .OpenOrder {
            API_Name = APIConstants.EXPORT_OPEN_ORDER_DETAILS.fullPath()
            param = ["order_type": selExportOpenOrderType == .partiallyDeliver ? "1" : "2"]
        } else if selectedType == .ShipmentPlan {
            API_Name = APIConstants.EXPORT_SHIPMENT_DETAILS.fullPath()
            param = ["order_type": selExportShipmentType == .dispatch ? "1" : "2"]
        } else if selectedType == .CompleteOrder {
            API_Name = APIConstants.EXPORT_COMPLETE_ORDER_DETAILS.fullPath()
        }
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: API_Name, param: param, headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Export_OpenOrder_Shipment_CompleteOrder_MoreDetail_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let mainData = response.data else { return }
                if mainData.count != 0 {
                    self.mainTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                    if self.selectedType == .OpenOrder {
                        self.export_OpenOrder_Data = mainData.compactMap { $0 }
                        self.calculate_Assign_MaxHeight(self.export_OpenOrder_Data.count)
                    } else if self.selectedType == .ShipmentPlan {
                        self.export_Shipment_Data = mainData.compactMap { $0 }
                        self.calculate_Assign_MaxHeight(self.export_Shipment_Data.count)
                    } else if self.selectedType == .CompleteOrder {
                        self.export_CompleteOrder_Data = mainData.compactMap { $0 }
                        self.calculate_Assign_MaxHeight(self.export_CompleteOrder_Data.count)
                    }
                    DispatchQueue.main.async { [weak self] in
                        self?.mainTableView.reloadData()
                    }
                    self.noDataView.isHidden = mainData.count != 0
                    self.mainTableView.isHidden = mainData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching \(self.selectedType.rawValue) More Detail List")
            }
        }
    }
    
    private func call_Export_Dispatch_API() {
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.EXPORT_DISPATCH_DETAILS.fullPath(),
                                            headers: [default_app_header], showHud: true)
        { [weak self] (isSuccess, response: Export_DispatchPlanning_MoreDetail_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let dispatchData = response.data else { return }
                if dispatchData.count != 0 {
                    self.export_Dispatch_Data = dispatchData.compactMap {$0}
                    self.mainTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                    self.calculate_Assign_MaxHeight(self.export_Dispatch_Data.count)
                    DispatchQueue.main.async { [weak self] in
                        self?.mainTableView.reloadData()
                    }
                    self.noDataView.isHidden = dispatchData.count != 0
                    self.mainTableView.isHidden = dispatchData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching Export Dispatch More Detail List!")
            }
        }
    }
    
    private func call_Export_UpcomingEvent_API() {
        let param = ["event_month" : selectedEventMonth]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.EVENT_LIST_EXPORT.fullPath(),
                                            param: param,
                                            headers: [default_app_header], showHud: true)
        { [weak self] (isSuccess, response: Event_List_Export_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let eventData = response.data else { return }
                if eventData.count != 0 {
                    self.export_Upcoming_Data = eventData.compactMap{$0}.sorted{ $0.start ?? "N/A" < $1.start ?? "N/A" }
                    self.mainTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                    DispatchQueue.main.async { [weak self] in
                        self?.mainTableView.reloadData()
                    }
                    self.noDataView.isHidden = eventData.count != 0
                    self.mainTableView.isHidden = eventData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching Export Upcoming Event More Detail List!")
            }
        }
    }
    
    
    private func call_Import_ShipmentTracking_API() {
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.IMPORT_SHIPMENT_TRACKING_MORE_DETAILS.fullPath(),
                                            headers: [default_app_header], showHud: true)
        { [weak self] (isSuccess, response: ShipmentTracking_MoreDetail_Import_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let eventData = response.data else { return }
                if eventData.count != 0 {
                    self.import_ShipmentTracking_Data = eventData.compactMap { $0 }
                    self.filtered_ImportShipment_Data = self.import_ShipmentTracking_Data
                    
                    self.arrImportAllVendors = Array(Set(self.import_ShipmentTracking_Data.compactMap { $0.vendorName }))
                    self.arrImportAllVendors.insert("All Vendors", at: 0)
                    
                    
                    self.arrImportAllBLNumbers = Array(Set(self.import_ShipmentTracking_Data.compactMap { $0.blNumber }))
                    self.arrImportAllBLNumbers.insert("All BLs", at: 0)
                    
                    self.arrImportAllTimes = All_ETA_Times.allCases.map { $0.rawValue }
                    
                    self.drop_Import_Vendors.optionArray = self.arrImportAllVendors
                    self.drop_Import_BL_Numbers.optionArray = self.arrImportAllBLNumbers
                    self.drop_Import_ETA.optionArray = self.arrImportAllTimes
                    
                    
                    self.drop_Import_Vendors.text = self.drop_Import_Vendors.optionArray.first
                    self.drop_Import_BL_Numbers.text = self.drop_Import_BL_Numbers.optionArray.first
                    self.drop_Import_ETA.text = self.drop_Import_ETA.optionArray.first
                    
                    self.selectedImportVendors.removeAll()
                    self.selectedImportBLs.removeAll()
                    
                    self.shipmentTable.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                   // self.calculate_Assign_MaxHeight(self.import_ShipmentTracking_Data.count)
                    DispatchQueue.main.async { [weak self] in
                        self?.shipmentTable.reloadData()
                    }
                    self.noDataView.isHidden = eventData.count != 0
                    self.shipmentTable.isHidden = eventData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching Import Shipment Tracking More Detail List!")
            }
        }
    }
    
    private func call_Export_ShipmentTracking_API() {
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.EXPORT_SHIPMENT_TRACKING_MORE_DETAILS.fullPath(),
                                            headers: [default_app_header], showHud: true)
        { [weak self] (isSuccess, response: ShipmentTracking_MoreDetail_Export_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                guard let eventData = response.data else { return }
                if eventData.count != 0 {
                    self.export_ShipmentTracking_Data = eventData.compactMap { $0 }
                    self.filtered_ExportShipment_Data = self.export_ShipmentTracking_Data
                    
                    self.arrExportAllBLNumbers.insert(contentsOf: Array(Set(self.export_ShipmentTracking_Data.compactMap { $0.blNo })), at: 1)
                    self.arrExportAllDelays.insert(contentsOf: Array(Set(self.export_ShipmentTracking_Data.compactMap { $0.delayedBy })), at: 1)
                    
                    self.arrExportAllTimes = All_ETA_Times.allCases.map { $0.rawValue }
                    
                    self.drop_Export_BL_Number.optionArray = self.arrExportAllBLNumbers
                    self.drop_Export_Delayed.optionArray = self.arrExportAllDelays
                    self.drop_Export_ETA.optionArray = self.arrExportAllTimes
                    
                    self.drop_Export_BL_Number.text = self.drop_Export_BL_Number.optionArray.first
                    self.drop_Export_Delayed.text = self.drop_Export_Delayed.optionArray.first
                    self.drop_Export_ETA.text = self.drop_Export_ETA.optionArray.first
                    
                    self.selectedExportDelays.removeAll()
                    self.selectedExportBLs.removeAll()
                    
                    self.shipmentTable.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
                    //self.calculate_Assign_MaxHeight(self.export_ShipmentTracking_Data.count)
                    DispatchQueue.main.async { [weak self] in
                        self?.shipmentTable.reloadData()
                    }
                    self.noDataView.isHidden = eventData.count != 0
                    self.shipmentTable.isHidden = eventData.count == 0
                }
            } else {
                appView.makeToast("Error Fetching Export Shipment Tracking More Detail List!")
            }
        }
    }
}
// MARK: - TableView Delegate Methods
extension MoreDetails_VC: UITableViewDataSource, UITableViewDelegate {
    @objc private func tappedOnSection(_ tap: UITapGestureRecognizer) {
        guard let tappedView = tap.view as? ThreeColumnRowView else { appView.makeToast("Unrecognised Touch"); return }
        let currentIndex = tappedView.currentRow
        if arrShowableSections.contains(currentIndex) {
            arrShowableSections = arrShowableSections.filter({ $0 != currentIndex })
        } else {
            arrShowableSections.append(currentIndex)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.shipmentTable.beginUpdates()
            self.shipmentTable.reloadSections(IndexSet(integer: currentIndex), with: .automatic)
            self.shipmentTable.endUpdates()
//            if self.arrShowableSections.contains(currentIndex) {
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.75, execute: { [weak self] in
//                    self?.reportTable.scrollToRow(at: IndexPath(row: 0, section: currentIndex), at: .top, animated: false)
//                })
//            }
        }
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedType == .ImportShipmentTracking { return filtered_ImportShipment_Data.count }
        else if selectedType == .ExportShipmentTracking { return filtered_ExportShipment_Data.count }
        else { return 1 }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedType {
        case .Intransit: return import_Intransit_Data.count
        case .PendingOrder: return import_Pending_Data.count
        case .ClosedOrder: return import_Closed_Data.count
        case .ImportUpcomingEvent: return import_Upcoming_Data.count
        case .LicenceInfo: return import_Licence_Data.count
        case .OpenOrder: return export_OpenOrder_Data.count
        case .ShipmentPlan: return export_Shipment_Data.count
        case .CompleteOrder: return export_CompleteOrder_Data.count
        case .DispatchPlan: return export_Dispatch_Data.count
        case .ExportUpcomingEvent: return export_Upcoming_Data.count
        case .ImportShipmentTracking: return (arrShowableSections.contains(section) ? 4 : 0)
        case .ExportShipmentTracking: return (arrShowableSections.contains(section) ? 4 : 0)
        }
    }
    // TODO: - Design header as in report view for import shipment tracking.
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if selectedType != .ImportUpcomingEvent && selectedType != .ExportUpcomingEvent {
            return getDynamicHeight(size: 45)
        }
        return 0
    }
    
    @objc private func export_Shipment_Detail_Tracking_Action(_ sender: UIButton) {
        HapticFeedbackGenerator.simpleFeedback(type: .light)
        debugPrint("More Detail Click Action..")
        if selectedType == .ExportShipmentTracking {
            let index = sender.tag
            guard let passableID = filtered_ExportShipment_Data[index].id else {
                debugPrint("Invalid ID!")
                return
            }
            call_Vessel_Info_API_Redirect(vesselID: passableID, detailButton: sender)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if selectedType != .ImportUpcomingEvent && selectedType != .ExportUpcomingEvent {
            if selectedType == .ImportShipmentTracking {
                let headerView = ThreeColumnRowView()
                headerView.isViewHeadingView(withSeparator: true, tag: section)
                let data = filtered_ImportShipment_Data[section]
                headerView.setData(firstLabel: data.vendorName, secondLabel: data.blNumber, thirdLabel: data.etaByTracking, fourthLabel: data.status, threeColumnView: true)
                headerView.isSelected(selected: arrShowableSections.contains(section), lastCell: section+1 == filtered_ImportShipment_Data.count)
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnSection(_:)))
                headerView.addGestureRecognizer(tapGesture)
                return headerView
            } else if selectedType == .ExportShipmentTracking {
                let headerView = ThreeColumnRowView()
                headerView.isViewHeadingView(withSeparator: true, tag: section)
                let data = filtered_ExportShipment_Data[section]
                headerView.setData(firstLabel: data.blNo, secondLabel: data.etaByTracking, thirdLabel: data.delayedBy, threeColumnView: true)
                headerView.isSelected(selected: arrShowableSections.contains(section), lastCell: section+1 == filtered_ExportShipment_Data.count, isCustomButton: true)
                headerView.customButton.setImage(UIImage(named: "icon-Navigate"), for: .normal)
                headerView.customButton.addTarget(self, action: #selector(export_Shipment_Detail_Tracking_Action(_ :)), for: .touchUpInside)
                return headerView
            } else {
                let headerView = tableView.dequeueReusableCell(withIdentifier: Four_Column_TableCell.identifier) as! Four_Column_TableCell
                switch selectedType {
                case .Intransit:
                    headerView.configureCellType(heading: true, importHeadingType: .InTransit)
                case .PendingOrder:
                    headerView.configureCellType(heading: true, importHeadingType: .Pending)
                case .ClosedOrder:
                    headerView.configureCellType(heading: true, importHeadingType: .Closed)
                case .ImportUpcomingEvent, .ExportUpcomingEvent: break;
                case .LicenceInfo:
                    headerView.configureCellType(heading: true, importHeadingType: .Licence)
                case .OpenOrder:
                    headerView.configureCellType(heading: true, exportHeadingType: .OpenOrder)
                case .ShipmentPlan:
                    headerView.configureCellType(heading: true, exportHeadingType: .ShipmentPlan)
                case .CompleteOrder:
                    headerView.configureCellType(heading: true, exportHeadingType: .CompleteOrder)
                case .DispatchPlan:
                    headerView.configureCellType(heading: true, exportHeadingType: .DispatchPlan)
                default: debugPrint("Unexpected Value")
                }
                return headerView
            }
        }
        return nil
    }
    
    @objc private func more_Detail_Button_Action(_ sender: UIButton) {
        HapticFeedbackGenerator.simpleFeedback(type: .light)
        if selectedType == .ImportShipmentTracking {
            guard let cell = shipmentTable.cellForRow(at: IndexPath(row: 3, section: sender.tag)) as? POReportExtraInfoCell else { debugPrint("Invalid TableCell"); return }
            let index = sender.tag
            guard let passableID = filtered_ImportShipment_Data[index].id else {
                debugPrint("Invalid ID!")
                return
            }
            call_Vessel_Info_API_Redirect(vesselID: passableID, detailButton: sender, detailStackView: cell.detainButton_StackView)
        }
    }
    
    private func call_Vessel_Info_API_Redirect(vesselID: Int, detailButton: UIButton, detailStackView: UIStackView? = nil) {
        let vc = vesselStoryboard.instantiateViewController(withIdentifier: "Vessel_Details_VC") as! Vessel_Details_VC
        let param = ["id" : vesselID]
        detailStackView?.alpha = 0
        debugPrint("VESSEL PARAM = \(param)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.VESSEL_INFO.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false, senderView: detailButton)
        { [weak self] (isSuccess, response: Vessel_Detail_Response_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if let data = response.data {
                    vc.mainVesselData = data

                    // Shipment Data Handling
                    if let eventData = data.shipmentEvent {
                        vc.arrEventData = eventData
                    }

                    // Origin To Port Data Handling
                    if let originData = data.originToPort {
                        vc.vessel_Origin_Port_Data = originData
                        debugPrint("Origin Data -- \(originData)")
                    }

                    // Port To Port Data Handling
                    if let portData = data.portToPort {
                        vc.vessel_Port_ToPort_Data = portData
                        debugPrint("POrt Data -- \(portData)")
                    }
                    detailStackView?.alpha = 1
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                    self.dismiss(animated: true) { [weak self] in
                        self?.refrenceToHomeVC.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {    // No Data Available...
                    HapticFeedbackGenerator.notificationFeedback(type: .error)
                    detailStackView?.alpha = 1
                    appView.makeToast("No Data Available!")
                }
            } else if let errorMessage {
                HapticFeedbackGenerator.notificationFeedback(type: .error)
                detailStackView?.alpha = 1
                debugPrint("Known error while fetching vessel info --- \(errorMessage)")
                appView.makeToast("Something Went Wrong,\nPlease Try Again Later!")
            } else {
                HapticFeedbackGenerator.notificationFeedback(type: .error)
                detailStackView?.alpha = 1
                debugPrint("Unknown error while fetching vessel info")
                appView.makeToast("Something Went Wrong,\nPlease Try Again Later!")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedType == .ImportUpcomingEvent || selectedType == .ExportUpcomingEvent {
            let cell = tableView.dequeueReusableCell(withIdentifier: Upcoming_Event_TableCell.identifier, for: indexPath) as! Upcoming_Event_TableCell
            
            let isImportType = selectedType == .ImportUpcomingEvent
            let importData: Event_List_Import_Data! = isImportType ? import_Upcoming_Data[indexPath.row] : nil
            let exportData: EventList_Export_Data! = isImportType ? nil : export_Upcoming_Data[indexPath.row]
            
            if let eventDateStr = isImportType ? importData.start : exportData.start {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let dataDate = formatter.date(from: eventDateStr)!
                cell.setAllData(day: dataDate.getDayInShort(), date: dataDate.getOnlyDate(), event: isImportType ? importData.title : exportData.title)
                cell.separatorInset = UIEdgeInsets(top: 0, left: tableView.bounds.width, bottom: 0, right: 0)
                DispatchQueue.main.async { [weak self] in
                    self?.mainTableHeight.constant = tableView.contentSize.height
                }
            }
            return cell
        } else if selectedType == .ImportShipmentTracking {
            let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
            if indexPath.row == 3 {
                cell.is_Detail_Button_Only(index: indexPath.section, title: "Detail Tracking")
                cell.btnDetail.addTarget(self, action: #selector(more_Detail_Button_Action(_:)), for: .touchUpInside)
                cell.isUserInteractionEnabled = true
            } else {
                cell.importShipmentTrackingData = filtered_ImportShipment_Data[indexPath.section]
                cell.setUpImportShipmentDataCell(for: indexPath.row)
                cell.isUserInteractionEnabled = false
            }
            return cell
        } else if selectedType == .ExportShipmentTracking {
            let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
            if indexPath.row == 3 {
                cell.is_Detail_Button_Only(index: indexPath.section, title: "Detail Tracking")
                cell.btnDetail.addTarget(self, action: #selector(more_Detail_Button_Action(_:)), for: .touchUpInside)
                cell.isUserInteractionEnabled = true
            } else {
                cell.exportShipmentTrackingData = filtered_ExportShipment_Data[indexPath.section]
                cell.setUpImportShipmentDataCell(for: indexPath.row)
                cell.isUserInteractionEnabled = false
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Four_Column_TableCell.identifier, for: indexPath) as! Four_Column_TableCell
            switch selectedType {
            case .Intransit:
                cell.configureCellType(heading: false, isEven: (indexPath.row)%2==0)
                let mainData = import_Intransit_Data[indexPath.row]
                cell.setAllData(data1: mainData.invoice, data2: mainData.invoiceDate,
                                data3: mainData.qty.flatMap { String($0) },
                                data4: mainData.total.flatMap { String($0) })
                cell.separatorInset = UIEdgeInsets(top: 0, left: import_Intransit_Data.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                
                return cell
                
            case .PendingOrder:
                
                cell.configureCellType(heading: false, isEven: (indexPath.row)%2==0)
                let mainData = import_Pending_Data[indexPath.row]
                cell.setAllData(data1: mainData.poNo, data2: mainData.poDate,
                                data3: mainData.poQty.flatMap { String($0) },
                                data4: mainData.total.flatMap { String($0) })
                cell.separatorInset = UIEdgeInsets(top: 0, left: import_Pending_Data.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                
                return cell
                
            case .ClosedOrder:
                cell.configureCellType(heading: false, isEven: (indexPath.row)%2==0)
                let mainData = import_Closed_Data[indexPath.row]
                cell.setAllData(data1: mainData.poNo, data2: mainData.date,
                                data3: mainData.qty.flatMap { String($0) },
                                data4: mainData.price.flatMap { String($0) })
                cell.separatorInset = UIEdgeInsets(top: 0, left: import_Closed_Data.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                
                return cell
                
            case .LicenceInfo:
                
                cell.configureCellType(heading: false, isEven: (indexPath.row)%2==0)
                let mainData = import_Licence_Data[indexPath.row]
                cell.setAllData(data1: mainData.licenceNo, data2: mainData.licenceDate,
                                data3: mainData.licenseValue.flatMap { String($0) },
                                data4: mainData.balance.flatMap { String($0) })
                cell.separatorInset = UIEdgeInsets(top: 0, left: import_Licence_Data.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                
                return cell
                
            case .OpenOrder:
                
                cell.configureCellType(heading: false, isEven: (indexPath.row)%2==0)
                let mainData = export_OpenOrder_Data[indexPath.row]
                cell.setAllData(data1: mainData.invoiceNo, data2: mainData.date,
                                data3: mainData.quantity.flatMap { String($0) },
                                data4: mainData.amount.flatMap { String($0) })
                cell.separatorInset = UIEdgeInsets(top: 0, left: export_OpenOrder_Data.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                
                return cell
                
            case .ShipmentPlan:
                cell.configureCellType(heading: false, isEven: (indexPath.row)%2==0)
                let mainData = export_Shipment_Data[indexPath.row]
                cell.setAllData(data1: mainData.invoiceNo, data2: mainData.date,
                                data3: mainData.quantity.flatMap { String($0) },
                                data4: mainData.amount.flatMap { String($0) })
                cell.separatorInset = UIEdgeInsets(top: 0, left: export_Shipment_Data.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                
                return cell
                
            case .CompleteOrder:
                
                cell.configureCellType(heading: false, isEven: (indexPath.row)%2==0)
                let mainData = export_CompleteOrder_Data[indexPath.row]
                cell.setAllData(data1: mainData.invoiceNo, data2: mainData.date,
                                data3: mainData.quantity.flatMap { String($0) },
                                data4: mainData.amount.flatMap { String($0) })
                cell.separatorInset = UIEdgeInsets(top: 0, left: export_CompleteOrder_Data.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                
                return cell
                
            case .DispatchPlan:
                
                cell.configureCellType(heading: false, isEven: (indexPath.row)%2==0)
                let mainData = export_Dispatch_Data[indexPath.row]
                cell.setAllData(data1: mainData.dispatchDate, data2: mainData.netWeight.flatMap { String($0) },
                                data3: mainData.noOfPi.flatMap { String($0) }, data4: nil)
                cell.separatorInset = UIEdgeInsets(top: 0, left: export_Dispatch_Data.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                
                return cell
                
            case .ImportShipmentTracking, .ExportShipmentTracking: return cell
            case .ImportUpcomingEvent, .ExportUpcomingEvent: return cell;
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedType != .ImportUpcomingEvent && selectedType != .ExportUpcomingEvent {
            if selectedType == .ImportShipmentTracking || selectedType == .ExportShipmentTracking {
                return UITableView.automaticDimension
            }
            return getDynamicHeight(size: 35)
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
