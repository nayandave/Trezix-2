//
//  Import_Dashboard_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 09/01/23.
//

import UIKit
import Charts

protocol OpenMonthPickerProtocol: AnyObject {
    func openMonthPicker()
}
class Import_Dashboard_VC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var mainStackView: UIStackView!
    
    // In-Transit Order Section Outlets
    @IBOutlet weak var inTransitContainerView: UIView!
    @IBOutlet weak var inTransitStackView: UIStackView!
    @IBOutlet weak var inTransitHeadingView: UIView!
    @IBOutlet weak var dropInTransitType: DropDown!
    
    @IBOutlet weak var inTransitDateDetailView: UIView!
    @IBOutlet weak var dropInTransitDateOption: DropDown!
    @IBOutlet weak var btnInTransitViewDetail: UIButton!
    
    @IBOutlet weak var inTransitTableContainerView: UIView!
    @IBOutlet weak var inTransitTableView: UITableView!
    @IBOutlet weak var inTransitNoDataView: UIView!
    @IBOutlet weak var inTransitLabelNoData: UILabel!
    @IBOutlet weak var inTransitTableHeight: NSLayoutConstraint!
    
    // Pending Order Section Outlets
    @IBOutlet weak var pendingOrderContainerView: UIView!
    @IBOutlet weak var pendingOrderStackView: UIStackView!
    @IBOutlet weak var pendingOrderHeadingView: UIView!
    @IBOutlet weak var lblTotal: UILabel!
    
    @IBOutlet weak var pendingOrderDateDetailView: UIView!
    @IBOutlet weak var dropPendingType: DropDown!
    @IBOutlet weak var btnPendingOrderViewDetail: UIButton!
    
    @IBOutlet weak var pendingOrderTableContainerView: UIView!
    @IBOutlet weak var pendingOrderTableView: UITableView!
    @IBOutlet weak var pendingOrderNoDataView: UIView!
    @IBOutlet weak var pendingOrderLabelNoData: UILabel!
    @IBOutlet weak var pendingOrderTableHeight: NSLayoutConstraint!
    
    // Closed Order Section Outlets
    @IBOutlet weak var closedOrderContainerView: UIView!
    @IBOutlet weak var closedOrderStackView: UIStackView!
    @IBOutlet weak var closedOrderHeadingView: UIView!
    
    @IBOutlet weak var closedOrderDateDetailView: UIView!
    @IBOutlet weak var btnClosedOrderViewDetail: UIButton!
    
    @IBOutlet weak var closedOrderTableContainerView: UIView!
    @IBOutlet weak var closedOrderTableView: UITableView!
    @IBOutlet weak var closedOrderNoDataView: UIView!
    @IBOutlet weak var closedOrderLabelNoData: UILabel!
    @IBOutlet weak var closedOrderTableHeight: NSLayoutConstraint!
    
    // Licence Info Section Outlets
    @IBOutlet weak var licenceInfoContainerView: UIView!
    @IBOutlet weak var licenceInfoStackView: UIStackView!
    @IBOutlet weak var licenceInfoHeadingView: UIView!
    
    @IBOutlet weak var licenceInfoDateDetailView: UIView!
    @IBOutlet weak var btnLicenceInfoViewDetail: UIButton!
    
    @IBOutlet weak var licenceInfoTableContainerView: UIView!
    @IBOutlet weak var licenceInfoTableView: UITableView!
    @IBOutlet weak var licenceInfoNoDataView: UIView!
    @IBOutlet weak var licenceInfoLabelNoData: UILabel!
    @IBOutlet weak var licenceInfoTableHeight: NSLayoutConstraint!
    
    // Upcoming Event Section Outlets
    @IBOutlet weak var upcomingEventContainerView: UIView!
    @IBOutlet weak var upcomingEventStackView: UIStackView!
    @IBOutlet weak var upcomingEventHeadingView: UIView!
    @IBOutlet weak var dropUpcomingEventType: DropDown!
    
    @IBOutlet weak var upcomingEventDateDetailView: UIView!
    @IBOutlet weak var btnUpcomingEventViewDetail: UIButton!
    
    @IBOutlet weak var upcomingEventTableContainerView: UIView!
    @IBOutlet weak var upcomingEventTableView: UITableView!
    @IBOutlet weak var upcomingEventNoDataView: UIView!
    @IBOutlet weak var upcomingEventLabelNoData: UILabel!
    @IBOutlet weak var upcomingEventTableHeight: NSLayoutConstraint!
    
    // Shipment Tracking Outlets
    @IBOutlet weak var shipmentTrackingContainerView: UIView!
    @IBOutlet weak var shipmentTrackingStackView: UIStackView!
    @IBOutlet weak var shipmentTrackingHeadingView: UIView!
    
    @IBOutlet weak var shipmentTrackingDateDetailView: UIView!
    @IBOutlet weak var btnShipmentTrackingViewDetail: UIButton!
    
    @IBOutlet weak var shipmentTrackingTableContainerView: UIView!
    @IBOutlet weak var shipmentTrackingTableView: UITableView!
    @IBOutlet weak var shipmentTrackingNoDataView: UIView!
    @IBOutlet weak var shipmentTrackingLabelNoData: UILabel!
    @IBOutlet weak var shipmentTrackingTableHeight: NSLayoutConstraint!
    
    
    // Revenu Chart Section Outlets
    @IBOutlet weak var chartContainerView: UIView!
    @IBOutlet weak var dropChartYearlyType: DropDown!
    @IBOutlet weak var chartHeadingView: UIView!
    @IBOutlet weak var mainChartContainerView: UIView!
    @IBOutlet weak var mainChartContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var chartNoDataView: UIView!
    @IBOutlet weak var barChartView: BarChartView!
    
    
    // MARK: - All Constants & Variables
    private let defaultTableHeight = getDynamicHeight(size: 100)
    
    weak var openMonthPickerDelegate: OpenMonthPickerProtocol?
    weak var notiDelegate: ChangeNotificationProtocol?
    
    private var observer: NSKeyValueObservation?
    var importStackHeight = CGFloat()
    
    enum All_InTransit_Types: String, CaseIterable {
        case invoice = "Invoice"
        case atBank = "At Bank"
        case inClearing = "In Clearing"
        case expected = "Expected to Arrive"
    }
    private var selecteIntransitType: All_InTransit_Types = .invoice
    
    enum All_DateRange_Options: String, CaseIterable {
        case thirtyDay = "30 Days Old"
        case thirtyToSixty = "30 to 60 Days Old"
        case sixtyPlus = "60+ Days Old"
    }
    private var selectedDateRange: All_DateRange_Options = .thirtyDay
    
    enum All_Pending_Options: String, CaseIterable {
        case fullyNotDispatch = "Fully not Dispatch"
        case partiallyDispatch = "Partially Dispatch"
    }
    private var selectedPendingOption: All_Pending_Options = .fullyNotDispatch
    
    enum All_Chart_Types: String, CaseIterable {
        case halfYear = "6 months"
        case fullYear = "Full Year"
        
        var associatedValue: String {
            switch self {
            case .halfYear: return "monthly";
            case .fullYear: return "yearly";
            }
        }
    }
    
    private var selectedEventMonth = String()
    private var selectedMonthOrYear: All_Chart_Types = .halfYear
    
    private var allTableView = [UITableView]()
    private var allDropdown = [DropDown]()
    
    private var allImportData: Import_Dashboard_Data! = nil
    private var importEventData: [Event_List_Import_Data?]! = nil
    
    private lazy var blurrView: UIVisualEffectView = {
        var blurr = UIVisualEffectView()
        blurr.frame = mainStackView.frame
        blurr.isOpaque = false
        return blurr
    }()
    
    var tryReloadAPICount = 0
    
    // MARK: - View Life Cycle Methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        debugPrint("Import Viewwill Appear")
//        DispatchQueue.global(qos: .utility).async { [weak self] in
//            self?.call_Import_Dashboard_API()
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) { 
        super.viewWillDisappear(animated)
        debugPrint("Import ViewWill Disappear")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allTableView = [inTransitTableView, pendingOrderTableView, closedOrderTableView, licenceInfoTableView, upcomingEventTableView, shipmentTrackingTableView]
        allDropdown = [dropInTransitType, dropInTransitDateOption, dropPendingType, dropUpcomingEventType, dropChartYearlyType]
//        reassignReachability()
        setUpHomePage()
        calculateCurrentEventMonth()
        NotificationCenter.default.addObserver(self, selector: #selector(dismissing_MonthPicker(_:)), name: .dismissingMonthPickerNotification, object: nil)
        call_Import_Dashboard_API()
        
        observer = mainStackView.layer.observe(\.bounds) { [weak self] object, _ in
            self?.importStackHeight = object.bounds.height
            scrollHeightObject.changingHeight(newHeight: object.bounds.height)
            print("Export Dashboard Height Changing...\(object.bounds)")
        }
    }
    
    @objc private func dismissing_MonthPicker(_ notification: NSNotification) {
        if let data = notification.object as? [String: Any] {
            if (data["Section"] as! Bool) {     //Import Section Month Picker
                if let selectedMonth = data["Month"] as? String {
                    selectedEventMonth = selectedMonth
                    DispatchQueue.main.async { [weak self] in
                        self?.dropUpcomingEventType.text = selectedMonth
                    }
                    call_UpcomingEvent_API(fromDrop: true)
                }
            }
        } else {
            debugPrint("Data passing through Notification Center went off track!")
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        allDropdown.forEach { if ($0 != dropInTransitDateOption && $0 != dropPendingType) { $0.layer.applyShadowAndRadius() } }
        
        [inTransitNoDataView, pendingOrderNoDataView, closedOrderNoDataView, licenceInfoNoDataView, upcomingEventNoDataView].forEach { $0.layer.applyShadowAndRadius() }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        allDropdown.forEach { drop in
            drop.hideList()
        }
    }
    
    // MARK: - All Actions
    @IBAction func inTransit_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail(withType: .Intransit, inTransitSubType: selecteIntransitType)
    }
    
    @IBAction func pendingOrder_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail(withType: .PendingOrder, pendingSubType: selectedPendingOption)
    }
    
    @IBAction func closedOrder_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail(withType: .ClosedOrder)
    }
    
    @IBAction func licenceInfo_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail(withType: .LicenceInfo)
    }
    
    @IBAction func upcomingEvent_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail(withType: .ImportUpcomingEvent)
    }
    
    @IBAction func shipment_ViewDetail_Action(_ sender: UIButton) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let moreVC = homeStoryboard.instantiateViewController(withIdentifier: "MoreDetails_VC") as! MoreDetails_VC
        moreVC.isImport_Section_Selected = true
        moreVC.selectedType = .ImportShipmentTracking
        moreVC.refrenceToHomeVC = self.parent
        present(moreVC, animated: true)
    }
    
    private func navigate_To_MoreDetail(withType: MoreDetails_VC.DetailType, inTransitSubType: All_InTransit_Types? = nil, pendingSubType: All_Pending_Options? = nil) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let moreVC = homeStoryboard.instantiateViewController(withIdentifier: "MoreDetails_VC") as! MoreDetails_VC
        
        moreVC.isImport_Section_Selected = true
        moreVC.selectedType = withType
        moreVC.selImportInTransitSubType = inTransitSubType
        moreVC.selImportPendingSubType = pendingSubType
        moreVC.selectedEventMonth = selectedEventMonth
        moreVC.selImportInTransitDateType = selectedDateRange
        
        navigationController?.present(moreVC, animated: true)
    }
}

// MARK: - All Helper Methods
extension Import_Dashboard_VC {
    //  MARK: - Initial Required Methods
    /// `Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable {
            callAllInitMethods()
        }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    private func callAllInitMethods(_ afterRefresh: Bool = false) {
        if !afterRefresh {
            setUpHomePage()
            calculateCurrentEventMonth()
        } 
        call_Import_Dashboard_API()
    }
    
    /// `Setting Up Homepage`
    private func setUpHomePage() {
        registerTableCells()
        initAllDropDowns()
    }
    
    private func registerTableCells() {
        allTableView.forEach { tableView in
            if tableView == upcomingEventTableView {
                tableView.register(UINib(nibName: Upcoming_Event_TableCell.identifier, bundle: nil), forCellReuseIdentifier: Upcoming_Event_TableCell.identifier)
            } else {
                tableView.register(UINib(nibName: Four_Column_TableCell.identifier, bundle: nil), forCellReuseIdentifier: Four_Column_TableCell.identifier)
            }
        }
    }
    
    private func initAllDropDowns() {
        allDropdown.forEach { dropDown in
            dropDown.arrowColor = .ourAppThemeColor
            dropDown.hideOptionsWhenSelect = true
            dropDown.isSearchEnable = false
            dropDown.handleKeyboard = true
            dropDown.checkMarkEnabled = false
            if (dropDown == dropInTransitDateOption || dropDown == dropPendingType) {
                dropDown.rowBackgroundColor = UIColor.hexStringToUIColor(hex: "CFDBE2")
            }
            dropDown.selectedRowColor = .ourAppThemeColor
            dropDown.selectedIndex = 0
            dropDown.rowHeight = getDynamicHeight(size: 45)
//            dropDown.listHeight = getDynamicHeight(size: 150)
            
            if dropDown != dropUpcomingEventType {
                if dropDown == dropInTransitType {  // In-Transit Type
                    dropDown.optionArray = All_InTransit_Types.allCases.map { $0.rawValue }
                } else if dropDown == dropInTransitDateOption {     // In-Transit Date Option Types
                    dropDown.optionArray = All_DateRange_Options.allCases.map { $0.rawValue }
                } else if dropDown == dropPendingType {     // Pending Order Option Types
                    dropDown.optionArray = All_Pending_Options.allCases.map { $0.rawValue }
                } else if dropDown == dropChartYearlyType {     // Chart Revenue Types
                    dropDown.optionArray = All_Chart_Types.allCases.map { $0.rawValue }
                }
                dropDown.text = dropDown.optionArray.first
                dropDown.didSelect(completion: { [weak self] selectedText, index, id in
                    guard let self = self else { return }
                    HapticFeedbackGenerator.simpleFeedback(type: .light)
                    if dropDown == self.dropInTransitType {  // In-Transit Type
                        self.selecteIntransitType = All_InTransit_Types.allCases[index]
                        self.setUpInvoiceDataInUI()
                    } else if dropDown == self.dropInTransitDateOption {     // In-Transit Date Option Types
                        self.selectedDateRange = All_DateRange_Options.allCases[index]
                        self.setUpInvoiceDataInUI()
                    } else if dropDown == self.dropPendingType {     // Pending Order Option Types
                        self.selectedPendingOption = All_Pending_Options.allCases[index]
                        self.setUpPendingOrderDataInUI()
                    } else if dropDown == self.dropChartYearlyType {     // Chart Revenue Types
                        self.selectedMonthOrYear = All_Chart_Types.allCases[index]
                        self.call_Import_Dashboard_API(fromDrop: true)
                    }
                    DispatchQueue.main.async {
                        dropDown.text = selectedText
                    }
                })
            } else {
                dropDown.setCustomTouchAction(completion: {
                    self.openMonthPickerDelegate?.openMonthPicker()
                })
            }
        }
    }
    
    private func calculateCurrentEventMonth() {
        let currDate = Date()
        // Calculating Year
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        let year = formatter.string(from: currDate)
        
        // Calculating Month
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MMM"
        let month = formatter2.string(from: currDate)
        
        selectedEventMonth = month + " " + year
        dropUpcomingEventType.text = selectedEventMonth
    }
    
    // MARK: - UI Methods
    private func setUIWithData() {
        setUpInvoiceDataInUI()
        setUpPendingOrderDataInUI()
        setUpClosedOrderDataInUI()
        setUpLicenceInfoDataInUI()
        setUpUpcomingDataInUI()
        setUpShipmentTrackingDataInUI()
        setUpChartData()
        DispatchQueue.main.async { [weak self] in
            self?.lblTotal.text = self!.allImportData.totalPendingOrder.flatMap { String($0) }
        }
        guard let notiCount = allImportData.unreadTotalNotification,
              let approvalCount = allImportData.totalPendingApprovalNotification else { debugPrint("No Notification Count in Import"); return }
        notiDelegate?.changeNotiCount(notiCount: notiCount, approvalNotiCount: approvalCount)
    }
    
    private func setUpInvoiceDataInUI() {
        self.inTransitTableView.remove_ShadowView()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            var dataCount = 0
            var totalDataCount = 0
            switch self.selecteIntransitType {
            case .invoice:
                switch self.selectedDateRange {
                case .thirtyDay:
                    dataCount = self.allImportData.invoice30D?.count ?? 0;
                    totalDataCount = self.allImportData.invoice30DCount ?? 0;
                    break
                case .thirtyToSixty:
                    dataCount = self.allImportData.invoice60D?.count ?? 0;
                    totalDataCount = self.allImportData.invoice60DCount ?? 0;
                    break
                case .sixtyPlus:
                    dataCount = self.allImportData.invoice60DPlus?.count ?? 0;
                    totalDataCount = self.allImportData.invoice60DPlusCount ?? 0;
                    break
                }
            case .atBank:
                switch self.selectedDateRange {
                case .thirtyDay:
                    dataCount = self.allImportData.atBank30D?.count ?? 0;
                    totalDataCount = self.allImportData.atBank30DCount ?? 0;
                    break
                case .thirtyToSixty:
                    dataCount = self.allImportData.atBank60D?.count ?? 0;
                    totalDataCount = self.allImportData.atBank60DCount ?? 0;
                    break
                case .sixtyPlus:
                    dataCount = self.allImportData.atBank60DPlus?.count ?? 0;
                    totalDataCount = self.allImportData.atBank60DPlusCount ?? 0;
                    break
                }
            case .inClearing:
                switch self.selectedDateRange {
                case .thirtyDay:
                    dataCount = self.allImportData.inClearing30D?.count ?? 0;
                    totalDataCount = self.allImportData.inClearing30DCount ?? 0;
                    break
                case .thirtyToSixty:
                    dataCount = self.allImportData.inClearing60D?.count ?? 0;
                    totalDataCount = self.allImportData.inClearing60DCount ?? 0;
                    break
                case .sixtyPlus:
                    dataCount = self.allImportData.inClearing60DPlus?.count ?? 0;
                    totalDataCount = self.allImportData.inClearing60DPlusCount ?? 0;
                    break
                }
            case .expected:
                switch self.selectedDateRange {
                case .thirtyDay:
                    dataCount = self.allImportData.expectedToArrive30D?.count ?? 0;
                    totalDataCount = self.allImportData.expectedToArrive30DCount ?? 0;
                    break
                case .thirtyToSixty:
                    dataCount = self.allImportData.expectedToArrive60D?.count ?? 0;
                    totalDataCount = self.allImportData.expectedToArrive60DCount ?? 0;
                    break
                case .sixtyPlus:
                    dataCount = self.allImportData.expectedToArrive60DPlus?.count ?? 0;
                    totalDataCount = self.allImportData.expectedToArrive60DPlusCount ?? 0;
                    break
                }
            }
            self.inTransitTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.btnInTransitViewDetail.isHidden = totalDataCount <= 4
            if dataCount == 0 {
                self.inTransitTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.inTransitTableView.isHidden = dataCount == 0
            self.inTransitNoDataView.isHidden = dataCount != 0
            self.inTransitTableView.reloadData()
        }
    }
    
    private func setUpPendingOrderDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            var dataCount = 0
            var totalDataCount = 0
            switch self.selectedPendingOption {
            case .fullyNotDispatch:
                dataCount = self.allImportData.pendingOrderFullyNotDispatchData?.count ?? 0
                totalDataCount = self.allImportData.pendingOrderFullyNotDispatch ?? 0
                break
            case .partiallyDispatch:
                dataCount = self.allImportData.pendingOrderPartiallyDispatchData?.count ?? 0
                totalDataCount = self.allImportData.pendingOrderPartiallyDispatch ?? 0
                break
            }
            
            self.pendingOrderTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.pendingOrderTableView.remove_ShadowView()
            self.btnPendingOrderViewDetail.isHidden = totalDataCount <= 4
            if dataCount == 0 {
                self.pendingOrderTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.pendingOrderTableView.isHidden = dataCount == 0
            self.pendingOrderNoDataView.isHidden = dataCount != 0
            self.pendingOrderTableView.reloadData()
        }
    }
    
    private func setUpClosedOrderDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let dataCount = self.allImportData.closeOrder?.count ?? 0
            let totalDataCount = (self.allImportData.totalOrders ?? 0) - (self.allImportData.totalPendingOrder ?? 0)
            
            self.closedOrderTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.closedOrderTableView.remove_ShadowView()
            self.closedOrderDateDetailView.isHidden = totalDataCount <= 4
            if dataCount == 0 {
                self.closedOrderTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.closedOrderTableView.isHidden = dataCount == 0
            self.closedOrderNoDataView.isHidden = dataCount != 0
            self.closedOrderTableView.reloadData()
        }
    }
    
    private func setUpLicenceInfoDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let dataCount = self.allImportData.licenceInformation?.count ?? 0
            
            self.licenceInfoTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.licenceInfoTableView.remove_ShadowView()
            self.licenceInfoDateDetailView.isHidden = dataCount == 0
            if dataCount == 0 {
                self.licenceInfoTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.licenceInfoTableView.isHidden = dataCount == 0
            self.licenceInfoNoDataView.isHidden = dataCount != 0
            self.licenceInfoTableView.reloadData()
        }
    }
    
    private func setUpShipmentTrackingDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let dataCount = self.allImportData.shipmentTracking?.count ?? 0
            let totalDataCount = self.allImportData.shipmentTrackingCount ?? 0
            
            self.shipmentTrackingTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.shipmentTrackingTableView.remove_ShadowView()
            self.shipmentTrackingDateDetailView.isHidden = totalDataCount <= 4
            if dataCount == 0 {
                self.shipmentTrackingTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.shipmentTrackingTableView.isHidden = dataCount == 0
            self.shipmentTrackingNoDataView.isHidden = dataCount != 0
            self.shipmentTrackingTableView.reloadData()
        }
    }
    
    private func setUpUpcomingDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let dataCount = self.importEventData.count
            
            self.upcomingEventTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.upcomingEventDateDetailView.isHidden = dataCount == 0
            if dataCount == 0 {
                DispatchQueue.main.async {
                    self.upcomingEventTableHeight.constant = getDynamicHeight(size: 60)
                }
            }
            self.upcomingEventTableView.isHidden = dataCount == 0
            self.upcomingEventNoDataView.isHidden = dataCount != 0
            self.upcomingEventTableView.reloadData()
        }
    }
    
    private func setUpChartData() {
        let dataCount = allImportData.pieChart?.count ?? 0
        
        DispatchQueue.main.async { [weak self] in
            self?.mainChartContainerHeight.constant = dataCount == 0 ? getDynamicHeight(size: 60) : getDynamicHeight(size: 300)
        }
        
        barChartView.isHidden = dataCount == 0
        chartNoDataView.isHidden = dataCount != 0
        
        if dataCount != 0 {
            let allChartData = allImportData.pieChart!
            barChartView.isUserInteractionEnabled = false
            barChartView.rightAxis.enabled = false
            
            let xAxis = barChartView.xAxis
            xAxis.labelFont = UIFont(name: AppCenturyFont.Regular.Name(), size: getDynamicHeight(size: 10))!
            xAxis.labelCount = dataCount
            xAxis.decimals = 0
//            xAxis.granularityEnabled = true
//            xAxis.granularity = 1
            xAxis.labelPosition = .bottom
            xAxis.wordWrapEnabled = true
            xAxis.labelRotationAngle = dataCount > 4 ? -(CGFloat(dataCount)*3 > 45 ? 45 : CGFloat(dataCount)*3) : 0
            xAxis.drawGridLinesEnabled = false
            
            let leftYAxis = barChartView.leftAxis
            leftYAxis.decimals = 0
            leftYAxis.axisMinimum = 0
            
            var entries = [BarChartDataEntry]()
            var allMonths = [String]()
            var maxTotal = Double()
            
            for i in 0..<dataCount {
                entries.append(BarChartDataEntry(x: Double(i), y: allChartData[i]!.total))
                allMonths.append(allChartData[i]!.newDate)
                maxTotal = (maxTotal > allChartData[i]!.total) ? maxTotal : allChartData[i]!.total
            }
            
            xAxis.valueFormatter = IndexAxisValueFormatter(values: allMonths)
            leftYAxis.granularity = maxTotal/2
            
            let set = BarChartDataSet(entries: entries, label: "Total Orders")
            set.colors = [NSUIColor(cgColor: UIColor.ourAppThemeColor.cgColor)]
            
            let chartData = BarChartData(dataSet: set)
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.barChartView.data = chartData
            }
        }
    }
    
    
    // MARK: - API Calling Method
    /// **Import Dashboard API Calling**
    func call_Import_Dashboard_API(fromDrop: Bool = false) {
        let param = ["monthly": selectedMonthOrYear.associatedValue,
                     "event_month": selectedEventMonth]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.IMPORT_DASHBOARD.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false,
                                            senderView: fromDrop ? dropChartYearlyType : nil)
        { [weak self] (isSuccess, response: Import_Dashboard_Data_Modal?, errorMessage) in
            guard let self = self else { return }
//            debugPrint("IMPORT DASH RESPONSE  == \(response)")
            debugPrint("IMPORT DASH ERROR  == \(isSuccess) REASON = \(String(describing: errorMessage))")
            if isSuccess, let response {
                guard let data = response.data else {
//                    let alert = UIAlertController(title: "Oops!", message: "Something Went Wrong!\nPlease Restart App!", preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "Restart App", style: .destructive, handler: { _ in
//                        exit(0)
//                    }))
//                    self.present(alert, animated: true)0
                    return
                }
                self.allImportData = data
                self.importEventData = data.eventList
                if fromDrop { HapticFeedbackGenerator.notificationFeedback(type: .success) }
                self.setUIWithData()
            } else {
                if self.tryReloadAPICount == 0 || self.tryReloadAPICount == 1 {
                    self.tryReloadAPICount += 1
                    self.call_Import_Dashboard_API()
                } else {
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                    debugPrint("Error Fetching Import Dashboard Data!")
                    appView.makeToast("Error Fetching Import Dashboard Data!")
                }
            }
        }
    }
    
    /// **Upcoming Event Detail API Calling Method For Selected Month**
    private func call_UpcomingEvent_API(fromDrop: Bool = false) {
        let param = ["month" : selectedEventMonth]
        
        NetworkClient.encodedNetworkRequest(method: .post, apiName: APIConstants.EVENT_LIST_IMPORT.fullPath(),
                                            param: param, headers: [default_app_header], showHud: false, senderView: fromDrop ? dropUpcomingEventType : nil)
        { [weak self] (isSuccess, response: Event_List_Import_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                guard let eventData = response.data else { return }
                self.importEventData = eventData.sorted{ $0?.start ?? "N/A" < $1?.start ?? "N/A" }
                self.setUpUpcomingDataInUI()
            } else {
                appView.makeToast("Error Fetching Upcoming Event List!")
            }
        }
    }
}

// MARK: - TableView Delegate Methods
extension Import_Dashboard_VC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let allImportData = allImportData else { return 0 }
        if tableView == inTransitTableView {    // In-Transit TableView
            switch selecteIntransitType {
            case .invoice:
                switch selectedDateRange {
                case .thirtyDay: return allImportData.invoice30D == nil ? 0 : allImportData.invoice30D!.count+1;
                case .thirtyToSixty: return allImportData.invoice60D == nil ? 0 : allImportData.invoice60D!.count+1
                case .sixtyPlus: return allImportData.invoice60DPlus == nil ? 0 : allImportData.invoice60DPlus!.count+1
                }
            case .atBank:
                switch selectedDateRange {
                case .thirtyDay: return allImportData.atBank30D == nil ? 0 : allImportData.atBank30D!.count+1
                case .thirtyToSixty: return allImportData.atBank60D == nil ? 0 : allImportData.atBank60D!.count+1
                case .sixtyPlus: return allImportData.atBank60DPlus == nil ? 0 : allImportData.atBank60DPlus!.count+1
                }
            case .inClearing:
                switch selectedDateRange {
                case .thirtyDay: return allImportData.inClearing30D == nil ? 0 : allImportData.inClearing30D!.count+1
                case .thirtyToSixty: return allImportData.inClearing60D == nil ? 0 : allImportData.inClearing60D!.count+1
                case .sixtyPlus: return allImportData.inClearing60DPlus == nil ? 0 : allImportData.inClearing60DPlus!.count+1
                }
            case .expected:
                switch selectedDateRange {
                case .thirtyDay: return allImportData.expectedToArrive30D == nil ? 0 : allImportData.expectedToArrive30D!.count+1
                case .thirtyToSixty: return allImportData.expectedToArrive60D == nil ? 0 : allImportData.expectedToArrive60D!.count+1
                case .sixtyPlus: return allImportData.expectedToArrive60DPlus == nil ? 0 : allImportData.expectedToArrive60DPlus!.count+1
                }
            }
        } else if tableView == pendingOrderTableView {     // Pending Order TableView
            switch selectedPendingOption {
            case .fullyNotDispatch: return allImportData.pendingOrderFullyNotDispatchData == nil ? 0 : allImportData.pendingOrderFullyNotDispatchData!.count+1
            case .partiallyDispatch: return allImportData.pendingOrderPartiallyDispatchData == nil ? 0 : allImportData.pendingOrderPartiallyDispatchData!.count+1
            }
        } else if tableView == closedOrderTableView {       // Closed Order TableView
            debugPrint("CLOSED COUNT --- \(allImportData.closeOrder!.count+1)")
            return allImportData.closeOrder == nil ? 0 : allImportData.closeOrder!.count+1
        } else if tableView == licenceInfoTableView {       // Licence Info. TableView
            debugPrint("LICENCE COUNT --- \(allImportData.licenceInformation!.count+1)")
            return allImportData.licenceInformation == nil ? 0 : allImportData.licenceInformation!.count+1
        } else if tableView == upcomingEventTableView {        // Upcoming Event TableView
            debugPrint("EVENT COUNT --- \(importEventData.count)")
            return importEventData == nil ? 0 : importEventData!.count
        } else {    // Shipment Tracking TableView
            return allImportData.shipmentTracking == nil ? 0 : allImportData.shipmentTracking!.count+1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == upcomingEventTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: Upcoming_Event_TableCell.identifier, for: indexPath) as! Upcoming_Event_TableCell
            if let arrData = importEventData, let eventData = arrData[indexPath.row], let eventDateStr = eventData.start {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let dataDate = formatter.date(from: eventDateStr)!
                cell.setAllData(day: dataDate.getDayInShort(), date: dataDate.getOnlyDate(), event: eventData.title)
                DispatchQueue.main.async { [weak self] in
                    self?.upcomingEventTableHeight.constant = tableView.contentSize.height
                }
            } else {
                debugPrint("No Upcoming Event Import Data Available!")
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Four_Column_TableCell.identifier, for: indexPath) as! Four_Column_TableCell
            if tableView == inTransitTableView {        //In-Transit TableView
                if indexPath.row == 0 {         // Heading Row
                    cell.configureCellType(importHeadingType: .InTransit);
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    var showableData: [Intransit_Import_Data?]? = nil
                    switch selecteIntransitType {
                    case .invoice:
                        
                        switch selectedDateRange {
                        case .thirtyDay:
                            showableData = allImportData.invoice30D
                        case .thirtyToSixty:
                            showableData = allImportData.invoice60D
                        case .sixtyPlus:
                            showableData = allImportData.invoice60DPlus
                        }
                        
                    case .atBank:
                        
                        switch selectedDateRange {
                        case .thirtyDay:
                            showableData = allImportData.atBank30D
                        case .thirtyToSixty:
                            showableData = allImportData.atBank60D
                        case .sixtyPlus:
                            showableData = allImportData.atBank60DPlus
                        }
                        
                    case .inClearing:
                        
                        switch selectedDateRange {
                        case .thirtyDay:
                            showableData = allImportData.inClearing30D
                        case .thirtyToSixty:
                            showableData = allImportData.inClearing60D
                        case .sixtyPlus:
                            showableData = allImportData.inClearing60DPlus
                        }
                        
                    case .expected:
                        
                        switch selectedDateRange {
                        case .thirtyDay:
                            showableData = allImportData.expectedToArrive30D
                        case .thirtyToSixty:
                            showableData = allImportData.expectedToArrive60D
                        case .sixtyPlus:
                            showableData = allImportData.expectedToArrive60DPlus
                        }
                    }
                    if let arrData = showableData, let mainData = arrData[indexPath.row-1] {        // -1 to remove Heading Cell Data Count
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        let finalAmount = (mainData.total.flatMap { $0.cleanValue } ?? "") + " " + (mainData.currency ?? "")
                        cell.setAllData(data1: mainData.invoice, data2: mainData.invoiceDate,
                                        data3: mainData.qty.flatMap { $0.cleanValue } , data4: finalAmount)
                        DispatchQueue.main.async { [weak self] in
                            self?.inTransitTableHeight.constant = tableView.contentSize.height
                            self?.inTransitTableView.layer.cornerRadius = 8
                            self?.inTransitTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.inTransitTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No InTransit Data Available!")
                    }
                    return cell
                }
            } else if tableView == pendingOrderTableView {      // Pending Order TableView
                if indexPath.row == 0 {         // Heading Row
                    cell.configureCellType(importHeadingType: .Pending)
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    var showableData: [Pending_Order_Import_Data?]? = nil
                    switch selectedPendingOption {
                    case .fullyNotDispatch: showableData = allImportData.pendingOrderFullyNotDispatchData
                    case .partiallyDispatch: showableData = allImportData.pendingOrderPartiallyDispatchData
                    }
                    
                    if let arrData = showableData, let mainData = arrData[indexPath.row-1] {    // -1 to remove Heading Cell Data Count
                        let finalAmount = (mainData.total.flatMap { $0.cleanValue } ?? "") + " " + (mainData.currency ?? "")
                        cell.setAllData(data1: mainData.poNo, data2: mainData.poDate,
                                        data3: mainData.poQty.flatMap { $0.cleanValue }, data4: finalAmount)
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        DispatchQueue.main.async { [weak self] in
                            self?.pendingOrderTableHeight.constant = tableView.contentSize.height
                            self?.pendingOrderTableView.layer.cornerRadius = 8
                            self?.pendingOrderTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.pendingOrderTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No Pending Data Available!")
                    }
                    return cell
                }
            } else if tableView == closedOrderTableView {       // Closed Order TableView
                if indexPath.row == 0 {         // Heading Row
                    cell.configureCellType(importHeadingType: .Closed)
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    if let arrData = allImportData.closeOrder, let mainData = arrData[indexPath.row-1] {    // -1 to remove Heading Cell Data Count
                        let finalAmount = (mainData.price.flatMap { $0.cleanValue } ?? "") + " " + (mainData.currency ?? "")
                        cell.setAllData(data1: mainData.poNo, data2: mainData.date,
                                        data3: mainData.qty.flatMap { $0.cleanValue }, data4: finalAmount)
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        DispatchQueue.main.async { [weak self] in
                            self?.closedOrderTableHeight.constant = tableView.contentSize.height
                            self?.closedOrderTableView.layer.cornerRadius = 8
                            self?.closedOrderTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.closedOrderTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No Closed Order Data Available!")
                    }
                    return cell
                }
            } else if tableView == licenceInfoTableView {        // Licence Information TableView
                if indexPath.row == 0 {        // Heading Row
                    cell.configureCellType(importHeadingType: .Licence)
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    if let arrData = allImportData.licenceInformation, let mainData = arrData[indexPath.row-1] {    // -1 to remove Heading Cell Data Count
                        cell.setAllData(data1: mainData.licenceNo, data2: mainData.licenceDate,
                                        data3: mainData.licenseValue.flatMap { $0.cleanValue }, data4: mainData.balance.flatMap { $0.cleanValue })
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        DispatchQueue.main.async { [weak self] in
                            self?.licenceInfoTableHeight.constant = tableView.contentSize.height
                            self?.licenceInfoTableView.layer.cornerRadius = 8
                            self?.licenceInfoTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.licenceInfoTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No Licence Information Data Available!")
                    }
                    return cell
                }
            } else {        // Shipment Tracking TableView
                if indexPath.row == 0 {        // Heading Row
                    cell.configureCellType(importHeadingType: .ShipmentTrack)
                    cell.three_ColumnView(required: true)
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    if let arrData = allImportData.shipmentTracking, let mainData = arrData[indexPath.row-1] {    // -1 to remove Heading Cell Data Count
                        cell.three_ColumnView(required: true)
                        cell.setAllData(data1: mainData.vendorName, data2: mainData.blNumber,
                                        data3: mainData.etaByTracking, data4: nil)
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        DispatchQueue.main.async { [weak self] in
                            self?.shipmentTrackingTableHeight.constant = tableView.contentSize.height
                            self?.shipmentTrackingTableView.layer.cornerRadius = 8
                            self?.shipmentTrackingTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.shipmentTrackingTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No Shipment Tracking Data Available!")
                    }
                    return cell
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView != upcomingEventTableView {
            if indexPath.row == 0 {
                return getDynamicHeight(size: 40)
            }
            return getDynamicHeight(size: 35)
        }
        return UITableView.automaticDimension
    }
}

extension Double {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%.2f", self)
    }
}
