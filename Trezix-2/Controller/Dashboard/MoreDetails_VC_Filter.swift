//
//  MoreDetails_VC_Filter.swift
//  Trezix-2
//
//  Created by Amar Panchal on 21/07/23.
//

import UIKit

extension MoreDetails_VC {
    
    @IBAction func show_Hide_Filter_Action(_ sender: UIButton) {
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        if selectedType == .ImportShipmentTracking {
            if !isFilterVisible {
                self.import_Filter_View.isHidden = false
            }
            UIView.transition(with: import_Filter_View, duration: 0.2, options: [.curveEaseInOut]) { [weak self] in
                guard let self = self else { return }
                self.import_FilterView_Height.constant = self.isFilterVisible ? 0 : 100
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                self.view.layoutSubviews()
            } completion: { [weak self] _ in
                guard let self = self else { return }
                self.isFilterVisible = !self.isFilterVisible
                if !self.isFilterVisible {
                    self.import_Filter_View.isHidden = true
                }
            }
        } else if selectedType == .ExportShipmentTracking {
            if !isFilterVisible {
                self.export_Filter_View.isHidden = false
            }
            UIView.transition(with: export_Filter_View, duration: 0.2, options: [.curveEaseInOut]) { [weak self] in
                guard let self = self else { return }
                self.export_FilterView_Height.constant = self.isFilterVisible ? 0 : 100
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                self.view.layoutSubviews()
            } completion: { [weak self] _ in
                guard let self = self else { return }
                self.isFilterVisible = !self.isFilterVisible
                if !self.isFilterVisible {
                    self.export_Filter_View.isHidden = true
                }
            }
        } else {
            debugPrint("Unexpected Behaviour Detected!")
        }
    }
    
    func open_Calendar(firstDateSelection: Bool) {
        btnDonePicker.setTitle(firstDateSelection ? "Next" : "Done", for: .normal)
        lblPickerTitle.text = firstDateSelection ? "Select Start Date" : "Select End Date"
        datePickerContentView.show_With_Popup_Effect()
        if firstDateSelection {
            fromDate = nil
            toDate = nil
            datePicker.minimumDate = nil
            mainBlurr.showBlurrAnimation(behind: datePickerContentView)
        }
    }
    
    @IBAction func cancel_Calendar_Action(_ sender: Any) {
        datePickerContentView.dismiss_With_Popup_Effect()
        mainBlurr.hideBlurrEffect()
        fromDate = nil
        toDate = nil
        datePicker.minimumDate = nil
        if selectedType == .ImportShipmentTracking {
            drop_Import_ETA.selectedIndex = 0
            drop_Import_ETA.text = drop_Import_ETA.optionArray.first
            selectedImportETA = .all
        } else {
            drop_Export_ETA.selectedIndex = 0
            drop_Export_ETA.text = drop_Export_ETA.optionArray.first
            selectedExportETA = .all
        }
    }
    
    @IBAction func done_Calendar_Action(_ sender: UIButton) {
        if fromDate == nil {    // From Date Selection
            fromDate = datePicker.date
            datePicker.minimumDate = fromDate
            datePicker.setDate(Date(), animated: true)
            open_Calendar(firstDateSelection: false)
        } else {    // To Date Selection
            toDate = datePicker.date
            datePicker.setDate(Date(), animated: false)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            if selectedType == .ImportShipmentTracking {
                drop_Import_ETA.text = formatter.string(from: fromDate) + " to " + formatter.string(from: toDate)
            } else if selectedType == .ExportShipmentTracking {
                drop_Export_ETA.text = formatter.string(from: fromDate) + " to " + formatter.string(from: toDate)
            } else {
                debugPrint("Unexpected Behaviour..")
            }
            datePickerContentView.dismiss_With_Popup_Effect()
            mainBlurr.hideBlurrEffect()
        }
    }
    
    @IBAction func apply_Import_Filters(_ sender: UIButton) {
        sender.animateLoader()
        Task {
            await apply_All_Import_Filters()
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                sender.remove_Activity_Animator {
                    self.shipmentTable.reloadData()
                    self.shipmentTable.isHidden = self.filtered_ImportShipment_Data.isEmpty
                    self.noDataView.isHidden = !self.filtered_ImportShipment_Data.isEmpty
                }
            }
        }
    }
    
    @IBAction func apply_Export_Filters(_ sender: UIButton) {
        sender.animateLoader()
        Task {
            await apply_All_Export_Filters()
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                sender.remove_Activity_Animator {
                    self.shipmentTable.reloadData()
                    self.shipmentTable.isHidden = self.filtered_ExportShipment_Data.isEmpty
                    self.noDataView.isHidden = !self.filtered_ExportShipment_Data.isEmpty
                }
            }
        }
    }
    
    
    func setUp_All_Import_Dropdowns() {
        [drop_Import_Vendors, drop_Import_ETA, drop_Import_BL_Numbers].forEach { dropDown in
            dropDown?.arrowColor = .ourAppThemeColor
            dropDown?.hideOptionsWhenSelect = true
            dropDown?.isSearchEnable = false
            dropDown?.handleKeyboard = true
            dropDown?.checkMarkEnabled = false
            dropDown?.selectedIndex = 0
            
            dropDown?.selectedRowColor = .ourAppThemeColor
            dropDown?.selectedIndex = 0
            dropDown?.rowHeight = getDynamicHeight(size: 30)
            
            if dropDown == drop_Import_Vendors || dropDown == drop_Import_BL_Numbers {
                dropDown?.isOutsideTouchAllowed = false     // Should be false to work properly
                dropDown?.isMultipleSelectionEnabled = true
            }
            
            if dropDown == drop_Import_ETA {
                dropDown?.didSelect(completion: { [weak self] selectedText, index, id in
                    guard let self = self else { return }
                    self.selectedImportETA = All_ETA_Times.allCases[index]
                    if self.selectedImportETA == .custom {    // Custom Date Selection
                        self.open_Calendar(firstDateSelection: true)
                    }
                })
            } else {
                dropDown?.didMultiSelect({ [weak self] selectedTexts, indexes, ids, isCanceled in
                    guard let self = self else { return }
                    if dropDown == self.drop_Import_Vendors {
                        self.selectedImportVendors = isCanceled ? [] : selectedTexts
                    } else {
                        self.selectedImportBLs = isCanceled ? [] : selectedTexts
                    }
                })
            }
        }
    }
    
    func setUp_All_Export_Dropdowns() {
        [drop_Export_Delayed, drop_Export_BL_Number, drop_Export_ETA].forEach { dropDown in
            dropDown?.arrowColor = .ourAppThemeColor
            dropDown?.hideOptionsWhenSelect = true
            dropDown?.isSearchEnable = false
            dropDown?.handleKeyboard = true
            dropDown?.checkMarkEnabled = false
            dropDown?.selectedIndex = 0
            
            dropDown?.selectedRowColor = .ourAppThemeColor
            dropDown?.selectedIndex = 0
            dropDown?.rowHeight = getDynamicHeight(size: 30)
            
            if dropDown == drop_Export_Delayed || dropDown == drop_Export_BL_Number {
                dropDown?.isOutsideTouchAllowed = false     // Should be false to work properly
                dropDown?.isMultipleSelectionEnabled = true
            }
            
            if dropDown == drop_Export_ETA {
                dropDown?.didSelect(completion: { [weak self] selectedText, index, id in
                    guard let self = self else { return }
                    self.selectedExportETA = All_ETA_Times.allCases[index]
                    if self.selectedExportETA == .custom {    // Custom Date Selection
                        self.open_Calendar(firstDateSelection: true)
                    }
                })
            } else {
                dropDown?.didMultiSelect({ [weak self] selectedTexts, indexes, ids, isCanceled in
                    guard let self = self else { return }
                    if dropDown == self.drop_Export_Delayed {
                        self.selectedExportDelays = isCanceled ? [] : selectedTexts
                    } else {
                        self.selectedExportBLs = isCanceled ? [] : selectedTexts
                    }
                })
            }
        }
    }
    
    private func apply_All_Import_Filters() async {
        var tempData = import_ShipmentTracking_Data
        
        
        if !selectedImportVendors.isEmpty {
            tempData = tempData.filter { temp in
                return selectedImportVendors.contains(temp.vendorName ?? "")
            }
        }
        
        if !selectedImportBLs.isEmpty {
            tempData = tempData.filter { temp in
                return selectedImportBLs.contains(temp.blNumber ?? "")
            }
        }
        
        tempData = await apply_Import_Date(filter: selectedImportETA, data: tempData)
        
        filtered_ImportShipment_Data = tempData
    }
    
    private func apply_All_Export_Filters() async {
        var tempData = export_ShipmentTracking_Data
        if !selectedExportDelays.isEmpty {
            tempData = tempData.filter {
                return selectedExportDelays.contains($0.delayedBy ?? "")
            }
        }
        
        if !selectedExportBLs.isEmpty {
            tempData = tempData.filter {
                return selectedExportBLs.contains($0.blNo ?? "")
            }
        }
        
        tempData = await apply_Export_Date(filter: selectedExportETA, data: tempData)
        filtered_ExportShipment_Data = tempData
    }
    
    private func apply_Import_Date(filter: All_ETA_Times, data: [ShipmentTracking_Import_Data]) async -> [ShipmentTracking_Import_Data] {
        var resultData = [ShipmentTracking_Import_Data]()
        switch filter {
        case .all:
            resultData = data
        case .sevenDays:
            resultData = data.compactMap {
                guard let comparableDate = getDateFrom($0.etaByTracking) else { return nil }
                guard let difference = Calendar.current.dateComponents([.day], from: comparableDate, to: Date()).day else { return nil }
                debugPrint("Difference is \(difference) and comparableDate is \(comparableDate.getOnlyDate()) and today \(Date())")
                if difference <= 7 { return $0 } else { return nil }
            }
        case .fifteenDays:
            resultData = data.compactMap {
                guard let comparableDate = getDateFrom($0.etaByTracking) else { return nil }
                guard let difference = Calendar.current.dateComponents([.day], from: comparableDate, to: Date()).day else { return nil }
                debugPrint("Difference is \(difference) and comparableDate is \(comparableDate)")
                if difference <= 15 { return $0 } else { return nil }
            }
        case .fifteenPlusDays:
            resultData = data.compactMap {
                guard let comparableDate = getDateFrom($0.etaByTracking) else { return nil }
                guard let difference = Calendar.current.dateComponents([.day], from: comparableDate, to: Date()).day else { return nil }
                debugPrint("Difference is \(difference) and comparableDate is \(comparableDate)")
                if difference > 15 { return $0 } else { return nil }
            }
        case .custom:
            resultData = data.compactMap {
                guard let comparableDate = getDateFrom($0.etaByTracking) else { return nil }
                guard let startDate = Calendar.current.date(byAdding: .day, value: -1, to: fromDate) else { return nil }
                if (min(startDate, toDate) ... max(startDate, toDate)).contains(comparableDate) { return $0 }
                else { return nil }
            }
        }
        return resultData
    }
    
    private func apply_Export_Date(filter: All_ETA_Times, data: [ShipmentTracking_Export_Data]) async -> [ShipmentTracking_Export_Data] {
        var resultData = [ShipmentTracking_Export_Data]()
        switch filter {
        case .all:
            resultData = data
        case .sevenDays:
            resultData = data.compactMap {
                guard let comparableDate = getDateFrom($0.etaByTracking) else { return nil }
                guard let difference = Calendar.current.dateComponents([.day], from: comparableDate, to: Date()).day else { return nil }
                debugPrint("Difference is \(difference) and comparableDate is \(comparableDate.getOnlyDate()) and today \(Date())")
                if difference <= 7 { return $0 } else { return nil }
            }
        case .fifteenDays:
            resultData = data.compactMap {
                guard let comparableDate = getDateFrom($0.etaByTracking) else { return nil }
                guard let difference = Calendar.current.dateComponents([.day], from: comparableDate, to: Date()).day else { return nil }
                debugPrint("Difference is \(difference) and comparableDate is \(comparableDate)")
                if difference <= 15 { return $0 } else { return nil }
            }
        case .fifteenPlusDays:
            resultData = data.compactMap {
                guard let comparableDate = getDateFrom($0.etaByTracking) else { return nil }
                guard let difference = Calendar.current.dateComponents([.day], from: comparableDate, to: Date()).day else { return nil }
                debugPrint("Difference is \(difference) and comparableDate is \(comparableDate)")
                if difference > 15 { return $0 } else { return nil }
            }
        case .custom:
            resultData = data.compactMap {
                guard let comparableDate = getDateFrom($0.etaByTracking) else { return nil }
                guard let startDate = Calendar.current.date(byAdding: .day, value: -1, to: fromDate) else { return nil }
                if (min(startDate, toDate) ... max(startDate, toDate)).contains(comparableDate) { return $0 }
                else { return nil }
            }
        }
        return resultData
    }
    
    
    private func getDateFrom(_ strDate: String?) -> Date? {
        guard let strDate else { return nil }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: strDate)
    }
    
}
