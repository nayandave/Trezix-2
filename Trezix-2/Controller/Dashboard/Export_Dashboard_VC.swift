//
//  Export_Dashboard_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 27/01/23.
//

import UIKit
import Charts

protocol ChangeNotificationProtocol: AnyObject {
    func changeNotiCount(notiCount: Int, approvalNotiCount: Int)
}
class Export_Dashboard_VC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var mainStackView: UIStackView!
    
    // Open Orders Section Outlets
    @IBOutlet weak var openOrderContainerView: UIView!
    @IBOutlet weak var openOrderStackView: UIStackView!
    @IBOutlet weak var openOrderHeadingView: UIView!
    
    @IBOutlet weak var openOrderDetailView: UIView!
    @IBOutlet weak var dropOpenOrderType: DropDown!
    @IBOutlet weak var btnOpenOrderViewDetail: UIButton!
    
    @IBOutlet weak var openOrderTableContainerView: UIView!
    @IBOutlet weak var openOrderTableView: UITableView!
    @IBOutlet weak var openOrderNoDataView: UIView!
    @IBOutlet weak var openOrderLabelNoData: UILabel!
    @IBOutlet weak var openOrderTableHeight: NSLayoutConstraint!
    
    // Shipment Planning Section Outlets
    @IBOutlet weak var shipmentPlanningContainerView: UIView!
    @IBOutlet weak var shipmentPlanningStackView: UIStackView!
    @IBOutlet weak var shipmentPlanningHeadingView: UIView!
    
    @IBOutlet weak var shipmentPlanningDetailView: UIView!
    @IBOutlet weak var dropShipmentPlanningType: DropDown!
    @IBOutlet weak var btnShipmentPlanningViewDetail: UIButton!
    
    @IBOutlet weak var shipmentPlanningTableContainerView: UIView!
    @IBOutlet weak var shipmentPlanningTableView: UITableView!
    @IBOutlet weak var shipmentPlanningNoDataView: UIView!
    @IBOutlet weak var shipmentPlanningLabelNoData: UILabel!
    @IBOutlet weak var shipmentPlanningTableHeight: NSLayoutConstraint!
    
    
    // Complete Order Section Outlets
    @IBOutlet weak var completeOrderContainerView: UIView!
    @IBOutlet weak var completeOrderStackView: UIStackView!
    @IBOutlet weak var completeOrderHeadingView: UIView!
    
    @IBOutlet weak var completeOrderDetailView: UIView!
    @IBOutlet weak var btnCompleteOrderViewDetail: UIButton!
    
    @IBOutlet weak var completeOrderTableContainerView: UIView!
    @IBOutlet weak var completeOrderTableView: UITableView!
    @IBOutlet weak var completeOrderNoDataView: UIView!
    @IBOutlet weak var completeOrderLabelNoData: UILabel!
    @IBOutlet weak var completeOrderTableHeight: NSLayoutConstraint!
    
    // Licence Info Section Outlets
    @IBOutlet weak var dispatchPlanningContainerView: UIView!
    @IBOutlet weak var dispatchPlanningStackView: UIStackView!
    @IBOutlet weak var dispatchPlanningHeadingView: UIView!
    
    @IBOutlet weak var dispatchPlanningDetailView: UIView!
    @IBOutlet weak var btnDispatchPlanningViewDetail: UIButton!
    
    @IBOutlet weak var dispatchPlanningTableContainerView: UIView!
    @IBOutlet weak var dispatchPlanningTableView: UITableView!
    @IBOutlet weak var dispatchPlanningNoDataView: UIView!
    @IBOutlet weak var dispatchPlanningLabelNoData: UILabel!
    @IBOutlet weak var dispatchPlanningTableHeight: NSLayoutConstraint!
    
    // Upcoming Event Section Outlets
    @IBOutlet weak var upcomingEventContainerView: UIView!
    @IBOutlet weak var upcomingEventStackView: UIStackView!
    @IBOutlet weak var upcomingEventHeadingView: UIView!
    @IBOutlet weak var dropUpcomingEventType: DropDown!
    
    @IBOutlet weak var upcomingEventDetailView: UIView!
    @IBOutlet weak var btnUpcomingEventViewDetail: UIButton!
    
    @IBOutlet weak var upcomingEventTableContainerView: UIView!
    @IBOutlet weak var upcomingEventTableView: UITableView!
    @IBOutlet weak var upcomingEventNoDataView: UIView!
    @IBOutlet weak var upcomingEventLabelNoData: UILabel!
    @IBOutlet weak var upcomingEventTableHeight: NSLayoutConstraint!
    
    
    // Shipment Tracking Outlets
    @IBOutlet weak var shipmentTrackingContainerView: UIView!
    @IBOutlet weak var shipmentTrackingStackView: UIStackView!
    @IBOutlet weak var shipmentTrackingHeadingView: UIView!
    
    @IBOutlet weak var shipmentTrackingDateDetailView: UIView!
    @IBOutlet weak var btnShipmentTrackingViewDetail: UIButton!
    
    @IBOutlet weak var shipmentTrackingTableContainerView: UIView!
    @IBOutlet weak var shipmentTrackingTableView: UITableView!
    @IBOutlet weak var shipmentTrackingNoDataView: UIView!
    @IBOutlet weak var shipmentTrackingLabelNoData: UILabel!
    @IBOutlet weak var shipmentTrackingTableHeight: NSLayoutConstraint!
    
    // Revenu Chart Section Outlets
    @IBOutlet weak var chartContainerView: UIView!
    @IBOutlet weak var dropChartYearlyType: DropDown!
    @IBOutlet weak var chartHeadingView: UIView!
    @IBOutlet weak var mainChartContainerView: UIView!
    @IBOutlet weak var mainChartContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var chartNoDataView: UIView!
    @IBOutlet weak var barChartView: BarChartView!
    
    // MARK: - All Constants & Variables
    
    weak var openMonthPickerDelegate: OpenMonthPickerProtocol?
    weak var notiDelegate: ChangeNotificationProtocol?
    private var observer: NSKeyValueObservation?
    var exportStackHeight = CGFloat()
    
    enum All_OpenOrder_Type: String, CaseIterable {
        case partiallyDeliver = "Partially Delivered"
        case fullPending = "Full Pending Order"
    }
    private var selectedOpenOrderType: All_OpenOrder_Type = .partiallyDeliver
    
    enum All_ShipmentPlanning_Type: String, CaseIterable {
        case billing = "Billing Planning"
        case dispatch = "Dispatch Planning"
    }
    private var selectedShipmentType: All_ShipmentPlanning_Type = .billing
    
    
    enum All_Chart_Types: String, CaseIterable {
        case halfYear = "6 months"
        case fullYear = "Full Year"
        
        var associatedValue: String {
            switch self {
            case .halfYear: return "monthly";
            case .fullYear: return "yearly";
            }
        }
    }
    
    private var selectedEventMonth = String()
    private var selectedMonthOrYear: All_Chart_Types = .halfYear
    
    private var allTableView = [UITableView]()
    private var allDropdown = [DropDown]()
    
    private var allExportData: Export_Dashboard_Data! = nil
    private var exportEventData: [EventList_Export_Data?]! = nil
    
    // MARK: - View Life Cycle Method
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        debugPrint("Export ViewWill Appear")
//        DispatchQueue.global(qos: .utility).async { [weak self] in
//            self?.call_Export_Dashboard_API()
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        debugPrint("Export ViewWill Disappear")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allTableView = [openOrderTableView, shipmentPlanningTableView, completeOrderTableView, dispatchPlanningTableView, upcomingEventTableView, shipmentTrackingTableView]
        allDropdown = [dropOpenOrderType, dropShipmentPlanningType, dropUpcomingEventType, dropChartYearlyType]
        setUpHomePage()
        calculateCurrentEventMonth()
        NotificationCenter.default.addObserver(self, selector: #selector(dismiss_MonthPicker(_:)), name: .dismissingMonthPickerNotification, object: nil)
        call_Export_Dashboard_API()
        observer = mainStackView.layer.observe(\.bounds) { [weak self] object, _ in
            self?.exportStackHeight = object.bounds.height
            scrollHeightObject.changingHeight(newHeight: object.bounds.height)
            print("Export Dashboard Height Changing...\(object.bounds)")
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        allDropdown.forEach { if ($0 != dropOpenOrderType && $0 != dropShipmentPlanningType) { $0.layer.applyShadowAndRadius() } }
        [openOrderNoDataView, shipmentPlanningNoDataView, completeOrderNoDataView, dispatchPlanningNoDataView, upcomingEventNoDataView, chartNoDataView].forEach { $0.layer.applyShadowAndRadius() }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        allDropdown.forEach { drop in
            drop.hideList()
        }
    }
    
    // MARK: - Helper Methods    
    @objc private func dismiss_MonthPicker(_ notification: NSNotification) {
        if let data = notification.object as? [String: Any] {
            if !(data["Section"] as! Bool) {     // Export Section Month Picker
                if let selectedMonth = data["Month"] as? String {
                    selectedEventMonth = selectedMonth
                    DispatchQueue.main.async { [weak self] in
                        self?.dropUpcomingEventType.text = selectedMonth
                    }
                    call_Export_UpcomingEvent_API(fromDrop: true)
                }
            }
        } else {
            debugPrint("Data passing through Notification Center went off track!")
        }
    }
    
    /// `Setting Up Homepage`
    private func setUpHomePage() {
        registerTableCells()
        initAllDropDowns()
    }
    
    private func registerTableCells() {
        allTableView.forEach { tableView in
            if tableView == upcomingEventTableView {
                tableView.register(UINib(nibName: Upcoming_Event_TableCell.identifier, bundle: nil), forCellReuseIdentifier: Upcoming_Event_TableCell.identifier)
            } else {
                tableView.register(UINib(nibName: Four_Column_TableCell.identifier, bundle: nil), forCellReuseIdentifier: Four_Column_TableCell.identifier)
            }
        }
    }
    
    private func initAllDropDowns() {
        allDropdown.forEach { dropDown in
            dropDown.arrowColor = .ourAppThemeColor
            dropDown.hideOptionsWhenSelect = true
            dropDown.isSearchEnable = false
            dropDown.handleKeyboard = true
            if (dropDown == dropOpenOrderType || dropDown == dropShipmentPlanningType) {
                dropDown.rowBackgroundColor = UIColor.hexStringToUIColor(hex: "CFDBE2")
            }
            dropDown.selectedRowColor = .ourAppThemeColor
            dropDown.selectedIndex = 0
//            dropDown.listHeight = getDynamicHeight(size: 150)
            dropDown.rowHeight = getDynamicHeight(size: 45)
            dropDown.checkMarkEnabled = false
            
            if dropDown != dropUpcomingEventType {
                dropDown.isCustomTouchEnabled = false
                dropDown.selectedIndex = 0
                if dropDown == dropOpenOrderType {  // Open Order Type
                    dropDown.optionArray = All_OpenOrder_Type.allCases.map { $0.rawValue }
                } else if dropDown == dropShipmentPlanningType {     // Shipment Planning Types
                    dropDown.optionArray = All_ShipmentPlanning_Type.allCases.map { $0.rawValue }
                } else if dropDown == dropChartYearlyType {     // Chart Revenue Types
                    dropDown.optionArray = All_Chart_Types.allCases.map { $0.rawValue }
                }
                dropDown.text = dropDown.optionArray.first
                dropDown.didSelect(completion: { [weak self] selectedText, index, id in
                    guard let self = self else { return }
                    HapticFeedbackGenerator.simpleFeedback(type: .light)
                    if dropDown == self.dropOpenOrderType {  // Open Order Type
                        self.selectedOpenOrderType = All_OpenOrder_Type.allCases[index]
                        self.setUpOpenOrderDataInUI()
                    } else if dropDown == self.dropShipmentPlanningType {     // Shipment Planning Types
                        self.selectedShipmentType = All_ShipmentPlanning_Type.allCases[index]
                        self.setUpShipmentPlanningDataInUI()
                    } else if dropDown == self.dropChartYearlyType {     // Chart Revenue Types
                        self.selectedMonthOrYear = All_Chart_Types.allCases[index]
                        self.call_Export_Dashboard_API(fromDrop: true)
                    }
                    DispatchQueue.main.async {
                        dropDown.text = selectedText
                    }
                })
            } else {
                dropDown.setCustomTouchAction(completion: {
                    self.openMonthPickerDelegate?.openMonthPicker()
                })
            }
        }
    }
    
    private func calculateCurrentEventMonth() {
        let currDate = Date()
        // Calculating Year
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        let year = formatter.string(from: currDate)
        
        // Calculating Month
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MMM"
        let month = formatter2.string(from: currDate)
        
        selectedEventMonth = month + " " + year
        dropUpcomingEventType.text = selectedEventMonth
    }
    
       
    // MARK: - UI Methods
    private func setUIWithData() {
        setUpOpenOrderDataInUI()
        setUpShipmentPlanningDataInUI()
        setUpCompleteOrderDataInUI()
        setUpDispatchPlanningDataInUI()
        setUpUpcomingDataInUI()
        setUpShipmentTrackingDataInUI()
        setUpChartData()
        guard let notiCount = allExportData.unreadTotalNotification,
              let approvalCount = allExportData.totalPendingApprovalNotification else { debugPrint("No Notification Count in Export"); return }
        notiDelegate?.changeNotiCount(notiCount: notiCount, approvalNotiCount: approvalCount)
    }
    
    private func setUpOpenOrderDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            var dataCount = 0
            var totalDataCount = 0
            
            switch self.selectedOpenOrderType {
            case .partiallyDeliver:
                dataCount = self.allExportData.openOrderPartiallyDelivery?.count ?? 0
                totalDataCount = self.allExportData.openOrderPartiallyDeliveryCount ?? 0
            case .fullPending:
                dataCount = self.allExportData.openFullPendingOrder?.count ?? 0
                totalDataCount = self.allExportData.openFullPendingOrderCount ?? 0
            }
            
            self.openOrderTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.openOrderTableView.remove_ShadowView()
            self.btnOpenOrderViewDetail.isHidden = totalDataCount <= 4
            if dataCount == 0 {
                self.openOrderTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.openOrderTableView.isHidden = dataCount == 0
            self.openOrderNoDataView.isHidden = dataCount != 0
            self.openOrderTableView.reloadData()
        }
    }
    
    private func setUpShipmentPlanningDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            var dataCount = 0
            var totalDataCount = 0
            switch self.selectedShipmentType {
            case .billing:
                dataCount = self.allExportData.shipmentBillingPlanning?.count ?? 0
                totalDataCount = self.allExportData.shipmentBillingPlanningCount ?? 0
                break
            case .dispatch:
                dataCount = self.allExportData.shipmentDispatchPlanning?.count ?? 0
                totalDataCount = self.allExportData.shipmentDispatchPlanningCount ?? 0
                break
            }
            
            self.shipmentPlanningTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.shipmentPlanningTableView.remove_ShadowView()
            self.btnShipmentPlanningViewDetail.isHidden = totalDataCount <= 4
            if dataCount == 0 {
                self.shipmentPlanningTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.shipmentPlanningTableView.isHidden = dataCount == 0
            self.shipmentPlanningNoDataView.isHidden = dataCount != 0
            self.shipmentPlanningTableView.reloadData()
        }
    }
    
    private func setUpCompleteOrderDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let dataCount = self.allExportData.completedOrder?.count ?? 0
//            let totalDataCount = (self.allExportData..totalOrders ?? 0) - (self.allImportData.totalPendingOrder ?? 0)
            
            self.completeOrderTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.completeOrderTableView.remove_ShadowView()
            self.completeOrderDetailView.isHidden = dataCount < 4 //totalDataCount <= 4
            if dataCount == 0 {
                self.completeOrderTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.completeOrderTableView.isHidden = dataCount == 0
            self.completeOrderNoDataView.isHidden = dataCount != 0
            self.completeOrderTableView.reloadData()
        }
    }
    
    private func setUpDispatchPlanningDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let dataCount = self.allExportData.dispatchPlanning?.count ?? 0
            
            self.dispatchPlanningTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.dispatchPlanningTableView.remove_ShadowView()
            self.dispatchPlanningDetailView.isHidden = dataCount == 0
            if dataCount == 0 {
                self.dispatchPlanningTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.dispatchPlanningTableView.isHidden = dataCount == 0
            self.dispatchPlanningNoDataView.isHidden = dataCount != 0
            self.dispatchPlanningTableView.reloadData()
        }
    }
    
    private func setUpUpcomingDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let dataCount = self.exportEventData.count
            
            self.upcomingEventTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.upcomingEventDetailView.isHidden = dataCount == 0
            if dataCount == 0 {
                DispatchQueue.main.async {
                    self.upcomingEventTableHeight.constant = getDynamicHeight(size: 60)
                }
            }
            self.upcomingEventTableView.isHidden = dataCount == 0
            self.upcomingEventNoDataView.isHidden = dataCount != 0
            self.upcomingEventTableView.reloadData()
        }
    }
    
    private func setUpShipmentTrackingDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let dataCount = self.allExportData.shipmentDetail?.count ?? 0
            let totalDataCount = self.allExportData.shipmentDetailCount ?? 0
            
            self.shipmentTrackingTableView.layer.applyShadowAndRadius(cornerRadius: 0, opacity: 0, shadowRadius: 0)
            self.shipmentTrackingTableView.remove_ShadowView()
            self.shipmentTrackingDateDetailView.isHidden = totalDataCount <= 4
            if dataCount == 0 {
                self.shipmentTrackingTableHeight.constant = getDynamicHeight(size: 60)
            }
            self.shipmentTrackingTableView.isHidden = dataCount == 0
            self.shipmentTrackingNoDataView.isHidden = dataCount != 0
            self.shipmentTrackingTableView.reloadData()
        }
    }
    
    
    private func setUpChartData() {
        let dataCount = allExportData.pieChart?.count ?? 0
        
        DispatchQueue.main.async { [weak self] in
            self?.mainChartContainerHeight.constant = dataCount == 0 ? getDynamicHeight(size: 60) : getDynamicHeight(size: 300)
        }
        
        barChartView.isHidden = dataCount == 0
        chartNoDataView.isHidden = dataCount != 0
        
        if dataCount != 0 {
            let allChartData = allExportData.pieChart!
            
            barChartView.isUserInteractionEnabled = false
            barChartView.rightAxis.enabled = false
            
            let xAxis = barChartView.xAxis
            xAxis.labelFont = UIFont(name: AppCenturyFont.Regular.Name(), size: getDynamicHeight(size: 10))!
            xAxis.labelCount = dataCount
            xAxis.decimals = 0
//            xAxis.granularityEnabled = true
//            xAxis.granularity = 1
            xAxis.labelPosition = .bottom
            xAxis.wordWrapEnabled = true
            xAxis.labelRotationAngle = dataCount > 4 ? -(CGFloat(dataCount)*3 > 45 ? 45 : CGFloat(dataCount)*3) : 0
            xAxis.drawGridLinesEnabled = false
            
            let leftYAxis = barChartView.leftAxis
            leftYAxis.decimals = 0
            leftYAxis.axisMinimum = 0
            
            var entries = [BarChartDataEntry]()
            var allMonths = [String]()
            var maxTotal = Double()
            
            for i in 0..<dataCount {
                entries.append(BarChartDataEntry(x: Double(i), y: allChartData[i]!.total))
                allMonths.append(allChartData[i]!.newDate)
                maxTotal = (maxTotal > allChartData[i]!.total) ? maxTotal : allChartData[i]!.total
            }
            
            xAxis.valueFormatter = IndexAxisValueFormatter(values: allMonths)
            leftYAxis.granularity = maxTotal/2
            
            let set = BarChartDataSet(entries: entries, label: "Total Orders")
            set.colors = [NSUIColor(cgColor: UIColor.ourAppThemeColor.cgColor)]
            
            let chartData = BarChartData(dataSet: set)
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.barChartView.data = chartData
            }
        }
    }
    
    // MARK: - API Calling Method
    var tryReloadAPICount = 0
    /// **Export Dashboard API Calling**
    func call_Export_Dashboard_API(fromDrop: Bool = false) {
        let param = ["monthly": selectedMonthOrYear.associatedValue,
                     "event_month": selectedEventMonth]
        debugPrint("PARAMETER --- \(param)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.EXPORT_DASHBOARD.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false, senderView: fromDrop ? dropChartYearlyType : nil)
        { [weak self] (isSuccess, response: Export_Dashboard_Data_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                guard let data = response.data else { return }
                self.allExportData = data
                self.exportEventData = data.eventList?.compactMap { $0.value }
//                self.exportEventData = data.eventList
                if fromDrop {
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                }
                self.setUIWithData()
            } else {
                if self.tryReloadAPICount == 0 || self.tryReloadAPICount == 1 {
                    self.tryReloadAPICount += 1
                    self.call_Export_Dashboard_API()
                } else {
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                    debugPrint("Error Fetching Export Dashboard Data!")
                    appView.makeToast("Error Fetching Export Dashboard Data!")
                }
            }
        }
    }
    
    /// **Upcoming Event Detail API Calling Method For Selected Month**
    private func call_Export_UpcomingEvent_API(fromDrop: Bool = false) {
        let param = ["event_month" : selectedEventMonth]
        
        debugPrint("Export PARAMETER = \(param)")
        NetworkClient.encodedNetworkRequest(method: .post, apiName: APIConstants.EVENT_LIST_EXPORT.fullPath(),
                                            param: param, headers: [default_app_header], showHud: false, senderView: fromDrop ? dropUpcomingEventType : nil)
        { [weak self] (isSuccess, response: Event_List_Export_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                guard let eventData = response.data else { debugPrint("Data missing from Upcoming Event Data Response!"); return }
                self.exportEventData = eventData.sorted { $0?.start ?? "N/A" < $1?.start ?? "N/A" }
                self.setUpUpcomingDataInUI()
            } else {
                appView.makeToast("Error Fetching Upcoming Event List!")
            }
        }
    }
    
    // MARK: - All Required Methods
    private func navigate_To_MoreDetail_VC(type: MoreDetails_VC.DetailType, openOrderType: All_OpenOrder_Type? = nil, shipmentType: All_ShipmentPlanning_Type? = nil) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let moreVC = homeStoryboard.instantiateViewController(withIdentifier: "MoreDetails_VC") as! MoreDetails_VC
        
        moreVC.isImport_Section_Selected = false
        moreVC.selectedType = type
        moreVC.selExportOpenOrderType = openOrderType
        moreVC.selExportShipmentType = shipmentType
        moreVC.selectedEventMonth = selectedEventMonth
        
        navigationController?.present(moreVC, animated: true)
    }
    
    // MARK: - All Actions
    @IBAction func openOrder_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail_VC(type: .OpenOrder, openOrderType: selectedOpenOrderType)
    }
    
    @IBAction func shipmentPlanning_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail_VC(type: .ShipmentPlan, shipmentType: selectedShipmentType)
    }
    
    @IBAction func completeOrder_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail_VC(type: .CompleteOrder)
    }
    
    @IBAction func dispatchPlanning_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail_VC(type: .DispatchPlan)
    }
    
    @IBAction func shipment_ViewDetail_Action(_ sender: UIButton) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let moreVC = homeStoryboard.instantiateViewController(withIdentifier: "MoreDetails_VC") as! MoreDetails_VC
        moreVC.isImport_Section_Selected = false
        moreVC.selectedType = .ExportShipmentTracking
        moreVC.refrenceToHomeVC = self.parent
        present(moreVC, animated: true)
    }
    
    @IBAction func upcomingEvents_ViewDetail_Action(_ sender: UIButton) {
        navigate_To_MoreDetail_VC(type: .ExportUpcomingEvent)
    }
}

extension Export_Dashboard_VC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let allExportData = allExportData else { return 0 }
        if tableView == openOrderTableView {    // Open Order TableView
            switch selectedOpenOrderType {
            case .partiallyDeliver: return allExportData.openOrderPartiallyDelivery == nil ? 0 : allExportData.openOrderPartiallyDelivery!.count+1
            case .fullPending: return allExportData.openFullPendingOrder == nil ? 0 : allExportData.openFullPendingOrder!.count+1
            }
        } else if tableView == shipmentPlanningTableView {     // Shipment Planning TableView
            switch selectedShipmentType {
            case .billing: return allExportData.shipmentBillingPlanning == nil ? 0 : allExportData.shipmentBillingPlanning!.count+1
            case .dispatch: return allExportData.shipmentDispatchPlanning == nil ? 0 : allExportData.shipmentDispatchPlanning!.count+1
            }
        } else if tableView == completeOrderTableView {       // Complete Order TableView
            return allExportData.completedOrder == nil ? 0 : allExportData.completedOrder!.count+1
        } else if tableView == dispatchPlanningTableView {       // Dispatch Planning TableView
            return allExportData.dispatchPlanning == nil ? 0 : allExportData.dispatchPlanning!.count+1
        } else if tableView == upcomingEventTableView {        // Upcoming Event TableView
            return exportEventData == nil ? 0 : exportEventData!.count
        } else {        // Shipment Tracking TableView
            return allExportData.shipmentDetail == nil ? 0 : allExportData.shipmentDetail!.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == upcomingEventTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: Upcoming_Event_TableCell.identifier, for: indexPath) as! Upcoming_Event_TableCell
            if let arrData = exportEventData, let eventData = arrData[indexPath.row],
                let eventDateStr = eventData.start {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let dataDate = formatter.date(from: eventDateStr)!
                cell.setAllData(day: dataDate.getDayInShort(), date: dataDate.getOnlyDate(), event: eventData.title)
                DispatchQueue.main.async { [weak self] in
                    self?.upcomingEventTableHeight.constant = tableView.contentSize.height
                }
            } else {
                debugPrint("No Upcoming Event Export Data Available!")
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Four_Column_TableCell.identifier, for: indexPath) as! Four_Column_TableCell
            if tableView == openOrderTableView {    // Open Order TableView
                if indexPath.row == 0 {     // Heading Row
                    cell.configureCellType(exportHeadingType: .OpenOrder)
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    var showableData: [OpenShipmentCompleted_Export_Data?]? = nil
                    switch selectedOpenOrderType {
                    case .partiallyDeliver: showableData = allExportData.openOrderPartiallyDelivery
                    case .fullPending: showableData = allExportData.openFullPendingOrder
                    }
                    
                    if let arrData = showableData, let mainData = arrData[indexPath.row-1] {        // -1 to remove Heading Cell Data Count
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        let finalAmount = (mainData.amount.flatMap { $0.cleanValue } ?? "") + " " + (mainData.currency ?? "")
                        let quantity = (mainData.quantity.flatMap { $0.cleanValue } ?? "") + " " + (mainData.uom ?? "")
                        cell.setAllData(data1: mainData.invoiceNo, data2: mainData.date,
                                        data3: quantity, data4: finalAmount)
                        DispatchQueue.main.async { [weak self] in
                            self?.openOrderTableHeight.constant = tableView.contentSize.height
                            self?.openOrderTableView.layer.cornerRadius = 8
                            self?.openOrderTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.openOrderTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No Open Order Data Available!")
                    }
                    return cell
                }
            } else if tableView == shipmentPlanningTableView {      //Shipment Planning Table
                if indexPath.row == 0 {         // Heading Row
                    cell.configureCellType(exportHeadingType: .ShipmentPlan)
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    var showableData: [OpenShipmentCompleted_Export_Data?]? = nil
                    switch selectedShipmentType {
                    case .billing: showableData = allExportData.shipmentBillingPlanning
                    case .dispatch: showableData = allExportData.shipmentDispatchPlanning
                    }
                    
                    if let arrData = showableData, let mainData = arrData[indexPath.row-1] {        // -1 to remove Heading Cell Data Count
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        let finalAmount = (mainData.amount.flatMap { $0.cleanValue } ?? "") + " " + (mainData.currency ?? "")
                        let quantity = (mainData.quantity.flatMap { $0.cleanValue } ?? "") + " " + (mainData.uom ?? "")
                        cell.setAllData(data1: mainData.invoiceNo, data2: mainData.date,
                                        data3: quantity, data4: finalAmount)
                        DispatchQueue.main.async { [weak self] in
                            self?.shipmentPlanningTableHeight.constant = tableView.contentSize.height
                            self?.shipmentPlanningTableView.layer.cornerRadius = 8
                            self?.shipmentPlanningTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.shipmentPlanningTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No Shipment Data Available!")
                    }
                    return cell
                }
            } else if tableView == completeOrderTableView {     // Complete Order Table
                if indexPath.row == 0 {        // Heading Row
                    cell.configureCellType(exportHeadingType: .CompleteOrder)
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    if let arrData = allExportData.completedOrder, let mainData = arrData[indexPath.row-1] {    // -1 to remove Heading Cell Data Count
                        let finalAmount = (mainData.amount.flatMap { $0.cleanValue } ?? "") + " " + (mainData.currency ?? "")
                        let quantity = (mainData.quantity.flatMap { $0.cleanValue } ?? "") + " " + (mainData.uom ?? "")
                        cell.setAllData(data1: mainData.invoiceNo, data2: mainData.date,
                                        data3: quantity, data4: finalAmount)
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        DispatchQueue.main.async { [weak self] in
                            self?.completeOrderTableHeight.constant = tableView.contentSize.height
                            self?.completeOrderTableView.layer.cornerRadius = 8
                            self?.completeOrderTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.completeOrderTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No Complete Order Data Available!")
                    }
                    return cell
                }
            } else if tableView == dispatchPlanningTableView {        // Dispatch Planning Table
                if indexPath.row == 0 {         // Heading Row
                    cell.three_ColumnView(required: true)
                    cell.configureCellType(exportHeadingType: .DispatchPlan)
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.three_ColumnView(required: true)
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    if let arrData = allExportData.dispatchPlanning, let mainData = arrData[indexPath.row-1] {    // -1 to remove Heading Cell Data Count
                        let quantity = (mainData.netWeight.flatMap { $0.cleanValue } ?? "") + " " + (mainData.uom ?? "")
                        cell.setAllData(data1: mainData.dispatchDate, data2: quantity,
                                        data3: mainData.noOfPi.flatMap { String($0) }, data4: nil)
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        DispatchQueue.main.async { [weak self] in
                            self?.dispatchPlanningTableHeight.constant = tableView.contentSize.height
                            self?.dispatchPlanningTableView.layer.cornerRadius = 8
                            self?.dispatchPlanningTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.dispatchPlanningTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No Closed Order Data Available!")
                    }
                    return cell
                }
            } else {        // Shipment Tracking TableView
                if indexPath.row == 0 {        // Heading Row
                    cell.configureCellType(exportHeadingType: .ShipmentTrack)
                    cell.three_ColumnView(required: true)
                    cell.separatorInset = .zero
                    return cell
                } else {
                    cell.configureCellType(heading: false, isEven: (indexPath.row - 1) % 2 == 0)
                    if let arrData = allExportData.shipmentDetail, let mainData = arrData[indexPath.row-1] {    // -1 to remove Heading Cell Data Count
                        cell.three_ColumnView(required: true)
                        cell.setAllData(data1: mainData.blNo, data2: mainData.etaByTracking,
                                        data3: mainData.delayedBy, data4: nil)
                        cell.separatorInset = UIEdgeInsets(top: 0, left: arrData.count == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
                        DispatchQueue.main.async { [weak self] in
                            self?.shipmentTrackingTableHeight.constant = tableView.contentSize.height
                            self?.shipmentTrackingTableView.layer.cornerRadius = 8
                            self?.shipmentTrackingTableView.layer.masksToBounds = true
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
                                self?.shipmentTrackingTableView.add_ShadowView()
                            })
                        }
                    } else {
                        debugPrint("No Shipment Tracking Data Available!")
                    }
                    return cell
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView != upcomingEventTableView {
            if indexPath.row == 0 {
                return getDynamicHeight(size: 40)
            }
        }
        return UITableView.automaticDimension
    }
}
