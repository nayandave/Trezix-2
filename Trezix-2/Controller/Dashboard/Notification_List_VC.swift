//
//  Notification_List_VC.swift
//  Trezix-Demo1
//
//  Created by Amar Panchal on 08/06/22.
//

import UIKit

class Notification_List_VC: UIViewController {
    
    // MARK: - ALL IB_OUTLETS
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var notiTableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    
    // MARK: - All Constants & Variables
    
    private var isNetworkRefreshed: Bool = false
    
    // Normal Notification Variables
    private var mainNotificationData: Main_Notification_Data! = nil
    
    private var arrNotificationData = [Notification_Modal]()
    
    private var numberOfSection = Int()
    
    private var arrSectionTitle = [String]()
    
    private var sectionWiseNormalNotificationData = [[Notification_Modal]]()
    
    private lazy var refresher: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: "Fetching New Notifications!",
                                                            attributes: [.foregroundColor: UIColor.ourAppThemeColor,
                                                                         .font: UIFont.systemFont(ofSize: getDynamicHeight(size: 12),
                                                                                                  weight: .bold)])
        refresher.tintColor = .ourAppThemeColor
        refresher.addTarget(self, action: #selector(self.refresh_Notification_Data), for: .valueChanged)
        return refresher
    }()
    
    // Approval Notification Variables
//    public var isApprovalNotificationMode = false
//
//    public var arrApprovalNotiData = [Approval_Item_Data]()
//
//    private var sectionWiseApprovalNotificationData = [[Approval_Item_Data]]()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable { callAllInitMethods() }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    private func callAllInitMethods(_ afterNetworkRefresh: Bool = false) {
        if !afterNetworkRefresh {
            settingUpNotiPage()
        }
        numberOfSection = 0
        arrSectionTitle.removeAll()
//        if isApprovalNotificationMode {
//            callApprovalNotificationAPI()
//        } else {
            callNotificationAPI()
//        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        navContentView.layer.applyShadowAndRadius(cornerRadius: 0)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.notiTableView.layoutIfNeeded()
            self.notiTableView.reloadData()
            self.viewDidLayoutSubviews()
        }
    }
}

// MARK: - ALL ACTIONS
extension Notification_List_VC {
    @IBAction func back_btn_action(_ sender: UIButton) {
        guard let navController = navigationController else {
            dismiss(animated: true)
            return
        }
        navController.popViewController(animated: true)
    }
}

// MARK: - ALL HELPER METHODS
extension Notification_List_VC {
    ///*Setting Up Notification Page*
    private func settingUpNotiPage() {
//        notiTableView.register(UINib(nibName: approvalNotiCell.identifier, bundle: nil), forCellReuseIdentifier: approvalNotiCell.identifier)
        notiTableView.register(UINib(nibName: notiTabCell.identifier, bundle: nil), forCellReuseIdentifier: notiTabCell.identifier)
        notiTableView.tableFooterView = nil
        notiTableView.refreshControl = refresher
    }
    
    /// *Refresh Controller Method*
    @objc private func refresh_Notification_Data() {
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
//            if self.isApprovalNotificationMode {
//                self.callApprovalNotificationAPI(refreshingMode: true)
//            } else {
                self.callNotificationAPI(refreshingMode: true)
//            }
        }
    }
    
    /// Fetches Approval Notification Data API
    /// - Parameter refreshingMode: `true` if method is called from refreshing controller & `false` if called from viewDidLoad
//    private func callApprovalNotificationAPI(refreshingMode: Bool = false) {
//        NetworkClient.encodedNetworkRequest(method: .get,
//                                            apiName: APIConstants.GET_APPROVAL_LIST.fullPath(),
//                                            headers: default_app_header,
//                                            showHud: true)
//        { [weak self] (isSuccess, response: Approval_List_Modal?, errorMessage) in
//            guard let self = self else { return }
//            if isSuccess, let response = response {
//                print(response)
//
//                let mainData = response.data!
//
//                self.arrApprovalNotiData = mainData
//
//                let formatter = DateFormatter()
//                formatter.dateFormat = "MMMM, yyyy"
//
//                var sectionData = [Approval_Item_Data]()
//
//                for index in 0..<mainData.count {
////                    #warning("Fix Date Type...")
//                    let datee = DateHelper.shared.getDateFromString(mainData[index].date, withFormat: "yyyy-MM-dd")
//                    let sectione = formatter.string(from: datee!)
//                    if !self.arrSectionTitle.contains(sectione) {
//                        self.arrSectionTitle.append(sectione)
//                        if index != 0 {     //Should not work in First Index
//                            self.sectionWiseApprovalNotificationData.insert(sectionData, at: self.numberOfSection-1)
//                            sectionData.removeAll()
//                        }
//                        self.numberOfSection += 1
//                    }
//                    sectionData.append(self.arrApprovalNotiData[index])
//                    // Last Data
//                    if index == self.arrApprovalNotiData.count-1 {
//                        self.sectionWiseApprovalNotificationData.insert(sectionData, at: self.numberOfSection-1)
//                    }
//                }
//                self.notiTableView.isHidden = mainData.count == 0
//                self.noDataView.isHidden = mainData.count != 0
//                self.notiTableView.reloadData()
//                if refreshingMode {
//                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
//                        HapticFeedbackGenerator.notificationFeedback(type: .success)
//                        self.refresher.endRefreshing()
//                        Loader.hideLoader()
//                    }
//                } else {
//                    Loader.hideLoader()
//                    HapticFeedbackGenerator.notificationFeedback(type: .success)
//                }
//            } else {
//                print("Error Message \(String(describing: errorMessage))")
//                Loader.hideLoader()
//                self.notiTableView.isHidden = true
//                self.noDataView.isHidden = false
//            }
//        }
//    }
    
    /// Fetches Normal Notification Data API
    /// - Parameter refreshingMode: `true` if method is called from refreshing controller & `false` if called from viewDidLoad
    private func callNotificationAPI(refreshingMode: Bool = false) {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_NOTIFICATION.fullPath(),
                                            headers: [default_app_header],
                                            showHud: true) {
            [weak self] (isSuccess, response: Notification_Response_Modal?, errorMessage) in
            guard let strongSelf = self else { return }
            if isSuccess, let response = response {
                if let mainData = response.data {
                    strongSelf.mainNotificationData = mainData
                    
                    if let notiData = mainData.notifications {
                        strongSelf.arrNotificationData = notiData
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MMMM, yyyy"
                        
                        var sectionData = [Notification_Modal]()
                        
                        for index in 0..<notiData.count {
                            let datee = DateHelper.shared.decodeDashboardDate(dateString: notiData[index].createdAt)
                            let sectione = formatter.string(from: datee)

                            
                            if !strongSelf.arrSectionTitle.contains(sectione) {
                                strongSelf.arrSectionTitle.append(sectione)
                                
                                if index != 0 {     //Should not work in First Index
                                    strongSelf.sectionWiseNormalNotificationData.insert(sectionData, at: strongSelf.numberOfSection-1)
                                    sectionData.removeAll()
                                }
                                strongSelf.numberOfSection += 1
                            }
                            
                            sectionData.append(notiData[index])
                            
                            // Last Data
                            if index == notiData.count-1 {
                                strongSelf.sectionWiseNormalNotificationData.insert(sectionData, at: strongSelf.numberOfSection-1)
                            }
                        }
                    }
                    
                    strongSelf.notiTableView.isHidden = strongSelf.arrNotificationData.isEmpty //mainData.notifications == nil
                    strongSelf.noDataView.isHidden = !strongSelf.arrNotificationData.isEmpty//mainData.notifications != nil
                    strongSelf.notiTableView.reloadData()
                    if refreshingMode {
                        strongSelf.refresher.endRefreshing()
                    }
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                }
            }
            Loader.hideLoader()
        }
    }
}



// MARK: - TABLEVIEW DELEGATE METHODS
extension Notification_List_VC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSection
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arrSectionTitle[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionWiseNormalNotificationData[section].count
        //isApprovalNotificationMode ? sectionWiseApprovalNotificationData[section].count : sectionWiseNormalNotificationData[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if isApprovalNotificationMode {
//            let cell = tableView.dequeueReusableCell(withIdentifier: notiTabCell.identifier, for: indexPath) as! notiTabCell
//            let data = sectionWiseApprovalNotificationData[indexPath.section][indexPath.row]
//            cell.setApprovalNotiData(modal: data)
//            return cell
//        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: notiTabCell.identifier, for: indexPath) as! notiTabCell
            let data = sectionWiseNormalNotificationData[indexPath.section][indexPath.row]
            cell.updateCellInfo(info: data)
            return cell
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return getEdgeNotchHeight(of: .bottomNotch) + 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
