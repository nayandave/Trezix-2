//
//  HomepageVC.swift
//  Trezix-Demo1
//
//  Created by Amar Panchal on 19/05/22.
//

import UIKit
import PopupDialog
import Alamofire
import Charts

public var mainUserData: UserDataModal! = nil
public var mainDashboardJSON: JSON! = nil

class HomeScrollHeight: NSObject {
    @objc dynamic var scrollViewHeight: CGFloat = 0
    
    func changingHeight(newHeight: CGFloat) {
        scrollViewHeight = newHeight
    }
}

var scrollHeightObject: HomeScrollHeight! = nil
// TODO: - Check Notification API data, and report API data...
class HomepageVC: UIViewController {
    
    // MARK: - ALL IB_OUTLETS
    // MARK: - Custom Navigation Bar Content View Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var notificationBadgeView: UIView!
    @IBOutlet weak var lblNotiBadge: UILabel!
    @IBOutlet weak var btnApprovalNoti: UIButton!
    
    @IBOutlet weak var importExportSegment: UISegmentedControl!
    private var bottomSegmentBar: UIView! = nil
    
    @IBOutlet weak var mainScrollView: UIScrollView!

    @IBOutlet weak var monthPickerContentView: UIView!
    @IBOutlet weak var btnMonthCancel: UIButton!
    @IBOutlet weak var btnMonthDone: UIButton!
    @IBOutlet weak var monthPickerView: MonthYearWheelPicker!
    
    // MARK: - ALL VARIABLES & CONSTANTS
    
    ///**Determines the Data showing on the Homepage UI**
    /// - TRUE: Showing Important Data on Homepage UI { Default }
    /// - FALSE: Showing Export Data on Homepage UI
    private var showingImportData = true
    
    private var lastContentOffset: CGFloat = 0
    
    private lazy var blurrView: UIVisualEffectView = {
        var blurr = UIVisualEffectView()
        blurr.frame = view.frame
        blurr.isOpaque = false
        return blurr
    }()
    
    
    private var importDashView: UIView! = nil
    private var exportDashView: UIView! = nil
    
    private var importVC: Import_Dashboard_VC! = nil
    private var exportVC: Export_Dashboard_VC! = nil
    
    // MARK: - VIEW LIFE CYCLE METHODS
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !(navigationController?.isNavigationBarHidden ?? false) {
            DispatchQueue.main.async { [weak self] in
                self?.navigationController?.navigationBar.isHidden = true
            }
        }
        if mainScrollView.contentSize.height != 1 {
            debugPrint("HOME PAGE REFRESHED...")
            DispatchQueue.global(qos: .utility).async { [weak self] in
                guard let self = self else { return }
                if self.showingImportData { self.importVC.call_Import_Dashboard_API() }
                else { self.exportVC.call_Export_Dashboard_API() }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
//            scheduleNotification(title: "This is to test text to speach functionality", message: "Custom SubTitle")
//        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        let origin = bottomSegmentBar.frame.origin.x
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            UIView.transition(with: self.bottomSegmentBar,
                              duration: 0) {
                self.bottomSegmentBar.frame.origin.x = origin
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let newHeight = change?[NSKeyValueChangeKey.newKey] as? CGFloat else { return }
        debugPrint("NEW HEIGHT == \(newHeight)")
        changing_Scroll_Height(newHeight)
    }
    
    private func changing_Scroll_Height(_ newHeight: CGFloat) {
        if showingImportData {
            let frameVC = CGRect(x: 0, y: 0, width: self.view.frame.width, height: newHeight + getEdgeNotchHeight(of: .bottomNotch))
            importDashView.frame = frameVC
        } else {
            let frameVC = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: newHeight + getEdgeNotchHeight(of: .bottomNotch))
            exportDashView.frame = frameVC
        }
        DispatchQueue.main.async { [weak self] in
            //self!.mainScrollView.subviews.last?.frame.origin = CGPoint(x: self!.view.frame.width, y: 0)
            self!.mainScrollView.contentSize.height = newHeight + getEdgeNotchHeight(of: .bottomNotch)
        }
    }
    
    private func animate_Changing_Section(toImport: Bool) {
        showingImportData = toImport
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
//        if toImport {
//            self.importVC.call_Import_Dashboard_API()
//        } else {
//            self.exportVC.call_Export_Dashboard_API()
//        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            UIView.animate(withDuration: 0.2) {
                self.bottomSegmentBar.frame.origin.x = toImport ? self.importExportSegment.frame.minX+10 : self.importExportSegment.frame.midX+10
            }
            
            let lastHeight = toImport ? (self.importVC.importStackHeight) : (self.exportVC.exportStackHeight)
            self.changing_Scroll_Height(lastHeight)
        }
        UIView.transition(with: mainScrollView, duration: 0.25, options: [.curveEaseInOut], animations: { [weak self] in
            self?.mainScrollView.contentOffset.x = toImport ? 0 : self!.view.frame.width
//            if toImport {
//                self?.importVC.viewWillAppear(true)
//            } else {
//                self?.exportVC.viewWillAppear(true)
//            }
        })
    }
    
    private func checkForAppUpdate() {
        DispatchQueue.global().async {
            do {
                let update = try appDelegate.isUpdateAvailable()
                debugPrint("Update Info == \(update)")
                DispatchQueue.main.async {
                    if update.0 {
                        self.popupUpdateDialogue(withVersion: update.1);
                    }
                }
            } catch {
                print(error)
            }
        }
    }
    
    private func popupUpdateDialogue(withVersion: String) {
           let alertMessage = "A new version of Trezix is available, Please update to the latest version for the better experience"
           let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: .alert)

           let okBtn = UIAlertAction(title: "Update Now", style: .default, handler: {(_ action: UIAlertAction) -> Void in
               if let url = URL(string: "itms-apps:apps.apple.com/us/app/trezix/id6444468369"),
                   UIApplication.shared.canOpenURL(url){
                   if #available(iOS 10.0, *) {
                       UIApplication.shared.open(url, options: [:], completionHandler: nil)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               }
           })
           let noBtn = UIAlertAction(title: "Later" , style: .destructive, handler: {(_ action: UIAlertAction) -> Void in})
           alert.addAction(okBtn)
           alert.addAction(noBtn)
           self.present(alert, animated: true, completion: nil)
           
       }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let touch = touches.first {
            if monthPickerContentView.alpha != 0 {
                if !monthPickerContentView.bounds.contains(touch.location(in: monthPickerContentView)) {
                    cancel_Month_Selection_Action(self)
                }
            }
        }
    }
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        DispatchQueue.main.async { [weak self] in
//            guard let self = self else { return }
//            self.shipmentTableView.layoutIfNeeded()
//            self.shipmentTableView.reloadData()
//            self.arrChartView.forEach { chart in
////                chart.setNeedsLayout()
////                chart.layoutIfNeeded()
//                chart.data = self.finalChartData
//                chart.animate(xAxisDuration: 0.25)
//            }
//            self.view.layoutIfNeeded()
//            self.viewDidLayoutSubviews()
//        }
//    }
    
//    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
//        super.traitCollectionDidChange(previousTraitCollection)
//        DispatchQueue.main.async { [weak self] in
//            guard let self = self else { return }
//            self.changeUIWithChangeSizeClass(sizeClass: self.traitCollection)
//            self.view.layoutIfNeeded()
//            self.viewDidLayoutSubviews()
//        }
//    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.setNeedsDisplay()
//        approvalNotiBadgeView.layer.applyShadowAndRadius(cornerRadius: approvalNotiBadgeView.frame.height/2)
        notificationBadgeView.layer.applyShadowAndRadius(cornerRadius: notificationBadgeView.frame.height/2)
        navContentView.layer.applyShadowAndRadius(cornerRadius: 0)

        monthPickerContentView.layer.applyShadowAndRadius()

        [btnMonthCancel, btnMonthDone].forEach { button in
            button?.layer.applyShadowAndRadius()
        }
    }
}

// MARK: - ALL HELPER METHODS
extension HomepageVC {
    //  MARK: - Initial Required Methods
    /// `Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable {
            callAllInitMethods()
        }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    private func callAllInitMethods(_ afterRefresh: Bool = false) {
        debugPrint("EMPTY SEGMENT -- \(bottomSegmentBar == nil)")
        if !afterRefresh {
            if (bottomSegmentBar == nil) {     // Purpose: When Dashboard is already present and user selects dashboard from sidemenu { bottomSegmentBar is nil for first time and becomes nil when this VC is deinitted
                settingUp_VCs_ScrollView()
                setUpHomePage()
                Add_Quick_Shortcuts()
                redirection_From_Shortcuts()
            }
        }
        checkForAppUpdate()
    }
    
    private func settingUp_VCs_ScrollView() {
        scrollHeightObject = HomeScrollHeight()
        scrollHeightObject.addObserver(self, forKeyPath: #keyPath(HomeScrollHeight.scrollViewHeight), options: [.new], context: nil)
        
        importVC = homeStoryboard.instantiateViewController(withIdentifier: "Import_Dashboard_VC") as? Import_Dashboard_VC
        exportVC = homeStoryboard.instantiateViewController(withIdentifier: "Export_Dashboard_VC") as? Export_Dashboard_VC
        
        importVC.openMonthPickerDelegate = self
        exportVC.openMonthPickerDelegate = self
        
        importVC.notiDelegate = self
        exportVC.notiDelegate = self
        
        importDashView = importVC.view
        exportDashView = exportVC.view
        
        
        mainScrollView.contentSize = CGSize(width: 2 * SCREEN_WIDTH, height: CGFloat(1.0))
        
        let VCs = [importVC, exportVC]
        
        var idx:Int = 0
        
        for viewController in VCs {
            guard let viewController else { debugPrint("Either import and/or export vc can be nil"); return }
            addChild(viewController)
            let originX:CGFloat = CGFloat(idx) * SCREEN_WIDTH
            viewController.view.frame = CGRect(x: originX, y: 0, width: mainScrollView.frame.width, height: mainScrollView.frame.height)
            mainScrollView.addSubview(viewController.view)
            idx += 1;
        }
        mainScrollView.delegate = self
    }
    
    
    /// `Redirect to selected shortcut when app is launched from Quick Shortcuts`
    private func redirection_From_Shortcuts() {
        if let shortcut = sceneDelegate.savedShortCutItem {
            if shortcut.type == AllQuickShortcutTypes.report.rawValue {
                // TODO: - Check Below Timing...
                DispatchQueue.main.asyncAfter(deadline: .now()+1.75) { [weak self] in
                    self?.sideMenuController?.revealMenu()
                    NotificationCenter.default.post(name: .select_Report_Notificaiton, object: nil)
                }
            } else if shortcut.type == AllQuickShortcutTypes.eximGPT.rawValue {
                navigate_To_EximGPTVC()
            } else if shortcut.type == AllQuickShortcutTypes.notification.rawValue {
                openNotificationVC()
            } else if shortcut.type == AllQuickShortcutTypes.approval.rawValue {
                navigate_To_Approval_VC()
            } else if shortcut.type == AllQuickShortcutTypes.vesselTrack.rawValue {
                navigate_To_VesselVC()
            }
        }
    }
    
    /// `Requests Notification Permission Here`
    private func getNotificationPermission() {
        UNUserNotificationCenter.current().getPermission()
        let general_Category = UNNotificationCategory(identifier: notification_Category,
                                                      actions: all_Gen_Noti_Actions,
                                                      intentIdentifiers: [],
                                                      options: .customDismissAction)
        UNUserNotificationCenter.current().setNotificationCategories([general_Category])
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    /// `Setting Up Homepage`
    private func setUpHomePage() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.setUpSegment()
            self.configureMonthPicker()
            let tapOnNotification = UITapGestureRecognizer(target: self, action: #selector(self.openNotificationVC))
            self.notificationBadgeView.addGestureRecognizer(tapOnNotification)
            
            NotificationCenter.default.removeObserver(self)
            NotificationCenter.default.addObserver(self, selector: #selector(self.selected_SideMenu_Options(_:)), name: .sideMenuSelectionNotification, object: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
                guard let self = self else { return }
                self.getNotificationPermission()
            }
        }
    }
    
    /// Push Navigates to `Notification VC`
    /// * Called on Notification Icon Button
    /// * Called on Rounded Notification Label
    ///  - Parameter approvalNotification: `true` if Approval Notification button clicks & `false` if Normal Notification button clicks
    @objc public func openNotificationVC(approvalNotification: Bool = false) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "Notification_List_VC") as! Notification_List_VC
//        vc.isApprovalNotificationMode = approvalNotification
        navigationController?.pushViewController(vc, animated: true)
    }
    
    ///*Confugures Month Wheel Picker's Date Range*
    private func configureMonthPicker() {
        let currDate = Date()
        
        let minDate = Calendar.current.date(byAdding: .month, value: -2, to: currDate)!
        let maxDate = Calendar.current.date(byAdding: .month, value: 3, to: currDate)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"

        monthPickerView.minimumDate = minDate
        monthPickerView.maximumDate = maxDate
        
        monthPickerView.reloadAllComponents()
    }
    
    ///*Change UI & calls required methods, When Import or Export Section is clicked*
    private func setUpSegment() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            // TODO: - Make themeColor image and set it as below image to test proper transparency in segmentControl
            self.importExportSegment.setBackgroundImage(UIImage(), for: .normal, barMetrics: .default)
            self.importExportSegment.backgroundColor = .clear
            
            self.importExportSegment.layer.backgroundColor = UIColor.ourAppThemeColor.cgColor
            self.importExportSegment.tintColor = .clear
            
            self.importExportSegment.setTitleTextAttributes([.font : UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 18))!,
                                                             .foregroundColor: UIColor.white], for: .selected)
            self.importExportSegment.setTitleTextAttributes([.font : UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 18))!,
                                                             .foregroundColor: UIColor.white.withAlphaComponent(0.75)], for: .normal)
        }
        bottomSegmentBar = UIView()
        bottomSegmentBar.translatesAutoresizingMaskIntoConstraints = false
        bottomSegmentBar.backgroundColor = .white
        
        navContentView.addSubview(bottomSegmentBar)
        bottomSegmentBar.topAnchor.constraint(equalTo: importExportSegment.bottomAnchor).isActive = true
        bottomSegmentBar.heightAnchor.constraint(equalToConstant: getDynamicHeight(size: 3)).isActive = true
        bottomSegmentBar.leadingAnchor.constraint(equalTo: importExportSegment.leadingAnchor).isActive = true
        bottomSegmentBar.widthAnchor.constraint(equalTo: importExportSegment.widthAnchor, multiplier: 1/CGFloat(self.importExportSegment.numberOfSegments)).isActive = true
        showingImportData = true
//        importExportSegment.isMomentary = true
        self.view.layoutIfNeeded()
    }
    
    /// Opens & Closes Month Picker with animation
    /// - Parameter open: `true` if opening animation & `false` if closing animation to be shown; `Default` value is `true`
    private func animatingMonthPicker(open: Bool = true) {
        if open {
            monthPickerContentView.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        }
        UIViewPropertyAnimator(duration: 0.3, curve: .easeInOut) { [weak self] in
            guard let self = self else { return }
            self.monthPickerContentView.alpha = open ? 1 : 0
            self.monthPickerContentView.transform = open ? CGAffineTransform(scaleX: 1, y: 1) : CGAffineTransform(scaleX: 0.75, y: 0.75)
        }.startAnimation()
        if open {
            blurrView.showBlurrAnimation(behind: monthPickerContentView)
        } else {
            blurrView.hideBlurrEffect()
        }
    }
    
    // MARK: - SideMenu Selection Methods
    /// Opens `Logout Alert`
    private func callLogoutAlert() {
        let attributedString = NSAttributedString(string: "Logout", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15), //your font here
            NSAttributedString.Key.foregroundColor : UIColor.red
        ])
        
        let logoutAlert = UIAlertController(title: "Logout", message: "Are you sure you want to logout ? \n This will not affect any saved data!", preferredStyle: .alert)
        logoutAlert.setValue(attributedString, forKey: "attributedTitle")
        
        logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        logoutAlert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { _ in
            HapticFeedbackGenerator.notificationFeedback(type: .error)
            Remove_All_Quick_Shortcuts()
            KeychainHelper.shared.delete(service: KC_UserID_Key, account: APPNAME)
            KeychainHelper.shared.delete(service: KC_AppToken_Key, account: APPNAME)
            userDefault.dictionaryRepresentation().keys.forEach { keyID in
                if keyID != "savedUserDomain" {     // Saving Domain..
                    print("Removable KEY ==== \(keyID)")
                    userDefault.removeObject(forKey: keyID)
                    userDefault.synchronize()
                }
            }
            DispatchQueue.main.async {
                sceneDelegate.redirect_LoginStoryboard()
            }
        }))
        present(logoutAlert, animated: true)
    }
    
    /// Navigates to `Order_Report_View_Controller` Page
    func navigate_To_OrderReport() {
        let vc = allReportStoryboard.instantiateViewController(withIdentifier: "FormOrderReportVC") as! FormOrderReportVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigate_To_ShipmentReport() {
        let viewController = allReportStoryboard.instantiateViewController(withIdentifier: "FormShipmentReportVC") as! FormShipmentReportVC
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigate_To_TimeLine() {
        let viewController = allReportStoryboard.instantiateViewController(withIdentifier: "POTimelineVC") as! POTimelineVC
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func navigate_To_SalesReport_VC(orderReport: Bool = true) {
        let vc = orderReport ? (allReportStoryboard.instantiateViewController(withIdentifier: "FormSalesOrderReport") as! FormSalesOrderReport) : (allReportStoryboard.instantiateViewController(withIdentifier: "FormSalesReport") as! FormSalesReport)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigate_To_DispatchPlanning_VC() {
        let vc = allReportStoryboard.instantiateViewController(withIdentifier: "FormDispatchPlanningReportVC") as! FormDispatchPlanningReportVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigate_To_Approval_VC() {
        let vc = approvalStoryboard.instantiateViewController(withIdentifier: "AllApprovalListVC") as! AllApprovalListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigate_To_LandingCost_VC(invoice: Bool = true) {
        if invoice {
            let vc = allReportStoryboard.instantiateViewController(withIdentifier: "LandingCostInvoiceVC") as! LandingCostInvoiceVC
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = allReportStoryboard.instantiateViewController(withIdentifier: "Form_LandingCost_Report_VC") as! Form_LandingCost_Report_VC
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func navigate_To_ScriptDrawback_VC() {
        let vc = allReportStoryboard.instantiateViewController(withIdentifier: "ScriptDrawbackReportVC") as! ScriptDrawbackReportVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigate_To_DocumentUpload_VC() {
        let vc = documentStoryboard.instantiateViewController(withIdentifier: "Document_List_VC") as! Document_List_VC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigate_To_EximGPTVC() {
        let vc = eximStoryboard.instantiateViewController(withIdentifier: "EximGPTVC") as! EximGPTVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigate_To_VesselVC() {
        let vc = vesselStoryboard.instantiateViewController(withIdentifier: "Vessel_List_VC") as! Vessel_List_VC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigate_To_QuotationListVC() {
        let vc = quotationStoryboard.instantiateViewController(withIdentifier: "Quotation_List_VC") as! Quotation_List_VC
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - ALL IB-ACTIONS
extension HomepageVC {
    ///**Segment Change**
    @IBAction @objc func changeSegmentSection(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            animate_Changing_Section(toImport: true)
        case 1:
            animate_Changing_Section(toImport: false)
        default:
            debugPrint("Warning!!!!!!\nDefault Value Selected!\nMay Cause Some Errors")
            HapticFeedbackGenerator.simpleFeedback(type: .medium)
        }
    }
    
    @IBAction func show_Side_Menu(_ sender: UIButton) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        NotificationCenter.default.post(name: .reloadSideMenuSelectionNotification, object: nil)
        sideMenuController?.revealMenu()
    }
    
    // Navigation Bar Button Actions
    ///*Opens Notification VC with Normal Notifications*
    @IBAction func open_notification_page(_ sender: UIButton) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        openNotificationVC()
    }
    
//    ///*Opens Notification VC with Approval Notifications*
//    @IBAction func open_Approval_Notification_Action(_ sender: UIButton) {
//        HapticFeedbackGenerator.simpleFeedback(type: .soft)
//        openNotificationVC(approvalNotification: true)
//    }
//
    @IBAction func cancel_Month_Selection_Action(_ sender: Any) {
        NotificationCenter.default.post(name: .dismissingMonthPickerNotification,
                                        object: ["Section": showingImportData, "Month" : nil])
        animatingMonthPicker(open: false)
    }
    
    @IBAction func done_Month_Selection_Action(_ sender: UIButton) {
        let selectedMonth = String(DateFormatter().monthSymbols[self.monthPickerView.month-1].uppercased().prefix(3)) + " \(self.monthPickerView.year)"
        NotificationCenter.default.post(name: .dismissingMonthPickerNotification,
                                        object: ["Section": showingImportData, "Month" : selectedMonth])
        animatingMonthPicker(open: false)
    }
}

extension HomepageVC {
    /// Detects which row is selected from SideMenu Taable and proceeds further accordingly
    /// * `NotificationCenter Automatically Calls Method As Any Row Selected From SideMenu`
    @objc private func selected_SideMenu_Options(_ notification: NSNotification) {
        if let identifier = notification.object as? AllSideMenuIdentifier {
            switch identifier {
            case .Dashboard:
                //reload Dashboard
                debugPrint("Dashboard Page Reloading Remains")
                callAllInitMethods()
                
            case .OrderReport:
                navigate_To_OrderReport()
                
            case .ShipmentReport:
                navigate_To_ShipmentReport()
                
            case .POTimeLine:
                navigate_To_TimeLine()
                
            case .SalesReport:
                navigate_To_SalesReport_VC(orderReport: false)
                
            case .SalesOrderReport:
                navigate_To_SalesReport_VC()
                
            case .Approval:
                navigate_To_Approval_VC()
                
            case .Logout:
                callLogoutAlert()
                
            case .LandingCostReport:
                navigate_To_LandingCost_VC(invoice: false)
                
            case .LandingCostInvoice:
                navigate_To_LandingCost_VC()
                
            case .ScriptDrawback:
                navigate_To_ScriptDrawback_VC()
                
            case .DispatchPlanningReport:
                navigate_To_DispatchPlanning_VC()
                
            case .DocumentUpload:
                navigate_To_DocumentUpload_VC()
                
            case .eximgpt:
                navigate_To_EximGPTVC()
                
            case .vesselTrack:
                navigate_To_VesselVC()
                
            case .Quotation:
                navigate_To_QuotationListVC()
                
            }
        }
    }
}

extension HomepageVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset > scrollView.contentOffset.x+40) {      // move Left
            debugPrint("Left Side")
            importExportSegment.selectedSegmentIndex = 0
            importExportSegment.sendActions(for: .valueChanged)
        } else if (self.lastContentOffset+40 < scrollView.contentOffset.x) {       // move Right
            debugPrint("Right Side")
            importExportSegment.selectedSegmentIndex = 1
            importExportSegment.sendActions(for: .valueChanged)
        } else {
          //debugPrint("Not Full Scroll")
            DispatchQueue.main.async { [weak self] in
                self?.mainScrollView.contentOffset.x = self!.showingImportData ? 0 : self!.view.frame.width
            }
        }
        self.lastContentOffset = scrollView.contentOffset.x
    }
}

extension HomepageVC: OpenMonthPickerProtocol {
    func openMonthPicker() {
        debugPrint("Open Month Picker")
        animatingMonthPicker(open: true)
    }
}

extension HomepageVC: ChangeNotificationProtocol {
    func changeNotiCount(notiCount: Int, approvalNotiCount: Int) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            debugPrint("Changing Notifications.. \(notiCount) ------- \(approvalNotiCount)")
            self.lblNotiBadge.text = String(notiCount)
            //self.lblApprovalNotiBadge.text = String(approvalNotiCount)
        }
    }
}
