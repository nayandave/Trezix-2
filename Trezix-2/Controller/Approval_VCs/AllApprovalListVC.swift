//
//  AllApprovalListVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 13/10/22.
//

import UIKit

class AllApprovalListVC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var dropApprovalType: DropDown!
    
    @IBOutlet weak var segStatus: UISegmentedControl!
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var allApprovalTableView: UITableView!
    
    private var arrApprovalList = [Approval_Item_Data]()
    private var showableList = [Approval_Item_Data]()
    
    // MARK: - All Constants & Variables
    enum ApprovalStatus: String {
        case Pending
        case Approve
        case Reject
    }
    
    private enum DocumentType {
        case All
        case PO
        case SO
        case DC
        case PSI
        case FI
    }
    
    private var arrStrAllDocType = ["All",
                                    "Pending Order(PO)",
                                    "Sales Order(SO)",
                                    "Duty Clearing(DC)",
                                    "Pre-Shipment Invoice(PSI)",
                                    "Final Invoice(FI)"]
    
    var currentStatus: ApprovalStatus = .Pending
    
    private var currDocType: DocumentType = .All
    
    private var segBottomBar: UIView! = nil
    
    var arrSectionTitle = [String]()
    
    var sectionWiseApprovalNotificationData = [[Approval_Item_Data]]()
    
    var numberOfSection = Int()
    
    private var refresher: UIRefreshControl! = nil
    
    private var observer: NSKeyValueObservation?
    
    // Used to refresh that cell, if approved/rejected in DetailVCs
    var selectedIndex = IndexPath()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
        observer = dropApprovalType.observe(\.layer.bounds, changeHandler: { dropDown, _ in
            debugPrint("DROP DOWN WIDTH == \(dropDown.bounds.width)")
            DispatchQueue.main.async {
                dropDown.layer.applyShadowAndRadius()
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        observer?.invalidate()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        noDataView.layer.applyShadowAndRadius()
        //        dropApprovalType.layer.applyShadowAndRadius()
    }
    
    // MARK: - All Helper Methods
    /// `Only Method to be called in` ``viewDidLoad()``
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable { callAllInitMethods() }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    ///  - Parameter afterNetworkRefresh: Boolean value that indicates if method if called first time or called after network refresh (after being offline & online)
    private func callAllInitMethods(_ afterNetworkRefresh: Bool = false) {
        if !afterNetworkRefresh {
            setUpSegment()
            setUpSegmentBottomBar()
            setApprovalDropdown()
            registerTableCell()
            setUpRefreshController()
            addNotiRefreshObserver()
        }
        fetchApprovalListData()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        let frame = segBottomBar.frame.origin.x
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            UIView.transition(with: self.segBottomBar, duration: 0) {
                self.segBottomBar.frame.origin.x = frame
            }
        }
    }
    
    private func addNotiRefreshObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(refresh_List_After_Approval(_:)), name: .refreshApprovalListNotification, object: nil)
    }
    
    @objc private func refresh_List_After_Approval(_ notification: NSNotification) {
        guard let _ = notification.object as? Bool else { debugPrint("Invalid Call Refresh Approval List"); return }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.fetchApprovalListData()
//            self.sectionWiseApprovalNotificationData[self.selectedIndex.section][self.selectedIndex.row].status = (isApproved ? ApprovalStatus.Approve.rawValue : ApprovalStatus.Reject.rawValue)
//            self.filterData(statusFilter: true, typeFilter: false)
        }
    }
    
    private func registerTableCell() {
        allApprovalTableView.register(UINib(nibName: approvalListTableCell.identifier, bundle: nil),
                                      forCellReuseIdentifier: approvalListTableCell.identifier)
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: allApprovalTableView.frame.width, height: getEdgeNotchHeight(of: .bottomNotch)+10))
        footerView.backgroundColor = .clear
        allApprovalTableView.tableFooterView = footerView
        if #available(iOS 15, *) {
            allApprovalTableView.sectionHeaderTopPadding = 0
        }
    }
    
    private func setUpSegment() {
        segStatus.backgroundColor = .clear
        segStatus.tintColor = .clear
        segStatus.setBackgroundImage(UIImage(), for: .normal, barMetrics: .default)
        segStatus.setTitleTextAttributes([.font : UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 18))!,
                                          .foregroundColor: UIColor.white], for: .selected)
        segStatus.setTitleTextAttributes([.font : UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 18))!,
                                          .foregroundColor: UIColor.white.withAlphaComponent(0.75)], for: .normal)
    }
    
    private func setUpSegmentBottomBar() {
        segBottomBar = UIView()
        segBottomBar.translatesAutoresizingMaskIntoConstraints = false
        segBottomBar.backgroundColor = .white
        
        navContentView.addSubview(segBottomBar)
        segBottomBar.topAnchor.constraint(equalTo: segStatus.bottomAnchor).isActive = true
        segBottomBar.heightAnchor.constraint(equalToConstant: getDynamicHeight(size: 3)).isActive = true
        segBottomBar.leadingAnchor.constraint(equalTo: segStatus.leadingAnchor).isActive = true
        segBottomBar.widthAnchor.constraint(equalTo: segStatus.widthAnchor, multiplier: 1/CGFloat(segStatus.numberOfSegments)).isActive = true
        debugPrint("Original Origin : \(segBottomBar.frame)")
    }
    
    private func setApprovalDropdown() {
        dropApprovalType.arrowColor = .ourAppThemeColor
        dropApprovalType.hideOptionsWhenSelect = true
        dropApprovalType.isSearchEnable = false
        dropApprovalType.handleKeyboard = true
        dropApprovalType.checkMarkEnabled = false
        dropApprovalType.optionArray = arrStrAllDocType
        dropApprovalType.text = arrStrAllDocType[0]
        dropApprovalType.selectedRowColor = .ourAppThemeColor
        dropApprovalType.selectedIndex = 0
        dropApprovalType.adjustsFontSizeToFitWidth = true
        dropApprovalType.didSelect { [weak self] selectedText, index, id in
            guard let self = self else { return }
            HapticFeedbackGenerator.simpleFeedback(type: .soft)
            switch index {
            case 0: self.currDocType = .All
            case 1: self.currDocType = .PO
            case 2: self.currDocType = .SO
            case 3: self.currDocType = .DC
            case 4: self.currDocType = .PSI
            case 5: self.currDocType = .FI
            default:
                self.currDocType = .All
            }
            self.filterData(statusFilter: false, typeFilter: true)
        }
    }
    
    private func setUpRefreshController() {
        refresher = UIRefreshControl()
        
        refresher.attributedTitle = NSAttributedString(string: "Fetching New Approvals!",
                                                       attributes: [.foregroundColor: UIColor.ourAppThemeColor,
                                                                    .font: UIFont.systemFont(ofSize: getDynamicHeight(size: 12),
                                                                                             weight: .bold)])
        refresher.tintColor = .ourAppThemeColor
        refresher.addTarget(self, action: #selector(refresh_Controller_Data), for: .valueChanged)
        
        allApprovalTableView.refreshControl = refresher
    }
    
    /// *Refresh Controller Method*
    @objc private func refresh_Controller_Data() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.fetchApprovalListData(refreshingMode: true)
        }
    }
    
    private func filterData(statusFilter: Bool, typeFilter: Bool) {
        if (!statusFilter && !typeFilter) {
            Loader.showLoader()
        }
        
        let currentData = showableList
        let allData = arrApprovalList
        
        if currentData.count == 0 && allData.count == 0 {   // When Data is Empty
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.allApprovalTableView.isHidden = self.showableList.count == 0
                self.noDataView.isHidden = self.showableList.count != 0
                self.allApprovalTableView.reloadData()
                return
            }
        }
        
        var tempShowable = [Approval_Item_Data]()
        
        tempShowable = filterStatusWiseData(allData)
        tempShowable = filterTypeWiseData(tempShowable)
        
        if tempShowable != currentData || numberOfSection == 0 {
            showableList = tempShowable
            let formatter = DateFormatter()
            formatter.dateFormat = "MMMM, yyyy"
            
            numberOfSection = 0
            arrSectionTitle.removeAll()
            sectionWiseApprovalNotificationData.removeAll()
            
            var sectionData = [Approval_Item_Data]()
            
            for index in 0..<showableList.count {
                let datee = DateHelper.shared.getDateFromString(showableList[index].date, withFormat: "yyyy-MM-dd")
                let sectione = formatter.string(from: datee!)
                
                if !self.arrSectionTitle.contains(sectione) {
                    self.arrSectionTitle.append(sectione)
                    if index != 0 {     //Should not work in First Index
                        self.sectionWiseApprovalNotificationData.insert(sectionData, at: self.numberOfSection-1)
                        sectionData.removeAll()
                    }
                    self.numberOfSection += 1
                }
                sectionData.append(showableList[index])
                // Last Data
                if index == showableList.count-1 {
                    self.sectionWiseApprovalNotificationData.insert(sectionData, at: self.numberOfSection-1)
                }
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                if (!statusFilter && !typeFilter) {
                    Loader.hideLoader()
                }
                self.allApprovalTableView.isHidden = self.showableList.count == 0
                self.noDataView.isHidden = self.showableList.count != 0
                self.allApprovalTableView.reloadData()
            }
        } else {
            if (!statusFilter && !typeFilter) {
                Loader.hideLoader()
            }
        }
    }
    
    private func filterTypeWiseData(_ data: [Approval_Item_Data]) -> [Approval_Item_Data] {
        let tempData = data
        var returnData = [Approval_Item_Data]()
        switch currDocType {
        case .All:
            returnData = tempData
        case .PO:
            returnData = tempData.filter( { $0.moduleName == "PO" } )
        case .SO:
            returnData = tempData.filter( { $0.moduleName == "SO" } )
        case .DC:
            returnData = tempData.filter( { $0.moduleName == "DC" } )
        case .PSI:
            returnData = tempData.filter( { $0.moduleName == "PSI" } )
        case .FI:
            returnData = tempData.filter( { $0.moduleName == "FI" } )
        }
        return returnData
    }
    
    private func fetchApprovalListData(refreshingMode: Bool = false) {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_APPROVAL_LIST.fullPath(),
                                            headers: [default_app_header])
        { [weak self] (isSuccess, response: Approval_List_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if let mainData = response.data {
                    self.arrApprovalList = mainData
                    self.showableList = mainData
                    self.allApprovalTableView.isHidden = mainData.count == 0
                    self.noDataView.isHidden = mainData.count != 0
                    if mainData.count != 0 {
                        self.filterData(statusFilter: true, typeFilter: true)
                    }
                    if refreshingMode {
                        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                            HapticFeedbackGenerator.notificationFeedback(type: .success)
                            self.refresher.endRefreshing()
                            Loader.hideLoader()
                        }
                    } else { Loader.hideLoader() }
                } else { debugPrint("No Approval Data") }
            } else if let errorMessage {
                debugPrint("Error: \(errorMessage)")
                Loader.hideLoader()
                self.allApprovalTableView.isHidden = true
                self.noDataView.isHidden = false
            }
        }
    }
    
    // MARK: - All Actions
    @IBAction func btn_Back_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func segStatusChange(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0: currentStatus = .Pending
        case 1: currentStatus = .Reject
        case 2: currentStatus = .Approve
        default:
            currentStatus = .Pending
        }
        
        filterData(statusFilter: true, typeFilter: false)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            HapticFeedbackGenerator.simpleFeedback(type: .soft)
            UIView.transition(with: self.segBottomBar, duration: 0.2) {
                self.segBottomBar.frame.origin.x = (self.segStatus.frame.width / CGFloat(self.segStatus.numberOfSegments)) * CGFloat(self.segStatus.selectedSegmentIndex)+5
                debugPrint("On Click Origin : \(self.segBottomBar.frame)")
            }
//            UIView.animate(withDuration: 0.2) {
//                HapticFeedbackGenerator.simpleFeedback(type: .soft)
//                self.segBottomBar.frame.origin.x = (self.segStatus.frame.width / CGFloat(self.segStatus.numberOfSegments)) * CGFloat(self.segStatus.selectedSegmentIndex)
//                debugPrint("On Click Origin : \(self.segBottomBar.frame)")
//            }
        }
    }
    
    private func filterStatusWiseData(_ data: [Approval_Item_Data]) -> [Approval_Item_Data] {
        let tempData = data
        let filteredData = tempData.filter { dataItem in
            return getStatusOfData(dataItem) == currentStatus
        }
        return filteredData
    }
}
extension AllApprovalListVC {
    func checkVerificationType(_ data: Approval_Item_Data,
                               _ returnVerificationType: Bool = false) -> (ApprovalStatus?, Bool?) {
        if returnVerificationType {     // For Setp Parameter in ApprovalDetailVC { Approve, Reject Button APIs Input Params }
            if data.tyoeOfVerification == "1" {
                return (getStatusOfData(data), true)
            } else {
                guard let ap1 = data.aP1 else {
                    debugPrint("Empty ap1")
                    guard let ap2 = data.aP2 else { debugPrint("Empty ap2"); return (nil, nil) }
                    if ap2.components(separatedBy: ",").contains("\(mainUserData.id)") {
                        return (getStatusOfData(data),false)
                    } else { return (nil, nil) }
                }
                if ap1.components(separatedBy: ",").contains("\(mainUserData.id)") {
                    return (getStatusOfData(data),true)
                } else {
                    return (getStatusOfData(data),false)
                }
            }
        } else {
            return (getStatusOfData(data), nil)
        }
//        if data.tyoeOfVerification == "1" {
//            return (data.step1_ApprovalStatusSlug, returnVerificationType ? true : nil)
//        } else {
//            guard let ap1 = data.aP1 else {
//                debugPrint("Empty ap1")
//                guard let ap2 = data.aP2 else { debugPrint("Empty ap2"); return (nil, nil) }
//                if ap2.components(separatedBy: ",").contains("\(mainUserData.id)") {
//                    return (data.step2_ApprovalStatusSlug, returnVerificationType ? false : nil)
//                } else { return (nil, nil) }
//            }
//            if ap1.components(separatedBy: ",").contains("\(mainUserData.id)") {
//                return (data.step1_ApprovalStatusSlug, returnVerificationType ? true : nil)
//            } else {
//                return (data.step2_ApprovalStatusSlug, returnVerificationType ? false : nil)
//            }
//        }
    }
    
    func getStatusOfData(_ data: Approval_Item_Data) -> ApprovalStatus {
        guard let status = data.status else { return ApprovalStatus.Pending }
        switch status {
        case "1": return ApprovalStatus.Pending
        case "2": return ApprovalStatus.Approve
        case "3": return ApprovalStatus.Reject
        default: return ApprovalStatus.Pending
        }
    }
}
