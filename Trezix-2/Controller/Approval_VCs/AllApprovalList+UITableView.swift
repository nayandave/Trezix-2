//
//  AllApprovalList+UITableView.swift
//  Trezix-2
//
//  Created by Amar Panchal on 18/10/22.
//

import UIKit

extension AllApprovalListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSection
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arrSectionTitle[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionWiseApprovalNotificationData[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: approvalListTableCell.identifier, for: indexPath) as! approvalListTableCell
        let data = sectionWiseApprovalNotificationData[indexPath.section][indexPath.row]
//        DispatchQueue.main.async {
//            let showableDate = DateHelper.shared.decodeDashboardDate(dateString: data.createdAt!)
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "dd/MM/yyyy"
//            let finalTime = dateFormatter.string(from: showableDate)
//            cell.setAllValues(value1: <#T##String#>, value2: <#T##String#>, value3: <#T##String#>, value4: <#T##String#>)
            
            var heading1 = data.moduleName ?? "PO"
            heading1 = heading1 == "DC" ? "BL" : heading1
            cell.setAllHeadings(heading1: "\(heading1) No.",
                                heading2: "\(data.moduleName ?? "PO") Date",
                                heading3: "Quantity",
                                heading4: "Amount")
            cell.setAllValues(value1: data.moduleName == "DC" ? (data.moduleID ?? "N/A") : (data.moduleOriginalID ?? "N/A") ,
                              value2: data.date ?? "N/A",
                              value3: data.quantity == nil ? "N/A" : "\(data.quantity!)",
                              value4: data.amount ?? "N/A")
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath
        tableView.deselectRow(at: indexPath, animated: true)
        let data = sectionWiseApprovalNotificationData[indexPath.section][indexPath.row]
        let vc = approvalStoryboard.instantiateViewController(withIdentifier: "ApprovalDetailVC") as! ApprovalDetailVC
        
        guard let typeOfVerification = checkVerificationType(data, true).1 else { debugPrint("Could not get Type of verification!"); return }
        
        switch data.moduleName {
        case nil:
            debugPrint("NIL VALUE IN MODULE NAME !")
        case "PO":
            guard let id = data.moduleOriginalID, let mainID = data.id else { debugPrint("Invalid ID for PO Detail!"); return }
            vc.currDocType = .PO;
            vc.idForFetchAPI = String(id)
            vc.idForApprovalAPI = String(mainID)
            vc.typeOfVerification = typeOfVerification
            vc.statusForApproval = getStatusOfData(data)
            navigationController?.pushViewController(vc, animated: true)
        case "SO":
            guard let id = data.moduleOriginalID, let mainID = data.id else { debugPrint("Invalid ID for SO Detail!"); return }
            vc.currDocType = .SO;
            vc.idForFetchAPI = String(id)
            vc.idForApprovalAPI = String(mainID)
            vc.typeOfVerification = typeOfVerification
            vc.statusForApproval = getStatusOfData(data)
            navigationController?.pushViewController(vc, animated: true)
        case "DC":
            guard let blNumber = data.moduleID, let id = data.id else { debugPrint("Invalid ID for DC Detail!"); return }
            let dcPage = approvalStoryboard.instantiateViewController(withIdentifier: "DutyClearingDetailVC") as! DutyClearingDetailVC
            dcPage.blNumForAPI = blNumber
            dcPage.typeOfVerification = typeOfVerification
            dcPage.statusForApproval = getStatusOfData(data)
            dcPage.idForAPI = String(id)
            navigationController?.pushViewController(dcPage, animated: true)
        case "PSI":
            guard let id = data.moduleOriginalID, let mainID = data.id else { debugPrint("Invalid id for PSI Detail!"); return }
            vc.currDocType = .PSI
            vc.idForFetchAPI = String(id)
            vc.idForApprovalAPI = String(mainID)
            vc.typeOfVerification = typeOfVerification
            vc.statusForApproval = getStatusOfData(data)
            navigationController?.pushViewController(vc, animated: true)
        case "FI":
            guard let id = data.id else { debugPrint("Invalid id for FI Detail!"); return }
            vc.currDocType = .FI
            vc.idForFetchAPI = String(id)
            vc.idForApprovalAPI = String(id)
            vc.typeOfVerification = typeOfVerification
            vc.statusForApproval = getStatusOfData(data)
            navigationController?.pushViewController(vc, animated: true)
        default :
            debugPrint("Something Went Wrong!")
        }
    }
}
