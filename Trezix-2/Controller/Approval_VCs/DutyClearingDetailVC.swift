//
//  DutyClearingDetailVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 15/10/22.
//

import UIKit

class DutyClearingDetailVC: UIViewController {

    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var basicDetailStackView: UIStackView!
    @IBOutlet weak var lblBLNumber: UILabel!
    @IBOutlet weak var lblExchangeRate: UILabel!
    @IBOutlet weak var lblInvoice: UILabel!
    @IBOutlet weak var lblFrightInsurance: UILabel!

    @IBOutlet weak var dutyTableContainerView: UIView!
    @IBOutlet weak var dutyTableHeight: NSLayoutConstraint!
    @IBOutlet weak var dutyTableView: UITableView!
    
    @IBOutlet weak var textComment: UITextView!
    @IBOutlet weak var textCommentHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var stackFinalButtons: UIStackView!
    @IBOutlet weak var stackFinalButton_BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    
    // MARK: - All Constants & Variables
    private var allDutyData: DC_Detail_Modal! = nil
    private var allDCLineItems = [DCProductLineItem]()
    private var showableSections = [Int]()
    
    /// Used for API to fetch Details for Duty Clearing.
    ///  * Value will be passed from ``AllApprovalListVC`` when navigating to this VC
    open var blNumForAPI = String()
    
    /// Used to approve-reject API
    ///  * Value will be passed from ``AllApprovalListVC`` when navigating to this VC
    var idForAPI = String()
    
    /// Used As "step" parameter when calling API for approval/reject.
    ///  * Value will be passed from ``AllApprovalListVC`` when navigating to this VC
    ///  * if TRUE, 1 will be passed in "step" param as String and if FALSE, 2 will be passed in "step" param as String,
    var typeOfVerification = Bool()
    
    /// Used  to determine if it's from Pending Or Rejected/Accepted section
    ///  * Value will be passed from ``AllApprovalListVC`` when navigating to this VC
    ///  * if `Pending` then Comment TextView and Approve/Reject Button StackView will be Visible, if `other then Pending` then Comment TextView & Approve/Reject Button StackView will be Hidden
    var statusForApproval: AllApprovalListVC.ApprovalStatus = .Pending
    
    /// All headings as dictionary for showable rows
    private var allHeadingsDict: [Int: String] = [0:"CC",
                                                  1:"EAIDC",
                                                  2:"CHCESS",
                                                  3:"SWC",
                                                  4:"ADD",
                                                  5:"IGST",
                                                  6:"PREFD",
                                                  7:"User"]
    
    private var allHeaderHeight: CGFloat = 0
    
    private let defaultPlaceHolder = "Add Comment..."
    
    private var textCommentHeightObserver: NSKeyValueObservation?
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        mainScrollView.delegate = self
        if #available(iOS 15, *) {
            self.dutyTableView.sectionHeaderTopPadding = 0
        }
        fetchDCDetailFromAPI()
        changeUIAccordingToStatus()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        dutyTableView.layer.borderWidth = 0.5
        dutyTableView.layer.borderColor = UIColor.lightGray.cgColor
        
        dutyTableContainerView.layer.shadowColor = UIColor.lightGray.cgColor
        dutyTableContainerView.layer.shadowOffset = .zero
        dutyTableContainerView.layer.shadowOpacity = 1.0
        dutyTableContainerView.layer.shadowRadius = 2.5
        
        dutyTableView.layer.masksToBounds = true
        dutyTableView.layer.cornerRadius = 8
        
        [btnReject, btnAccept].forEach { button in
            button?.layer.applyShadowAndRadius()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        textCommentHeightObserver?.invalidate()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // MARK: - All IB-Actions
    @IBAction func reject_Button_Action(_ sender: UIButton) {
        callApproveRejectAPI(isApproved: false)
    }
    
    @IBAction func accept_Button_Action(_ sender: UIButton) {
        callApproveRejectAPI(isApproved: true)
    }
    
    @IBAction func back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - All Helper Methods
    private func setBasicData() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lblBLNumber.text = self.allDutyData.blNo ?? "N/A"
            self.lblExchangeRate.text = self.allDutyData.exchangeRate ?? "N/A"
            self.lblInvoice.text = self.allDutyData.invoiceNum ?? "N/A"
            self.lblFrightInsurance.text = self.allDutyData.addFreight ?? "N/A"
            self.allHeaderHeight = (CGFloat(self.allDCLineItems.count+1) * getDynamicHeight(size: 50))
            self.dutyTableHeight.constant = self.allHeaderHeight
            debugPrint("Showable Header = \(self.allDCLineItems.count)")
            self.dutyTableView.reloadData()
            Loader.hideLoader()
        }
    }
    
    private func fetchDCDetailFromAPI() {
        let param = ["bl_no" : blNumForAPI]
        debugPrint("Input Param -- \(param)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.GET_DUTY_CLEARING_APPROVAL_DETAIL.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Approval_DC_Response_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if let allData = response.data {
                    self.allDutyData = allData
                    if let lineItem = allData.productLineItem {
                        self.allDCLineItems = lineItem
                    } else {
                        self.allDCLineItems.removeAll()
                    }
                    self.setBasicData()
                } else {
                    self.allDutyData = nil
                }
            } else if let errorMessage {
                debugPrint("Error in DC Detail View \(errorMessage)")
            } else {
                debugPrint("Unknown Error!")
            }
        }
    }
    
    private func changeUIAccordingToStatus() {
        [stackFinalButtons, textComment].forEach { view in
            view?.isHidden = statusForApproval != .Pending
        }
        if statusForApproval == .Pending {
            setTextView()
            debugPrint("Stack Constant - \(stackFinalButton_BottomConstraint.constant)")
            debugPrint("Bottom Edge -- \(getEdgeNotchHeight(of: .bottomNotch))")
            stackFinalButton_BottomConstraint.constant = 0
            self.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.view.setNeedsDisplay()
        } else {
            stackFinalButton_BottomConstraint.constant = 0
            textCommentHeight.constant = 0
        }
    }
    
    /// `Tap On Header of dutyTableView which Hides/Shows extra info cells`
    @objc private func tappOnHeader(_ tap: UITapGestureRecognizer) {
        view.endEditing(true)
        guard let tappedView = tap.view as? ThreeColumnRowView else { appView.makeToast("Unrecognised Touch"); return }
        let currentIndex = tappedView.currentRow
        if showableSections.contains(currentIndex) {
            showableSections = showableSections.filter({ $0 != currentIndex })
        } else {
            showableSections.append(currentIndex)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.dutyTableView.beginUpdates()
            UIView.transition(with: self.dutyTableView, duration: 0.2, options: [.curveEaseInOut]) {
                self.dutyTableView.reloadSections(IndexSet(integer: currentIndex), with: .fade)
                self.dutyTableHeight.constant = self.allHeaderHeight + (CGFloat(self.showableSections.count*4) * getDynamicHeight(size: 40))
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            } completion: { _ in
                self.dutyTableView.endUpdates()
            }

//            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear) {
//                self.dutyTableView.reloadSections(IndexSet(integer: currentIndex), with: .fade)
//                self.dutyTableHeight.constant = self.allHeaderHeight + (CGFloat(self.showableSections.count*4) * getDynamicHeight(size: 40))
//                self.view.setNeedsLayout()
//                self.view.layoutIfNeeded()
//            } completion: { _ in
//                self.dutyTableView.endUpdates()
//            }
        }
    }
    
    private func callApproveRejectAPI(isApproved: Bool) {
        var finalComment = String()
        if textComment.hasText {
            if textComment.text != defaultPlaceHolder {
                finalComment = textComment.text!
            } else {
                textComment.errorShake()
                appView.makeToast("Comment can not be empty!")
                return
            }
        } else {
            textComment.errorShake()
            appView.makeToast("Comment can not be empty!")
            return
        }
        
        let param: [String:Any] = ["id" : idForAPI,
                                   "status" : isApproved ? "1" : "0",
                                   "step" : typeOfVerification ? "1" : "2",
                                   "comment" : finalComment]
        	
        print("Input Param -- \(param)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.APPROVE_REJCT_APPROVAL.fullPath(),
                                            param: param, headers: [default_app_header], showHud: false, senderView: (isApproved ? btnAccept : btnReject))
        { [weak self] (isSuccess, response: BasicResponseModalForApproval? , errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if response.status == 1 { // Success
                    self.showSuccessAlertAndExitVC(approvalMsg: isApproved)
                } else {    // No Data Found or any other error
                    debugPrint("response status is not 1")
                    appView.makeToast("Something Went Wrong!\nTry Again Later!")
                }
            } else if let errorMessage {
                debugPrint("Error in Approve Reject APi in DutyClearing == \(errorMessage)")
            } else {
                debugPrint("Unknown Error in Approve Reject APi in DutyClearing")
            }
        }
    }
    
    private func showSuccessAlertAndExitVC(approvalMsg: Bool = true) {
        let alert = UIAlertController(title: "Success", message: "This approval request is \(approvalMsg ? "accepted" : "rejected") successfully!", preferredStyle: .alert)
        let acceptBtn = UIAlertAction(title: "Okay", style: .default) { [weak self] _ in
            guard let self = self else { return }
            self.dismiss(animated: true) {
                self.navigationController?.popViewController(animated: true)
                NotificationCenter.default.post(name: .refreshApprovalListNotification, object: false)
            }
        }
        alert.addAction(acceptBtn)
        present(alert, animated: true)
    }
}

// MARK: - TextView Delegate Methods
extension DutyClearingDetailVC: UITextViewDelegate {
    private func setTextView() {
        textComment.delegate = self
        textComment.text = defaultPlaceHolder
        textComment.textColor = UIColor.lightGray
        textComment.textContainerInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        
        /// `To Set Initial Height of Related TextView`
//         let fixedWidth = textComment.frame.size.width
//         let newSize = textComment.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//         textCommentHeight.constant = newSize.height
        textCommentHeightObserver = textComment.observe(\.bounds, changeHandler: { commentTextView, _ in
            commentTextView.layer.borderWidth = 0.75
            commentTextView.layer.borderColor = UIColor.secondarySystemBackground.cgColor
            commentTextView.layer.applyShadowAndRadius(opacity: 0.75, shadowRadius: 2)
        })
    }
    
    func adjustTextViewHeight() {
        let fixedWidth = textComment.frame.size.width
        let newSize = textComment.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        if newSize.height > getDynamicHeight(size: 100) {
            textComment.isScrollEnabled = false
            textCommentHeight.constant = newSize.height
        }
        view.layoutIfNeeded()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.adjustTextViewHeight()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if !textComment.text!.isEmpty && textComment.text! == defaultPlaceHolder {
            textComment.text = ""
            textComment.textColor = UIColor.label
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textComment.text.isEmpty {
            textComment.text = defaultPlaceHolder
            textComment.textColor = UIColor.placeholderText
        }
    }
}

// MARK: - TableView Delegate Methods
extension DutyClearingDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return allDCLineItems.count+1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getDynamicHeight(size: 50)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ThreeColumnRowView()
        if section == 0 {
            headerView.setData(firstLabel: "BCD", secondLabel: "CAIDC", thirdLabel: "SAFD", threeColumnView: true)
        } else {
            let data = allDCLineItems[section-1]
            headerView.setData(firstLabel: data.bcd == nil ? "-" : "\(data.bcd!)", secondLabel: data.cvd == nil ? "-" : "\(data.cvd!)", thirdLabel: data.sad == nil ? "-" : "\(data.sad!)", threeColumnView: true)
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappOnHeader(_:)))
            headerView.addGestureRecognizer(tapGesture)
            headerView.isSelected(selected: showableSections.contains(section))
        }
        headerView.isViewHeadingView(heading: section == 0, withSeparator: true, tag: section)
        headerView.topSep.isHidden = true
        headerView.bottomSep.isHidden = true
        headerView.leadingSep.isHidden = true
        headerView.trailingSep.isHidden = true
        return headerView
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showableSections.contains(section) ? 4 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getDynamicHeight(size: 40)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dutyClearingDetailTableCell", for: indexPath) as! dutyClearingDetailTableCell
        cell.clipsToBounds = true
        
        cell.isFirstCell(indexPath.row == 0)
        let showableIndex = indexPath.row*2
        cell.setHeadings(left: allHeadingsDict[showableIndex]!, right: allHeadingsDict[showableIndex+1]!)
        let data = allDCLineItems[indexPath.section-1]
        switch indexPath.row {
        case 0: cell.setValues(left: data.cessOfCvd == nil ? "-" : "\(data.cessOfCvd!)",
                               right: data.eduCess == nil ? "-" : "\(data.eduCess!)")
        case 1: cell.setValues(left: data.secHEducCessOnCvd == nil ? "-" : "\(data.secHEducCessOnCvd!)",
                               right: data.socialWelfare ?? "N/A")
        case 2: cell.setValues(left: data.antiDumping == nil ? "-" : "\(data.antiDumping!)",
                               right: data.igst ?? "N/A")
        case 3:
            var finalName = "N/A"
            if let firstName = data.firstName, let lastName = data.lastName {
                finalName = firstName + " " + lastName
            }
            cell.setValues(left: data.penalty == nil ? "-" : "\(data.penalty!)",
                           right: finalName)
        default:
            cell.setValues(left: "-", right: "-")
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return getDynamicHeight(size: 1)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let separator = UIView(frame: CGRect(x: 10, y: 0, width: tableView.frame.width-20, height: 1))
        separator.backgroundColor = .label
        return separator
    }
}

// MARK: - Scroll View Delegate Method
extension DutyClearingDetailVC {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if textComment.isFirstResponder {
            self.view.endEditing(true)
        }
    }
}
