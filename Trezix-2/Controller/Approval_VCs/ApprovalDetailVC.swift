//
//  ApprovalDetailVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 13/10/22.
//

import UIKit

class ApprovalDetailVC: UIViewController {
    
    // MARK: - All Outlets
    // MARK: - Common Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var lineItemTableView: SelfSizingTableView!
    @IBOutlet weak var lineItemTableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var stackTotalAmount: UIStackView!
    @IBOutlet weak var totalAmountContentView: UIView!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var textComment: UITextView!
    @IBOutlet weak var textCommentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var stackFinalButtons: UIStackView!
    @IBOutlet weak var stackFinalButton_BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var btnApprove: UIButton!
    
    // MARK: - PO Detail View Outlets
    // Basic Detail View (Section 1)
    @IBOutlet weak var basicDetailView: UIView!
    @IBOutlet weak var lblFirmName: UILabel!
    @IBOutlet weak var lblFirmAddress: UILabel!
    @IBOutlet weak var lblFirmPhone: UILabel!
    @IBOutlet weak var lblFirmMail: UILabel!
    
    @IBOutlet weak var lblHeadingPONumber: UILabel!
    @IBOutlet weak var lblPONumber: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    // Vendor Detail View (Section 2)
    @IBOutlet weak var poSection1: UIView!
    @IBOutlet weak var lblHeadingVendor: UILabel!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblVendorAddress: UILabel!
    @IBOutlet weak var lblVendorPhone: UILabel!
    
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblBankAddress: UILabel!
    @IBOutlet weak var lblBankPhone: UILabel!
    @IBOutlet weak var lblBankAccNumber: UILabel!
    
    // Shipping Detail View (Section 3)
    @IBOutlet weak var poSection2: UIView!
    @IBOutlet weak var lblShipBy: UILabel!
    @IBOutlet weak var lblCountryOrigin: UILabel!
    @IBOutlet weak var lblIncoterm: UILabel!
    
    @IBOutlet weak var lblFinalDestination: UILabel!
    @IBOutlet weak var lblPaymentTerm: UILabel!
    
    // Shipping Instruction View (Section 4)
    @IBOutlet weak var poSection3: UIView!
    @IBOutlet weak var lblHeadingInstruction1: UILabel!
    @IBOutlet weak var lblInstruction1: UILabel!
    @IBOutlet weak var lblHeadingInstruction2: UILabel!
    @IBOutlet weak var lblInstruction2: UILabel!
    @IBOutlet weak var lblHeadingInstruction3: UILabel!
    @IBOutlet weak var lblInstruction3: UILabel!
    @IBOutlet weak var lblHeadingInstruction4: UILabel!
    @IBOutlet weak var lblInstruction4: UILabel!
    
    
    // MARK: - SO Detail View Outlets
    // Buyer Detail (Section 1)
    @IBOutlet weak var soSectionView1: UIView!
    @IBOutlet weak var lblBuyerOrderNum: UILabel!
    @IBOutlet weak var lblBuyerDate: UILabel!
    
    // IEC Details(Section 2)
    @IBOutlet weak var soSectionView2: UIView!
    @IBOutlet weak var lblIECnum: UILabel!
    @IBOutlet weak var lblISONum: UILabel!
    @IBOutlet weak var lblOtherReferences: UILabel!
    
    // Buyer Vessel Detail(Section 3)
    @IBOutlet weak var soSectionView3: UIView!
    @IBOutlet weak var lblOtherBuyer: UILabel!
    @IBOutlet weak var lblPreCarriageBy: UILabel!
    @IBOutlet weak var lblPlaceOfReceiptBy: UILabel!
    @IBOutlet weak var lblVesselFlightNum: UILabel!
    
    // Port Detail (Section 4)
    @IBOutlet weak var soSectionView4: UIView!
    @IBOutlet weak var lblPortOfLoading: UILabel!
    @IBOutlet weak var lblSOCountryOfGoods: UILabel!
    @IBOutlet weak var lblPortOfDischarge: UILabel!
    @IBOutlet weak var lblCountryOfDestination: UILabel!
    @IBOutlet weak var lblTermsOfDelPayment: UILabel!
    
    // Final Destination Detail (Section 5)
    @IBOutlet weak var soSectionView5: UIView!
    @IBOutlet weak var lblSOFinalDestination: UILabel!
    
    
    // MARK: - Pre-Shipment & Final Invoices Detail View Outlets
    // Tax Detail (Invoice Section 1)
    @IBOutlet weak var invoiceSectionView1: UIView!
    @IBOutlet weak var lblHeadTaxOrPreShipInvoice: UILabel! //In Pre-Shipment invoice it will be "Tax Inv. No" & In Final Invoice Page it will be "Pre-Ship nv. No.
    @IBOutlet weak var lblTaxOrPreShipInvoice: UILabel!
    @IBOutlet weak var lblDateTaxOrPreShipInvoice: UILabel!
    
    // Buyer Detail (Invoice Section 2)
    @IBOutlet weak var invoiceSectionView2: UIView!
    @IBOutlet weak var lblInvoiceBuyerOrderNum: UILabel!
    @IBOutlet weak var lblDateBuyerOrder: UILabel!
    
    // Delivery Details (Invoice Section 3)
    @IBOutlet weak var invoiceSectionView3: UIView!
    @IBOutlet weak var lblInvoiceDeliveryNum: UILabel!
    @IBOutlet weak var lblInvoiceOtherReferences: UILabel!
    
    // Consignee Detail (Invoice Section 4)
    @IBOutlet weak var invoiceSectionView4: UIView!
    @IBOutlet weak var lblInvoiceConsignee: UILabel!
    @IBOutlet weak var lblInvoiceBuyerName: UILabel!
    @IBOutlet weak var lblInvoicePlaceOfReceiptBy: UILabel!
    @IBOutlet weak var lblInvoicePreCarriageBy: UILabel!
    
    // Port Detail (Invoice Section 5)
    @IBOutlet weak var invoiceSectionView5: UIView!
    @IBOutlet weak var lblInvoicePortOfLoading: UILabel!
    @IBOutlet weak var lblInvoiceCountryOfGoods: UILabel!
    @IBOutlet weak var lblInvoicePortOfDischarge: UILabel!
    @IBOutlet weak var lblInvoiceCountryOfFinalDestination: UILabel!
    @IBOutlet weak var lblInvoiceTermsOfPayment: UILabel!
    
    // Final Destination Detail (Invoice Section 6)
    @IBOutlet weak var invoiceSectionView6: UIView!
    @IBOutlet weak var lblInvoiceFinalDestination: UILabel!
    @IBOutlet weak var lblInvoiceVesselFlightNum: UILabel!
    
    // MARK: - All Constants & Variables
    /// Used for API to fetch Details for PO/SO etc.
    ///  * Value will be passed from ``AllApprovalListVC`` when navigating to this VC
    var idForFetchAPI = String()
    
    /// Used for Approve/Reject API
    ///  * Value will be passed from ``AllApprovalListVC`` when navigating to this VC
    var idForApprovalAPI = String()
    
    /// Used As "step" parameter when calling API for approval/reject.
    ///  * Value will be passed from ``AllApprovalListVC`` when navigating to this VC
    ///  * if `TRUE`, 1 will be passed in "step" param as String and if `FALSE`, 2 will be passed in "step" param as String,
    var typeOfVerification = Bool()
    
    
    /// Used  to determine if it's from Pending Or Rejected/Accepted section
    ///  * Value will be passed from ``AllApprovalListVC`` when navigating to this VC
    ///  * if `Pending` then Comment TextView and Approve/Reject Button StackView will be Visible, if `other then Pending` then Comment TextView & Approve/Reject Button StackView will be Hidden
    var statusForApproval: AllApprovalListVC.ApprovalStatus = .Pending
    
    private var allPOData: PO_Detail_Modal! = nil
    private var allPOLineItems = [POProductLineItem]()
    
    private var allSOData: SO_Detail_Modal! = nil
    private var allSOLineItems = [SOProductLineItem]()
    
    private var allPSIData: PSI_Detail_Modal! = nil
    private var allPSILineItems = [PSIItemList]()
    
    private var allFIData: FI_Detail_Modal! = nil
    private var allFILineItems = [FIItemList]()
    
    private let defaultPlaceHolder = "Add Comment..."
    
    private var textCommentHeightObserver: NSKeyValueObservation?
    
    /// `Types of Document that this VC can show`
    public enum ApprovalDocType: String {
        case PO
        case SO
        case PSI
        case FI
    }
    
    /// `Determines the type of Document to make API call & make UI changes accordingly`
    open var currDocType: ApprovalDocType = .PO
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpThisVC()
        switch currDocType {
        case .PO:
            fetchPODetailFromAPI()
        case .SO:
            fetchSODetailFromAPI()
        case .PSI, .FI:
            fetchPreShipInvoiceDetailFromAPI()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        [poSection1, poSection2, poSection3,
         soSectionView1, soSectionView2, soSectionView3, soSectionView4, soSectionView5,
         invoiceSectionView1, invoiceSectionView2, invoiceSectionView3, invoiceSectionView4, invoiceSectionView5, invoiceSectionView6].forEach { containerView in
            containerView.layer.applyShadowAndRadius()
        }
        lineItemTableView.layer.applyShadowAndRadius()
        [btnReject, btnApprove].forEach { button in
            button?.layer.applyShadowAndRadius()
        }
        lblTotalAmount.layer.masksToBounds = false
        lblTotalAmount.clipsToBounds = true
        lblTotalAmount.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        lblTotalAmount.layer.cornerRadius = 8
        totalAmountContentView.layer.applyShadowAndRadius()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        textCommentHeightObserver?.invalidate()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        debugPrint("Touch Began ... ")
        self.view.endEditing(true)
    }
    
    // MARK: - All Actions
    @IBAction func back_Button_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func reject_Button_Action(_ sender: Any) {
        callApproveRejectAPI(isApproved: false)
    }
    
    @IBAction func accept_Button_Action(_ sender: Any) {
        callApproveRejectAPI(isApproved: true)
    }
    
    // MARK: - Required Helper Methods
    private func setUpThisVC() {
        changeUIAccordingDoc()
        mainScrollView.delegate = self
        if #available(iOS 15, *) {
            lineItemTableView.sectionHeaderTopPadding = 0
        }
        lineItemTableView.tableFooterView = UIView()
        changeUIAccordingToStatus()
    }
    
    private func changeUIAccordingToStatus() {
        [stackFinalButtons, textComment].forEach { view in
            view?.isHidden = statusForApproval != .Pending
        }
        if statusForApproval == .Pending {
            setTextView()
            debugPrint("Stack Constant - \(stackFinalButton_BottomConstraint.constant)")
            debugPrint("Bottom Edge -- \(getEdgeNotchHeight(of: .bottomNotch))")
            stackFinalButton_BottomConstraint.constant = 0
            self.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.view.setNeedsDisplay()
        } else {
            stackFinalButton_BottomConstraint.constant = 0
            textCommentHeight.constant = 0
        }
    }
    
    private func changeUIAccordingDoc() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lblHeadingPONumber.text = "\(self.currDocType.rawValue) No."
            self.lblHeadTaxOrPreShipInvoice.text = self.currDocType == .PSI ? "TAX Inv. No." : (self.currDocType == .FI ? "Pre-Shipment Inv. No." : "Unexpected Field")
            switch self.currDocType {
            case .PO : self.btnBack.setTitle("Purchase Order Details", for: .normal)
            case .SO : self.btnBack.setTitle("Sales Order Details", for: .normal)
            case .PSI : self.btnBack.setTitle("Pre-Shipment Invoice Details", for: .normal)
            case .FI : self.btnBack.setTitle("Final Invoice Details", for: .normal)
            }
        }

        [poSection1, poSection2, poSection3].forEach { section in
            section?.isHidden = currDocType != .PO
        }
        [soSectionView1, soSectionView2, soSectionView3, soSectionView4, soSectionView5].forEach { section in
            section?.isHidden = currDocType != .SO
        }
        [invoiceSectionView1, invoiceSectionView2, invoiceSectionView3, invoiceSectionView4, invoiceSectionView5, invoiceSectionView6].forEach { section in
            section?.isHidden = (currDocType != .PSI) && (currDocType != .FI)
        }
    }
    
    private func setTextView() {
        textComment.delegate = self
        textComment.text = defaultPlaceHolder
        textComment.textColor = UIColor.lightGray
        textComment.textContainerInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        
        /// `To Set Initial Height of Related TextView`
//         let fixedWidth = textComment.frame.size.width
//         let newSize = textComment.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//         textCommentHeight.constant = newSize.height
        textCommentHeightObserver = textComment.observe(\.bounds, changeHandler: { commentTextView, _ in
            commentTextView.layer.borderWidth = 0.75
            commentTextView.layer.borderColor = UIColor.secondarySystemBackground.cgColor
            commentTextView.layer.applyShadowAndRadius(opacity: 0.75, shadowRadius: 2)
        })
    }
    
    private func callApproveRejectAPI(isApproved: Bool) {
//        showSuccessAlertAndExitVC(approvalMsg: true)
//        var id = String()
//        if currDocType == .PO {
//            guard let poID = allPOData.poID else { debugPrint("Invalid PO ID"); return }
//            id = String(poID)
//        } else if currDocType == .SO {
//            guard let soID = allSOData.soID else { debugPrint("Invalid SO ID"); return }
//            id = String(soID)
//        } else if currDocType == .PSI {
//            guard let invoiceID = allPSIData.invoiceID else { debugPrint("Invalid Pre-Shipment Invoice ID"); return }
//            id = String(invoiceID)
//        } else {
//            guard let invoiceID = allFIData.invoiceID else { debugPrint("Invalid Final Invoice ID"); return }
//            id = String(invoiceID)
//        }

        var finalComment = String()
        if textComment.hasText {
            if textComment.text != defaultPlaceHolder {
                finalComment = textComment.text!
            } else {
                textComment.errorShake()
                appView.makeToast("Comment can not be empty!")
                return
            }
        } else {
            textComment.errorShake()
            appView.makeToast("Comment can not be empty!")
            return
        }

        let param: [String:Any] = ["id" : idForApprovalAPI,
                                   "status" : isApproved ? "1" : "0",
                                   "step" : typeOfVerification ? "1" : "2",
                                   "comment" : finalComment]
        print("Input Params-- \(param)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.APPROVE_REJCT_APPROVAL.fullPath(),
                                            param: param, headers: [default_app_header], showHud: false, senderView: isApproved ? btnApprove : btnReject)
        { [weak self] (isSuccess, response: BasicResponseModalForApproval? , errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if response.status == 1 { // Success
                    self.showSuccessAlertAndExitVC(approvalMsg: isApproved)
                } else {    // No Data Found or any other error
                    debugPrint("response status is not 1")
                    appView.makeToast("Something Went Wrong!\nTry Again Later!")
                }
            } else if let errorMessage {
                debugPrint("Error in Approve Reject APi in DutyClearing == \(errorMessage)")
            } else {
                debugPrint("Unknown Error in Approve Reject APi in DutyClearing")
            }
        }
    }
    
    private func showSuccessAlertAndExitVC(approvalMsg: Bool = true) {
        let alert = UIAlertController(title: "Success", message: "This approval request is \(approvalMsg ? "accepted" : "rejected") successfully!", preferredStyle: .alert)
        let acceptBtn = UIAlertAction(title: "Okay", style: .default) { [weak self] _ in
            guard let self = self else { return }
            self.dismiss(animated: true) {
                self.navigationController?.popViewController(animated: true)
                NotificationCenter.default.post(name: .refreshApprovalListNotification, object: true)
            }
        }
        alert.addAction(acceptBtn)
        present(alert, animated: true)
    }
}

// MARK: - All API Calling Methods
extension ApprovalDetailVC {
    /// `Fetches Data From PO Approval Detail API`
    private func fetchPODetailFromAPI() {
        let para = ["id" : idForFetchAPI]
        debugPrint("PO PARAM == \(para)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.GET_PO_APPROVAL_DETAIL.fullPath(),
                                            param: para,
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Approval_PO_Response_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if let allData = response.data {
                    self.allPOData = allData
                    if let lineItem = allData.productLineItem {
                        self.allPOLineItems = lineItem
                    } else {
                        self.allPOLineItems.removeAll()
                    }
                    self.setUpDataInUI()
                } else {
                    self.allPOData = nil
                    appView.makeToast("No PO Details Available!")
                    Loader.hideLoader()
                    DispatchQueue.main.async { [weak self] in
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            } else if let errorMessage {
                debugPrint("Error in PO Detail View \(errorMessage)")
                appView.makeToast("No PO Details Available!")
                Loader.hideLoader()
                DispatchQueue.main.async { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    /// `Fetches Data From SO Approval Detail API`
    private func fetchSODetailFromAPI() {
        let para = ["id" : idForFetchAPI]
        debugPrint("SO PARAM == \(para)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.GET_SO_APPROVAL_DETAIL.fullPath(),
                                            param: para,
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Approval_SO_Response_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if let allData = response.data {
                    self.allSOData = allData
                    if let lineItem = allData.lineItem {
                        self.allSOLineItems = lineItem
                    } else {
                        self.allSOLineItems.removeAll()
                    }
                    self.setUpSODetailInUI()
                } else {
                    self.allSOData = nil
                    appView.makeToast("No SO Details Available!")
                    Loader.hideLoader()
                    DispatchQueue.main.async { [weak self] in
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            } else if let errorMessage {
                debugPrint("Error in SO Detail View \(errorMessage)")
                appView.makeToast("No SO Details Available!")
                Loader.hideLoader()
                DispatchQueue.main.async { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    /// `Fetches Data From Pre-Shipment Invoice Approval Detail API`
    private func fetchPreShipInvoiceDetailFromAPI() {
        let para = ["id" : idForFetchAPI]
        debugPrint("PSI PARAM == \(para)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.GET_PSI_APPROVAL_DETAIL.fullPath(),
                                            param: para,
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Approval_PSI_Response_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if let allData = response.data {
                    debugPrint("BILL -- \(allData.billingType.non_NilEmpty())")
                    self.allPSIData = allData
                    if let lineItem = allData.invoiceItemList {
                        self.allPSILineItems = lineItem
                    } else {
                        self.allPSILineItems.removeAll()
                    }
                    self.setUpPSInvoiceDetailInUI()
                } else {
                    self.allPSIData = nil
                    appView.makeToast("No PSI Details Available!")
                    Loader.hideLoader()
                    DispatchQueue.main.async { [weak self] in
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            } else if let errorMessage {
                debugPrint("Error in PSI Detail View \(errorMessage)")
                appView.makeToast("No PSI Details Available!")
                Loader.hideLoader()
                DispatchQueue.main.async { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    /// `Fetches Data From Final Invoice Approval Detail API`
    private func fetchFinalInvoiceDetailFromAPI() {
        let para = ["id" : idForFetchAPI]
        debugPrint("FI PARAM == \(para)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.GET_PSI_APPROVAL_DETAIL.fullPath(),
                                            param: para,
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Approval_FI_Response_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if let allData = response.data {
                    debugPrint("BILL -- \(allData.billingType.non_NilEmpty())")
                    self.allFIData = allData
                    if let lineItem = allData.invoiceItemList {
                        self.allFILineItems = lineItem
                    } else {
                        self.allFILineItems.removeAll()
                    }
                    self.setUpPSInvoiceDetailInUI()
                } else {
                    self.allFIData = nil
                    appView.makeToast("No FI Details Available!")
                    Loader.hideLoader()
                    DispatchQueue.main.async { [weak self] in
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            } else if let errorMessage {
                debugPrint("Error in FI Detail View \(errorMessage)")
                appView.makeToast("No FI Details Available!")
                Loader.hideLoader()
                DispatchQueue.main.async { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

// MARK: - PO Approval Detail UI Methods
extension ApprovalDetailVC {
    private func setUpDataInUI() {
        setUpFirmDataInUI()
        setUpVendorDataInUI()
        setUpBankDataInUI()
        setUpShippingDataInUI()
        setUpInstructionDataInUI()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lineItemTableView.separatorStyle = self.allPOLineItems.count == 1 ? .none : .singleLine
            self.lineItemTableView.reloadData()
            Loader.hideLoader()
        }
    }
    
    private func setUpFirmDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allPOData else { return }
            // Section 1
            self.lblFirmName.text = data.company.non_NilEmpty()
            self.lblFirmAddress.text = "Address: " + (data.companyAddress == nil ? "N/A" : (data.companyAddress! + (data.companyAddress2 ?? "")))
            self.lblFirmPhone.text = "Phone: \(data.mobileNumber.non_NilEmpty())"
            self.lblFirmMail.text = "Email: \(data.email.non_NilEmpty())"
            self.lblPONumber.text = data.poNo.non_NilEmpty()
            self.lblDate.text = data.poDate.non_NilEmpty()
        }
    }
    
    private func setUpVendorDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allPOData else { return }
            // Section 2 (Part-1)
            self.lblVendorName.text = data.vendor.non_NilEmpty()
            var address = String()
            if data.vendorAddress1 == nil {
                address = "N/A"
            } else {
                address.append(data.vendorAddress1!)
                address.append(data.vendorAddress2 ?? "")
                address.append(data.vendorAddress3 ?? "")
                address.append(data.vendorAddress4 ?? "")
                address.append(data.vendorAddressRegion ?? "")
                address.append(data.vendorCountry ?? "")
            }
            
            self.lblVendorAddress.text = "Address: " + address
            self.lblVendorPhone.text = "Phone: \(data.vendorMobileNumber.non_NilEmpty())"
        }
    }
    
    private func setUpBankDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allPOData else { return }
            // Section 2 (Part-2)
            let isBankAvailable = (data.bank != nil) && !data.bank!.isEmpty
            var bankData: Bank_Detail_Modal! = nil
            if isBankAvailable {
                bankData = data.bank!.last ?? data.bank!.first!
            }
            self.lblBankName.text = "Bank: " + (isBankAvailable ? (bankData.bankName.non_NilEmpty()) : "N/A")
            self.lblBankAddress.text = "Address: " + (isBankAvailable ? (bankData.bankAddress.non_NilEmpty()) : "N/A")
            self.lblBankPhone.text = "Tel: " + (isBankAvailable ? (bankData.contactPersonNum.non_NilEmpty()) : "N/A")
            self.lblBankAccNumber.text = "A/C No.: " + (isBankAvailable ? (bankData.acNo.non_NilEmpty()) : "N/A")
        }
    }
    
    private func setUpShippingDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allPOData else { return }
            // Section 3
            self.lblShipBy.text = data.shipBy.non_NilEmpty()
            self.lblCountryOrigin.text = data.countryOfOriginGoods.non_NilEmpty()
            self.lblIncoterm.text = data.incoTerm.non_NilEmpty()
            self.lblFinalDestination.text = data.finalDestination == nil ? "N/A" : (data.finalDestination! + (data.finalDestinationCountry ?? ""))
            self.lblPaymentTerm.text = data.paymentTerm.non_NilEmpty()
            self.lblTotalAmount.text = data.total == nil ? "N/A" : "\(data.total!)"
        }
    }
    
    private func setUpInstructionDataInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allPOData else { return }
            // Section 4
            self.lblInstruction1.text = data.shippingInstruction.non_NilEmpty()
            self.lblInstruction2.text = data.shippingSchedule.non_NilEmpty()
            self.lblInstruction3.text = data.remark.non_NilEmpty()
            self.lblInstruction4.text = data.packingInstruction.non_NilEmpty()
        }
    }
}

// MARK: - SO Approval Detail UI Changing Methods
extension ApprovalDetailVC {
    private func setUpSODetailInUI() {
        setUpFirmDataForSOInUI()
        setSection1DataForSOInUI()
        setSection2DataForSOInUI()
        setSection3DataForSOInUI()
        setSection4DataForSOInUI()
        setSection5DataForSOInUI()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lineItemTableView.separatorStyle = self.allSOLineItems.count == 1 ? .none : .singleLine
            self.lineItemTableView.reloadData()
            Loader.hideLoader()
        }
    }
    
    /// `Setting Company Detail For SO in UI`
    private func setUpFirmDataForSOInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allSOData else { return }
            // Company Data
            self.lblFirmName.text = data.company.non_NilEmpty()
            self.lblFirmAddress.text = "Address: " + (data.companyAddress == nil ? "N/A" : (data.companyAddress! + (data.companyAddress2 ?? "")))
            self.lblFirmPhone.text = "Phone: \(data.mobileNumber.non_NilEmpty())"
            self.lblFirmMail.text = "Email: \(data.email.non_NilEmpty())"
            self.lblPONumber.text = data.soNo.non_NilEmpty()
            self.lblDate.text = data.soDate.non_NilEmpty()
        }
    }
    
    /// `Setting Detail For Section 1 For SO in UI`
    private func setSection1DataForSOInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allSOData else { return }
            // Buyer Detail
            self.lblBuyerOrderNum.text = data.buyerOrderNo.non_NilEmpty()
            self.lblBuyerDate.text = data.buyerOrderDate.non_NilEmpty()
        }
    }
    
    /// `Setting Detail For Section 2 For SO in UI`
    private func setSection2DataForSOInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allSOData else { return }
            // IEC, ISO, Reference Details
            self.lblIECnum.text = data.iecNo.non_NilEmpty()
            self.lblISONum.text = (data.iso.non_NilEmpty()) == "" ? "-" : (data.iso.non_NilEmpty())
            self.lblOtherReferences.text = (data.otherReference.non_NilEmpty()) == "" ? "-" : (data.otherReference.non_NilEmpty())
        }
    }
    
    /// `Setting Detail For Section 3 For SO in UI`
    private func setSection3DataForSOInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allSOData else { return }
            // Buyer Details
            var buyerDetail = data.buyerName.non_NilEmpty()
            if buyerDetail != "N/A" {
                buyerDetail = buyerDetail + "\nCountry: \(data.buyerCountry ?? "N/A")"
            }
            self.lblOtherBuyer.text = buyerDetail
            self.lblPreCarriageBy.text = data.preCarriageBy.non_NilEmpty()
            self.lblPlaceOfReceiptBy.text = data.placeOfReceipt.non_NilEmpty()
            self.lblVesselFlightNum.text = data.vesselFlightNo.non_NilEmpty()
        }
    }
    
    /// `Setting Detail For Section 4 For SO in UI`
    private func setSection4DataForSOInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allSOData else { return }
            // Ports Details
            self.lblPortOfLoading.text = data.portOfLoading.non_NilEmpty()
            self.lblSOCountryOfGoods.text = data.countryOfOriginGoods.non_NilEmpty()
            self.lblPortOfDischarge.text = (data.portOfDischarge.non_NilEmpty()) == "" ? "-" : (data.portOfDischarge.non_NilEmpty())
            self.lblCountryOfDestination.text = data.finalDestinationCountry.non_NilEmpty()
            self.lblTermsOfDelPayment.text = data.termsOfPaymentText.non_NilEmpty()
        }
    }
    
    /// `Setting Detail For Section 5 For SO in UI`
    private func setSection5DataForSOInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let data = self.allSOData else { return }
            // Final Destination
            self.lblSOFinalDestination.text = data.finalDestination.non_NilEmpty()
            self.lblTotalAmount.text = data.total == nil ? "N/A" : "\(data.total!)"
        }
    }
}

// MARK: - Invoice Approval Detail UI Changing Methods
extension ApprovalDetailVC {
    private func setUpPSInvoiceDetailInUI() {
        setUpFirmDataFromInvoiceInUI()
        setSection1DataForInvoiceInUI()
        setSection2DataForInvoiceInUI()
        setSection3DataForInvoiceInUI()
        setSection4DataForInvoiceInUI()
        setSection5DataForInvoiceInUI()
        setSection6DataForInvoiceInUI()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.currDocType == .PSI {
                self.lineItemTableView.separatorStyle = self.allPSILineItems.count == 1 ? .none : .singleLine
            } else {
                self.lineItemTableView.separatorStyle = self.allFILineItems.count == 1 ? .none : .singleLine
            }
            self.lineItemTableView.reloadData()
            Loader.hideLoader()
        }
    }
    
    /// `Setting Company Detail From Pre-Ship Invoice Detail in UI`
    private func setUpFirmDataFromInvoiceInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.currDocType == .PSI {
                guard let data = self.allPSIData else { return }
                // Company Details
                self.lblFirmName.text = data.company.non_NilEmpty()
                self.lblFirmAddress.text = "Address: " + (data.companyAddress == nil ? "N/A" : (data.companyAddress! + (data.companyAddress2 ?? "")))
                self.lblFirmPhone.text = "Phone: \(data.mobileNumber.non_NilEmpty())"
                self.lblFirmMail.text = "Email: \(data.email.non_NilEmpty())"
                self.lblPONumber.text = data.invoiceNo.non_NilEmpty()
                self.lblDate.text = data.invoiceDate.non_NilEmpty()
            } else {
                guard let data = self.allFIData else { return }
                // Company Details
                self.lblFirmName.text = data.company ?? "N/A"
                self.lblFirmAddress.text = "Address: " + (data.companyAddress == nil ? "N/A" : (data.companyAddress! + (data.companyAddress2 ?? "")))
                self.lblFirmPhone.text = "Phone: \(data.mobileNumber.non_NilEmpty())"
                self.lblFirmMail.text = "Email: \(data.email.non_NilEmpty())"
                self.lblPONumber.text = data.invoiceNo.non_NilEmpty()
                self.lblDate.text = data.invoiceDate.non_NilEmpty()
            }
        }
    }
    
    /// `Setting Detail For Section 1 For Invoice in UI`
    private func setSection1DataForInvoiceInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.currDocType == .PSI {
                guard let data = self.allPSIData else { return }
                // Tax Invoice Details
                self.lblTaxOrPreShipInvoice.text = data.taxInvNo.non_NilEmpty() == "" ? "-" : (data.taxInvNo.non_NilEmpty())
                self.lblDateTaxOrPreShipInvoice.text = data.taxInvDate.non_NilEmpty() == "" ? "-" : (data.taxInvDate.non_NilEmpty())
            } else {
                guard let data = self.allFIData else { return }
                // Tax Invoice Details
                self.lblTaxOrPreShipInvoice.text = data.preshipmentInvoiceNo.non_NilEmpty() == "" ? "-" : (data.preshipmentInvoiceNo.non_NilEmpty())
                self.lblDateTaxOrPreShipInvoice.text = data.preshipmentInvoiceNoDate.non_NilEmpty() == "" ? "-" : (data.preshipmentInvoiceNoDate.non_NilEmpty())
            }
        }
    }
    
    /// `Setting Detail For Section 2 For Invoice in UI`
    private func setSection2DataForInvoiceInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.currDocType == .PSI {
                guard let data = self.allPSIData else { return }
                // Buyer Order Details
                self.lblInvoiceBuyerOrderNum.text = (data.buyerOrderNo.non_NilEmpty()) == "" ? "-" : (data.buyerOrderNo.non_NilEmpty())
                self.lblDateBuyerOrder.text = (data.buyerOrderDate.non_NilEmpty()) == "" ? "-" : (data.buyerOrderDate.non_NilEmpty())
            } else {
                guard let data = self.allFIData else { return }
                // Buyer Order Details
                self.lblInvoiceBuyerOrderNum.text = (data.buyerOrderNo.non_NilEmpty()) == "" ? "-" : (data.buyerOrderNo.non_NilEmpty())
                self.lblDateBuyerOrder.text = (data.buyerOrderDate.non_NilEmpty()) == "" ? "-" : (data.buyerOrderDate.non_NilEmpty())
            }
        }
    }
    
    /// `Setting Detail For Section 3 For Invoice in UI`
    private func setSection3DataForInvoiceInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.currDocType == .PSI {
                guard let data = self.allPSIData else { return }
                // Delivery Details
                self.lblInvoiceDeliveryNum.text = data.deliveryNo.non_NilEmpty()
                self.lblInvoiceOtherReferences.text = data.otherReference.non_NilEmpty()
            } else {
                guard let data = self.allFIData else { return }
                // Delivery Details
                self.lblInvoiceDeliveryNum.text = data.deliveryNo.non_NilEmpty()
                self.lblInvoiceOtherReferences.text = data.otherReference.non_NilEmpty()
            }
        }
    }

    /// `Setting Detail For Section 4 For Invoice in UI`
    private func setSection4DataForInvoiceInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.currDocType == .PSI {
                guard let data = self.allPSIData else { return }
                // Consignee Details
                self.lblInvoiceConsignee.text = data.consigneeName.non_NilEmpty()
                self.lblInvoiceBuyerName.text = data.buyerName.non_NilEmpty()
                self.lblInvoicePlaceOfReceiptBy.text = data.placeOfReceipt.non_NilEmpty()
                self.lblInvoicePreCarriageBy.text = data.preCarriageBy.non_NilEmpty()
            } else {
                guard let data = self.allFIData else { return }
                // Consignee Details
                self.lblInvoiceConsignee.text = data.consigneeName.non_NilEmpty()
                self.lblInvoiceBuyerName.text = data.buyerName.non_NilEmpty()
                self.lblInvoicePlaceOfReceiptBy.text = data.placeOfReceipt.non_NilEmpty()
                self.lblInvoicePreCarriageBy.text = data.preCarriageBy.non_NilEmpty()
            }
        }
    }
    
    /// `Setting Detail For Section 5 For Invoice in UI`
    private func setSection5DataForInvoiceInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.currDocType == .PSI {
                guard let data = self.allPSIData else { return }
                // Port Details
                self.lblInvoicePortOfLoading.text = data.portOfLoading.non_NilEmpty()
                self.lblInvoiceCountryOfGoods.text = data.countryOfOriginGoods.non_NilEmpty()
                self.lblInvoicePortOfDischarge.text = data.portOfDischarge.non_NilEmpty()
                self.lblInvoiceCountryOfFinalDestination.text = data.finalDestinationCountry.non_NilEmpty()
                self.lblInvoiceTermsOfPayment.text = data.paymentTerm.non_NilEmpty()
            } else {
                guard let data = self.allFIData else { return }
                // Port Details
                self.lblInvoicePortOfLoading.text = data.portOfLoading.non_NilEmpty()
                self.lblInvoiceCountryOfGoods.text = data.countryOfOriginGoods.non_NilEmpty()
                self.lblInvoicePortOfDischarge.text = data.portOfDischarge.non_NilEmpty()
                self.lblInvoiceCountryOfFinalDestination.text = data.finalDestinationCountry.non_NilEmpty()
                self.lblInvoiceTermsOfPayment.text = data.paymentTerm.non_NilEmpty()
            }
        }
    }
    
    /// `Setting Detail For Section 6 For Invoice in UI`
    private func setSection6DataForInvoiceInUI() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.currDocType == .PSI {
                guard let data = self.allPSIData else { return }
                // Final Destination Details
                self.lblInvoiceFinalDestination.text = data.finalDestination.non_NilEmpty()
                self.lblInvoiceVesselFlightNum.text = data.vesselFlightNo.non_NilEmpty()
                self.lblTotalAmount.text = data.total == nil ? "N/A" : "\(data.total!)"
            } else {
                guard let data = self.allFIData else { return }
                // Final Destination Details
                self.lblInvoiceFinalDestination.text = data.finalDestination.non_NilEmpty()
                self.lblInvoiceVesselFlightNum.text = data.vesselFlightNo.non_NilEmpty()
                self.lblTotalAmount.text = data.total == nil ? "N/A" : "\(data.total!)"
            }
        }
    }
}

// MARK: - TextView Delegate Methods
extension ApprovalDetailVC: UITextViewDelegate {
    func adjustTextViewHeight() {
        let fixedWidth = textComment.frame.size.width
        let newSize = textComment.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        if newSize.height > getDynamicHeight(size: 100) {
            textComment.isScrollEnabled = false
            textCommentHeight.constant = newSize.height
        }
        view.layoutIfNeeded()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.adjustTextViewHeight()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if !textComment.text!.isEmpty && textComment.text! == defaultPlaceHolder {
            textComment.text = ""
            textComment.textColor = UIColor.label
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textComment.text.isEmpty {
            textComment.text = defaultPlaceHolder
            textComment.textColor = UIColor.placeholderText
        }
    }
}

// MARK: - TableView Delegate Methods
extension ApprovalDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch currDocType {
        case .PO:
            return allPOLineItems.count
        case .SO:
            return allSOLineItems.count
        case .PSI:
            return allPSILineItems.count
        case .FI:
            return allFILineItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UITableViewHeaderFooterView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: getDynamicHeight(size: 30)))
        headerView.clipsToBounds = true

        headerView.tintColor = .ourAppThemeColor
        headerView.layer.cornerRadius = 8
        headerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

        let lblHeading = UILabel()
        lblHeading.clipsToBounds = true
        lblHeading.translatesAutoresizingMaskIntoConstraints = false
        lblHeading.changeFontStyle(style: .Bold, fontSize: 18)
        lblHeading.DynamicHeight = true
        lblHeading.textColor = .white
        lblHeading.text = "Product Details"

        headerView.addSubview(lblHeading)
        lblHeading.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
        lblHeading.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        lblHeading.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10).isActive = true
        lblHeading.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5).isActive = true
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "approvalDetailCell", for: indexPath) as! approvalDetailCell
        cell.showHideMarkStackView(true)
        switch currDocType {
        case .PO:
            let data = allPOLineItems[indexPath.row]
            cell.fillAllDetails(index: indexPath.row+1, name: data.product ?? "N/A", desc: "HSN Code: \(data.hsnCode ?? "N/A")", rate: data.price ?? "N/A", quantity: data.quantity ?? "N/A", amount: data.amount == nil ? "N/A" : "\(data.amount!)", ifNotPOMarkContainerNum: (true, nil))
            cell.separatorInset = UIEdgeInsets(top: 0, left: allPOLineItems.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
        case .SO:
            let data = allSOLineItems[indexPath.row]
            cell.fillAllDetails(index: indexPath.row+1, name: data.product ?? "N/A", desc: "HSN Code: \(data.hsnCode ?? "N/A")", rate: data.price ?? "N/A", quantity: data.quantity ?? "N/A", amount: data.amount == nil ? "N/A" : "\(data.amount!)", ifNotPOMarkContainerNum: (false, data.marks))
            cell.separatorInset = UIEdgeInsets(top: 0, left: allSOLineItems.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
        case .PSI:
            let data = allPSILineItems[indexPath.row]
            cell.fillAllDetails(index: indexPath.row+1, name: data.product ?? "N/A", desc: "HSN Code: \(data.hsnCode ?? "N/A")", rate: data.price ?? "N/A", quantity: data.quantity ?? "N/A", amount: data.amount == nil ? "N/A" : "\(data.amount!)", ifNotPOMarkContainerNum: (false, data.marks ?? "N/A"))
            cell.separatorInset = UIEdgeInsets(top: 0, left: allPSILineItems.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
        case .FI:
            let data = allFILineItems[indexPath.row]
            cell.fillAllDetails(index: indexPath.row+1, name: data.product ?? "N/A", desc: "HSN Code: \(data.hsnCode ?? "N/A")", rate: data.price ?? "N/A", quantity: data.quantity ?? "N/A", amount: data.amount == nil ? "N/A" : "\(data.amount!)", ifNotPOMarkContainerNum: (false, data.marks ?? "N/A"))
            cell.separatorInset = UIEdgeInsets(top: 0, left: allFILineItems.count-1 == indexPath.row ? tableView.bounds.width : 0, bottom: 0, right: 0)
        }
        DispatchQueue.main.async { [weak self] in
            self?.lineItemTableHeight.constant = tableView.contentSize.height + 4
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class SelfSizingTableView: UITableView {
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        if bounds.size != intrinsicContentSize {
            invalidateIntrinsicContentSize()
        }
    }
}

// MARK: - Scroll View Delegate Method
extension ApprovalDetailVC {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if textComment.isFirstResponder {
            self.view.endEditing(true)
        }
    }
}
