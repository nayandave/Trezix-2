//
//  LogInVC.swift
//  Trezix-Demo1
//
//  Created by Amar Panchal on 18/05/22.
//

import UIKit

class LogInVC: UIViewController {
    // MARK: - ALL IB-OUTLETS
    @IBOutlet weak var textDomain: UITextField!
    @IBOutlet weak var textDomainHeight: NSLayoutConstraint!
        
    @IBOutlet weak var textUsername: UITextField!
    @IBOutlet weak var textUsernameHeight: NSLayoutConstraint!
    
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textPasswordHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblMailError: UILabel!
    @IBOutlet weak var lblPasswordError: UILabel!
    @IBOutlet weak var lblDomainError: UILabel!
    
    @IBOutlet weak var btnForgetPass: UIButton!
    @IBOutlet weak var btnLogIn: UIButton!
    
    // MARK: - ALL CONSTANTS & VARIABLES
    ///*Determine password entry is secured or not;* **initially it's TRUE e.g. secured password entry**
    var isPassSecured = true
    
    ///*Determines is password field should be hidden or not*, **Initially FALSE e.g. SIgnup API is remaining to be called**
    var isDomainAndMailValid = false
    
    var isUserNameEditingDisabled = false
    
    var isDomainEditingDisabled = false
    
    // MARK: - VIEW LIFE CYCLE METHODS
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        settingUpTextFields()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
        textPassword.isHidden = true
        [textDomain, textUsername, textPassword].forEach { textField in
            textField?.text = nil
            textField?.delegate = self
            textField?.addTarget(self, action: #selector(changeEditingOn(_:)), for: .editingChanged)
        }
        DispatchQueue.main.async { [weak self] in
            self?.btnLogIn.setTitle("NEXT", for: .normal)
            self?.btnLogIn.layer.applyShadowAndRadius(cornerRadius: self!.btnLogIn.frame.height/2)
            if let savedDomain = getUserDomain() {
                self?.textDomain.text = savedDomain
                self?.isDomainEditingDisabled = true
                self?.editButtonConfigDomain(addEditButton: true)
                self?.textUsername.becomeFirstResponder()
            }
        }
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
//            self.changeUIWithChangeSizeClass(sizeClass: self.traitCollection)
            self.view.layoutIfNeeded()
//            self.viewDidLayoutSubviews()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if !isRunningOnIPAD {
            self.textDomainHeight.constant = getDynamicHeight(size: 45)
            self.textUsernameHeight.constant = getDynamicHeight(size: 45)
            self.textPasswordHeight.constant = getDynamicHeight(size: 45)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.cornerRadiusTo(textField: self.textUsername)
            self.cornerRadiusTo(textField: self.textDomain)
        }
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
////        DispatchQueue.main.async { [weak self] in
////            guard let self = self else { return }
////            UIViewPropertyAnimator(duration: 1.5, curve: .easeInOut) {
//        if !isRunningOnIPAD {
//            self.textDomainHeight.constant = getDynamicHeight(size: 45)
//            self.textUsernameHeight.constant = getDynamicHeight(size: 45)
//            self.textPasswordHeight.constant = getDynamicHeight(size: 45)
//        }
//        self.cornerRadiusTo(textField: self.textUsername)
//        self.cornerRadiusTo(textField: self.textDomain)
////            }.startAnimation()
////            self.cornerRadiusTo(textField: self.textUsername)
////        }
//    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    
    // MARK: - ALL IB-ACTIONS
    ///**LogIn Button Action**
    @IBAction func Login_Action(_ sender: UIButton) {
        checkAllValidation()
    }
    
    ///**Forget Password Action**
    @IBAction func Forget_Pass_Action(_ sender: UIButton) {
        appView.makeToast("Code in in progress")
    }
    
    ///**Secure/Unsecure Password Text**
    @objc private func secure_Unsecure_Password(_ sender: UIButton) {
        isPassSecured = !isPassSecured
        textPassword.isSecureTextEntry = isPassSecured
    }
    
    @objc func editing_Domain(_ sender: UIButton) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.textPassword.layer.shadowOpacity = 0
            self.textPassword.leftView?.isHidden = true
            self.textPassword.rightView?.isHidden = true
            UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 0.65, initialSpringVelocity: 0.65, options: .curveEaseInOut) {
                self.btnForgetPass.isHidden = true
                self.textPasswordHeight.constant = 0
                self.editButtonConfigDomain(addEditButton: false)
                self.textUsername.text?.removeAll()
                self.editButtonConfigUserMail(addEditButton: false)
                self.view.layoutSubviews()
            } completion: { _ in
                self.isDomainAndMailValid = false
                self.btnLogIn.setTitle("NEXT", for: .normal)
                self.isDomainEditingDisabled = false
                self.isUserNameEditingDisabled = false
                self.textPassword.isHidden = true
                self.textDomain.becomeFirstResponder()
            }
        }
    }
    
    @objc func editing_User_Mail(_ sender: UIButton) {
//        saveUserDomain(domain: nil)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.textPassword.layer.shadowOpacity = 0
            self.textPassword.leftView?.isHidden = true
            self.textPassword.rightView?.isHidden = true
            UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 0.65, initialSpringVelocity: 0.65, options: .curveEaseInOut) {
                self.btnForgetPass.isHidden = true
                self.textPasswordHeight.constant = 0
                self.editButtonConfigUserMail(addEditButton: false)
                self.view.layoutSubviews()
            } completion: { _ in
                self.isDomainAndMailValid = false
                self.btnLogIn.setTitle("NEXT", for: .normal)
                self.isUserNameEditingDisabled = false
                self.textPassword.isHidden = true
                self.textUsername.becomeFirstResponder()
            }
        }
    }
}

// MARK: - ALL HELPER FUNCTIONS
extension LogInVC {
    /// **Gives rounded corner & shadow to UITextField**
    ///  - Parameter textField: UItextField on which you want add corner radius & shadow
    private func cornerRadiusTo(textField: UITextField) {
        textField.layer.borderColor = UIColor.white.cgColor
        textField.layer.borderWidth = 1
        textField.layer.applyShadowAndRadius(cornerRadius: textField.frame.height/2, shadowRadius: 1.25)
    }
    
    
    /// **Initial Setup of Username & Password Textfields**
    /// * **LeftViewMode Image For Both TextField**
    /// * **RightViewMode Hide/Unhide Texts in Password TextField**
    private func settingUpTextFields() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.textUsername.keyboardType = .emailAddress
            [self.textDomain, self.textUsername, self.textPassword].forEach { textField in
                let textFieldHeight = textField!.bounds.height
                
                let dynamic10 = getDynamicHeight(size: 10)
                let dynamic20 = getDynamicHeight(size: 20)
                
                let imageView = UIImageView(frame: CGRect(x: dynamic10, y: dynamic10, width: textFieldHeight-dynamic20, height: textFieldHeight-dynamic20))
                let strLeftImage = textField == self.textDomain ? "ic-Domain" : (textField == self.textUsername ? "ic-LoginUser" : "ic-LogInLock")
                let leftImage = UIImage(named: strLeftImage)
                imageView.tintColor = .ourAppThemeColor
                imageView.image = leftImage
                
                let contentView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldHeight, height: textFieldHeight))
                contentView.addSubview(imageView)
                textField?.leftView = contentView
                textField?.leftViewMode = .always
                
                textField?.returnKeyType = .next
                
                if textField == self.textPassword {
                    textField?.returnKeyType = .done
                    
                    let contentView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldHeight, height: textFieldHeight))
                    
                    let image = UIImageView(image: UIImage(named: "ic-LogInHide"))
                    image.frame = CGRect(x: dynamic10, y: dynamic10, width: textFieldHeight-dynamic20, height: textFieldHeight-dynamic20)
                    image.contentMode = .scaleAspectFit
                    contentView.addSubview(image)
                    
                    let button = UIButton(type: .custom)
                    button.setTitle("", for: .normal)
                    button.frame = contentView.frame
                    button.addTarget(self, action: #selector(self.secure_Unsecure_Password(_:)), for: .touchUpInside)
                    
                    contentView.addSubview(button)
                    
                    textField?.rightView = contentView
                    textField?.rightViewMode = .always
                }
            }
        }
    }
    
    ///**Shows Password TextField With Animation**
    ///
    /// - Gives Password TextField Desirable Height & Sets *hiddenPassword* to *FALSE*
    private func show_Password_TextField() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.textPassword.isHidden = false
            self.textPassword.leftView?.isHidden = false
            self.textPassword.rightView?.isHidden = false
            UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 0.65, initialSpringVelocity: 0.65, options: .curveEaseInOut) {
                self.btnForgetPass.isHidden = false
                self.textPasswordHeight.constant = self.textUsernameHeight.constant
//                self.view.layoutSubviews()
            } completion: { _ in
                self.isDomainAndMailValid = true
                self.btnLogIn.setTitle("LOG-IN", for: .normal)
                self.isUserNameEditingDisabled = true
                self.isDomainEditingDisabled = true
                self.editButtonConfigUserMail()
                self.editButtonConfigDomain()
                UIViewPropertyAnimator(duration: 0.25, curve: .easeInOut) {
                    self.cornerRadiusTo(textField: self.textPassword)
                    self.view.layoutSubviews()
                }.startAnimation()
            }
        }
    }
    
    private func editButtonConfigUserMail(addEditButton: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if addEditButton {
                let textFieldHeight = self.textUsernameHeight.constant
                
                let dynamic10 = getDynamicHeight(size: 10)
                let dynamic20 = getDynamicHeight(size: 20)
                
                let contentView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldHeight, height: textFieldHeight))
                
                let image = UIImageView(image: UIImage(named: "ic-Edit"))
                image.tintColor = .ourAppThemeColor
                image.frame = CGRect(x: dynamic10, y: dynamic10, width: textFieldHeight-dynamic20, height: textFieldHeight-dynamic20)
                image.contentMode = .scaleAspectFit
                contentView.addSubview(image)
                
                let button = UIButton(type: .custom)
                button.setTitle("", for: .normal)
                button.frame = contentView.frame
                button.addTarget(self, action: #selector(self.editing_User_Mail(_:)), for: .touchUpInside)
                
                contentView.addSubview(button)
                
                self.textUsername.rightView = contentView
                self.textUsername.rightViewMode = .always
            } else {
                self.textUsername.rightView = nil
                self.textUsername.rightViewMode = .never
            }
        }
    }
    private func editButtonConfigDomain(addEditButton: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if addEditButton {
                let textFieldHeight = self.textDomainHeight.constant
                
                let dynamic10 = getDynamicHeight(size: 10)
                let dynamic20 = getDynamicHeight(size: 20)
                
                let contentView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldHeight, height: textFieldHeight))
                
                let image = UIImageView(image: UIImage(named: "ic-Edit"))
                image.tintColor = .ourAppThemeColor
                image.frame = CGRect(x: dynamic10, y: dynamic10, width: textFieldHeight-dynamic20, height: textFieldHeight-dynamic20)
                image.contentMode = .scaleAspectFit
                contentView.addSubview(image)
                
                let button = UIButton(type: .custom)
                button.setTitle("", for: .normal)
                button.frame = contentView.frame
                button.addTarget(self, action: #selector(self.editing_Domain(_:)), for: .touchUpInside)
                
                contentView.addSubview(button)
                
                self.textDomain.rightView = contentView
                self.textDomain.rightViewMode = .always
            } else {
                self.textDomain.rightView = nil
                self.textDomain.rightViewMode = .never
            }
        }
    }
    
    
    ///**Checks All TextField's Validation, Shows Alerts & Calls Login_API or Valid_Mail_API Accordingly**
    private func checkAllValidation() {
        let allValidationLabels = [lblDomainError, lblMailError, lblPasswordError]
        if textDomain.hasText {
            if textUsername.hasText {
                if textUsername.text!.isValidEmail {
                    if isDomainAndMailValid {    // If Mail is valid means user is registered
                        if textPassword.hasText {
                            allValidationLabels.forEach { $0?.isHidden = true }
                            textUsername.text = textUsername.text?.trimmingCharacters(in: .whitespaces)
                            debugPrint("USER TEXT == \(textUsername.text!)...")
                            call_Login_API()
                        } else {
                            textPassword.errorShake()
                            lblPasswordError.text = "Password can not be empty!"
                            lblPasswordError.isHidden ? (lblPasswordError.isHidden = false) : (lblPasswordError.errorShake())
                        }
                    } else {
                        view.endEditing(true)
                        allValidationLabels.forEach { $0?.isHidden = true }
                        textDomain.text = textDomain.text?.trimmingCharacters(in: .whitespaces)
                        debugPrint("DOMAIN TEXT == \(textDomain.text!)...")
                        call_Valid_Mail_API()
                    }
                } else {
                    textUsername.errorShake()
                    lblMailError.text = "Enter Valid E-mail!"
                    lblMailError.isHidden ? (lblMailError.isHidden = false) : (lblMailError.errorShake())
                }
            } else {
                textUsername.errorShake()
                lblMailError.text = "Username can not be empty!"
                lblMailError.isHidden ? (lblMailError.isHidden = false) : (lblMailError.errorShake())
            }
        } else {
            textDomain.errorShake()
            lblDomainError.text = "Domain can not be empty!"
            lblDomainError.isHidden ? (lblDomainError.isHidden = false) : (lblDomainError.errorShake())
        }
    }
    
    ///**E-mail Validation Signup API Call**
    /// - Shows if Entered Mail Address is Registered in System or not
    /// - If email is registered it will allow Password textField to show otherwise will show error toast
    /// - Parameter email: Entered mail address that will be checked if it's registered or not and returns User's Domain
    private func call_Valid_Mail_API() {
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        let para = ["domain" : textDomain.text!,
                    "email": textUsername.text!]
        NetworkClient.networkRequest(method: .post,
                                     apiName: APIConstants.VALIDATE_MAIL.fullPath(),
                                     param: para,
                                     showHud: false, senderView: textUsername)
        { [weak self] (isSuccess, response: BasicResponseModal?) in
            guard let self = self else { return }
//            Loader.hideLoader()
            if isSuccess, let response = response {
                if response.status == 1 {
                    saveUserDomain(domain: response.msg)
                    self.show_Password_TextField()
                } else {
                    if response.msg == "Domain not found." {
                        self.lblDomainError.text = "Domain Not Found!"
                        self.lblDomainError.isHidden = false
                    } else if response.msg == "Opps!!Invalid Email!" {
                        self.lblMailError.text = "Please Enter Valid Mail!"
                        self.lblMailError.isHidden = false
                    } else {
                        debugPrint("Something Went Wrong in Valid Mail API")
                        debugPrint(response.msg)
                        appView.makeToast(response.msg)
                    }
                }
            }
        }
    }
    
    
    ///**Calls Login API**
    /// - Parameter email: Entered Email Address
    /// - Parameter password: Entered Password
    /// - Parameter token: Device Token For Firebase Push Notification
    /// - Shows if Entered Mail Address is Registered in System or not
    /// - If email is registered it will allow Password textField to show otherwise will show error toast
    private func call_Login_API() {
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        let para = ["email": textUsername.text!,
                    "password": textPassword.text!,
                    "device_name": device_name,
                    "device_id": unique_device_id,
                    "token": get_Device_Token_UD()]
        debugPrint("LOGIN PARA = \(para)")
        NetworkClient.networkRequest(method: .post,
                                     apiName: APIConstants.LOGIN.fullPath(),
                                     param: para,
                                     showHud: false, senderView: btnLogIn)
        { [weak self] (isSuccess, response: LoginResponseModal?) in
            guard let self = self else { return }
            if let response = response {
                if isSuccess { //Successful Login
                    if response.status == 1, let userData = response.data {
                        HapticFeedbackGenerator.notificationFeedback(type: .success)
                        saveUserData(userData: userData)
                        saveUserDomain(domain: self.textDomain.text!)
                        //                            saveAppToken(appToken: userData.appToken)
                        KeychainHelper.shared.save(userData.appToken, service: KC_AppToken_Key, account: APPNAME)
                        KeychainHelper.shared.save(userData.id, service: KC_UserID_Key, account: APPNAME)
                        mainUserData = userData
                        user_Is_Logged_In(is_set: true)
                        self.check_All_Permission(permissions: userData.permission)
                    } else if response.status == 2 {
                        HapticFeedbackGenerator.notificationFeedback(type: .warning)
                        debugPrint(response.msg)
                        appView.makeToast(response.msg)
                    } else {
                        if response.msg == "Opps!!Invalid Password!" {
                            self.textPassword.errorShake()
                        }
                        HapticFeedbackGenerator.notificationFeedback(type: .warning)
                        debugPrint(response.msg)
                        appView.makeToast(response.msg)
                    }
                } else { //Error in Login
                    HapticFeedbackGenerator.notificationFeedback(type: .error)
                    debugPrint("Data Pasrsing Error in Call Login API!")
                    appView.makeToast("Something Went Wrong!\nPlease Try Again Later!!")
                }
            } else { // Error Calling API
                HapticFeedbackGenerator.notificationFeedback(type: .error)
                debugPrint("Error calling LOGIN API Due to Unknown Reason")
                appView.makeToast("Something Went Wrong!\nPlease Try Again Later!")
            }
        }
    }
    
    private func check_All_Permission(permissions: [String]?) {
        guard let permissions else {
            all_Permissions = All_Grantable_Permission.allCases
            save_Granted_Permissions()
            sceneDelegate.redirect_HomeStoryboard()
            return
        }
        let allStrPermissions = All_Grantable_Permission.allCases.map { $0.rawValue }
        let perm = Set(permissions).intersection(Set(allStrPermissions))
        all_Permissions = All_Grantable_Permission.allCases.filter { perm.contains($0.rawValue) }
        save_Granted_Permissions()
        sceneDelegate.redirect_HomeStoryboard()
    }
    
}

extension LogInVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var shouldReturn = false
        if textField == textDomain {
            shouldReturn = true
            textUsername.becomeFirstResponder()
        } else if textField == textUsername {
            if isDomainAndMailValid {
                shouldReturn = true
                textPassword.becomeFirstResponder()
            } else {
                checkAllValidation()
            }
        } else {
            view.endEditing(true)
            shouldReturn = true
        }
        return shouldReturn
    }
    
    @objc private func changeEditingOn(_ textField: UITextField) {
        if textField == textDomain {
            lblDomainError.isHidden = true
        } else if textField == textUsername {
            lblMailError.isHidden = true
        } else {
            lblPasswordError.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !textField.hasText {
            if textField == textDomain {
                lblDomainError.text = "Please Enter Domain First!"
                lblDomainError.isHidden ? (lblDomainError.isHidden = false) : (lblDomainError.errorShake())
            } else if textField == textUsername {
                lblMailError.text = "Please Enter E-mail!"
                lblMailError.isHidden ? (lblMailError.isHidden = false) : (lblMailError.errorShake())
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textDomain && !isDomainEditingDisabled {
            if textField.hasText {
                return string != "."
            }
            return true
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textDomain {
            return !isDomainEditingDisabled
        } else if textField == textUsername {
            return !isUserNameEditingDisabled
        }
        return true
    }
}


//// MARK: - KEYBOARD HIDE/SHOW NOTIFICATION CENTER ACTIONS
//extension LogInVC {
//    @objc func keyboardWillShow(notification: NSNotification) {
//        if let userInfo = notification.userInfo {
//            if let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
//                self.textStackViewBottomSpace.constant = keyboardSize.height - appView.safeAreaInsets.bottom + 10
//                self.view.layoutIfNeeded()
//            }
//        }
//    }
//
//    @objc func keyboardWillHide(notification: NSNotification) {
//        self.textStackViewBottomSpace.constant = 60
//        self.view.layoutIfNeeded()
//    }
//}
