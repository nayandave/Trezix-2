//
//  InitialAnimationVC.swift
//  Trezix-Demo1
//
//  Created by Amar Panchal on 15/06/22.
//

import UIKit

class InitialAnimationVC: UIViewController {
    
    // MARK: - ALL IB_OUTLETS
    @IBOutlet weak var logoContentView: UIStackView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var lblMainTag: UILabel!
    
    // MARK: - ALL VARIABLES & CONSTANTS
    let rech = Reachability()!
    
    // MARK: - VIEW LIFE CYCLE METHODS
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.shouldAnimate(animate: false)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addNetworkNotificationObserver()
    }
}
 
// MARK: - ALL HELPER FUNCTIONS
extension InitialAnimationVC {
    /// `Adds Network Reachability Notification Observer and Start Reachability Notifier`
    /// - Adds NotificationCenter Observer for Network Rechability
    /// - Starts Rechability StartNotifier function which triggers NotificationCenter Even when Network Changes occurs
    private func addNetworkNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.changedReachability), name: ReachabilityChangedNotification, object: nil)
        do {
            try rech.startNotifier()
        } catch let err {
            debugPrint("Error while starting network notifier in InitialAnimationVC Due to \(err.localizedDescription)")
        }
    }
    
    
    /// `Handles Change in Network Availability`
    /// * Reachability Notification Observer is added in viewDidLoad
    /// 1. If Internet is available from before opening app, then this function will be executed immediatly
    /// 2. If Internet is not available from beginning, then this function won't be called as Notification Observer won't be triggered unless internet is available; When internet returns this function will be called
    /// * When this function will be called, Notification Observer will be removed from this VC, so it won't execute when moved from this VC to any other VCs
    /// * Currently, it's called once as this VC is called once, if in future this VC is called again then, some action should be needed as Notification Observer is added in viewDidLoad
    @objc func changedReachability() {
        let reach = appDelegate.reachability
        if reach.isReachable {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.shouldAnimate(animate: true)
            }
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    /// `Fetches All Data & Processes To Next VCs`
    /// * If **Data is Available & User is logged in**, then processes to **HomePageVC**
    /// * If **Data is not Available**, then processes to **LogInVC**
    private func processNextView() {
        if is_User_Logged_In() { //User Has Logged In Before
            if mainUserData == nil {
                mainUserData = getUserData()
            }
            if all_Permissions.isEmpty {
                all_Permissions = get_Granted_Permissions()
            }
            sceneDelegate.redirect_HomeStoryboard()
        } else {
            sceneDelegate.redirect_LoginStoryboard()
        }
    }
    
    /// `Starting/Removing Initial Animation`
    /// * Starts Initial Animation
    private func shouldAnimate(animate: Bool) {
        if animate {
            UIView.animate(withDuration: 0.625, delay: 0.5, options: [.allowAnimatedContent], animations: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.logoContentView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                strongSelf.logoContentView.alpha = 0.6
            }, completion: { [weak self] _ in
                UIView.animate(withDuration: 1.25, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
                    guard let self = self else { return }
                    self.logoContentView.transform = CGAffineTransform(scaleX: 1, y: 1)
                    self.logoContentView.alpha = 1
                }, completion: { [weak self] _ in
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.processNextView()
                    }
                })
            })
        } else {
            if logoContentView.layer.animationKeys() != nil {
                logoContentView.transform = .identity
                logoContentView.layer.removeAllAnimations()
            }
        }
    }
}
