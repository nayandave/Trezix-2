//
//  Preview_Document_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 03/04/23.
//

import UIKit
import PDFKit
import MobileCoreServices
import Photos
import Alamofire

public struct DocumentInfo {
    let docName: String
    let isPDF: Bool
    let docData: NetworkClient.imageOrFilePath
}

class Preview_Document_VC: UIViewController {
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblDocumentName: UILabel!
    @IBOutlet weak var documentPDFFrameView: UIView!
    @IBOutlet weak var documentImageFrameView: UIView!
    @IBOutlet weak var invalidDocView: UIView!
    @IBOutlet weak var lblInvalidDoc: UILabel!
    
    @IBOutlet weak var btn_Delete_Upload_Doc: UIButton!
    
    @IBOutlet weak var docCollectionView: UICollectionView!
    
    
    // MARK: - All Constants & Variables
    public var inUpload_Preview = true
    /// When ``inUpload_Preview`` is `FALSE`, below value is used
    public var documentData: Document_List_Data! = nil
    public var mainURL = String()
    private var allDocNames = [String]()
    
    
    /// When ``inUpload_Preview`` is `TRUE`, below values are used
    public var arrAllDocuments = [DocumentInfo]()
    
    private var doc_Image_View: UIImageView! = nil
    private var doc_PDF_View: PDFView! = nil
    
    private var selectedIndex = 0
    public var doc_Type: arrDocumentTypes = .po
    
    private var openedCamera: Bool = false
    
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize_VC()
        dump(arrAllDocuments)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        documentPDFFrameView.layer.applyShadowAndRadius()
        documentImageFrameView.layer.applyShadowAndRadius()
        docCollectionView.layer.borderWidth = 1
        docCollectionView.layer.borderColor = UIColor.separator.cgColor
        invalidDocView.layer.applyShadowAndRadius()
        btn_Delete_Upload_Doc.layer.applyShadowAndRadius()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if !openedCamera {
            debugPrint("Deletion Code Called!!!")
            delete_All_Temp_Documents()
        } else {
            debugPrint("Deletion Code NOTT Called!!!")
        }
    }
    
    // MARK: - All Helper Methods
    
    // MARK: - Initial Required Methods
    private func initialize_VC() {
        docCollectionView.register(UINib(nibName: "Doc_Preview_CollectionCell", bundle: nil), forCellWithReuseIdentifier: "Doc_Preview_CollectionCell")
        changeUI_According_Preview()
        initDocPDFView()
        initDocImageView()
        if inUpload_Preview {
            setUpPreview(forDoc: arrAllDocuments[selectedIndex])
        } else {
            split_All_Names()
            setUpPreview(forIndex: selectedIndex)
        }
        docCollectionView.reloadData()
    }
    
    private func split_All_Names() {
        guard let fullName = documentData.fileName else { show_Invalid_Doc_Alert(docName: documentData.fileName, invalidName: true); return }
        allDocNames = fullName.components(separatedBy: ",")
        debugPrint("All DocNames - \(allDocNames)")
    }
    
    private func initDocPDFView() {
        doc_PDF_View = PDFView()
        configurePDF()
        documentPDFFrameView.addSubview(doc_PDF_View)
        doc_PDF_View.translatesAutoresizingMaskIntoConstraints = false
        doc_PDF_View.topAnchor.constraint(equalTo: documentPDFFrameView.topAnchor, constant: 5).isActive = true
        doc_PDF_View.bottomAnchor.constraint(equalTo: documentPDFFrameView.bottomAnchor, constant: -5).isActive = true
        doc_PDF_View.leadingAnchor.constraint(equalTo: documentPDFFrameView.leadingAnchor, constant: 5).isActive = true
        doc_PDF_View.trailingAnchor.constraint(equalTo: documentPDFFrameView.trailingAnchor, constant: -5).isActive = true
    }
    
    private func configurePDF() {
        doc_PDF_View.maxScaleFactor = 3
        doc_PDF_View.minScaleFactor = doc_PDF_View.scaleFactor
        doc_PDF_View.autoScales = true
        doc_PDF_View.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        doc_PDF_View.displayMode = .singlePageContinuous
        doc_PDF_View.displaysPageBreaks = true
        doc_PDF_View.displayDirection = .vertical
        doc_PDF_View.backgroundColor = .clear
    }
    
    private func initDocImageView() {
        doc_Image_View = UIImageView()
        doc_Image_View.contentMode = .scaleAspectFit
        doc_Image_View.backgroundColor = .clear
        documentImageFrameView.addSubview(doc_Image_View)
        doc_Image_View.translatesAutoresizingMaskIntoConstraints = false
        doc_Image_View.topAnchor.constraint(equalTo: documentImageFrameView.topAnchor, constant: 5).isActive = true
        doc_Image_View.bottomAnchor.constraint(equalTo: documentImageFrameView.bottomAnchor, constant: -5).isActive = true
        doc_Image_View.leadingAnchor.constraint(equalTo: documentImageFrameView.leadingAnchor, constant: 5).isActive = true
        doc_Image_View.trailingAnchor.constraint(equalTo: documentImageFrameView.trailingAnchor, constant: -5).isActive = true
    }
    
    // MARK: - UI Changing Methods
    /// `Generates Preview For ToBe Uploaded Documents which are selected by user to upload`
    private func setUpPreview(forDoc: DocumentInfo) {
        documentPDFFrameView.isHidden = !forDoc.isPDF
        documentImageFrameView.isHidden = forDoc.isPDF
        
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            if forDoc.isPDF {
                if case let .path(pdfURL) = forDoc.docData {
                    
                    guard let stringPath = pdfURL.gettingPath().removingPercentEncoding,
                          let pdfDoc = PDFDocument(url: URL(fileURLWithPath: stringPath)) else {
                        self?.show_Invalid_Doc_Alert(docName: forDoc.docName)
                        return
                    }
                    DispatchQueue.main.async {
                        self?.doc_PDF_View.document = pdfDoc
                        self?.invalidDocView.isHidden = true
                    }
                } else {
                    DispatchQueue.main.async {
                        self?.show_Invalid_Doc_Alert(docName: forDoc.docName)
                    }
                }
            }
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lblDocumentName.text = forDoc.docName
            if !forDoc.isPDF {
                if case let .image(image) = forDoc.docData {
                    self.invalidDocView.isHidden = true
                    self.doc_Image_View.image = image
                } else {
                    self.show_Invalid_Doc_Alert(docName: forDoc.docName)
                }
            }
        }
    }
    
    /// `Generates Preview For Uploaded Documents which are only in ViewMode and already downloaded`
    private func setUpPreview(forIndex: Int) {
        var urlString = API_BaseURL
        urlString.append("\(mainURL)")
        urlString.append("\(allDocNames[forIndex])")
        guard let finalURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { debugPrint("Invalid URL Format to pass"); appView.makeToast("Invalid URL!"); return }
        debugPrint("FINAL URL - \(finalURL)")
        let isPDF = (finalURL as NSString).pathExtension.contains("pdf")
        let url = URL(string: finalURL)!
        
        documentPDFFrameView.isHidden = !isPDF
        documentImageFrameView.isHidden = isPDF
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            if isPDF {
                guard let pdfDoc = PDFDocument(url: url) else {
                    self?.show_Invalid_Doc_Alert(docName: self?.allDocNames[forIndex])
                    return
                }
                DispatchQueue.main.async { [weak self] in
                    self?.invalidDocView.isHidden = true
                    self?.doc_PDF_View.document = pdfDoc
                }
            }
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lblDocumentName.text = self.allDocNames[forIndex]
            if isPDF {

            } else {
                self.doc_Image_View.animateLoader(.gray)
                self.doc_Image_View.sd_setImage(with: url) { (image, error, cache, url) in
                    self.doc_Image_View.remove_Activity_Animator {
                        if let image {
                            self.invalidDocView.isHidden = true
                            self.doc_Image_View.image = image
                        } else {
                            self.show_Invalid_Doc_Alert(docName: self.allDocNames[forIndex])
                        }
                    }
                }
            }
        }
    }
    
    private func show_Invalid_Doc_Alert(docName: String?, invalidName: Bool = false) {
        if let docName {
            debugPrint("Can't Load Document named \(docName)")
        }
        if invalidName { debugPrint("Invalid Name when viewing uploaded document") }
        DispatchQueue.main.async { [weak self] in
            self?.invalidDocView.isHidden = false
            appView.makeToast("Unable to load document!")
        }
    }
    
    private func changeUI_According_Preview() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.btn_Delete_Upload_Doc.setTitle(self.inUpload_Preview ? "Upload Document" : "Delete Document", for: .normal)
            self.btn_Delete_Upload_Doc.backgroundColor = self.inUpload_Preview ? .ourAppThemeColor : UIColor.hexStringToUIColor(hex: "C00606")
        }
    }
    
    
    // MARK: - Upload Section Methods
    private func show_Upload_Alert() {
        let uploadAlert = UIAlertController(title: "Wait", message: "Are you sure, you want to upload this document(s) ?", preferredStyle: .alert)
        uploadAlert.addAction(UIAlertAction(title: "No", style: .destructive))
        let uploadAction = UIAlertAction(title: "Upload", style: .default) { [weak self] _ in
            self?.upload_Docs_API { [weak self] isSuccess in
                if isSuccess {
                    self?.delete_All_Temp_Documents()
                    self?.dismiss(animated: true, completion: {
                        let vcs = self!.navigationController!.viewControllers
                        self?.navigationController?.popToViewController(vcs[1] as! Document_List_VC, animated: true)
                        NotificationCenter.default.post(name: .refreshDocumentListNotification, object: nil)
                    })
                } else {
                    let alert = UIAlertController(title: "Oops!", message: "This document(s) can't be uploaded at the time, Please try again leter.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self?.present(alert, animated: true)
                }
            }
        }
        uploadAlert.addAction(uploadAction)
        present(uploadAlert, animated: true)
    }
    
    private func upload_Docs_API(completion: @escaping ((_ isSuccess: Bool) -> Void)) {
        debugPrint("Upload Process Started!!!")
        var allFilesData = [NetworkClient.FileInfo]()
        for file in arrAllDocuments {
            if file.isPDF {
                allFilesData.append(NetworkClient.FileInfo(paramName: "file[]", ImageOrFilePath: file.docData))
            } else {
                allFilesData.append(NetworkClient.FileInfo(paramName: "file[]", imagename: file.docName, ImageOrFilePath: file.docData))
            }
        }
        let param: [String: Any] = ["file_type" : doc_Type.rawValue]
        NetworkClient.postMultipleFilesWithMultipartData(url: APIConstants.UPLOAD_DOC.fullPath(),
                                                         param: param, filesInfo: allFilesData, header: default_app_header, showLoader: true)
        { [weak self] (isSuccess, response: BasicResponseModalForApproval?, errorMessage) in
            guard let _ = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                if response.status == 1 {
                    appView.makeToast("Document Uploaded Successfully!")
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                    debugPrint("SUCCESS UPLOAD")
                    completion(true)
                } else {
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                    debugPrint("Error Messgae - \(response.message ?? "NA")")
                    completion(false)
                }
            } else {
                HapticFeedbackGenerator.notificationFeedback(type: .error)
                debugPrint("Something went wrong while uploading : \(errorMessage ?? "NAA")")
                completion(false)
            }
        }
    }
    
    private func delete_All_Temp_Documents() {
        if inUpload_Preview {
            let tempDocs = arrAllDocuments
            DispatchQueue.global(qos: .utility).async { [weak self] in
                for doc in tempDocs {
                    if case let .path(tempURL) = doc.docData {
                        
                        guard let tempDocURL = tempURL.gettingPath().removingPercentEncoding else {
                            appView.makeToast("Error deleting \(tempURL.lastPathComponent)")
                            debugPrint("Spaces in Document URL..")
                            return
                        }
                        
                        debugPrint("Temp DocURL - \(tempDocURL)")
                        if FileManager.default.fileExists(atPath: tempDocURL) {
                            do {
                                try FileManager.default.removeItem(atPath: tempDocURL)
                                debugPrint("Deleted Document Named - \(doc.docName) in URL = \(tempDocURL)")
                            } catch let err {
                                debugPrint("While deleting path at \(tempDocURL) error occured as \(err.localizedDescription)")
                            }
                        } else {
                            debugPrint("Document Named \(doc.docName) Does Not Exist in URL = \(tempDocURL)!")
                        }
                    }
                }
            }
        }
    }
    
    
    // MARK: - Delete Section Methods
    private func show_Delete_Alert() {
        let deleteAlert = UIAlertController(title: "Wait", message: "Are you sure want to delete ?\n\nNOTE: This action can not be undone later!", preferredStyle: .alert)
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        let deleteButton = UIAlertAction(title: "Delete", style: .destructive) { [weak self] _ in
            guard let self = self else { return }
            self.delete_Doc_API { [weak self] isSuccess in
                if isSuccess {
                    appView.makeToast("Document Deleted Successfully!")
                    self?.dismiss(animated: true, completion: {
                        let vcs = self!.navigationController!.viewControllers
                        self?.navigationController?.popToViewController(vcs[1] as! Document_List_VC, animated: true)
                        NotificationCenter.default.post(name: .refreshDocumentListNotification, object: nil)
                    })
                } else {
                    let alert = UIAlertController(title: "Oops!", message: "This document(s) can't be deleted at the time, Please try again leter.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self?.present(alert, animated: true)
                }
            }
        }
        deleteAlert.addAction(deleteButton)
        self.present(deleteAlert, animated: true)
    }
    
    private func delete_Doc_API(completion: @escaping ((_ isSuccess: Bool) -> Void)) {
        guard let id = documentData.id else {
            debugPrint("Invalid ID to delete document!")
            appView.makeToast("Can't delete right now!\nPlease try again later!")
            return
        }
        let param = ["id" : id]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.DELETE_DOC.fullPath(),
                                            param: param, headers: [default_app_header], showHud: true)
        { (isSuccess, response: BasicResponseModalForApproval?, errorMessage) in
            Loader.hideLoader()
            if isSuccess , let response {
                if response.status == 1 {
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                    completion(true)
                } else {
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                    completion(false)
                }
            } else {
                HapticFeedbackGenerator.notificationFeedback(type: .error)
                completion(false)
            }
        }
    }
    
    // MARK: - Add More Documents Helper Methods
    /// **Initialize and present Document Picker View Controller**
    private func init_DocumentPicker() {
        HapticFeedbackGenerator.simpleFeedback(type: .light)
        var documentVC: UIDocumentPickerViewController! = nil
        if #available(iOS 14, *) {
            documentVC = UIDocumentPickerViewController(forOpeningContentTypes: [.image, .pdf])
        } else {
            documentVC = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String,
                                                                        kUTTypeImage as String], in: .import)
        }
        documentVC.delegate = self
        documentVC.allowsMultipleSelection = true
        documentVC.modalPresentationStyle = .popover
        present(documentVC, animated: true)
    }
    
    /// **Open Camera**
    private func open_Camera() {
        HapticFeedbackGenerator.simpleFeedback(type: .light)
        PHPhotoLibrary.handle_Authorization_CompletionHandler(mediaType: .camera) { [weak self] in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.openedCamera = true
                let cameraVc = UIImagePickerController()
                cameraVc.delegate = self
                cameraVc.sourceType = .camera
                DispatchQueue.main.async {
                    appViewController?.present(cameraVc, animated: true)
                }
//                self.present(cameraVc, animated: true)
            }
        }
    }
    
    /// **Open Photo Gallery**
    private func open_Photo_Gallery() {
        HapticFeedbackGenerator.simpleFeedback(type: .light)
        PHPhotoLibrary.handle_Authorization_CompletionHandler { [weak self] in
            DispatchQueue.main.async { [weak self] in
                let picker = NohanaImagePickerController()
                picker.maximumNumberOfSelection = Int.max
                picker.toolbarHidden = true
                picker.delegate = self
                self?.present(picker, animated: true)
            }
        }
    }
    
    // MARK: - All Actions
    @IBAction func back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @objc private func add_More_Doc_Action(_ sender: UIButton) {
        if arrAllDocuments.count < 5 {
            more_Doc_Option_Alert()
        } else {
            let limitAlert = UIAlertController(title: "Oops", message: "You have reached limit of maximum number of documents!", preferredStyle: .alert)
            limitAlert.addAction(UIAlertAction(title: "OK", style: .destructive))
            present(limitAlert, animated: true)
        }
    }
    
    private func more_Doc_Option_Alert() {
        let optionAlert = UIAlertController(title: "Add Documents", message: "Please select from below options to add more document(s).", preferredStyle: .alert)
        let galleryAction = UIAlertAction(title: "Photos", style: .default) { [weak self] _ in
            self?.open_Photo_Gallery()
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { [weak self] _ in
            self?.open_Camera()
        }
        
        let filesAction = UIAlertAction(title: "Files", style: .default) { [weak self] _ in
            self?.init_DocumentPicker()
        }
        
        optionAlert.addAction(galleryAction)
        optionAlert.addAction(cameraAction)
        optionAlert.addAction(filesAction)
        optionAlert.addAction(UIAlertAction(title: "Cancel", style: .destructive))
        
        present(optionAlert, animated: true)
    }
    
    
    @IBAction func delete_Upload_Button_Action(_ sender: UIButton) {
        if inUpload_Preview {
            show_Upload_Alert()
        } else {
            show_Delete_Alert()
        }
    }

    @objc private func cancel_Document_Action(_ sender: UIButton) {
        let hitPoint = sender.convert(CGPoint.init(x: 5, y: 5), to: docCollectionView)
        if let indexPath = docCollectionView.indexPathForItem(at: hitPoint) {
            // use indexPath to get needed data
            debugPrint("Calculated IndexPath - \(indexPath)")
            if arrAllDocuments.count == 1 {     //Only One Document
                arrAllDocuments.removeAll()
                navigationController?.popViewController(animated: true)
            } else {
                if selectedIndex != indexPath.item {     // To be deleted document is not currently selected
                    arrAllDocuments.remove(at: indexPath.item)
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.docCollectionView.deleteItems(at: [indexPath])
                    }
                } else {        // Currently selected document is to be deleted
                    arrAllDocuments.remove(at: indexPath.item)
                    if indexPath.item != 0 { selectedIndex -= 1 }
                    setUpPreview(forDoc: arrAllDocuments[selectedIndex])
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.docCollectionView.deleteItems(at: [indexPath])
                    }
                }
            }
        }
    }
}


// MARK: - CollectionView Delegate Methods
extension Preview_Document_VC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return inUpload_Preview ? (arrAllDocuments.count + 1) : (allDocNames.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Doc_Preview_CollectionCell", for: indexPath) as! Doc_Preview_CollectionCell
        if inUpload_Preview {
            if indexPath.item < arrAllDocuments.count {
                let mainData = arrAllDocuments[indexPath.row]
                if mainData.isPDF {
                    // NOTE: - Extraxting Associated Value from enum
                    if case let .path(pdfPath) = mainData.docData {
                        cell.setPDF(pdfURL: pdfPath)
                    }
                } else {
                    if case let .image(image) = mainData.docData {
                        cell.setImage(image: image)
                    }
                }
                cell.btnCancel.tag = indexPath.item
                cell.btnCancel.addTarget(self, action: #selector(cancel_Document_Action(_:)), for: .touchUpInside)
            } else {
                cell.isButton()
                cell.btnAdd.addTarget(self, action: #selector(add_More_Doc_Action(_:)), for: .touchUpInside)
            }
        } else {
            var url = API_BaseURL
            url.append("\(mainURL)")
            url.append("\(allDocNames[indexPath.item])")
            guard let finalURL = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { debugPrint("Invalid URL Format to pass"); appView.makeToast("Invalid URL!"); return cell }
            debugPrint("Final URL in Cell -- \(finalURL)")
            cell.setData(finalURL: finalURL)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if inUpload_Preview {
            if indexPath.item != arrAllDocuments.count {     // Not Last Add Button Cell
                if indexPath.item != selectedIndex {
                    selectedIndex = indexPath.item
                    setUpPreview(forDoc: arrAllDocuments[indexPath.item])
                }
            }
        } else {
            if indexPath.item != selectedIndex {
                selectedIndex = indexPath.item
                setUpPreview(forIndex: selectedIndex)
            }
        }
    }
}
// MARK: - CollectionView Flow Delegate Methods
extension Preview_Document_VC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (docCollectionView.frame.height-20)
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
}


// MARK: - Additional Delegate Methods
extension Preview_Document_VC: UIDocumentPickerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, NohanaImagePickerControllerDelegate {
    func nohanaImagePickerDidCancel(_ picker: NohanaImagePickerController) {
        debugPrint("Multiple Asset Picker Cancelled!")
        picker.dismiss(animated: true)
    }
    
    func nohanaImagePicker(_ picker: NohanaImagePickerController, didFinishPickingPhotoKitAssets pickedAssts: [PHAsset]) {
        let group = DispatchGroup()
        group.enter()
        DispatchQueue.main.async {
            Loader.showLoader()
        }
        var counter = 0
        for asset in pickedAssts {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isSynchronous = true
//            asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { (contentEditingInput, dictInfo) in
//                if asset.mediaType == .image
//                {
//                  if let strURL = contentEditingInput?.fullSizeImageURL?.absoluteString
//                  {
//                      url = strURL
//                     print("IMAGE URL: ", strURL)
//                  }
//                }
//            }
            manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize,
                                 contentMode: .aspectFit, options: option, resultHandler: { [weak self] (result, info) -> Void in
                guard let self = self else { return }
                let info = PHAssetResource.assetResources(for: asset)
                guard let first = info.first, let image = result else { return }
                self.arrAllDocuments.append(DocumentInfo(docName: first.originalFilename, isPDF: false, docData: .image(image)))
                self.docCollectionView.insertItems(at: [IndexPath(item: self.arrAllDocuments.count-1, section: 0)])
                counter += 1
                if counter == pickedAssts.count {
                    group.leave()
                }
            })
        }
        
        group.notify(queue: .main) { [weak self] in
            picker.dismiss(animated: true) {
                Loader.hideLoader()
                self?.docCollectionView.reloadData()
            }
        }
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let disGroup = DispatchGroup()
        disGroup.enter()
        for url in urls {
            var finalURL: URL! = nil
            var tempDocURL: URL! = nil
            
            if #available(iOS 16.0, *) {
                tempDocURL = tempFolderPath.appending(path: url.lastPathComponent)
            } else {
                tempDocURL = tempFolderPath.appendingPathComponent(url.lastPathComponent)
            }
            if !FileManager.default.fileExists(atPath: tempDocURL.gettingPath()) {
                let secured = url.startAccessingSecurityScopedResource()
                do {
                    if #available(iOS 16.0, *) {
                        try FileManager.default.copyItem(at: url, to: tempDocURL)
                        finalURL = tempDocURL
                    } else {
                        // Fallback on earlier versions
                        try FileManager.default.copyItem(at: url, to: tempDocURL)
                        finalURL = tempDocURL
                    }
                    if secured { url.stopAccessingSecurityScopedResource() }
                } catch let err {
                    debugPrint("Error Coping Document from iCloud - \(err.localizedDescription)")
                    finalURL = tempDocURL
                }
            } else {
                finalURL = tempDocURL
            }
            arrAllDocuments.append(DocumentInfo(docName: url.lastPathComponent, isPDF: true, docData: .path(finalURL)))
            docCollectionView.insertItems(at: [IndexPath(item: arrAllDocuments.count-1, section: 0)])
        }
        disGroup.leave()
        disGroup.notify(queue: .main) { [weak self] in
            self?.docCollectionView.reloadData()
        }
        
        
//        urls.forEach { (url: URL) in
//
//            let secured = url.startAccessingSecurityScopedResource()
//            var finalURL: URL! = nil
//
//            let tempDocURL = tempFolderPath.appendingPathComponent(url.lastPathComponent)
//
//            if !FileManager.default.fileExists(atPath: tempDocURL.gettingPath()) {
//                do {
//                    if #available(iOS 16.0, *) {
//                        try FileManager.default.copyItem(at: url, to: tempDocURL)
//                        finalURL = tempDocURL
//                    } else {
//                        // Fallback on earlier versions
//                        try FileManager.default.copyItem(at: url, to: tempDocURL)
//                        finalURL = tempDocURL
//                    }
//                } catch let err {
//                    debugPrint("Error Coping Document from iCloud - \(err.localizedDescription)")
//                    finalURL = url
//                }
//            } else {
//                finalURL = tempDocURL
//            }
//
//            if secured { url.stopAccessingSecurityScopedResource() }
//            arrAllDocuments.append(DocumentInfo(docName: url.lastPathComponent, isPDF: true, docData: .path(finalURL)))
//            docCollectionView.insertItems(at: [IndexPath(item: arrAllDocuments.count-1, section: 0)])
//        }
//        DispatchQueue.main.async { [weak self] in
//            self?.docCollectionView.reloadData()
//        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            debugPrint("Original Image Not Found in picker")
            appView.makeToast("Image Not Found!\nPlease Try Again Later!")
            return
        }
        // TODO: - When, opening camera `delete_Temp_Files` is called..which is issue, fix it.
        let fixedPickedImage = image.fixOrientation()//this is the new fixed orientation image.
        
        if let imageName = (info[UIImagePickerController.InfoKey.imageURL] as? URL)?.lastPathComponent {
            DispatchQueue.main.async { [weak self] in
                picker.dismiss(animated: true) {
                    self?.arrAllDocuments.append(DocumentInfo(docName: imageName, isPDF: false, docData: .image(fixedPickedImage)))
                    self?.docCollectionView.insertItems(at: [IndexPath(item: self!.arrAllDocuments.count-1, section: 0)])
                }
            }
        } else {
            // Default Name Given
            let forma = DateFormatter()
            forma.dateFormat = "ddMMyyyy_HHmmss"
            let strDate = forma.string(from: Date())
            let imageName = self.doc_Type.rawValue + "_" + "IMG_" + strDate
            
            DispatchQueue.main.async { [weak self] in
                picker.dismiss(animated: true) {
                    self?.openedCamera = false
                    self?.arrAllDocuments.append(DocumentInfo(docName: imageName, isPDF: false, docData: .image(fixedPickedImage)))
                    self?.docCollectionView.insertItems(at: [IndexPath(item: self!.arrAllDocuments.count-1, section: 0)])
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        appViewController?.dismiss(animated: true) { [weak self] in
            self?.openedCamera = false
            debugPrint("Default Image Picker Dismissed..!")
        }
    }
}
