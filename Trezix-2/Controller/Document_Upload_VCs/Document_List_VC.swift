//
//  Document_List_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 31/03/23.
//

import UIKit

class Document_List_VC: UIViewController, FloatyDelegate {
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var headingView: UIView!              
    @IBOutlet weak var drop_Doc_Type: DropDown!
    
    @IBOutlet weak var docTableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    
    // MARK: - All Constants & Variables
    private var selected_Documemnt: arrDocumentTypes = .po
    private var arr_Documents = [Document_List_Data]()
    private var arr_Filtererd_Documents = [Document_List_Data]()
    private var btnFloaty = Floaty()
    
    private var docURL: String! = nil
    
    private lazy var refresher: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: "Refreshing Document List!",
                                                            attributes: [.foregroundColor: UIColor.ourAppThemeColor,
                                                                         .font: UIFont.systemFont(ofSize: getDynamicHeight(size: 12),
                                                                                                  weight: .bold)])
        refresher.tintColor = .ourAppThemeColor
        refresher.addTarget(self, action: #selector(self.refresh_Document_List), for: .valueChanged)
        return refresher
    }()
    
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    /// *Refresh Controller Method*
    @objc private func refresh_Document_List() {
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.call_Document_List_API(fromRefresh: true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        noDataView.layer.applyShadowAndRadius()
    }
    
    
    // MARK: - All Helper Methods
    
    /// `Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable {
            callAllInitMethods()
        }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(false)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    private func callAllInitMethods(_ calledFirstTime: Bool = true) {
        if calledFirstTime {
            setUpThisVC()
        }
        call_Document_List_API()
    }
    
    private func setUpThisVC() {
        initDropDown()
        register_Table_Cell()
        configure_Floaty()
        docTableView.refreshControl = refresher
        NotificationCenter.default.addObserver(self, selector: #selector(refresh_Document_List_Noti(_:)), name: .refreshDocumentListNotification, object: nil)
    }
    
    private func register_Table_Cell() {
        docTableView.register(UINib(nibName: DocumentList_TableCell.identifier, bundle: nil), forCellReuseIdentifier: DocumentList_TableCell.identifier)
    }
    
    private func initDropDown() {
        drop_Doc_Type.textColor = .ourAppThemeColor
        drop_Doc_Type.arrowColor = .ourAppThemeColor
        drop_Doc_Type.hideOptionsWhenSelect = true
        drop_Doc_Type.isSearchEnable = false
        drop_Doc_Type.handleKeyboard = true
        drop_Doc_Type.selectedRowColor = .ourAppThemeColor
        drop_Doc_Type.selectedIndex = 0
        drop_Doc_Type.listHeight = getDynamicHeight(size: 150)
        drop_Doc_Type.checkMarkEnabled = false
        drop_Doc_Type.optionArray = arrDocumentTypes.allCases.compactMap { $0.rawValue }
        drop_Doc_Type.text = arrDocumentTypes.po.rawValue
        drop_Doc_Type.didSelect { [weak self] selectedText, index, id in
            guard let self = self else { return }
            HapticFeedbackGenerator.simpleFeedback(type: .soft)
            self.selected_Documemnt = arrDocumentTypes.allCases[index]
            self.filter_Documents_By_Type()
        }
    }
    
    /// *Configuration Method For Floaty*
    private func configure_Floaty() {
        btnFloaty.size = getDynamicHeight(size: 70)
        btnFloaty.hasShadow = true
        btnFloaty.paddingX = 20
        btnFloaty.paddingY = getEdgeNotchHeight(of: .bottomNotch) + 20
        btnFloaty.fabDelegate = self
        btnFloaty.friendlyTap = true
        
        btnFloaty.buttonColor = .ourAppThemeColor
        btnFloaty.itemImageColor = .white
        btnFloaty.plusColor = .white

        let firstItem = FloatyItem()
        firstItem.handler = { [weak self] _ in
            guard let self = self else { return }
            HapticFeedbackGenerator.simpleFeedback(type: .soft)
            let vc = documentStoryboard.instantiateViewController(withIdentifier: "Select_Doc_Upload_VC")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        btnFloaty.addItem(item: firstItem)
        btnFloaty.handleFirstItemDirectly = true
        
        view.addSubview(btnFloaty)
    }
    
    private func call_Document_List_API(fromRefresh: Bool = false) {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.FETCH_DOC_LIST.fullPath(),
                                            headers: [default_app_header], showHud: !fromRefresh)
        { [weak self] (isSuccess, response: Document_List_Response_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if fromRefresh { self.refresher.endRefreshing() }
            if isSuccess, let response {
                guard let data = response.data else {
                    debugPrint("No Document Found!")
                    self.noDataView.isHidden = false
                    self.docTableView.isHidden = true
                    return
                }
                guard let url = response.url else { debugPrint("Invalid Document URL!"); appView.makeToast("Invalid Document URL!"); return}
                self.arr_Documents = data
                self.docURL = url
                
                self.filter_Documents_By_Type()
            } else {
                HapticFeedbackGenerator.notificationFeedback(type: .warning)
                debugPrint("Error Fetching Document List!")
                appView.makeToast("Error Fetching Document List!")
            }
        }
    }
    
    private func filter_Documents_By_Type() {
        arr_Filtererd_Documents = arr_Documents.filter { $0.fileType == selected_Documemnt.rawValue }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.noDataView.isHidden = !self.arr_Filtererd_Documents.isEmpty
            self.docTableView.isHidden = self.arr_Filtererd_Documents.isEmpty
            self.docTableView.reloadData()
        }
    }
    
    private func navigate_To_PreviewVC(index: Int) {
        let prevVC = documentStoryboard.instantiateViewController(withIdentifier: "Preview_Document_VC") as! Preview_Document_VC
        prevVC.documentData = arr_Filtererd_Documents[index]
        prevVC.mainURL = docURL
        prevVC.inUpload_Preview = false
        navigationController?.pushViewController(prevVC, animated: true)
    }
    
    // MARK: - All Action Methods
    
    @IBAction func back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func view_Doc_Button_Action(_ sender: UIButton) {
        navigate_To_PreviewVC(index: sender.tag)
    }
    
    @objc private func delete_Doc_Button_Action(_ sender: UIButton) {
        alert_Confirm_delete(document: arr_Filtererd_Documents[sender.tag])
    }
    
    private func alert_Confirm_delete(document: Document_List_Data) {
        let docName = document.fileName
        let alert = UIAlertController(title: "Wait", message: "Are you sure want to delete \"\(docName ?? "")\" ?\n\nNOTE: This action can not be undone later!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default))
        let delete_Action = UIAlertAction(title: "Delete", style: .destructive) { [weak self] _ in
            guard let docID = document.id else { debugPrint("Invalid Document ID"); return }
            self?.delete_Document_API(id: docID)
        }
        alert.addAction(delete_Action)
        present(alert, animated: true)
    }
    
    private func delete_Document_API(id: Int) {
        let param = ["id" : String(id)]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.DELETE_DOC.fullPath(),
                                            param: param, headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: BasicResponseModalForApproval?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                if response.status == 1 {
                    appView.makeToast("Document Deleted Successfully!")
                    debugPrint("Document Deleted Successfully!")
                    DispatchQueue.main.async {
                        HapticFeedbackGenerator.notificationFeedback(type: .success)
                        self.call_Document_List_API()
                    }
                } else {
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                    debugPrint("Error Deleting Document Due to \(response.message ?? "")")
                    appView.makeToast("Can't Delete Document!\nPlease Try Again Later!")
                }
            } else {
                HapticFeedbackGenerator.notificationFeedback(type: .error)
                debugPrint("Error Deleting Document!")
                appView.makeToast("Can't Delete Document!\nPlease Try Again Later!")
            }
        }
    }
}

// MARK: - TableView Delegate Methods
extension Document_List_VC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_Filtererd_Documents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DocumentList_TableCell.identifier, for: indexPath) as! DocumentList_TableCell
        cell.set_Document_Details(name: arr_Filtererd_Documents[indexPath.row].fileName, tag: indexPath.row)
        cell.btnViewDocument.addTarget(self, action: #selector(view_Doc_Button_Action(_:)), for: .touchUpInside)
        cell.btnDeleteDocument.addTarget(self, action: #selector(delete_Doc_Button_Action(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return getDynamicHeight(size: 200)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension Document_List_VC {
    @objc private func refresh_Document_List_Noti(_ notification: NSNotification) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.refresher.beginRefreshing()
            self?.call_Document_List_API(fromRefresh: true)
        }
    }
}
