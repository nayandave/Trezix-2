//
//  Select_Doc_Upload_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 16/02/23.
//

import UIKit
import MobileCoreServices
import Photos
import Alamofire

class Select_Doc_Upload_VC: UIViewController, FloatyDelegate {
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var docTypeCollView: UICollectionView!
    
    // MARK: - All Constants & Variables    
    private var btnFloaty = Floaty()
    private var selectedType: arrDocumentTypes = .po
    
    private var allSelectedDocs = [DocumentInfo]()
    
    /// `Continue Alert Action Button For Naming Image Captured From Camera`
//    private var continueAlertActionButton: UIAlertAction! = nil

    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        register_Cell()
        configure_Floaty()
        add_Floaty_Butttons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        allSelectedDocs.removeAll()
    }
    
    // MARK: - All Helper Methods
    private func register_Cell() {
        docTypeCollView.register(UINib(nibName: Select_DocType_CollCell.identifier, bundle: nil),
                                 forCellWithReuseIdentifier: Select_DocType_CollCell.identifier)
    }
    
    /// *Configuration Method For Floaty*
    private func configure_Floaty() {
        btnFloaty.size = getDynamicHeight(size: 80)
        btnFloaty.hasShadow = true
        btnFloaty.paddingX = (self.view.frame.width-btnFloaty.frame.width)/2
        btnFloaty.paddingY = getEdgeNotchHeight(of: .bottomNotch) + 20
        btnFloaty.fabDelegate = self
        btnFloaty.friendlyTap = false
        
        btnFloaty.buttonColor = .ourAppThemeColor
        
        btnFloaty.buttonImage = UIImage(named: "icon-DocUpload")
        btnFloaty.itemImageColor = .white
        btnFloaty.rotationDegrees = 180
        btnFloaty.plusColor = .white
        btnFloaty.animationSpeed = 0.15
//        btnFloaty.itemSpace = 100
        
        btnFloaty.openAnimationType = .semiCircular
        btnFloaty.circleAnimationDegreeOffset = 30
        btnFloaty.layoutIfNeeded()
        btnFloaty.setNeedsDisplay()
//        btnFloaty.circleAnimationDegreeOffset = getDynamicHeight(size: 30) + (getEdgeNotchHeight(of: .bottomNotch) == 0 ? 10 : 5)    // define offset in degrees
        btnFloaty.circleAnimationRadius = getDynamicHeight(size: 90)
    }
    
    /// *Adding Floaty Item Buttons*
    private func add_Floaty_Butttons() {
        debugPrint("Screen Height - \(SCREEN_HEIGHT)")
        let buttonSize = SCREEN_HEIGHT > 850 ? 70 : 60
        let imageOffset = SCREEN_HEIGHT > 850 ? 12.5 : 7.5
        for i in 0..<3 {
            let button = FloatyItem()
            button.size = CGFloat(buttonSize)
            button.imageOffset = CGPoint(x: -imageOffset, y: 0) //CGPoint(x: -getDynamicHeight(size: 7.5), y: 0)
//            button.imageOffset = getEdgeNotchHeight(of: .bottomNotch) != 0 ? CGPoint(x: -getDynamicHeight(size: 30*0.25), y: 0) : .zero
            button.hasShadow = true
//            button.imageSize = CGSize(width: getDynamicHeight(size: 60*0.5), height: getDynamicHeight(size: 60*0.5))
            button.layoutIfNeeded()
            button.setNeedsDisplay()
//
            switch i {
            case 0:
                button.icon = UIImage(named: "ic-Files")
                button.handler = { [weak self] _ in
                    self?.init_DocumentPicker()
                }
            case 1:
                button.icon = UIImage(named: "ic-Galley")
                button.handler = { [weak self] _ in
                    self?.open_Photo_Gallery()
                }
            case 2:
                button.icon = UIImage(named: "ic-Camera")
                button.handler = { [weak self] _ in
                    self?.open_Camera()
                }
            default: debugPrint("Invalid Index")
            }
            button.iconTintColor = .white
            button.buttonColor = .ourAppThemeColor
            btnFloaty.addItem(item: button)
        }
        view.addSubview(btnFloaty)
        view.layoutIfNeeded()
        view.setNeedsDisplay()
        
    }
    
    private func navigate_To_PreviewVC() {
        let prevVC = documentStoryboard.instantiateViewController(withIdentifier: "Preview_Document_VC") as! Preview_Document_VC
        prevVC.arrAllDocuments = allSelectedDocs
        prevVC.doc_Type = selectedType
//        prevVC.is_Document_PDF = isPDF
//        prevVC.docName = docName
//        if isPDF { prevVC.pdfDocURL = pdfDocURL! }
//        else { prevVC.docImage = docImage! }
        navigationController?.pushViewController(prevVC, animated: true)
    }
}

// MARK: - All Actions
extension Select_Doc_Upload_VC {
    @IBAction func back_Button_Action(_ sender: UIButton) {
        guard let navController = navigationController else {
            debugPrint("Empty Navigation Controller!")
            appView.makeToast("Please Try Again Later!")
            return
        }
        navController.popViewController(animated: true)
    }
    
    /// **Initialize and present Document Picker View Controller**
    private func init_DocumentPicker() {
        HapticFeedbackGenerator.simpleFeedback(type: .light)
        var documentVC: UIDocumentPickerViewController! = nil
        if #available(iOS 14, *) {
            documentVC = UIDocumentPickerViewController(forOpeningContentTypes: [.image, .pdf])
        } else {
            documentVC = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String,
                                                                        kUTTypeImage as String], in: .import)
        }
        documentVC.delegate = self
        documentVC.allowsMultipleSelection = true
        documentVC.modalPresentationStyle = .popover
        present(documentVC, animated: true)
    }
    
    /// **Open Camera**
    private func open_Camera() {
        HapticFeedbackGenerator.simpleFeedback(type: .light)
        PHPhotoLibrary.handle_Authorization_CompletionHandler(mediaType: .camera) { [weak self] in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                let cameraVc = UIImagePickerController()
                cameraVc.delegate = self
                cameraVc.sourceType = .camera
                cameraVc.allowsEditing = false
                cameraVc.cameraCaptureMode = .photo
                cameraVc.cameraDevice = .rear
                cameraVc.showsCameraControls = true
                DispatchQueue.main.async {
                    appViewController?.present(cameraVc, animated: true)
                }
            }
        }
    }
    
    /// **Open Photo Gallery**
    private func open_Photo_Gallery() {
        HapticFeedbackGenerator.simpleFeedback(type: .light)
        PHPhotoLibrary.handle_Authorization_CompletionHandler { [weak self] in
            DispatchQueue.main.async { [weak self] in
                let picker = NohanaImagePickerController()
                picker.maximumNumberOfSelection = 5 //Int.max
                picker.toolbarHidden = true
                picker.delegate = self
                self?.present(picker, animated: true)
            }
        }
    }
}

// MARK: - Collection View Delegate Methods
extension Select_Doc_Upload_VC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDocumentTypes.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Select_DocType_CollCell", for: indexPath) as! Select_DocType_CollCell
        cell.setHeading(heading: arrDocumentTypes.allCases[indexPath.row].rawValue)
        DispatchQueue.main.async { [weak self] in
            cell.shadowContentView.layer.applyShadowAndRadius()
            if self?.selectedType == arrDocumentTypes.allCases[indexPath.row] {
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        debugPrint("SELECTED INDEX - \(indexPath.row)")
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let cell = collectionView.cellForItem(at: indexPath) as! Select_DocType_CollCell
        cell.isSelected = true
        selectedType = arrDocumentTypes.allCases[indexPath.row]
    }
}

// MARK: - CollectionView Flow Delegate Methods
extension Select_Doc_Upload_VC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (docTypeCollView.frame.width-35)/4
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}

// MARK: - Additional Delegate Methods
extension Select_Doc_Upload_VC: UIDocumentPickerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, NohanaImagePickerControllerDelegate {
    func nohanaImagePickerDidCancel(_ picker: NohanaImagePickerController) {
        debugPrint("Multiple Asset Picker Cancelled!")
        picker.dismiss(animated: true)
    }
    
    func nohanaImagePicker(_ picker: NohanaImagePickerController, didFinishPickingPhotoKitAssets pickedAssts: [PHAsset]) {
//        var selectedData = [DocumentInfo]()
//        var allImageData = [String: UIImage]()
//        var url : String! = nil
        let group = DispatchGroup()
        group.enter()
        DispatchQueue.main.async {
            Loader.showLoader()
        }
        var counter = 0
        for asset in pickedAssts {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isSynchronous = true
//            asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { (contentEditingInput, dictInfo) in
//                if asset.mediaType == .image
//                {
//                  if let strURL = contentEditingInput?.fullSizeImageURL?.absoluteString
//                  {
//                      url = strURL
//                     print("IMAGE URL: ", strURL)
//                  }
//                }
//            }
            manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize,
                                 contentMode: .aspectFit, options: option, resultHandler: { (result, info) -> Void in
                let info = PHAssetResource.assetResources(for: asset)
                guard let first = info.first, let image = result else { return }
//                allImageData[first.originalFilename] = image
                self.allSelectedDocs.append(DocumentInfo(docName: first.originalFilename, isPDF: false, docData: .image(image)))
                counter += 1
                if counter == pickedAssts.count {
                    group.leave()
                }
            })
        }
        
        group.notify(queue: .main) { [weak self] in
            picker.dismiss(animated: true) {
                Loader.hideLoader()
                self?.navigate_To_PreviewVC()
            }
        }
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let disGroup = DispatchGroup()
        disGroup.enter()
        for url in urls {
            var finalURL: URL! = nil
            var tempDocURL: URL! = nil
            
            if #available(iOS 16.0, *) {
                tempDocURL = tempFolderPath.appending(path: url.lastPathComponent)
            } else {
                tempDocURL = tempFolderPath.appendingPathComponent(url.lastPathComponent)
            }
            if !FileManager.default.fileExists(atPath: tempDocURL.gettingPath()) {
                let secured = url.startAccessingSecurityScopedResource()
                do {
                    if #available(iOS 16.0, *) {
                        try FileManager.default.copyItem(at: url, to: tempDocURL)
                        finalURL = tempDocURL
                    } else {
                        // Fallback on earlier versions
                        try FileManager.default.copyItem(at: url, to: tempDocURL)
                        finalURL = tempDocURL
                    }
                    if secured { url.stopAccessingSecurityScopedResource() }
                } catch let err {
                    debugPrint("Error Coping Document from iCloud - \(err.localizedDescription)")
                    finalURL = tempDocURL
                }
            } else {
                finalURL = tempDocURL
            }
            allSelectedDocs.append(DocumentInfo(docName: url.lastPathComponent, isPDF: true, docData: .path(finalURL)))
        }
        disGroup.leave()
        disGroup.notify(queue: .main) { [weak self] in
            self?.navigate_To_PreviewVC()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            debugPrint("Original Image Not Found in picker")
            appView.makeToast("Image Not Found!\nPlease Try Again Later!")
            return
        }
        let fixedPickedImage = image.fixOrientation()//this is the new fixed orientation image.
        
        if let imageName = (info[UIImagePickerController.InfoKey.imageURL] as? URL)?.lastPathComponent {
            DispatchQueue.main.async { [weak self] in
                self?.allSelectedDocs.append(DocumentInfo(docName: imageName, isPDF: false, docData: .image(fixedPickedImage)))
                self?.navigate_To_PreviewVC()
            }
        } else {
            // Default Name Given
            let forma = DateFormatter()
            forma.dateFormat = "ddMMyyyy_HHmmss"
            let strDate = forma.string(from: Date())
            let imageName = self.selectedType.rawValue + "_" + "IMG_" + strDate
            
            DispatchQueue.main.async { [weak self] in
                self?.allSelectedDocs.append(DocumentInfo(docName: imageName, isPDF: false, docData: .image(fixedPickedImage)))
                self?.navigate_To_PreviewVC()
            }
//            let textFieldAlert = UIAlertController(title: "Wait", message: "Please specify name for the taken photo.", preferredStyle: .alert)
//            textFieldAlert.addTextField { textField in
//                textField.placeholder = "Enter Name"
//
//                // Default Name
//                let forma = DateFormatter()
//                forma.dateFormat = "ddMMyyyy_HHmmss"
//                let strDate = forma.string(from: Date())
//                textField.text = self.selectedType.rawValue + "_" + "IMG_" + strDate
//
//                textField.delegate = self
//                textField.addTarget(self, action: #selector(self.checkEmptyImageName), for: .editingChanged)
//            }
//            continueAlertActionButton = UIAlertAction(title: "Continue",
//                                                      style: .default) { [weak self] _ in
//                guard let textFields = textFieldAlert.textFields,
//                let imageName = textFields[0].text else { return }
//                DispatchQueue.main.async { [weak self] in
//                    self?.allSelectedDocs.append(DocumentInfo(docName: imageName, isPDF: false, docData: .image(fixedPickedImage)))
//                    self?.navigate_To_PreviewVC()
//                }
//            }
//            textFieldAlert.addAction(continueAlertActionButton)
//            continueAlertActionButton.isEnabled = true
//            present(textFieldAlert, animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        appViewController?.dismiss(animated: true) {
            debugPrint("Default Image Picker Dismissed..!")
        }
    }
}

//extension Select_Doc_Upload_VC: UITextFieldDelegate {
//    @objc private func checkEmptyImageName(_ textField: UITextField) {
//        continueAlertActionButton.isEnabled = textField.hasText
//    }
//}

extension UIImage {
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }
}
