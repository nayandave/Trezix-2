//
//  FormSalesReport.swift
//  Trezix-2
//
//  Created by Amar Panchal on 12/12/22.
//

import UIKit

class FormSalesReport: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var textFromPSINumber: UITextField!
    @IBOutlet weak var textToPSINumber: UITextField!

    @IBOutlet weak var btnFromPSIDate: UIButton!
    @IBOutlet weak var btnToPSIDate: UIButton!
    
    @IBOutlet weak var btnFromInvoiceDate: UIButton!
    @IBOutlet weak var btnToInvoiceDate: UIButton!
    
    @IBOutlet weak var textFromInvoiceNumber: UITextField!
    @IBOutlet weak var textToInvoiceNumber: UITextField!
    
    @IBOutlet weak var dropSelectMaterial: DropDown!
    @IBOutlet weak var dropSelectCustomer: DropDown!
    
    @IBOutlet weak var datePickerContentView: UIView!
    @IBOutlet weak var btnCancelPicker: UIButton!
    @IBOutlet weak var btnDonePicker: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    // MARK: - All Constants & Variables
    /// Selected From Pre-Shipment Invoice Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var fromPSIDate: Date! = nil
    
    /// Selected To Pre-Shipment Invoice Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var toPSIDate: Date! = nil
    
    /// Selected From Final Invoice Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var fromFIDate: Date! = nil
    
    /// Selected To Final Invoice Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var toFIDate: Date! = nil
    
    /// ID of  selected Material from it's DropDown
    ///  - Used in ``call_Order_Report_API()``
    private var selMaterialID: Int! = nil
    
    /// ID of selected Customer from it's DropDown
    ///  - Used in ``call_Order_Report_API()``
    private var selCustomerID: String! = nil
    
    /// Array of Material Data fetched from ``APIConstants/GET_MATERIAL_DROPDOWN``
    private var arrMaterialData = [Material_Data]()
    
    /// Array of Material Group Data fetched from ``APIConstants/GET_MATERIAL_GROUP_DROPDOWN``
    private var arrMaterialGroupData = [Material_Group_Data]()
    
    /// Determines if  Calendar is showing from Which Button
    private var isFromSectionCalendar: DateMode = .None
    
    /// DIfferent Types From Which DatePicker View is Called
    private enum DateMode {
        case FromPSI
        case ToPSI
        case FromFI
        case ToFI
        case None
    }
    
    private lazy var mainBlurr: UIVisualEffectView = {
        var blurrView = UIVisualEffectView()
        blurrView.frame = view.frame
        blurrView.alpha = 0.9
        return blurrView
    }()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVC()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        [btnFromPSIDate, btnToPSIDate, btnFromInvoiceDate, btnToInvoiceDate, btnCancelPicker, btnDonePicker, btnSubmit].forEach { button in
            button?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
        datePickerContentView.layer.applyShadowAndRadius(shadowRadius: 1.5)
        [textFromPSINumber, textToPSINumber, textFromInvoiceNumber, textToInvoiceNumber, dropSelectMaterial, dropSelectCustomer].forEach { textField in
            textField?.layer.borderWidth = 0.5
            textField?.layer.borderColor = UIColor.systemBackground.cgColor
            textField?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
    }
    
    // MARK: - All Helper Methods
    private func setUpVC() {
        initDropDowns()
        datePicker.setPickerStyle_iOS14()
        
        DispatchQueue.global(qos: .utility).async { [weak self] in
            self?.get_Material_Dropdown_Data()
            self?.get_Material_Group_Dropdown_Data()
        }
    }
    
    private func initDropDowns() {
        [dropSelectMaterial, dropSelectCustomer].forEach { down in
            down?.arrowColor = .ourAppThemeColor
            
            down?.hideOptionsWhenSelect = true
            down?.checkMarkEnabled = false
            down?.isSearchEnable = true
            down?.handleKeyboard = true
            down?.selectedRowColor = .ourAppThemeColor
            
            down?.listHeight = dropSelectMaterial.distanceFromSafeArea(distanceType: .lowerDistanceFromSafeArea)
            
            down?.didSelect(completion: { [weak self] selectedText, index, id in
                guard let strongSelf = self else { return }
                HapticFeedbackGenerator.simpleFeedback(type: .light)
                down?.text = selectedText
                
                if down == strongSelf.dropSelectMaterial {
                    strongSelf.selMaterialID = index == 0 ? nil : strongSelf.arrMaterialData[index].id
                } else {
                    strongSelf.selCustomerID = index == 0 ? nil : selectedText
                }
            })
        }
    }
    
    /// *Fetechs Dropdown Data of* `Select Material`*Dropdown*
    private func get_Material_Dropdown_Data() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_MATERIAL_DROPDOWN.fullPath(),
                                            headers: [default_app_header],
                                            showHud: false)
        { [weak self] (isSuccess, response: Material_Dropdown_Modal?, errorMessage) in
            Loader.hideLoader()
            guard let self = self else { return }
            if isSuccess, let response = response {
                guard let data = response.data else { debugPrint("No Data Available"); return }
                self.arrMaterialData = data
                
                var allMaterialNames = data.compactMap { $0.materialDescription }
                allMaterialNames.insert("Select Material", at: 0)
                
                self.dropSelectMaterial.selectedIndex = 0
                self.dropSelectMaterial.text = allMaterialNames[0]
                self.dropSelectMaterial.optionArray = allMaterialNames
            } else if let errorMessage = errorMessage {
                debugPrint("Error Fetching Material Dropdown Data because of \(errorMessage)!")
                appView.makeToast("Error Fetching Material Dropdown Data!")
            }
        }
    }
    
    /// *Fetechs Dropdown Data of* `Select Material Group` *Dropdown*
    private func get_Material_Group_Dropdown_Data() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_MATERIAL_GROUP_DROPDOWN.fullPath(),
                                            headers: [default_app_header],
                                            showHud: false)
        { [weak self] (isSuccess, response: Material_Group_Dropdown_Modal?, errorMessage) in
            Loader.hideLoader()
            guard let self = self else { return }
            if isSuccess, let response = response {
                if let data = response.data {
                    self.arrMaterialGroupData = data
                    var allMaterialGroupNames = ["Select Material Group"]
                    for group in data {
                        let sep = group.value.components(separatedBy: ",")
                        allMaterialGroupNames.append(contentsOf: sep)
                    }
                    self.dropSelectCustomer.selectedIndex = 0
                    self.dropSelectCustomer.text = allMaterialGroupNames[0]
                    self.dropSelectCustomer.optionArray = allMaterialGroupNames
                    Loader.hideLoader()
                } else {
                    // No Data Available
                }
            } else if let errorMessage = errorMessage {
                debugPrint("Error Fetching Material Dropdown Data because of \(errorMessage)!")
                appView.makeToast("Error Fetching Material Dropdown Data!")
            }
        }
    }
    
    
    /// *Fetechs Dropdown Data of* `Select Customer`*Dropdown*
//    private func get_Customer_Dropdown_Data() {
//        NetworkClient.encodedNetworkRequest(method: .get,
//                                            apiName: APIConstants.GET_MATERIAL_DROPDOWN.fullPath(),
//                                            headers: default_app_header,
//                                            showHud: false)
//        { [weak self] (isSuccess, response: Material_Dropdown_Modal?, errorMessage) in
//            Loader.hideLoader()
//            guard let self = self else { return }
//            if isSuccess  {
//                if let response = response {
//                    guard let data = response.data else { debugPrint("No Data Available"); return }
//                    self.arrMaterialData = data
//
//                    var allMaterialNames = data.compactMap { $0.materialDescription }
//                    allMaterialNames.insert("Select Material", at: 0)
//
//                    self.dropSelectCustomer.selectedIndex = 0
//                    self.dropSelectCustomer.text = allMaterialNames[0]
//                    self.dropSelectCustomer.optionArray = allMaterialNames
//                }
//            } else if let errorMessage = errorMessage {
//                debugPrint("Error Fetching Customer Dropdown Data because of \(errorMessage)!")
//                appView.makeToast("Error Fetching Customer Dropdown Data!")
//            }
//        }
//    }
    
    
    // MARK: - All Actions
    @IBAction func back_Button_Action(_ sender: UIButton) {
        guard let nav = navigationController else { return }
        nav.popViewController(animated: true)
    }
    
    @IBAction func cancel_picker_button_Action(_ sender: UIButton) {
        datePickerContentView.dismiss_With_Popup_Effect()
        mainBlurr.hideBlurrEffect()
    }
    
    @IBAction func done_picker_button_Action(_ sender: UIButton) {
        datePickerContentView.dismiss_With_Popup_Effect()
        
        switch isFromSectionCalendar {
        case .FromPSI: fromPSIDate = datePicker.date
        case .ToPSI: toPSIDate = datePicker.date
        case .FromFI: fromFIDate = datePicker.date
        case .ToFI: toFIDate = datePicker.date
        case .None: break;
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainBlurr.hideBlurrEffect()
            
            switch self.isFromSectionCalendar {
            case .FromPSI: self.btnFromPSIDate.setTitle(DateHelper.shared.getStringFrom(date: self.fromPSIDate), for: .normal)
            case .ToPSI: self.btnToPSIDate.setTitle(DateHelper.shared.getStringFrom(date: self.toPSIDate), for: .normal)
            case .FromFI: self.btnFromInvoiceDate.setTitle(DateHelper.shared.getStringFrom(date: self.fromFIDate), for: .normal)
            case .ToFI: self.btnToInvoiceDate.setTitle(DateHelper.shared.getStringFrom(date: self.toFIDate), for: .normal)
            case .None: break
            }
        }
    }
    
    @IBAction func from_date_buttons_Action(_ sender: UIButton) {
        isFromSectionCalendar = sender == btnFromPSIDate ? .FromPSI : .FromFI
        datePickerContentView.show_With_Popup_Effect()
        datePicker.setDate(Date(), animated: true)
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func to_date_buttons_Action(_ sender: UIButton) {
        isFromSectionCalendar = sender == btnToPSIDate ? .ToPSI : .ToFI
        datePickerContentView.show_With_Popup_Effect()
        datePicker.setDate(Date(), animated: true)
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func submit_button_Action(_ sender: UIButton) {
        view.endEditing(true)
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        let param = ["PI_date": fromPSIDate != nil ? API_parsing_dateString(date: fromPSIDate) : "",
                     "PI_to_date": toPSIDate != nil ? API_parsing_dateString(date: toPSIDate) : "",
                     "PI_no_1": textFromPSINumber.text ?? "",
                     "PI_no_2": textToPSINumber.text ?? "",
                     "FI_date": fromFIDate != nil ? API_parsing_dateString(date: fromFIDate) : "",
                     "FI_to_date": toFIDate != nil ? API_parsing_dateString(date: toFIDate) : "",
                     "Fi_no_1": textFromInvoiceNumber.text ?? "",
                     "FI_no2": textToInvoiceNumber.text ?? "",
                     "material": selMaterialID == nil ? "" : (dropSelectMaterial.text ?? ""),
                     "customer": selCustomerID == nil ? "" : (dropSelectCustomer.text ?? "")]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.GET_SALES_REPORT.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false, senderView: btnSubmit)
        { [weak self] (isSuccess, response: Sales_Report_Data_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response = response {
                Loader.hideLoader()
                if let mainData = response.data, mainData.count > 0 {    // DATA AVAILABLE
                    #warning("Data Implementation")
                    debugPrint("SUCCESS")
                    let vc = allReportStoryboard.instantiateViewController(withIdentifier: "SalesReportVC") as! SalesReportVC
                    vc.arrSalesReportData = mainData
                    self.navigationController?.pushViewController(vc, animated: true)
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                } else {    // NO DATA AVAILABLE
                    appView.makeToast("NO DATA AVAILABLE!")
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                }
            } else if let errorMessage {
                debugPrint("Sales Report Error - \(errorMessage)")
                HapticFeedbackGenerator.notificationFeedback(type: .error)
            }
        }
    }
}

extension UIViewController {
    func API_parsing_dateString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter.string(from: date)
    }
}
