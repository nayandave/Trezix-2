//
//  SalesReportVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 12/12/22.
//

import UIKit

class SalesReportVC: UIViewController {
    
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnFirstSecSort: UIButton!
    @IBOutlet weak var btnSecondSecSort: UIButton!
    @IBOutlet weak var btnThirdSecSort: UIButton!
    
    @IBOutlet weak var imageFirstSecAscending: UIImageView!
    @IBOutlet weak var imageFirstSecDescending: UIImageView!
    
    @IBOutlet weak var imageSecondSecAscending: UIImageView!
    @IBOutlet weak var imageSecondSecDescending: UIImageView!
    
    @IBOutlet weak var imageThirdSecAscending: UIImageView!
    @IBOutlet weak var imageThirdSecDescending: UIImageView!
    
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var btnExportPDF: UIButton!
    
    ///*Value to be pushed from* ``FormSalesReport``
    var arrSalesReportData = [Sales_Report_Data]()
    
    ///Contains all indexes of section of which rows are visible to user { i.e. Currently showing extra info. on user click }
    private var arrShowableSections = [Int]()
    
    private var isPreShipInvAscending = true
    private var isPIDateAscending = true
    private var isFinalInvNumberAscending = true

    override func viewDidLoad() {
        super.viewDidLoad()
        settingUpThisVC()
    }
    
    // MARK: - All Helper Methods
    private func settingUpThisVC() {
        mainTableView.register(UINib(nibName: POReportExtraInfoCell.identifier, bundle: nil), forCellReuseIdentifier: POReportExtraInfoCell.identifier)
        mainTableView.setSectionPadding_iOS_15()
//        initSpreadSheet()
        debugPrint("DATA COUNT -- \(arrSalesReportData.count)")
        changeSorting(type: .firstSecAscending)
    }
    
    private func changeSorting(type: CurrentSorting) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let arrAscendingImages = [imageFirstSecAscending, imageSecondSecAscending, imageThirdSecAscending]
        let arrDescendingImages = [imageFirstSecDescending, imageSecondSecDescending, imageThirdSecDescending]
        applyFilterAndRefresh(filter: type)
        switch type {
        case .firstSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 0 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .firstSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 0 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .secondSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 1 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .secondSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 1 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .thirdSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 2 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .thirdSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 2 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        }
    }
    
    private func applyFilterAndRefresh(filter: CurrentSorting) {
        switch filter {
        case .firstSecAscending:
            arrSalesReportData = arrSalesReportData.sorted { $0.piNo.non_NilEmpty(true).localizedCompare($1.piNo.non_NilEmpty(true)) == .orderedAscending }
        case .secondSecAscending:
            arrSalesReportData = arrSalesReportData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.piDate) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.piDate) else { return false }
                return date1.compare(date2) == .orderedAscending
            })
        case .thirdSecAscending:
            arrSalesReportData = arrSalesReportData.sorted { $0.fiNo.non_NilEmpty(true).localizedCompare($1.fiNo.non_NilEmpty(true)) == .orderedAscending }
        case .firstSecDescending:
            arrSalesReportData = arrSalesReportData.sorted { $0.piNo.non_NilEmpty(true).localizedCompare($1.piNo.non_NilEmpty(true)) == .orderedDescending }
        case .secondSecDescending:
            arrSalesReportData = arrSalesReportData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.piDate) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.piDate) else { return false }
                return date1.compare(date2) == .orderedDescending
            })
        case .thirdSecDescending:
            arrSalesReportData = arrSalesReportData.sorted { $0.fiNo.non_NilEmpty(true).localizedCompare($1.fiNo.non_NilEmpty(true)) == .orderedAscending }
        }
        DispatchQueue.main.async {  [weak self] in
            self?.mainTableView.reloadData()
        }
    }
    
    // MARK: - All Actions
    @IBAction func Back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func tappedOnSection(_ tap: UITapGestureRecognizer) {
        guard let tappedView = tap.view as? ThreeColumnRowView else { appView.makeToast("Unrecognised Touch"); return }
        let currentIndex = tappedView.currentRow
        if arrShowableSections.contains(currentIndex) {
            arrShowableSections = arrShowableSections.filter({ $0 != currentIndex })
        } else {
            arrShowableSections.append(currentIndex)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainTableView.beginUpdates()
            self.mainTableView.reloadSections(IndexSet(integer: currentIndex), with: .automatic)
            self.mainTableView.endUpdates()
        }
    }
    
    @IBAction func Change_First_Section_Sorting(_ sender: UIButton) {
        if isPreShipInvAscending {
            changeSorting(type: .firstSecDescending)
        } else {
            changeSorting(type: .firstSecAscending)
        }
        isPreShipInvAscending = !isPreShipInvAscending
        isPIDateAscending = true
        isFinalInvNumberAscending = true
    }
    
    @IBAction func Change_Second_Section_Sorting(_ sender: UIButton) {
        if isPIDateAscending {
            changeSorting(type: .secondSecAscending)
        } else {
            changeSorting(type: .secondSecDescending)
        }
        isPIDateAscending = !isPIDateAscending
        isPreShipInvAscending = true
        isFinalInvNumberAscending = true
    }
    
    @IBAction func Change_Third_Section_Sorting(_ sender: UIButton) {
        if isFinalInvNumberAscending {
            changeSorting(type: .thirdSecAscending)
        } else {
            changeSorting(type: .thirdSecDescending)
        }
        isFinalInvNumberAscending = !isFinalInvNumberAscending
        isPreShipInvAscending = true
        isPIDateAscending = true
    }
    
    
    @IBAction func Export_Button_Action(_ sender: UIButton) {
        appView.makeToast("Upcoming Feature!")
    }
}

extension SalesReportVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSalesReportData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getDynamicHeight(size: 50)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0 //getDynamicHeight(size: 1)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ThreeColumnRowView()
        headerView.isViewHeadingView(withSeparator: true, tag: section)
        let data = arrSalesReportData[section]
        headerView.setData(firstLabel: data.piNo, secondLabel: data.piDate, thirdLabel: data.fiNo, threeColumnView: true)
        headerView.isSelected(selected: arrShowableSections.contains(section), lastCell: section+1 == arrSalesReportData.count)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnSection(_:)))
        headerView.addGestureRecognizer(tapGesture)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrShowableSections.contains(section) ? 50 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
        cell.salesReportData = arrSalesReportData[indexPath.section]
        cell.setUpSalesReportCell(for: indexPath.row)
        cell.isUserInteractionEnabled = false
        return cell
    }
}
