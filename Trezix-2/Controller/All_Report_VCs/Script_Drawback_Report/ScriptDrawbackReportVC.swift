//
//  ScriptDrawbackReportVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 31/12/22.
//

import UIKit

class ScriptDrawbackReportVC: UIViewController {

    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var dropSelectPSI: DropDown!
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var tableShadowView: UIView!
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    // MARK: - All Constants & Variables
    private var allPSIList = [Invoice_List_Data]()
    private var arrAllPSIStr = [String]()
    private var arrAllPSIIds = [String]()
    private var selectedPSIID: String! = nil
    
    private let dropDownPlaceholder = "Select Any Pre-Shipment Invoice"
    private let noDataPlaceholder = "NO DATA AVAILABLE"
    private let noDataSelectPlaceholder = "PLEASE SELECT ANY INVOICE FIRST!"
    
    private var arrScriptDrawbackData = [Script_Drawback_Data]()
    
    /// Contains all indexes of section of which rows are visible to user { i.e. Currently showing extra info. on user click }
    private var arrShowableSections = [Int]()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableShadowView.layer.applyShadowAndRadius(cornerRadius: 5)
        dropSelectPSI.layer.applyShadowAndRadius()
        noDataView.layer.applyShadowAndRadius()
    }
    
    
    // MARK: - All Helper Methods
    
    /// `Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable {
            callAllInitMethods()
        }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    private func callAllInitMethods(_ afterRefresh: Bool = false) {
        if !afterRefresh { //Will be called Once
            settingUpThisVC()
        }
        fetchInvoiceList()
    }
    
    private func settingUpThisVC() {
        lblNoData.text = noDataSelectPlaceholder
        mainTableView.register(UINib(nibName: POReportExtraInfoCell.identifier, bundle: nil), forCellReuseIdentifier: POReportExtraInfoCell.identifier)
        if #available(iOS 15, *) {
            mainTableView.sectionHeaderTopPadding = 0
        }
        mainTableView.tableFooterView = UIView()
        tableShadowView.isHidden = true
        noDataView.isHidden = false
        initDropdown()
    }
    
    /// Setting up ``dropSelectPSI`` drop-down of po list
    private func initDropdown() {
        dropSelectPSI.arrowColor = .ourAppThemeColor
        dropSelectPSI.hideOptionsWhenSelect = true
        dropSelectPSI.checkMarkEnabled = false
        dropSelectPSI.isSearchEnable = false
        dropSelectPSI.handleKeyboard = true
        dropSelectPSI.selectedRowColor = .ourAppThemeColor
        
        dropSelectPSI.listHeight = dropSelectPSI.distanceFromSafeArea(distanceType: .lowerDistanceFromSafeArea)
        
        dropSelectPSI.didSelect(completion: { [weak self] selectedText, index, id in
            guard let strongSelf = self else { return }
            HapticFeedbackGenerator.simpleFeedback(type: .light)
            strongSelf.dropSelectPSI.text = selectedText
            if index == 0 {
                strongSelf.selectedPSIID = nil
                strongSelf.tableShadowView.isHidden = true
                strongSelf.lblNoData.text = strongSelf.noDataSelectPlaceholder
                strongSelf.noDataView.isHidden = false
            } else {
                strongSelf.lblNoData.text = strongSelf.noDataPlaceholder
                debugPrint("Invoice ID = \(strongSelf.arrAllPSIIds[index])")
                strongSelf.selectedPSIID = strongSelf.arrAllPSIIds[index]
                strongSelf.call_Script_Drawback_Data_API()
            }
        })
    }
    
    /// Fetching Pre-Shipment Invoice List from API calling
    private func fetchInvoiceList() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_PSI_LIST.fullPath(),
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Get_PSI_List_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                guard let alldata = response.data else {
                    debugPrint("Response Message -- \(response.msg ?? "")")
                    if let message = response.msg, message == "Pre-Shipment Invoice Data Not founded" {
                        self.dropSelectPSI.text = "No Pre-Shipment Invoice Found!"
                        appView.makeToast("No PSI Found!")
                    }
                    Loader.hideLoader()
                    return
                }
                self.arrAllPSIStr = alldata.compactMap({ String($0.preshipmentInvoiceNo ?? "N/A") })
                self.arrAllPSIIds = alldata.compactMap({ String($0.deliveryNo ?? "N/A") })
                
                self.arrAllPSIStr.insert(self.dropDownPlaceholder, at: 0)
                self.arrAllPSIIds.insert("N/A", at: 0)
                
                self.dropSelectPSI.text = self.arrAllPSIStr.first
                self.dropSelectPSI.optionArray = self.arrAllPSIStr
                Loader.hideLoader()
            } else if let errorMessage {
                debugPrint("Fetch Pre-Shipment Invoice Error - \(errorMessage)")
                appView.makeToast(errorMessage)
                Loader.hideLoader()
            }
        }
    }
    
    /// Fetching Script Draback Report Data From API from selected Pre-Shipment Invoice ID as
    private func call_Script_Drawback_Data_API() {
        guard let psi = selectedPSIID else { debugPrint("Invalid Invoice!"); appView.makeToast("Invalid Pre-Shipment Invoice"); return }
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        let param = ["pi_no": psi]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.SCRIPT_DRAWBACK_REPORT.fullPath(),
                                            param: param, headers: [default_app_header], showHud: false, senderView: dropSelectPSI)
        { [weak self] (isSuccess, response: Script_Drawback_Report_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                guard let allData = response.data else {
                    debugPrint("No Script Drawback Data"); appView.makeToast("No Script Drawback Data!");
                    self.tableShadowView.isHidden = true; self.lblNoData.text = self.noDataPlaceholder; self.noDataView.isHidden = false;
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                    return
                }
                self.arrScriptDrawbackData = allData
                HapticFeedbackGenerator.notificationFeedback(type: .success)
                DispatchQueue.main.async { [weak self] in
                    self?.noDataView.isHidden = true
                    self?.tableShadowView.isHidden = false
                    self?.mainTableView.reloadData()
                }
            } else if let errorMessage {
                debugPrint("Fetch Script Drawback Report Data Error - \(errorMessage)")
                appView.makeToast(errorMessage)
                HapticFeedbackGenerator.notificationFeedback(type: .error)
            }
        }
    }
    
//    @objc private func tappedOnSection(_ tap: UITapGestureRecognizer) {
//        guard let tappedView = tap.view as? ThreeColumnRowView else { appView.makeToast("Unrecognised Touch"); return }
//        let currentIndex = tappedView.currentRow
//        if arrShowableSections.contains(currentIndex) {
//            arrShowableSections = arrShowableSections.filter({ $0 != currentIndex })
//        } else {
//            arrShowableSections.append(currentIndex)
//        }
//        DispatchQueue.main.async { [weak self] in
//            guard let self = self else { return }
//            self.mainTableView.beginUpdates()
//            self.mainTableView.reloadSections(IndexSet(integer: currentIndex), with: .fade)
//            self.mainTableView.endUpdates()
////            if self.arrShowableSections.contains(currentIndex) {
////                DispatchQueue.main.asyncAfter(deadline: .now() + 0.75, execute: { [weak self] in
////                    self?.mainTableView.scrollToRow(at: IndexPath(row: 0, section: currentIndex), at: .top, animated: false)
////                })
////            }
//        }
//    }
    
    // MARK: - All Actions
    @IBAction func Back_Button_Action(_ sender: UIButton) {
        guard let navController = navigationController else { debugPrint("Nav Controller Nil"); appView.makeToast("Please Try Again Later!"); return }
        navController.popViewController(animated: true)
    }
}

// MARK: - TableView Delegate Methods
//extension ScriptDrawbackReportVC: UITableViewDelegate, UITableViewDataSource {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return arrScriptDrawbackData.count
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return getDynamicHeight(size: 50)
//    }
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 10
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = ThreeColumnRowView()
//        headerView.isViewHeadingView(withSeparator: true, tag: section)
//        let data = arrScriptDrawbackData[section]
//        headerView.setData(firstLabel: data.piNo, secondLabel: data.piDate, thirdLabel: nil)
//        headerView.isSelected(selected: arrShowableSections.contains(section), lastCell: section+1 == arrScriptDrawbackData.count)
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnSection(_:)))
//        headerView.addGestureRecognizer(tapGesture)
//        return headerView
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrShowableSections.contains(section) ? 21 : 0
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
//        cell.scriptDrabackData = arrScriptDrawbackData[indexPath.section]
//        cell.setUpScriptDrawbackCell(for: indexPath.row)
//        cell.isUserInteractionEnabled = false
//        return cell
//    }
//}
extension ScriptDrawbackReportVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrScriptDrawbackData.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 22
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
        cell.scriptDrabackData = arrScriptDrawbackData[0]
        cell.setUpScriptDrawbackCell(for: indexPath.row)
        cell.isUserInteractionEnabled = false
        return cell
    }
}
