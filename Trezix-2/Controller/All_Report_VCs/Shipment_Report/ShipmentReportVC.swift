//
//  ShipmentReportVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 03/10/22.
//

import UIKit

class ShipmentReportVC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnFirstSecSort: UIButton!
    @IBOutlet weak var btnSecondSecSort: UIButton!
    @IBOutlet weak var btnThirdSecSort: UIButton!
    
    @IBOutlet weak var imageFirstSecAscending: UIImageView!
    @IBOutlet weak var imageFirstSecDescending: UIImageView!
    
    @IBOutlet weak var imageSecondSecAscending: UIImageView!
    @IBOutlet weak var imageSecondSecDescending: UIImageView!
    
    @IBOutlet weak var imageThirdSecAscending: UIImageView!
    @IBOutlet weak var imageThirdSecDescending: UIImageView!
    
    @IBOutlet weak var mainShipmentTable: UITableView!
    @IBOutlet weak var btnExportPDF: UIButton!
    
    // MARK: - All Constants & Variables
    
    ///*Value to be pushed from* ``FormShipmentReportVC``
    public var arrShipmentReportData = [MainShipmentData]()
    
    ///Contains all indexes of section of which rows are visible to user { i.e. Currently showing extra info. on user click }
    private var arrShowableSections = [Int]()
    
    private var currentSortingType: CurrentSorting! = .firstSecAscending
    
    private var isPreShiInvAscending = true
    private var isPIDateAscending = true
    private var isFinalInvNumAscending = true

    var arr = CGFloat()
    var spreadView: SpreadsheetView! = nil
    
    private let firstRow = ["Sr No" ,"PO No", "PO Date", "LC No.", "LC Date", "LC Expiry Date", "Latest date of Shipment", "Company Plant", "Vendor Name",  "Vendor Group1", "Vendor Group2", "Material Code", "Material Name", "PO Quantity", "Shipped Quantity", "Pending Quantity", "Units of Measurement", "Rate", "Currency", "FC Value", "INR Value"]
    
    private let coulmnWidth = [0.0175, 0.05, 0.045, 0.045, 0.045, 0.045, 0.045, 0.045, 0.07, 0.045, 0.06, 0.035, 0.04, 0.05, 0.055, 0.045, 0.045, 0.05, 0.05, 0.035, 0.04]
    
    
    //0.005+0.02+0.005+0.005+0.005+0.025
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        debugPrint("TOTAL DATA -- \(arrShipmentReportData.count)")
        setUpThisVC()
    }
    
    // MARK: - All Actions
    @IBAction func back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Change_First_Section_Sorting(_ sender: UIButton) {
        if isPreShiInvAscending {
            changeSorting(type: .firstSecDescending)
        } else {
            changeSorting(type: .firstSecAscending)
        }
        isPreShiInvAscending = !isPreShiInvAscending
        isPIDateAscending = false
        isFinalInvNumAscending = false
    }
    
    @IBAction func Change_Second_Section_Sorting(_ sender: UIButton) {
        if isPIDateAscending {
            changeSorting(type: .secondSecAscending)
        } else {
            changeSorting(type: .secondSecDescending)
        }
        isPIDateAscending = !isPIDateAscending
        isPreShiInvAscending = false
        isFinalInvNumAscending = false
    }
    
    @IBAction func Change_Third_Section_Sorting(_ sender: UIButton) {
        if isFinalInvNumAscending {
            changeSorting(type: .thirdSecAscending)
        } else {
            changeSorting(type: .thirdSecDescending)
        }
        isFinalInvNumAscending = !isFinalInvNumAscending
        isPreShiInvAscending = false
        isPIDateAscending = false
    }
    
    @objc private func tappedOnSection(_ tap: UITapGestureRecognizer) {
        guard let tappedView = tap.view as? ThreeColumnRowView else { appView.makeToast("Unrecognised Touch"); return }
        let currentIndex = tappedView.currentRow
        if arrShowableSections.contains(currentIndex) {
            arrShowableSections = arrShowableSections.filter({ $0 != currentIndex })
        } else {
            arrShowableSections.append(currentIndex)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainShipmentTable.beginUpdates()
            self.mainShipmentTable.reloadSections(IndexSet(integer: currentIndex), with: .automatic)
            self.mainShipmentTable.endUpdates()
//            if !self.arrShowableSections.contains(currentIndex) {
//                DispatchQueue.main.async {
//                    self.mainShipmentTable.scrollToRow(at: IndexPath(row: NSNotFound, section: currentIndex), at: .middle, animated: false)
//                }
//            }
        }
    }
    
    @IBAction func export_Button_Action(_ sender: UIButton) {
        Loader.showLoader()
        DispatchQueue.main.async { [weak self] in
            self?.ExportAsPDF()
        }
    }
    
    // MARK: - All Helper Methods
    private func setUpThisVC() {
        mainShipmentTable.register(UINib(nibName: POReportExtraInfoCell.identifier, bundle: nil), forCellReuseIdentifier: POReportExtraInfoCell.identifier)
        if #available(iOS 15, *) {
            mainShipmentTable.sectionHeaderTopPadding = 0
        }
        initSpreadSheet()
        mainShipmentTable.tableFooterView = UIView()
        changeSorting(type: .firstSecAscending)
    }
    
    private func initSpreadSheet() {
        if spreadView == nil {
            spreadView = SpreadsheetView(frame: CGRect(x: self.view.frame.minX, y: self.view.frame.minY, width: self.view.frame.width, height: self.view.frame.height))
            spreadView.delegate = self
            spreadView.dataSource = self
            spreadView.register(MyLabelCell.self, forCellWithReuseIdentifier: MyLabelCell.identifier)
            spreadView.showsHorizontalScrollIndicator = false
            spreadView.showsVerticalScrollIndicator = false
            spreadView.bounces = false
//            spreadView.gridStyle = .solid(width: 0.5, color: .darkGray)
            spreadView.intercellSpacing = .zero
            spreadView.gridStyle = .none
            spreadView.backgroundColor = .white
            spreadView.reloadData()
        }
    }

    private func ExportAsPDF() {
        let data = NSMutableData()
        
        let pageHeight: CGFloat = 11 * 72
        let pageWidth: CGFloat = 17 * 72
        
        let frame = CGRect(origin: .zero, size: CGSize(width: pageWidth, height: pageHeight))
        
        UIGraphicsBeginPDFContextToData(data, frame, nil)
        
        // Main Container View
        let tempContainerView = UIView(frame: CGRect(x: 0, y: 0, width: pageWidth, height: pageHeight))
        
        // HeaderView With Label
        let headerHeight = getDynamicHeight(size: 50) + 20
        let headingView = PDFHeadingView()
        headingView.backgroundColor = .clear
        headingView.frame = CGRect(x: 0, y: 10, width: pageWidth, height: getDynamicHeight(size: 50))
        headingView.lblHeading.text = "Shipment Report - Trezix"
        headingView.layoutIfNeeded()
        
        // Temp SpreadSheet View
        let tempSpreadView: SpreadsheetView = self.spreadView
        tempSpreadView.contentOffset = CGPoint.zero
        tempSpreadView.backgroundColor = .clear
        
        // Row Count Calculation
        var arrPageHeight: [CGFloat] = [0]
        var allRowMaxHeight = [CGFloat]()
        var allPageLastRowHeight: [CGFloat] = [0]
        
        let spreadSheetWidth: CGFloat = (17 * 72) - 20
        
        for row in 0..<arrShipmentReportData.count {
            let mirror = Mirror(reflecting: arrShipmentReportData[row])
            var i = 0
            var maxHeight: CGFloat = 0
            let allProperty = mirror.children.filter({ $0.label != "id" && $0.label != "packingInstruction" && $0.label != "materialGrpValue" && $0.label != "paymentTerm" && $0.label != "incoTerm1" && $0.label != "incoTerm2" && $0.label != "invoicingParty" && $0.label != "invoiceNum" && $0.label != "invoiceDate" && $0.label != "deliveryPort" && $0.label != "blNo" && $0.label != "blDate" && $0.label != "etaDate" && $0.label != "intimationNumber" && $0.label != "intimationDate" && $0.label != "documentClearingDate" && $0.label != "nominatedCha" && $0.label != "beNo" && $0.label != "beDate" && $0.label != "igmNo" && $0.label != "igmDate" && $0.label != "status" })
            for child in allProperty {
                if let text = child.value as? String {
                    let label = UILabel(frame: CGRectMake(0, 0, spreadSheetWidth*coulmnWidth[i], getDynamicHeight(size: 40)))
                    label.DynamicHeight = true
                    label.numberOfLines = 0
                    label.minimumScaleFactor = 0.5
                    label.lineBreakMode = .byWordWrapping
                    label.font = .systemFont(ofSize: getDynamicHeight(size: 10))
                    label.text = text
                    label.sizeToFit()
                    maxHeight = label.frame.height > maxHeight ? label.frame.height : maxHeight
                }
                i += 1
            }
            allRowMaxHeight.append(maxHeight)
        }
        
        var pageCounter = 0
        var rowCOunt = 0
        for rowHeight in allRowMaxHeight {
            var availableHeight: CGFloat = 0
            if pageCounter == 0 {   //First Page Height Calculation
                availableHeight = pageHeight - headerHeight - 20
            } else {    //Other Page Height Calculation
                availableHeight = pageHeight - getDynamicHeight(size: 50) - 40
            }
            
            if arrPageHeight.count == pageCounter {
                arrPageHeight.append(0)
                allPageLastRowHeight.append(0)
            }
            
            print("On Page \(pageCounter+1), Available Page Height \(availableHeight), currHeight = \(arrPageHeight[pageCounter]), remaining page height = \(availableHeight-arrPageHeight[pageCounter])\n\n\n")
            
            if (arrPageHeight[pageCounter] + rowHeight) <= availableHeight {
                arrPageHeight[pageCounter] += rowHeight
                allPageLastRowHeight[pageCounter] = rowHeight
                rowCOunt += 1
//                debugPrint("Current Page = \(pageCounter+1) && Total Height Array = \(arrPageHeight)")
            } else {
                debugPrint("On Page \(pageCounter+1) == Row Count \(rowCOunt)")
                rowCOunt = 0
                pageCounter += 1
            }
        }
        
        tempContainerView.addSubview(tempSpreadView)
        tempContainerView.addSubview(headingView)
        
        var currentPage = 0
        var scrollHeight: CGFloat = 0
        for page in arrPageHeight {
            UIGraphicsBeginPDFPage()
            tempSpreadView.setContentOffset(CGPoint(x: 0, y: currentPage == 0 ? 0 : scrollHeight), animated: false)
            
            print("Page--\(page)")
            print("pageHeight--\(pageHeight)")
            print("Header Height--\(headerHeight)")
            print("Last Row Height--\(allPageLastRowHeight[currentPage])\n\n")
            
            tempSpreadView.frame = CGRect(x: 10, y: currentPage == 0 ? headerHeight : 20, width: self.spreadView.contentSize.width, height: page)
            tempContainerView.layer.render(in: UIGraphicsGetCurrentContext()!)
            if currentPage == 0 {
                headingView.removeFromSuperview()
            }
            scrollHeight += (page-getDynamicHeight(size: 50))
            currentPage += 1
        }
        UIGraphicsEndPDFContext()
        let path = allReportPath.appendingPathComponent("Shipment Report - Trezix.pdf")
        
        do {
            try data.write(to: path, options: .atomic)
            debugPrint("PDF Created Successfully")
            debugPrint("With Total == \(arrShipmentReportData.count) rows..")
//            appView.makeToast("PDF Created Successfully!\nand Saved in Files")
            
            let openPath = allReportPath.absoluteString.replacingOccurrences(of: "file://", with: "shareddocuments://")
            let url = URL(string: openPath)!
            Loader.hideLoader()
            PDFCompleteAlert(url: url)
        } catch let ree {
            debugPrint("Somwthing Went Wrong Creating PDF = \(ree.localizedDescription)")
            appView.makeToast("Something Went Wrong!\nTry Again Later!")
            Loader.hideLoader()
        }
    }
    
    private func PDFCompleteAlert(url: URL) {
        let alert = UIAlertController(title: "Success", message: "Shipment Report is successfully exported as PDF!", preferredStyle: .alert)
        
        let cancelBtn = UIAlertAction(title: "Cancel", style: .cancel)
        let openFile = UIAlertAction(title: "Open File", style: .default) { _ in
            UIApplication.shared.open(url)
            self.dismiss(animated: true)
        }
        
        alert.addAction(cancelBtn)
        alert.addAction(openFile)
        
        present(alert, animated: true)
    }
    
    private func changeSorting(type: CurrentSorting) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let arrAscendingImages = [imageFirstSecAscending, imageSecondSecAscending, imageThirdSecAscending]
        let arrDescendingImages = [imageFirstSecDescending, imageSecondSecDescending, imageThirdSecDescending]
        applyFilterAndRefresh(filter: type)
        switch type {
        case .firstSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 0 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .firstSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 0 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .secondSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 1 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .secondSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 1 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .thirdSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 2 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .thirdSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 2 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        }
    }
    
    private func applyFilterAndRefresh(filter: CurrentSorting) {
        switch filter {
        case .firstSecAscending:
            arrShipmentReportData = arrShipmentReportData.sorted { $0.poNo.localizedCompare($1.poNo) == .orderedAscending }
        case .secondSecAscending:
            arrShipmentReportData = arrShipmentReportData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.poDate) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.poDate) else { return false }
                return date1.compare(date2) == .orderedAscending
            })
        case .thirdSecAscending:
            arrShipmentReportData = arrShipmentReportData.sorted { $0.vendorName.non_NilEmpty(true).localizedCompare($1.vendorName.non_NilEmpty(true)) == .orderedAscending }
        case .firstSecDescending:
            arrShipmentReportData = arrShipmentReportData.sorted { $0.poNo.localizedCompare($1.poNo) == .orderedDescending }
        case .secondSecDescending:
            arrShipmentReportData = arrShipmentReportData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.poDate) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.poDate) else { return false }
                return date1.compare(date2) == .orderedDescending
            })
        case .thirdSecDescending:
            arrShipmentReportData = arrShipmentReportData.sorted { $0.vendorName.non_NilEmpty(true).localizedCompare($1.vendorName.non_NilEmpty(true)) == .orderedDescending }
        }
        DispatchQueue.main.async {  [weak self] in
            self?.mainShipmentTable.reloadData()
        }
    }
}

extension ShipmentReportVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrShipmentReportData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getDynamicHeight(size: 50)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0 //getDynamicHeight(size: 1)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ThreeColumnRowView()
        headerView.isViewHeadingView(withSeparator: true, tag: section)
        let data = arrShipmentReportData[section]
        headerView.setData(firstLabel: data.poNo, secondLabel: data.poDate, thirdLabel: data.vendorName, threeColumnView: true)
        headerView.isSelected(selected: arrShowableSections.contains(section), lastCell: section+1 == arrShipmentReportData.count)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnSection(_:)))
        headerView.addGestureRecognizer(tapGesture)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrShowableSections.contains(section) ? 40 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
        cell.shipmentData = arrShipmentReportData[indexPath.section]
        cell.setShipReportInfo(forRow: indexPath.row)
        cell.isUserInteractionEnabled = false
        return cell
    }
}
extension ShipmentReportVC: SpreadsheetViewDelegate, SpreadsheetViewDataSource {
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        debugPrint("FIRST == \(firstRow.count)")
        return firstRow.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return arrShipmentReportData.count+1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        let spreadWidth: CGFloat = (17 * 72)-20
        return spreadWidth * coulmnWidth[column]
    }

    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if row == 0 {
            return getDynamicHeight(size: 50)
        } else {
            
            var maxHeight: CGFloat = 0

            let wi: CGFloat = (17 * 72)-20
            let dd = Mirror(reflecting: arrShipmentReportData[row-1])
            
            let allProperty = dd.children.filter({ $0.label != "id" && $0.label != "packingInstruction" && $0.label != "materialGrpValue" && $0.label != "paymentTerm" && $0.label != "incoTerm1" && $0.label != "incoTerm2" && $0.label != "invoicingParty" && $0.label != "invoiceNum" && $0.label != "invoiceDate" && $0.label != "deliveryPort" && $0.label != "blNo" && $0.label != "blDate" && $0.label != "etaDate" && $0.label != "intimationNumber" && $0.label != "intimationDate" && $0.label != "documentClearingDate" && $0.label != "nominatedCha" && $0.label != "beNo" && $0.label != "beDate" && $0.label != "igmNo" && $0.label != "igmDate" && $0.label != "status" })
            

            var i = 0
            for chil in allProperty {
                if let text = chil.value as? String {
                    let label = UILabel(frame: CGRectMake(0, 0, wi*coulmnWidth[i], getDynamicHeight(size: 40)))
                    label.numberOfLines = 0
                    label.minimumScaleFactor = 0.5
                    label.lineBreakMode = .byWordWrapping
                    label.font = .systemFont(ofSize: getDynamicHeight(size: 10))
                    label.text = text
                    label.sizeToFit()
                    maxHeight = label.frame.height > maxHeight ? label.frame.height : maxHeight
                }
                i += 1
            }
            arr += maxHeight
            return maxHeight
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: MyLabelCell.identifier, for: indexPath) as! MyLabelCell
        if indexPath.row == 0 {
            cell.setup(with: firstRow[indexPath.section], isHeading: true, multiLine: true)
            return cell
        } else {
            let currentData = arrShipmentReportData[indexPath.row-1]
            cell.backgroundColor = indexPath.row % 2 == 0 ? .lightGray.withAlphaComponent(0.25) : .clear
            cell.gridlines = .all(.none)
            cell.borders = .all(.none)
            switch indexPath.section {
            case 0: cell.setup(with: "\(indexPath.row)"); return cell
            case 1: cell.setup(with: currentData.poNo, multiLine: true); return cell
            case 2: cell.setup(with: currentData.poDate); return cell
            case 3: cell.setup(with: currentData.lcNo); return cell
            case 4: cell.setup(with: currentData.lcDate); return cell
            case 5: cell.setup(with: currentData.lcExpiryDate); return cell
            case 6: cell.setup(with: currentData.lastDateOfShipment); return cell
            case 7: cell.setup(with: currentData.companyPlant); return cell
            case 8: cell.setup(with: currentData.vendorName, multiLine: true); return cell
            case 9: cell.setup(with: currentData.vendorGroup1); return cell
            case 10: cell.setup(with: currentData.vendorGroup2); return cell
            case 11: cell.setup(with: currentData.materialCode, multiLine: true); return cell
            case 12: cell.setup(with: currentData.materialName, multiLine: true); return cell
            case 13: cell.setup(with: currentData.poQuantity); return cell
            case 14: cell.setup(with: currentData.shippedQuantity == nil ? "-" : "\(currentData.shippedQuantity!)"); return cell
            case 15: cell.setup(with: currentData.pendingQuantity == nil ? "-" : "\(currentData.pendingQuantity!)"); return cell
            case 16: cell.setup(with: currentData.unit); return cell
            case 17: cell.setup(with: currentData.rate); return cell
            case 18: cell.setup(with: currentData.currency); return cell
            case 19: cell.setup(with: currentData.fcValue == nil ? "-" : "\(currentData.fcValue!)", multiLine: true); return cell
            case 20: cell.setup(with: currentData.inrValue == nil ? "-" : "\(currentData.inrValue!)", multiLine: true); return cell
            default:
                cell.setup(with: "Default Cell")
                return cell
            }
        }
    }

}
