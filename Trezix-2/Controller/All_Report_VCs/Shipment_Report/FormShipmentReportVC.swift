//
//  FormShipmentReportVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 03/10/22.
//

import UIKit

class FormShipmentReportVC: UIViewController {
    
    // MARK: - ALL IBOUTLETS
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var textPlantName: UITextField!
    @IBOutlet weak var textFromPONumber: UITextField!
    @IBOutlet weak var textToPONumber: UITextField!
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    @IBOutlet weak var imageCheckMarkPODate: UIImageView!
    @IBOutlet weak var imageCheckMarkBEDate: UIImageView!
    @IBOutlet weak var btnSelectPODate: UIButton!
    @IBOutlet weak var btnSelectBEDate: UIButton!
    @IBOutlet weak var dropSelectMaterial: DropDown!
    @IBOutlet weak var dropSelectMaterialGroup: DropDown!
    
    @IBOutlet weak var datePickerContentView: UIView!
    @IBOutlet weak var btnCancelPicker: UIButton!
    @IBOutlet weak var btnDonePicker: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var btnSubmit: UIButton!

    // MARK: - ALL CONSTANTS & VARIABLES
    /// Selected From Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var fromDate: Date! = nil
    
    /// Selected To Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var toDate: Date! = nil
    
    /// ID of  selected Material from it's DropDown
    ///  - Used in ``call_Order_Report_API()``
    private var selMaterialID: Int! = nil
    
    /// ID of selected Material Group from it's DropDown
    ///  - Used in ``call_Order_Report_API()``
    private var selMaterialGroupID: String! = nil
    
    /// Array of Material Data fetched from ``APIConstants/GET_MATERIAL_DROPDOWN``
    private var arrMaterialData = [Material_Data]()
    
    /// Array of Material Group Data fetched from ``APIConstants/GET_MATERIAL_GROUP_DROPDOWN``
    private var arrMaterialGroupData = [Material_Group_Data]()
    
    /// Determines if  Calendar is showing from `From PO Number` or `To PO Number` Button
    private var isFromSectionCalendar = true
    
    private lazy var mainBlurr: UIVisualEffectView = {
        var blurrView = UIVisualEffectView()
        blurrView.frame = view.frame
        blurrView.alpha = 0.9
        return blurrView
    }()
    
    private var isPODateSelected = true
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        [btnFromDate, btnToDate, btnSubmit, btnCancelPicker, btnDonePicker].forEach { button in
            button?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
        datePickerContentView.layer.applyShadowAndRadius(shadowRadius: 1.5)
        [textPlantName, textFromPONumber, textToPONumber, dropSelectMaterial, dropSelectMaterialGroup].forEach { textField in
            textField?.layer.borderWidth = 0.5
            textField?.layer.borderColor = UIColor.systemBackground.cgColor
            textField?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
    }
    
    
    // MARK: - All Actions
    @IBAction func back_Button_Action(_ sender: UIButton) {
        if navigationController != nil {
            navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func cancel_Picker_Button_Action(_ sender: Any) {
        datePickerContentView.dismiss_With_Popup_Effect()
        mainBlurr.hideBlurrEffect()
    }
    
    @IBAction func done_Picker_Button_Action(_ sender: Any) {
        datePickerContentView.dismiss_With_Popup_Effect()
        
        isFromSectionCalendar ? (fromDate = datePicker.date) : (toDate = datePicker.date)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainBlurr.hideBlurrEffect()
            self.isFromSectionCalendar ? (self.btnFromDate.setTitle(DateHelper.shared.getStringFrom(date: self.fromDate), for: .normal)) : (self.btnToDate.setTitle(DateHelper.shared.getStringFrom(date: self.toDate), for: .normal))
        }
    }
    
    @IBAction func from_Date_Button_Action(_ sender: Any) {
        self.view.endEditing(true)
        isFromSectionCalendar = true
        datePickerContentView.show_With_Popup_Effect()
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func to_Date_Button_Action(_ sender: Any) {
        self.view.endEditing(true)
        isFromSectionCalendar = false
        datePickerContentView.show_With_Popup_Effect()
        datePicker.setDate(Date(), animated: true)
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func submit_Button_Action(_ sender: Any) {
        view.endEditing(true)
        call_Shipment_Report_API()
    }
    
    @IBAction func select_PO_Date_Checkmark(_ sender: UIButton) {
        isPODateSelected = true
        change_Selection_Checkmark()
    }
    
    @IBAction func select_BE_Date_Checkmark(_ sender: UIButton) {
        isPODateSelected = false
        change_Selection_Checkmark()
    }
    
    private func change_Selection_Checkmark() {
        [imageCheckMarkPODate, imageCheckMarkBEDate].forEach { checkMarkIcon in
            if (checkMarkIcon == imageCheckMarkPODate) {
                checkMarkIcon.image = isPODateSelected ? UIImage(systemName: "checkmark.circle") :  UIImage(systemName: "circle")
            } else {
                checkMarkIcon.image = !isPODateSelected ? UIImage(systemName: "checkmark.circle") :  UIImage(systemName: "circle")
            }
        }
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
    }
    
    
    // MARK: - All Helper Methods
    /// `Only Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable { callAllInitMethods() }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    ///  - Parameter afterNetworkRefresh: Boolean value that indicates if method if called first time or called after network refresh (after being offline & online)
    private func callAllInitMethods(_ afterNetworkRefresh: Bool = false) {
        initBothDropdowns()
        change_Selection_Checkmark()
        if !afterNetworkRefresh {
            datePicker.setPickerStyle_iOS14()
            let apiDispatch = DispatchQueue.global(qos: .utility)
            apiDispatch.async { [weak self] in
                self?.get_Material_Dropdown_Data()
                self?.get_Material_Group_Dropdown_Data()
            }
        }
    }
    
    private func initBothDropdowns() {
        [dropSelectMaterial, dropSelectMaterialGroup].forEach { down in
            down?.arrowColor = .ourAppThemeColor
            
            down?.hideOptionsWhenSelect = true
            down?.checkMarkEnabled = false
            down?.isSearchEnable = true
            down?.handleKeyboard = true
            down?.selectedRowColor = .ourAppThemeColor
            
            down?.listHeight = dropSelectMaterial.distanceFromSafeArea(distanceType: .lowerDistanceFromSafeArea)
            
            down?.didSelect(completion: { [weak self] selectedText, index, id in
                guard let strongSelf = self else { return }
                HapticFeedbackGenerator.simpleFeedback(type: .light)
                down?.text = selectedText
                
                if down == strongSelf.dropSelectMaterial {
                    strongSelf.selMaterialID = index == 0 ? nil : strongSelf.arrMaterialData[index].id
                } else {
                    strongSelf.selMaterialGroupID = index == 0 ? nil : selectedText
                }
            })
        }
    }
    
    /// *Fetechs Dropdown Data of* `Select Material`*Dropdown*
    private func get_Material_Dropdown_Data() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_MATERIAL_DROPDOWN.fullPath(),
                                            headers: [default_app_header],
                                            showHud: false)
        { [weak self] (isSuccess, response: Material_Dropdown_Modal?, errorMessage) in
            Loader.hideLoader()
            guard let self = self else { return }
            if isSuccess, let response = response {
                if let data = response.data {
                    self.arrMaterialData = data
                    
                    var allMaterialNames = data.compactMap { $0.materialDescription }
                    allMaterialNames.insert("Select Material", at: 0)
                    
                    self.dropSelectMaterial.selectedIndex = 0
                    self.dropSelectMaterial.text = allMaterialNames[0]
                    self.dropSelectMaterial.optionArray = allMaterialNames
                } else {
                    // No Material Data Available
                }
            } else if let errorMessage = errorMessage {
                debugPrint("Error Fetching Material Dropdown Data because of \(errorMessage)!")
                appView.makeToast("Error Fetching Material Dropdown Data!")
            }
        }
    }
    
    /// *Fetechs Dropdown Data of* `Select Material Group` *Dropdown*
    private func get_Material_Group_Dropdown_Data() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_MATERIAL_GROUP_DROPDOWN.fullPath(),
                                            headers: [default_app_header],
                                            showHud: false)
        { [weak self] (isSuccess, response: Material_Group_Dropdown_Modal?, errorMessage) in
            Loader.hideLoader()
            guard let self = self else { return }
            if isSuccess, let response = response {
                if let data = response.data {
                    self.arrMaterialGroupData = data
                    var allMaterialGroupNames = ["Select Material Group"]
                    for group in data {
                        let sep = group.value.components(separatedBy: ",")
                        allMaterialGroupNames.append(contentsOf: sep)
                    }
                    self.dropSelectMaterialGroup.selectedIndex = 0
                    self.dropSelectMaterialGroup.text = allMaterialGroupNames[0]
                    self.dropSelectMaterialGroup.optionArray = allMaterialGroupNames
                    Loader.hideLoader()
                } else {
                    // No Data Available
                }
            } else if let errorMessage = errorMessage {
                debugPrint("Error Fetching Material Dropdown Data because of \(errorMessage)!")
                appView.makeToast("Error Fetching Material Dropdown Data!")
            }
        }
    }
    
    private func call_Shipment_Report_API() {
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        let param: [String: Any] = ["plant": textPlantName.text ?? "",
                                    "po_no_1": textFromPONumber.text ?? "",
                                    "po_no_2": textToPONumber.text ?? "",
                                    "date_1": fromDate != nil ? API_parsing_dateString(date: fromDate) : "",
                                    "date_2": toDate != nil ? API_parsing_dateString(date: toDate) : "",
                                    "material": selMaterialID == nil ? "" : (dropSelectMaterial.text ?? ""),
                                    "material_grp": selMaterialGroupID == nil ? "" : (dropSelectMaterialGroup.text ?? ""),
                                    "date_type" : isPODateSelected ? "PO" : "BE"]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.SHIPMENT_REPORT.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false, senderView: btnSubmit)
        { [weak self] (isSuccess, response: ShipmentReportDataModal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response = response {
                if let mainData = response.data, mainData.count > 0 {    // DATA AVAILABLE
                    let vc = allReportStoryboard.instantiateViewController(withIdentifier: "ShipmentReportVC") as! ShipmentReportVC
                    vc.arrShipmentReportData = mainData
                    self.navigationController?.pushViewController(vc, animated: true)
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                } else {    // NO DATA AVAILABLE
                    appView.makeToast("NO DATA AVAILABLE!")
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                }
            } else if let errorMessage {
                debugPrint("Shipment Report Error - \(errorMessage)")
                HapticFeedbackGenerator.notificationFeedback(type: .error)
            }
        }
    }
}
