//
//  SalesOrderReportVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 12/12/22.
//

import UIKit

class SalesOrderReportVC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnSONumberSort: UIButton!
    @IBOutlet weak var btnSODateSort: UIButton!
    @IBOutlet weak var btnCustNameSort: UIButton!
    
    @IBOutlet weak var imageFirstSecAscending: UIImageView!
    @IBOutlet weak var imageFirstSecDescending: UIImageView!
    
    @IBOutlet weak var imageSecondSecAscending: UIImageView!
    @IBOutlet weak var imageSecondSecDescending: UIImageView!
    
    @IBOutlet weak var imageThirdSecAscending: UIImageView!
    @IBOutlet weak var imageThirdSecDescending: UIImageView!
    
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var btnExportPDF: UIButton!
    
    
    ///*Value to be pushed from* ``FormSalesOrderReport``
    var arrSalesOrderReportData = [Sales_Order_Report_Data]()
    
    ///Contains all indexes of section of which rows are visible to user { i.e. Currently showing extra info. on user click }
    private var arrShowableSections = [Int]()
    
    private var isSONumberAscending = true
    private var isSODateAscending = true
    private var isCustNameAscending = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingUpThisVC()
        debugPrint("CodingKey Experiment -- \(Sales_Order_Report_Data.allCases)")
        debugPrint("DATA COUNT -- \(arrSalesOrderReportData.count)")
    }
    // MARK: - All Helper Methods
    private func settingUpThisVC() {
        mainTableView.register(UINib(nibName: POReportExtraInfoCell.identifier, bundle: nil), forCellReuseIdentifier: POReportExtraInfoCell.identifier)
        if #available(iOS 15, *) {
            mainTableView.sectionHeaderTopPadding = 0
        }
//        initSpreadSheet()
        mainTableView.tableFooterView = UIView()
        changeSorting(type: .firstSecAscending)
    }
    
    private func changeSorting(type: CurrentSorting) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let arrAscendingImages = [imageFirstSecAscending, imageSecondSecAscending, imageThirdSecAscending]
        let arrDescendingImages = [imageFirstSecDescending, imageSecondSecDescending, imageThirdSecDescending]
        applyFilterAndRefresh(filter: type)
        switch type {
        case .firstSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 0 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .firstSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 0 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .secondSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 1 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .secondSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 1 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .thirdSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 2 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .thirdSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 2 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        }
    }
    
    private func applyFilterAndRefresh(filter: CurrentSorting) {
        switch filter {
        case .firstSecAscending:
            arrSalesOrderReportData = arrSalesOrderReportData.sorted { $0.soNo.non_NilEmpty(true).localizedCompare($1.soNo.non_NilEmpty(true)) == .orderedAscending }
        case .secondSecAscending:
            arrSalesOrderReportData = arrSalesOrderReportData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.soDate) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.soDate) else { return false }
                return date1.compare(date2) == .orderedAscending
            })
        case .thirdSecAscending:
            arrSalesOrderReportData = arrSalesOrderReportData.sorted { $0.customerName.non_NilEmpty(true).localizedCompare($1.customerName.non_NilEmpty(true)) == .orderedAscending }
        case .firstSecDescending:
            arrSalesOrderReportData = arrSalesOrderReportData.sorted { $0.soNo.non_NilEmpty(true).localizedCompare($1.soNo.non_NilEmpty(true)) == .orderedDescending }
        case .secondSecDescending:
            arrSalesOrderReportData = arrSalesOrderReportData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.soDate) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.soDate) else { return false }
                return date1.compare(date2) == .orderedDescending
            })
        case .thirdSecDescending:
            arrSalesOrderReportData = arrSalesOrderReportData.sorted { $0.customerName.non_NilEmpty(true).localizedCompare($1.customerName.non_NilEmpty(true)) == .orderedDescending }
        }
        DispatchQueue.main.async {  [weak self] in
            self?.mainTableView.reloadData()
        }
    }
    
    // MARK: - All Actions
    @IBAction func Back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func tappedOnSection(_ tap: UITapGestureRecognizer) {
        guard let tappedView = tap.view as? ThreeColumnRowView else { appView.makeToast("Unrecognised Touch"); return }
        let currentIndex = tappedView.currentRow
        if arrShowableSections.contains(currentIndex) {
            arrShowableSections = arrShowableSections.filter({ $0 != currentIndex })
        } else {
            arrShowableSections.append(currentIndex)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainTableView.beginUpdates()
            self.mainTableView.reloadSections(IndexSet(integer: currentIndex), with: .automatic)
            self.mainTableView.endUpdates()
        }
    }
    
    @IBAction func Change_First_Section_Sorting(_ sender: UIButton) {
        if isSONumberAscending {
            changeSorting(type: .firstSecDescending)
        } else {
            changeSorting(type: .firstSecAscending)
        }
        isSONumberAscending = !isSONumberAscending
        isSODateAscending = true
        isCustNameAscending = true
    }
    
    @IBAction func Change_Second_Section_Sorting(_ sender: UIButton) {
        if isSODateAscending {
            changeSorting(type: .secondSecAscending)
        } else {
            changeSorting(type: .secondSecDescending)
        }
        isSODateAscending = !isSODateAscending
        isSONumberAscending = true
        isCustNameAscending = true
    }
    
    @IBAction func Change_Third_Section_Sorting(_ sender: UIButton) {
        if isCustNameAscending {
            changeSorting(type: .thirdSecAscending)
        } else {
            changeSorting(type: .thirdSecDescending)
        }
        isCustNameAscending = !isCustNameAscending
        isSONumberAscending = true
        isSODateAscending = true
    }
    
    @IBAction func Export_Button_Action(_ sender: UIButton) {
        appView.makeToast("Upcoming Feature!")
    }
}

extension SalesOrderReportVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSalesOrderReportData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getDynamicHeight(size: 50)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0    //getDynamicHeight(size: 1)
    }

//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let separator = UIView(frame: CGRect(x: 10, y: 0, width: tableView.frame.width-20, height: 1))
//        separator.backgroundColor = .secondaryLabel
//        return separator
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ThreeColumnRowView()
        headerView.isViewHeadingView(withSeparator: true, tag: section)
        let data = arrSalesOrderReportData[section]
        headerView.setData(firstLabel: data.soNo, secondLabel: data.soDate, thirdLabel: data.customerName, threeColumnView: true)
        headerView.isSelected(selected: arrShowableSections.contains(section), lastCell: section+1 == arrSalesOrderReportData.count)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnSection(_:)))
        headerView.addGestureRecognizer(tapGesture)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrShowableSections.contains(section) ? 25 : 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
        cell.salesOrderReportData = arrSalesOrderReportData[indexPath.section]
        cell.setSalesOrderReportInfo(forRow: indexPath.row)
        cell.isUserInteractionEnabled = false
        return cell
    }
}
