//
//  FormSalesOrderReport.swift
//  Trezix-2
//
//  Created by Amar Panchal on 12/12/22.
//

import UIKit

class FormSalesOrderReport: UIViewController {
    
    //MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var textFromSONumber: UITextField!
    @IBOutlet weak var textToSONumber: UITextField!
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    @IBOutlet weak var dropSelectMaterial: DropDown!
    
    @IBOutlet weak var datePickerContentView: UIView!
    @IBOutlet weak var btnCancelPicker: UIButton!
    @IBOutlet weak var btnDonePicker: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    // MARK: - All Constants & Variables
    /// Selected From Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var fromSODate: Date! = nil
    
    /// Selected To Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var toSODate: Date! = nil
    
    /// Array of Material Names  of selected materials from it's DropDown
    ///  - Used in ``call_Order_Report_API()``
    private var arrSelectedMaterials: String! = nil
        
    /// Array of Material Data fetched from ``APIConstants/GET_MATERIAL_DROPDOWN``
    private var arrMaterialData = [Material_Data]()
    
    /// Determines if  Calendar is showing from `From SO Number` or `To SO Number` Button
    private var isFromSectionCalendar = true
    
    private lazy var mainBlurr: UIVisualEffectView = {
        var blurrView = UIVisualEffectView()
        blurrView.frame = view.frame
        blurrView.alpha = 0.9
        return blurrView
    }()
    
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVC()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        [btnFromDate, btnToDate, btnCancelPicker, btnDonePicker, btnSubmit].forEach { button in
            button?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
        datePickerContentView.layer.applyShadowAndRadius(shadowRadius: 1.5)

        [textFromSONumber, textToSONumber, dropSelectMaterial].forEach { textField in
            textField?.layer.borderWidth = 0.5
            textField?.layer.borderColor = UIColor.systemBackground.cgColor
            textField?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
    }
    
    // MARK: - All Helper Methods
    private func setUpVC() {
        initDropDown()
        datePicker.setPickerStyle_iOS14()
        DispatchQueue.global(qos: .utility).async { [weak self] in
            self?.get_Material_Dropdown_Data()
        }
    }
    
    private func initDropDown() {
        dropSelectMaterial.arrowColor = .ourAppThemeColor
        dropSelectMaterial.hideOptionsWhenSelect = true
        dropSelectMaterial.checkMarkEnabled = false
        dropSelectMaterial.isSearchEnable = true
        dropSelectMaterial.handleKeyboard = true
        dropSelectMaterial.selectedRowColor = .ourAppThemeColor
        dropSelectMaterial.isMultipleSelectionEnabled = true

        dropSelectMaterial.listHeight = dropSelectMaterial.distanceFromSafeArea(distanceType: .lowerDistanceFromSafeArea)
        dropSelectMaterial.didMultiSelect { [weak self] selectedTexts, indexes, ids, isCanceled  in
            HapticFeedbackGenerator.simpleFeedback(type: .medium)
            self?.arrSelectedMaterials = isCanceled ? nil : ids.compactMap { String($0) }.joined(separator: ",") //selectedTexts.joined(separator: ",")
        }
    }
    
    /// *Fetechs Dropdown Data of* `Select Material`*Dropdown*
    private func get_Material_Dropdown_Data() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_MATERIAL_DROPDOWN.fullPath(),
                                            headers: [default_app_header],
                                            showHud: false)
        { [weak self] (isSuccess, response: Material_Dropdown_Modal?, errorMessage) in
            Loader.hideLoader()
            guard let self = self else { return }
            if isSuccess, let response = response {
                guard let data = response.data else { debugPrint("No Data Available"); return }
                self.arrMaterialData = data
                var allMaterialNames = data.compactMap { $0.materialDescription }
                allMaterialNames.insert("Select Material", at: 0)
                let all_Materials_IDs = data.compactMap { $0.id }
                print(all_Materials_IDs)
                self.dropSelectMaterial.optionIds = all_Materials_IDs
                self.dropSelectMaterial.selectedIndex = 0
                self.dropSelectMaterial.text = allMaterialNames[0]
                self.dropSelectMaterial.optionArray = allMaterialNames
            } else if let errorMessage = errorMessage {
                debugPrint("Error Fetching Material Dropdown Data because of \(errorMessage)!")
                appView.makeToast("Error Fetching Material Dropdown Data!")
            }
        }
    }
    
    // MARK: - All Actions
    @IBAction func back_Button_Action(_ sender: UIButton) {
        guard let navController = navigationController else {
            dismiss(animated: true)
            return
        }
        navController.popViewController(animated: true)
    }
    
    @IBAction func cancel_Picker_Button_Action(_ sender: Any) {
        datePickerContentView.dismiss_With_Popup_Effect()
        mainBlurr.hideBlurrEffect()
    }
    
    @IBAction func done_Picker_Button_Action(_ sender: Any) {
        datePickerContentView.dismiss_With_Popup_Effect()
        
        isFromSectionCalendar ? (fromSODate = datePicker.date) : (toSODate = datePicker.date)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainBlurr.hideBlurrEffect()
            self.isFromSectionCalendar ? (self.btnFromDate.setTitle(DateHelper.shared.getStringFrom(date: self.fromSODate), for: .normal)) : (self.btnToDate.setTitle(DateHelper.shared.getStringFrom(date: self.toSODate), for: .normal))
        }
    }
    
    @IBAction func from_Date_Button_Action(_ sender: Any) {
        isFromSectionCalendar = true
        datePickerContentView.show_With_Popup_Effect()
        datePicker.setDate(Date(), animated: true)
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func to_Date_Button_Action(_ sender: Any) {
        self.view.endEditing(true)
        isFromSectionCalendar = false
        datePickerContentView.show_With_Popup_Effect()
        datePicker.setDate(Date(), animated: true)
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func submit_Button_Action(_ sender: Any) {
        view.endEditing(true)
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        let param = ["from_SO_no": textFromSONumber.text ?? "",
                     "to_SO_no": textToSONumber.text ?? "",
                     "from_date": fromSODate != nil ? API_parsing_dateString(date: fromSODate) : "",
                     "to_date": toSODate != nil ? API_parsing_dateString(date: toSODate) : "",
                     "material": arrSelectedMaterials ?? ""]
        debugPrint("Passing Param = \(param)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.GET_SALES_ORDER_REPORT.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false, senderView: btnSubmit)
        { [weak self] (isSuccess, response: Sales_Order_Report_Data_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response = response {
                Loader.hideLoader()
                if let mainData = response.data, mainData.count > 0 {
                    debugPrint("SUCCESSSS!!!")
                    let vc = allReportStoryboard.instantiateViewController(withIdentifier: "SalesOrderReportVC") as! SalesOrderReportVC
                    vc.arrSalesOrderReportData = mainData
                    self.navigationController?.pushViewController(vc, animated: true)
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                } else { //No Data Available
                    appView.makeToast("No Data Available!")
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                }
            } else if let errorMessage {
                debugPrint("Sales Order Report Error - \(errorMessage)")
                HapticFeedbackGenerator.notificationFeedback(type: .error)
            }
        }
    }
}
