//
//  LandingCostInvoiceVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 24/12/22.
//

import UIKit

class LandingCostInvoiceVC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var dropSelectInvoice: DropDown!
    
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    // MARK: - All Constants & Variables
    private var allInvoiceList = [Invoice_List_Data]()
    private var arrAllInvoiceStr = [String]()
    private var arrAllInvoiceIds = [String]()
    private var selectedInvoiceID: String! = nil
    
    private let dropDownPlaceholder = "Select Any Invoice"
    private let noDataPlaceholder = "NO DATA AVAILABLE"
    private let noDataSelectPlaceholder = "PLEASE SELECT ANY INVOICE FIRST!"
    
    private var arrLandingCostData = [Landing_Cost_Invoice_Data]()
    
    /// Contains all indexes of section of which rows are visible to user { i.e. Currently showing extra info. on user click }
    private var arrShowableSections = [Int]()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    /// `Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable {
            callAllInitMethods()
        }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    private func callAllInitMethods(_ afterRefresh: Bool = false) {
        if !afterRefresh { //Will be called Once
            settingUpThisVC()
        }
        fetchInvoiceList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        dropSelectInvoice.layer.applyShadowAndRadius()
        noDataView.layer.applyShadowAndRadius(cornerRadius: 8)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        DispatchQueue.main.async { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
    
    // MARK: - All Helper Methods
    private func settingUpThisVC() {
        lblNoData.text = noDataSelectPlaceholder
        mainTableView.setSectionPadding_iOS_15()
        mainTableView.register(UINib(nibName: "Landing_Cost_Cell", bundle: nil), forCellReuseIdentifier: "Landing_Cost_Cell")
        mainTableView.register(UINib(nibName: "Landing_Cost_HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "Landing_Cost_HeaderView")
        mainTableView.isHidden = true
        noDataView.isHidden = false
        initDropdown()
    }
    
    /// Setting up ``dropSelectInvoice`` drop-down of po list
    private func initDropdown() {
        dropSelectInvoice.arrowColor = .ourAppThemeColor
        dropSelectInvoice.hideOptionsWhenSelect = true
        dropSelectInvoice.checkMarkEnabled = false
        dropSelectInvoice.isSearchEnable = false
        dropSelectInvoice.handleKeyboard = true
        dropSelectInvoice.selectedRowColor = .ourAppThemeColor
        
        dropSelectInvoice.listHeight = dropSelectInvoice.distanceFromSafeArea(distanceType: .lowerDistanceFromSafeArea)
        
        dropSelectInvoice.didSelect(completion: { [weak self] selectedText, index, id in
            guard let strongSelf = self else { return }
            HapticFeedbackGenerator.simpleFeedback(type: .light)
            strongSelf.dropSelectInvoice.text = selectedText
            if index == 0 {
                strongSelf.selectedInvoiceID = nil
                strongSelf.mainTableView.isHidden = true
                strongSelf.lblNoData.text = strongSelf.noDataSelectPlaceholder
                strongSelf.noDataView.isHidden = false
            } else {
                strongSelf.lblNoData.text = strongSelf.noDataPlaceholder
                debugPrint("Invoice ID = \(strongSelf.arrAllInvoiceIds[index])")
                strongSelf.selectedInvoiceID = strongSelf.arrAllInvoiceIds[index]
                strongSelf.callLandingCostDataAPI()
            }
        })
    }
    
    /// Fetching Invoice List from API calling
    private func fetchInvoiceList() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_INVOICE_LIST.fullPath(),
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Get_Invoice_List_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                guard let alldata = response.result else {
                    debugPrint("Error Fetching Invoice List..")
                    appView.makeToast("Something Went Wrong\nTry Again Later!")
                    Loader.hideLoader()
                    return
                }
                self.arrAllInvoiceStr = alldata.compactMap({ $0.invoiceNum ?? "N/A" })
                self.arrAllInvoiceIds = alldata.compactMap({ $0.id })
                
                self.arrAllInvoiceStr.insert(self.dropDownPlaceholder, at: 0)
                self.arrAllInvoiceIds.insert("N/A", at: 0)
                
                self.dropSelectInvoice.text = self.arrAllInvoiceStr.first
                self.dropSelectInvoice.optionArray = self.arrAllInvoiceStr
                Loader.hideLoader()
            } else if let errorMessage {
                debugPrint("Fetch Invoice List Error - \(errorMessage)")
                appView.makeToast(errorMessage)
                Loader.hideLoader()
            }
        }
    }
    
    /// Fetching Landing Cost Report Data From API
    private func callLandingCostDataAPI() {
        guard let invoiceID = selectedInvoiceID else { debugPrint("Invalid Invoice!"); appView.makeToast("Invalid Invoice"); return }
        
        let param = ["id" : invoiceID]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.LANDING_COST_INVOICE.fullPath(),
                                            param: param,
                                            headers: [default_app_header], showHud: false, senderView: dropSelectInvoice)
        { [weak self] (isSuccess, response: Landing_Cost_Invoice_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                guard let allData = response.result else {
                    debugPrint("No Landing Cost Data"); appView.makeToast("No Landing Cost Data!");
                    self.mainTableView.isHidden = true; self.lblNoData.text = self.noDataPlaceholder; self.noDataView.isHidden = false;
                    return
                }
                DispatchQueue.global().asyncAfter(deadline: .now() + 0.1) {
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                }
                self.arrLandingCostData = allData
                DispatchQueue.main.async { [weak self] in
                    self?.noDataView.isHidden = true
                    self?.mainTableView.isHidden = false
                    self?.mainTableView.reloadData()
                }
            } else if let errorMessage {
                debugPrint("Fetch Landing Cost Report Error - \(errorMessage)")
                HapticFeedbackGenerator.notificationFeedback(type: .error)
                appView.makeToast(errorMessage)
            }
        }
    }
    
    // MARK: - All Actions
    @IBAction func Back_Button_Action(_ sender: UIButton) {
        guard let navController = navigationController else {
            debugPrint("Nil Navigation Controller!")
            appView.makeToast("Please Try Again Later!")
            return
        }
        navController.popViewController(animated: true)
    }
    
    @objc private func tappedOnSection(_ tap: UITapGestureRecognizer) {
        guard let tappedView = tap.view as? Landing_Cost_HeaderView else { appView.makeToast("Unrecognised Touch"); return }
        let currentIndex = tappedView.currentRow
        if arrShowableSections.contains(currentIndex) {
            arrShowableSections = arrShowableSections.filter({ $0 != currentIndex })
        } else {
            arrShowableSections.append(currentIndex)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainTableView.beginUpdates()
            self.mainTableView.reloadSections(IndexSet(integer: currentIndex), with: .automatic)
            self.mainTableView.endUpdates()
        }
    }
}
// MARK: - TableView Delegate Methods
extension LandingCostInvoiceVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrLandingCostData.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vi = UIView()
        vi.backgroundColor = .systemBackground
        return vi
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Landing_Cost_HeaderView") as! Landing_Cost_HeaderView
        let data = arrLandingCostData[section]
        headerView.setAllData(productCode: data.productCode, productName: data.productName, quantity: data.qty, price: data.price, total: data.total, currency: data.currncy, tag: section)
        headerView.isSectionOpenedClosed(open: arrShowableSections.contains(section))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnSection(_:)))
        headerView.addGestureRecognizer(tapGesture)
        return headerView
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var otherCount = 0
        if let otherCharges = arrLandingCostData[section].otherCharges {
            let elementCount = otherCharges.filter { $0.pvalue != nil }.count   // Suppose it's 5 elements but row count should be 3 for 5 elements
            otherCount = (elementCount % 2 == 0) ? (elementCount/2) : ((elementCount+1)/2)  // if 6 elements row count =6/2=3; if 5 elements rowCount = (5+1)/2 = 3;
        }
        return arrShowableSections.contains(section) ? (11 + otherCount) : 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Landing_Cost_Cell", for: indexPath) as! Landing_Cost_Cell
        cell.landingCostData = arrLandingCostData[indexPath.section]
        cell.setLandingCostReportInfo(forRow: indexPath.row)
        cell.isUserInteractionEnabled = false
        return cell
    }
}
