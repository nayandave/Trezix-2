//
//  Landing_CostReport_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 09/03/23.
//

import UIKit

class Landing_CostReport_VC: UIViewController {
    
    // MARK: - ALL OUTLETS
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    // Heading Section
    @IBOutlet weak var sortingHeadView: UIView!
    @IBOutlet weak var lblFirstSection: UILabel!
    @IBOutlet weak var imageFirstSecAscending: UIImageView!
    @IBOutlet weak var imageFirstSecDescending: UIImageView!
    
    @IBOutlet weak var lblSecondSection: UILabel!
    @IBOutlet weak var imageSecondSecAscending: UIImageView!
    @IBOutlet weak var imageSecondSecDescending: UIImageView!
    
    @IBOutlet weak var lblThirdSection: UILabel!
    @IBOutlet weak var imageThirdSecAscending: UIImageView!
    @IBOutlet weak var imageThirdSecDescending: UIImageView!
    
    @IBOutlet weak var btnFirstSecSort: UIButton!
    @IBOutlet weak var btnSecondSecSort: UIButton!
    @IBOutlet weak var btnThirdSecSort: UIButton!
    
    @IBOutlet weak var dataTableView: UITableView!
    
    // MARK: - All Constants & Variables
    var arrLandingCostData = [Landing_Cost_Report_Data]()
    var arrAllOtherCharges = [Int : [(String, String)]]()
    
    ///Contains all indexes of section of which rows are visible to user { i.e. Currently showing extra info. on user click }
    private var arrShowableSections = [Int]()
    
    private var isInvoiceAscending = true
    private var isDateAscending = true
    private var isCompanyAscending = true
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        settingUpThisVC()
    }
    
    // MARK: - All Helper Methods
    private func changeSorting(type: CurrentSorting) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let arrAscendingImages = [imageFirstSecAscending, imageSecondSecAscending, imageThirdSecAscending]
        let arrDescendingImages = [imageFirstSecDescending, imageSecondSecDescending, imageThirdSecDescending]
        applyFilterAndRefresh(filter: type)
        switch type {
        case .firstSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 0 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .firstSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 0 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .secondSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 1 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .secondSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 1 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .thirdSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 2 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .thirdSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 2 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        }
    }
    
    private func applyFilterAndRefresh(filter: CurrentSorting) {
        switch filter {
        case .firstSecAscending:
            arrLandingCostData = arrLandingCostData.sorted { $0.invoiceNo.non_NilEmpty(true).localizedCompare($1.invoiceNo.non_NilEmpty(true)) == .orderedAscending  }
        case .secondSecAscending:
            arrLandingCostData = arrLandingCostData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.creationDate) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.creationDate) else { return false }
                return date1.compare(date2) == .orderedAscending
            })
        case .thirdSecAscending:
            arrLandingCostData = arrLandingCostData.sorted { $0.companyname.non_NilEmpty(true).localizedCompare($1.companyname.non_NilEmpty(true)) == .orderedAscending }
        case .firstSecDescending:
            arrLandingCostData = arrLandingCostData.sorted(by: { first, second in first.invoiceNo.non_NilEmpty(true) > second.invoiceNo.non_NilEmpty(true) })
        case .secondSecDescending:
            arrLandingCostData = arrLandingCostData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.creationDate) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.creationDate) else { return false }
                return date1.compare(date2) == .orderedDescending
            })
        case .thirdSecDescending:
            arrLandingCostData = arrLandingCostData.sorted { $0.companyname.non_NilEmpty(true).localizedCompare($1.companyname.non_NilEmpty(true)) == .orderedDescending }
        }
        DispatchQueue.main.async {  [weak self] in
            self?.dataTableView.reloadData()
        }
    }
    
    private func settingUpThisVC() {
        dataTableView.register(UINib(nibName: POReportExtraInfoCell.identifier, bundle: nil), forCellReuseIdentifier: POReportExtraInfoCell.identifier)
        if #available(iOS 15, *) {
            dataTableView.sectionHeaderTopPadding = 0
        }
//        initSpreadSheet()
        dataTableView.tableFooterView = UIView()
        changeSorting(type: .firstSecAscending)
    }
    
    // MARK: - All Actions
    @IBAction private func back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func tappedOnSection(_ tap: UITapGestureRecognizer) {
        guard let tappedView = tap.view as? ThreeColumnRowView else { appView.makeToast("Unrecognised Touch"); return }
        let currentIndex = tappedView.currentRow
        if arrShowableSections.contains(currentIndex) {
            arrShowableSections = arrShowableSections.filter({ $0 != currentIndex })
        } else {
            arrShowableSections.append(currentIndex)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.dataTableView.beginUpdates()
            self.dataTableView.reloadSections(IndexSet(integer: currentIndex), with: .top)
            self.dataTableView.endUpdates()
        }
    }
    
    @IBAction func Change_First_Section_Sorting(_ sender: UIButton) {
        if isInvoiceAscending {
            changeSorting(type: .firstSecDescending)
        } else {
            changeSorting(type: .firstSecAscending)
        }
        isInvoiceAscending = !isInvoiceAscending
        isDateAscending = true
        isCompanyAscending = true
    }
    
    @IBAction func Change_Second_Section_Sorting(_ sender: UIButton) {
        if isDateAscending {
            changeSorting(type: .secondSecAscending)
        } else {
            changeSorting(type: .secondSecDescending)
        }
        isDateAscending = !isDateAscending
        isInvoiceAscending = true
        isCompanyAscending = true
    }
    
    @IBAction func Change_Third_Section_Sorting(_ sender: UIButton) {
        if isCompanyAscending {
            changeSorting(type: .thirdSecAscending)
        } else {
            changeSorting(type: .thirdSecDescending)
        }
        isCompanyAscending = !isCompanyAscending
        isInvoiceAscending = true
        isDateAscending = true
    }
}
// MARK: - TableView Delegate Methods
extension Landing_CostReport_VC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrLandingCostData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getDynamicHeight(size: 50)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ThreeColumnRowView()
        headerView.isViewHeadingView(withSeparator: true, tag: section)
        let data = arrLandingCostData[section]
        headerView.setData(firstLabel: data.invoiceNo, secondLabel: data.creationDate, thirdLabel: data.companyname, threeColumnView: true)
        headerView.isSelected(selected: arrShowableSections.contains(section), lastCell: section+1 == arrLandingCostData.count)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnSection(_:)))
        headerView.addGestureRecognizer(tapGesture)
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0    //getDynamicHeight(size: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let other = arrAllOtherCharges[section] {
            let totalCount = other.count    // Array(otherCharges.keys).count
            let addition = totalCount.isMultiple(of: 2) ? (totalCount/2) : ((totalCount-1)/2)
            return arrShowableSections.contains(section) ? (17+addition) : 0
        } else {
            return arrShowableSections.contains(section) ? 17 : 0
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
        cell.landingCostReportData = arrLandingCostData[indexPath.section]
        
        var allHeadings: [String]! = nil
        var allValues: [String]! = nil
        
        if let otherData = arrAllOtherCharges[indexPath.section] {
            allHeadings = otherData.compactMap { $0.0 }
            allValues = otherData.compactMap { $0.1 }
        }
        
        cell.set_Landing_Report_Info(for: indexPath.row, otherHeadings: allHeadings, otherValues: allValues)
        cell.isUserInteractionEnabled = false
        return cell
    }
}
