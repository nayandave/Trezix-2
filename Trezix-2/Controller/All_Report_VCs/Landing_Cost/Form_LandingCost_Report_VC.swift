//
//  Form_LandingCost_Report_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 09/03/23.
//

import UIKit

class Form_LandingCost_Report_VC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var drop_Select_Company: DropDown!
    @IBOutlet weak var drop_Select_Plant: DropDown!
    @IBOutlet weak var drop_Select_Vendor: DropDown!
    @IBOutlet weak var drop_Select_Material: DropDown!
    @IBOutlet weak var drop_Select_Invoice: DropDown!
    
    @IBOutlet weak var text_From_PO: UITextField!
    @IBOutlet weak var text_To_PO: UITextField!
    
    @IBOutlet weak var btn_From_Invoice_Date: UIButton!
    @IBOutlet weak var btn_To_Invoice_Date: UIButton!
    
    @IBOutlet weak var datePickerContentView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnCancelPicker: UIButton!
    @IBOutlet weak var btnDonePicker: UIButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    // MARK: - All Constants & Variables
    private var selected_Company: String? = nil
    private var selected_Plant: String? = nil
    private var selected_Vendor: String? = nil
    private var selected_Material: String? = nil
    private var selected_Invoice_ID: String? = nil
    
    private var all_Invoice_IDs = [String]()
    
    private var from_Invoice_Date: String? = nil
    private var to_Invoice_Date: String? = nil
    
    private var all_Company_List = ["Select Company"]
    private var all_Plant_List = ["Select Plant"]
    private var all_Vendor_List = ["Select Vendor"]
    private var all_Material_List = ["Select Material"]
    private var all_Invoice_List = ["Select Invoice"]
    
    private var selected_Company_ID: String! = nil
    
    private var all_Company_List_IDs = [String]()
    private var all_Plant_List_IDs = [String]()
    private var all_Vendor_List_IDs = [Int]()
    private var all_Material_List_IDs = [Int]()
    private var all_Invoice_List_IDs = [String]()
    
    /// Selected From Invoice Date From Calendar
    ///  - Used in ``call_LandingCost_Report_API()``
    private var fromDate: Date! = nil
    
    /// Selected To Date Invoice From Calendar
    ///  - Used in ``call_LandingCost_Report_API()``
    private var toDate: Date! = nil
    
    /// Determines if  Calendar is showing from `From Invoice Number` or `To Invoice Number` Button
    private var isFromSectionCalendar = true
    
    private lazy var mainBlurr: UIVisualEffectView = {
        var blurrView = UIVisualEffectView()
        blurrView.frame = view.frame
        blurrView.alpha = 0.9
        return blurrView
    }()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp_All_Dropdowns()
        [text_From_PO, text_To_PO].forEach { textField in
            textField?.delegate = self
        }
        datePicker.setPickerStyle_iOS14()
//        DispatchQueue.global(qos: .utility).async { [weak self] in
            self.fetch_All_Dropdown_List_API()
//        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        [drop_Select_Company, drop_Select_Plant, drop_Select_Vendor, drop_Select_Material, drop_Select_Invoice, text_From_PO, text_To_PO].forEach { dropDown in
            dropDown?.layer.borderWidth = 0.5
            dropDown?.layer.borderColor = UIColor.systemBackground.cgColor
            dropDown?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
        [btn_From_Invoice_Date, btn_To_Invoice_Date, btnCancelPicker, btnDonePicker, btnSubmit].forEach { button in
            button?.layer.applyShadowAndRadius()
        }
    }
    
    // MARK: - All Helper Methods
    private func setUp_All_Dropdowns() {
        [drop_Select_Company,
         drop_Select_Plant,
         drop_Select_Vendor,
         drop_Select_Material,
         drop_Select_Invoice].forEach { dropDown in
            dropDown?.arrowColor = .ourAppThemeColor
            dropDown?.hideOptionsWhenSelect = true
            dropDown?.checkMarkEnabled = false
            dropDown?.handleKeyboard = true
            dropDown?.selectedRowColor = .ourAppThemeColor
            if dropDown == drop_Select_Vendor || dropDown == drop_Select_Material {
                dropDown?.isOutsideTouchAllowed = false     // Should be false to work properly
                dropDown?.isMultipleSelectionEnabled = true
            }
            let topHeight = dropDown!.distanceFromSafeArea(distanceType: .upperDistanceFromSafeArea)
            let bottomHeight = dropDown!.distanceFromSafeArea(distanceType: .lowerDistanceFromSafeArea)
            
            dropDown?.listHeight = bottomHeight //topHeight > bottomHeight ? bottomHeight : topHeight
            
            if dropDown == drop_Select_Plant {
                dropDown?.optionArray = all_Plant_List
                dropDown?.text = all_Plant_List.first
                
                dropDown?.listWillAppear(completion: { [weak self] in
                    guard let self = self else { return }
                    if self.selected_Company == nil {
                        DispatchQueue.main.async {
                            appView.makeToast("Please Select Comapany First!", duration: 1.25)
                            DispatchQueue.main.async {
                                dropDown?.touchAction()
                            }
                        }
                    }
                })
            }
            
            if dropDown == drop_Select_Vendor || dropDown == drop_Select_Material {
                dropDown?.didMultiSelect { [weak self] selectedTexts, indexes, ids, isCanceled in
                    guard let self = self else { return }
                    HapticFeedbackGenerator.simpleFeedback(type: .medium)
                    debugPrint("Selected Textes = \(ids)")
                    if dropDown == self.drop_Select_Vendor {
                        self.selected_Vendor = isCanceled ? nil : ids.compactMap { String($0) }.joined(separator: ",")
                    } else {
                        self.selected_Material = isCanceled ? nil : ids.compactMap { String($0) }.joined(separator: ",")
                    }
                }
            } else {
                dropDown?.didSelect(completion: { [weak self] selectedText, index, id in
                    guard let self = self else { return }
                    HapticFeedbackGenerator.simpleFeedback(type: .light)
                    dropDown?.text = selectedText
                    
                    if dropDown == self.drop_Select_Company {
                        if index == 0 {
                            self.selected_Company = nil
                            self.selected_Company_ID = nil
                            self.all_Plant_List = ["Select Plant"]
                            self.drop_Select_Plant.optionArray = self.all_Plant_List
                            self.drop_Select_Plant.text = self.all_Plant_List[0]
                        } else {
                            self.selected_Company = self.all_Company_List[index]
                            self.selected_Company_ID = self.all_Company_List_IDs[index-1]
                            self.fetch_Plant_List_API()
                        }
                    } else if dropDown == self.drop_Select_Plant {
                        if index == 0 {
                            self.selected_Plant = nil
                        } else {
                            self.selected_Plant = self.all_Plant_List_IDs[index-1]
                        }
                    } else {    // Dropdown Invoice
                        if index == 0 {
                            self.selected_Invoice_ID = nil
                        } else {
                            self.selected_Invoice_ID = self.all_Invoice_List_IDs[index-1]
                        }
                    }
                })
            }
        }
    }

    private func fetch_All_Dropdown_List_API() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_ALL_DROPDOWNS_LANDING_COST.fullPath(),
                                            headers: [default_app_header], showHud: true)
        { [weak self] (isSuccess, response: Landing_Cost_All_Dropdown_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            if isSuccess, let response {
                if let data = response.data {
                    
                    if let arrMaterialData = data.material {
                        let nonNilData = arrMaterialData.filter { $0.materialDescription != nil }
                        self.all_Material_List = nonNilData.compactMap { $0.materialDescription }
                        self.all_Material_List_IDs = nonNilData.compactMap { $0.id }
//                        self.all_Material_List_IDs.insert(0, at: 0)
                        self.drop_Select_Material.optionIds = self.all_Material_List_IDs
                        self.all_Material_List.insert("Select Material", at: 0)
                        
                        self.drop_Select_Material.optionArray = self.all_Material_List
                        self.drop_Select_Material.selectedIndex = 0
                        self.drop_Select_Material.text = self.all_Material_List[0]
                    }
                    
                    if let arrInvoiceData = data.invoices {
                        let nonNilData = arrInvoiceData.filter { $0.invoiceNum != nil }
                        self.all_Invoice_List_IDs = nonNilData.compactMap { String($0.id) }
                        self.all_Invoice_List = nonNilData.compactMap { $0.invoiceNum }
                        self.all_Invoice_List.insert("Select Invoice", at: 0)
                        
                        self.drop_Select_Invoice.optionArray = self.all_Invoice_List
                        self.drop_Select_Invoice.selectedIndex = 0
                        self.drop_Select_Invoice.text = self.all_Invoice_List[0]
                    }
                    
                    if let arrCompanyData = data.companysetup {
                        let nonNilData = arrCompanyData.filter { $0.name != nil }
                        self.all_Company_List_IDs = nonNilData.compactMap { String($0.id) }
                        self.all_Company_List = nonNilData.compactMap { $0.name }
                        self.all_Company_List.insert("Select Company", at: 0)
//                        debugPrint("LIST IDs \(self.all_Company_List_IDs)")
                        self.drop_Select_Company.optionArray = self.all_Company_List
                        self.drop_Select_Company.selectedIndex = 0
                        self.drop_Select_Company.text = self.all_Company_List[0]
                    }
                    
                    if let arrVendorData = data.vendor {
                        let nonNilData = arrVendorData.filter { $0.companyName != nil }
                        self.all_Vendor_List_IDs = nonNilData.compactMap { $0.id }
//                        self.all_Vendor_List_IDs.insert(0, at: 0)
                        self.drop_Select_Vendor.optionIds = self.all_Vendor_List_IDs
                        self.all_Vendor_List = nonNilData.compactMap { $0.companyName }
                        self.all_Vendor_List.insert("Select Vendor", at: 0)
                        
                        self.drop_Select_Vendor.optionArray = self.all_Vendor_List
                        self.drop_Select_Vendor.selectedIndex = 0
                        self.drop_Select_Vendor.text = self.all_Vendor_List[0]
                    }
                } else {
                    // No Data Available
                }
            } else {
                debugPrint("Error fetching all drodown data due to \(String(describing: errorMessage))")
                appView.makeToast("Error Fetching All Dropdown Data!")
            }
        }
    }

    private func fetch_Plant_List_API() {
        guard let companyID = selected_Company_ID else { debugPrint("Select Company First to get related plants"); return }
        let param = ["id" : companyID]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.GET_PLANT_LIST_FROM_COMPANY.fullPath(),
                                            param: param, headers: [default_app_header], showHud: false)
        { [weak self] (isSuccess, response: Landing_Cost_Company_Plant_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if let arrPlantData = response.data {
                    let nonNilData = arrPlantData.filter { $0.label != nil }
                    self.all_Plant_List_IDs = nonNilData.compactMap { String($0.id) }
                    self.all_Plant_List = nonNilData.compactMap { $0.label }
                    self.all_Plant_List.insert("Select Plant", at: 0)
                    
                    self.drop_Select_Plant.optionArray = self.all_Plant_List
                }
            } else if let errorMessage {
                debugPrint("Fetching Plant list error due to \(errorMessage)")
                appView.makeToast("Something went erong fetching plant list\nPlease Try Again Later!")
            }
        }
    }
    
    private func call_LandingCost_Report_API() {
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        
        let param : [String : Any] = ["material": selected_Material ?? "",
                                      "type": selected_Invoice_ID ?? "",
                                      "ship_address": selected_Plant ?? "",
                                      "company": selected_Company_ID ?? "",
                                      "vendor": selected_Vendor ?? "",
                                      "date_1": fromDate != nil ? API_parsing_dateString(date: fromDate) : "",
                                      "date_2": toDate != nil ? API_parsing_dateString(date: toDate) : "",
                                      "po_no_2": text_To_PO.text ?? "",
                                      "po_no_1": text_From_PO.text ?? ""]
        print("Parameter -- \(param)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.LANDING_COST_REPORT.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false,
                                            senderView: btnSubmit)
        { [weak self] (isSuccess, response: Landing_Cost_Report_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response = response {
                if let mainData = response.data, mainData.count > 0 {       // DATA AVAILABLE
                    debugPrint("Data Fetched Successfully!")
                    let allCharges = mainData.compactMap { $0.keyvalue }.compactMap { $0.to_String_Double_JSON() }
                    
                    var allOtherDic = [Int: [(String,String)]]()
                    
                    
                    for index in 0..<mainData.count {
                        if let otherCharge = mainData[index].keyvalue.to_String_Double_JSON() {
                            allOtherDic[index] = otherCharge.compactMap { ($0.key, $0.value.cleanValue) }.sorted(by: <)
                        } else {
                            allOtherDic[index] = []
                        }
                    }
                    
                    let vc = allReportStoryboard.instantiateViewController(withIdentifier: "Landing_CostReport_VC") as! Landing_CostReport_VC
                    vc.arrLandingCostData = mainData
                    vc.arrAllOtherCharges = allOtherDic
                    self.navigationController?.pushViewController(vc, animated: true)
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                } else {        // NO DATA AVAILABLE
                    appView.makeToast("NO DATA AVAILABLE!")
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                }
            } else if let errorMessage {
                debugPrint("Order Report Error - \(errorMessage)")
                HapticFeedbackGenerator.notificationFeedback(type: .error)
            }
        }
    }
    
    // MARK: - All Actions
    @IBAction func Back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func from_Invoice_Date_Action(_ sender: UIButton) {
        self.view.endEditing(true)
        isFromSectionCalendar = true
        datePickerContentView.show_With_Popup_Effect()
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func to_Invoice_Date_Action(_ sender: UIButton) {
        self.view.endEditing(true)
        isFromSectionCalendar = false
        datePickerContentView.show_With_Popup_Effect()
        datePicker.setDate(Date(), animated: true)
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    
    @IBAction func Cancel_PickerButton_Action(_ sender: UIButton) {
        datePickerContentView.dismiss_With_Popup_Effect()
        mainBlurr.hideBlurrEffect()
    }
    
    @IBAction func Done_PickerButton_Action(_ sender: UIButton) {
        datePickerContentView.dismiss_With_Popup_Effect()
        
        isFromSectionCalendar ? (fromDate = datePicker.date) : (toDate = datePicker.date)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainBlurr.hideBlurrEffect()
            self.isFromSectionCalendar ? (self.btn_From_Invoice_Date.setTitle(DateHelper.shared.getStringFrom(date: self.fromDate), for: .normal)) : (self.btn_To_Invoice_Date.setTitle(DateHelper.shared.getStringFrom(date: self.toDate), for: .normal))
        }
    }
    
    @IBAction func Submit_Button_Action(_ sender: UIButton) {
        sender.show_Tap_Animation { [weak self] in
            guard let self = self else { return }
            self.call_LandingCost_Report_API()
        }
    }
}

extension Form_LandingCost_Report_VC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == text_From_PO {
            text_To_PO.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

extension String? {
    public func to_String_Double_JSON() -> [String: Double]? {
        guard let string = self else { return nil }
        guard let data = string.data(using: .utf8, allowLossyConversion: false) else { return nil }
        
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Double] {
                return json
            } else {
                debugPrint("Can't convert JSON String to Dictionary")
                return nil
            }
            
        } catch let err {
            debugPrint(err.localizedDescription)
            return nil
        }
        
    }
}
