//
//  PDFViewVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 08/12/22.
//

import UIKit
import PDFKit
import Alamofire

class PDFViewVC: UIViewController, UIPopoverPresentationControllerDelegate {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var failedLoadView: UIView!
    
    @IBOutlet weak var btnShare: UIButton!
    
    // MARK: - All Constants & Variables
    public var pdfLink: URL! = nil
    public var documentType: POTimelineVC.DownloadableSectionName! = nil
    public var pdfName = String()
    public var isViewingPDF: Bool = true
    
    private var pdfView: PDFView! = nil
    private var pdfDocument: Data! = nil
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initMethod()
        //Loader.showLoader()
        processingPDF(view: isViewingPDF)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        failedLoadView.layer.applyShadowAndRadius()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        let tempURL = tempFolderPath.appendingPathComponent(pdfName)
        if FileManager.default.fileExists(atPath: tempURL.gettingPath()) {
            do {
                try FileManager.default.removeItem(at: tempURL)
                debugPrint("Temp PDF Deleted Successfully!")
            } catch let err {
                debugPrint("Can't delete tempFile due to \(err.localizedDescription)")
            }
        }
    }
    
    // MARK: - Helper Methods
    
    private func initMethod() {
        // Setting Back Button Heading
        setUpBackHeading()
        
        pdfView = PDFView()
        
        //PDF View Configuration
        configurePDF()
        
        // PDF View Constraints COnfiguration
        configurePDFViewConstraints()
    }
    
    private func configurePDFViewConstraints() {
        //PDF View Constraints
        view.addSubview(pdfView)
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        let safeAreaFrame = view.safeAreaLayoutGuide
        pdfView.leadingAnchor.constraint(equalTo: safeAreaFrame.leadingAnchor, constant: 5).isActive = true
        pdfView.centerXAnchor.constraint(equalTo: safeAreaFrame.centerXAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: navContentView.bottomAnchor, constant: 5).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
    }
    
    private func showPDF() {
        guard let pdfData = pdfDocument else { Loader.hideLoader(); debugPrint("NIL Document!"); return }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            guard let pdfDoc = PDFDocument(data: pdfData) else {
                DispatchQueue.main.async { [weak self] in
                    self?.pdfView.isHidden = true
                    self?.failedLoadView.isHidden = false
                }
                Loader.hideLoader()
                return
            }
            DispatchQueue.main.async { [weak self] in
                self?.pdfView.isHidden = false
                self?.failedLoadView.isHidden = true
            }
            self.pdfView.document = pdfDoc
            Loader.hideLoader()
        }
    }
    
    private func configurePDF() {
        pdfView.maxScaleFactor = 3
        pdfView.minScaleFactor = pdfView.scaleFactor
        pdfView.autoScales = true
        pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pdfView.displayMode = .singlePageContinuous
        pdfView.displaysPageBreaks = true
        pdfView.displayDirection = .vertical
        
        pdfView.backgroundColor = .clear
    }
    
    private func setUpBackHeading() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.btnBack.setTitle(self.documentType.rawValue, for: .normal)
        }
    }
    
    private func processingPDF(view: Bool = true) {
        guard let url = pdfLink else {
            Loader.hideLoader()
            debugPrint("Invalid PDF Link!")
            appView.makeToast("Invalid PDF Link!")
            return
        }
        let downloadURL = poTimelineReportPath.appendingPathComponent(pdfName)
        if FileManager.default.fileExists(atPath: downloadURL.gettingPath()) {      //File Already Exists
            do {
                pdfLink = downloadURL
                pdfDocument = try Data(contentsOf: downloadURL)
                showPDF()
            } catch let err {
                debugPrint("Error Loading existing pdf due to \(err.localizedDescription)")
                appView.makeToast("PDF loading failed!")
                return
            }
        } else {
            AF.request(url)
                .responseData
            { [weak self] response in
                guard let self = self else { return }
                switch response.result {
                case .success(let data):
                    debugPrint("SUCCESS DATA!")
                    do {
                        if view {
                            let tempURL = tempFolderPath.appendingPathComponent(self.pdfName)
                            debugPrint("pdf successfully saved in temp folder !")
                            try data.write(to: tempURL, options: .atomic)
                            self.pdfLink = tempURL
                        } else {
                            debugPrint("Download URL == \(downloadURL)")
                            try data.write(to: downloadURL)
                            debugPrint("pdf successfully saved!")
                            self.pdfLink = downloadURL
                            self.showSuccessAlert(downloadURL)
                        }
                        self.pdfDocument = data
                        self.showPDF()
                    } catch let err {
                        debugPrint("Pdf could not be saved due to \(err.localizedDescription)")
                        appView.makeToast("PDF could not be saved!")
                        Loader.hideLoader()
                        self.popNavigateBack()
                    }
                case .failure(let err):
                    debugPrint("AF ERROR == \(err.localizedDescription)")
                    appView.makeToast("PDF could not be saved!")
                    Loader.hideLoader()
                    self.popNavigateBack()
                }
            }
        }
    }
    
    private func popNavigateBack() {
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
    private func showSuccessAlert(_ openURL: URL) {
        let alert = UIAlertController(title: "Success", message: "Your PDF named \(pdfName) is successfully downloaded!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        
        let openButton = UIAlertAction(title: "Open PDF", style: .cancel) { _ in
            if let strValue = openURL.absoluteString.removingPercentEncoding {
                openInFilesApp(URL(fileURLWithPath: strValue))
            }
        }
        alert.addAction(openButton)
        DispatchQueue.main.async { [weak self] in
            self?.present(alert, animated: true)
        }
    }
    
    // MARK: - Action Methods
    @IBAction func back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func share_Button_Action(_ sender: UIButton) {
        btnShare.animateLoader()
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        debugPrint("FINAL PDF LINK == \(pdfLink)")
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                let activityViewController = UIActivityViewController(activityItems: [self.pdfLink as Any], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                //activityViewController.popoverPresentationController!.sourceRect = self!.pdfView.frame
                DispatchQueue.main.async {
                    self.present(activityViewController, animated: true, completion: { [weak self] in
                        HapticFeedbackGenerator.notificationFeedback(type: .success)
                        self?.btnShare.remove_Activity_Animator()
                    })
                }
            }
        }
    }
}
