//
//  POTimelineVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 28/11/22.
//

import UIKit
import Alamofire

class POTimelineVC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblHeadingSelectPO: UILabel!
    @IBOutlet weak var dropSelectPO: DropDown!
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var timelineTableView: UITableView!
    
    // MARK: - All Constants & Variables
    private var selectedPO: String! = nil
    private var allPOIds = [String]()
    private var allPOList = [String]()
    
    private let dropDownPlaceholder = "Select Any PO"
    private let noDataPlaceholder = "NO DATA AVAILABLE"
    private let noDataSelectPlaceholder = "PLEASE SELECT ANY PO FIRST!"
    
    private var timeLineData: TimeLine_Data! = nil
    
    private var totalCount = 0
    
    // Individual Count
    private var poCount = 0
    private var vscCount = 0
    private var lcCount = 0
    private var pssCount = 0
    private var viCount = 0
    private var spCount = 0
    private var drCount = 0
    private var dcCount = 0
    private var boeCount = 0
    private var paymentCount = 0
    private var grnCount = 0
    
    // Less than counts for table
    private var lessLC = 0
    private var lessPSS = 0
    private var lessVI = 0
    private var lessSP = 0
    private var lessDR = 0
    private var lessDC = 0
    private var lessBOE = 0
    private var lessPAY = 0
    private var lessGRN = 0
    
    
    /// * All TimeLine Header Headings
    private enum AllTimeLineSections : String {
        case PurchaseOrder = "Purchase Order"
        case VendorSalesContract = "Vendor Sales Contract"
        case LetterOfCredit = "Letter of Credit"
        case PreShipmentSample = "Pre Shipment Sample"
        case VendorInvoice = "Vendor Invoice"
        case ShipmentPlanning = "Shipment Planning"
        case DocumentReceived = "Document Received At Bank"
        case DutyClearing = "Duty Clearing"
        case BillOfEntry = "Bill of Entry"
        case Payment = "Payment"
        case GoodsReceivedNote = "Goods Received Note"
    }
    
    /// * All Header Background Color of TimeLine
    enum POTimeLineBackColor: String {
        case darkBlue = "#41516c"
        case darkYellow = "#fbca3e"
        case darkPink = "#e24a68"
        case normalBlue = "#1b5f8c"
        case lightGreen = "#4cadad"
        
        var associatedDarkColor: UIColor {
            switch self {
            case .darkBlue: return UIColor.hexStringToUIColor(hex: "#344056")
            case .darkYellow: return UIColor.hexStringToUIColor(hex: "#f5b705")
            case .darkPink: return UIColor.hexStringToUIColor(hex: "#ce2143")
            case .normalBlue: return UIColor.hexStringToUIColor(hex: "#154c70")
            case .lightGreen: return UIColor.hexStringToUIColor(hex: "#3c8a8a")
            }
        }
    }
    
    
    /// Dictionary which connects each section with according backGround Color
    private let sectionWiseColor: [AllTimeLineSections: POTimeLineBackColor] = [.PurchaseOrder: .darkBlue,
                                                                                .VendorSalesContract: .darkYellow,
                                                                                .LetterOfCredit: .darkPink,
                                                                                .PreShipmentSample: .normalBlue,
                                                                                .VendorInvoice: .lightGreen,
                                                                                .ShipmentPlanning: .darkBlue,
                                                                                .DocumentReceived: .darkYellow,
                                                                                .DutyClearing: .darkPink,
                                                                                .BillOfEntry: .normalBlue,
                                                                                .Payment: .lightGreen,
                                                                                .GoodsReceivedNote: .darkBlue]
    
    /// All Downloadble File Category Names
    enum DownloadableSectionName: String {
        case PO
        case PO_Document
        case Sales_Contract
        case Other
        case Invoice
    }
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        dropSelectPO.layer.applyShadowAndRadius()
        noDataView.layer.applyShadowAndRadius(cornerRadius: 8)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        DispatchQueue.main.async { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
}
// MARK: - Helper Methods
extension POTimelineVC {
    /// Setting up ``dropSelectPO`` drop-down of po list
    private func initDropdown() {
        dropSelectPO.arrowColor = .ourAppThemeColor
        dropSelectPO.hideOptionsWhenSelect = true
        dropSelectPO.checkMarkEnabled = false
        dropSelectPO.isSearchEnable = false
        dropSelectPO.handleKeyboard = true
        dropSelectPO.selectedRowColor = .ourAppThemeColor
        
        debugPrint("Bottom Height = \(dropSelectPO.distanceFromSafeArea(distanceType: .lowerDistanceFromSafeArea))")
        dropSelectPO.listHeight = view.frame.height*0.5
        
        dropSelectPO.didSelect(completion: { [weak self] selectedText, index, id in
            guard let strongSelf = self else { return }
            HapticFeedbackGenerator.simpleFeedback(type: .light)
            strongSelf.dropSelectPO.text = selectedText
            if index == 0 {
                strongSelf.selectedPO = nil
                strongSelf.timelineTableView.isHidden = true
                strongSelf.lblNoData.text = strongSelf.noDataSelectPlaceholder
                strongSelf.noDataView.isHidden = false
            } else {
                strongSelf.lblNoData.text = strongSelf.noDataPlaceholder
                debugPrint("PO ID = \(strongSelf.allPOIds[index])")
                strongSelf.selectedPO = strongSelf.allPOIds[index]
                strongSelf.callPOTimeLineDataFetchAPI()
            }
        })
    }
    
    /// Registeres ``poTimeLineTableCell`` tableCell for tableView
    private func registerTableCell() {
        timelineTableView.register(UINib(nibName: "poTimeLineTableCell", bundle: nil),
                                   forCellReuseIdentifier: poTimeLineTableCell.identifier)
    }
    
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable { callAllInitMethods() }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    private func callAllInitMethods(_ afterNetworkRefresh: Bool = false) {
        if !afterNetworkRefresh {
            settingUpThisVC()
        }
        callPOListFetchAPI()
    }
    
    /// Setting up ``POTimelineVC`` ViewController initial method
    private func settingUpThisVC() {
        lblNoData.text = noDataSelectPlaceholder
        timelineTableView.isHidden = true
        noDataView.isHidden = false
        timelineTableView.estimatedRowHeight = 250
        registerTableCell()
        initDropdown()
    }
    
    /// Fetching PO List from API calling
    private func callPOListFetchAPI() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.FETCH_PO_LIST.fullPath(),
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Fetch_PO_List_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                guard let alldata = response.result else {
                    debugPrint("Error Fetching PO List..")
                    appView.makeToast("Something Went Wrong\nTry Again Later!")
                    Loader.hideLoader()
                    return
                }
                self.allPOList = alldata.compactMap({ String($0.poNo ?? "N/A") })
                self.allPOIds = alldata.compactMap({ String($0.id) })
                
                self.allPOList.insert(self.dropDownPlaceholder, at: 0)
                self.allPOIds.insert("N/A", at: 0)
                
                self.dropSelectPO.text = self.allPOList.first
                self.dropSelectPO.optionArray = self.allPOList
                Loader.hideLoader()
            } else if let errorMessage {
                debugPrint("Fetch PO List Error - \(errorMessage)")
                appView.makeToast(errorMessage)
                Loader.hideLoader()
            }
        }
    }
    
    /// API Calling of po timeline
    private func callPOTimeLineDataFetchAPI() {
        let param = ["id" : selectedPO!]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.PO_TIMELINE_DETAILS.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false, senderView: dropSelectPO)
        { [weak self] (isSuccess, response: PO_TimeLine_Data_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                guard let data = response.data else {
                    self.noDataView.isHidden = false
                    self.timelineTableView.isHidden = true
                    HapticFeedbackGenerator.notificationFeedback(type: .error)
//                    Loader.hideLoader()
                    return
                }
                self.timeLineData = data
//                Loader.hideLoader()
                self.calculateReload(data: data)
                HapticFeedbackGenerator.notificationFeedback(type: .success)
            } else if let errorMessage {
                debugPrint("Fetching TimeLine Error - \(errorMessage)")
                appView.makeToast("Something Went Wrong!\nTry Again Later!")
                self.timelineTableView.isHidden = true
                self.noDataView.isHidden = false
                HapticFeedbackGenerator.notificationFeedback(type: .error)
//                Loader.hideLoader()
            }
        }
    }
    
    /// Calculates different types of count e.g. poCount, boeCount etc.
    /// - Parameter data: ``TimeLine_Data`` type of data to be passed which is fetched from API
    private func calculateReload(data: TimeLine_Data) {
        let calGroup = DispatchGroup()
        calGroup.enter()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }
            self.poCount = data.pochange?.count ?? 0
            self.vscCount = data.salescontract?.count ?? 0
            self.lcCount = data.lc?.count ?? 0
            self.pssCount = data.pss?.count ?? 0
            self.viCount = data.invoice?.count ?? 0
            self.spCount = data.shipment?.count ?? 0
            self.drCount = data.docbank?.count ?? 0
            self.dcCount = data.duty?.count ?? 0
            self.boeCount = data.boe?.count ?? 0
            self.paymentCount = data.payment?.count ?? 0
            self.grnCount = data.grn?.count ?? 0
            
            self.lessLC = self.poCount + self.vscCount
            self.lessPSS = self.lessLC + self.lcCount
            self.lessVI = self.lessPSS + self.pssCount
            self.lessSP = self.lessVI + self.viCount
            self.lessDR = self.lessSP + self.spCount
            self.lessDC = self.lessDR + self.drCount
            self.lessBOE = self.lessDC + self.dcCount
            self.lessPAY = self.lessBOE + self.boeCount
            self.lessGRN = self.lessPAY + self.paymentCount
            
            self.totalCount = self.poCount + self.vscCount + self.lcCount + self.pssCount + self.viCount + self.spCount + self.drCount + self.dcCount + self.boeCount + self.paymentCount + self.grnCount
            
            debugPrint("\(self.poCount),\(self.vscCount),\(self.lcCount),\(self.pssCount),\(self.viCount),\(self.spCount),\(self.drCount),\(self.dcCount),\(self.boeCount),\(self.paymentCount),\(self.grnCount)")
            calGroup.leave()
        }
        calGroup.notify(queue: .main) {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.noDataView.isHidden = true
                self.timelineTableView.isHidden = false
                self.timelineTableView.reloadData()
            }
        }
    }
    
    /// Downloads multiple types of PDF
    /// - Parameter type: Type of data determined by enum ``POTimelineVC.DownloadableSectionName``
    /// - Parameter tag: Index of data passed according to tableView cell tag
    /// - Parameter subIndex: Index of Invoice Attachment Array { non-nil value only if type == Invoice }
    private func download_Diff_Type_PDF(type: DownloadableSectionName, tag: Int, subIndex: Int! = nil) {
        Loader.showLoader()
        downloadViewPDF(of: type, index: tag, downloadView: true, subIndex: subIndex)
    }
    
    /// Views multiple types of PDF
    /// - Parameter type: Type of data determined by enum ``POTimelineVC.DownloadableSectionName``
    /// - Parameter tag: Index of data passed according to tableView cell tag
    /// - Parameter subIndex: Index of Invoice Attachment Array { non-nil value only if type == Invoice }
    private func view_Diff_Type_PDF(type: DownloadableSectionName, tag: Int, subIndex: Int! = nil) {
        Loader.showLoader()
        downloadViewPDF(of: type, index: tag, downloadView: false, subIndex: subIndex)
    }
    
    /// Downloads or View PDF Helper Function
    /// - Parameter of: Type of data determined by enum ``POTimelineVC.DownloadableSectionName``
    /// - Parameter index: Index of data passed by caller function according to cell index of tableView
    /// - Parameter downloadView: Boolean value `TRUE` if PDF to be downloaded & `FALSE` if PDF to be viewed
    /// - Parameter subIndex: Index of Invoice Attachment Array { non-nil value only if type == Invoice }
    private func downloadViewPDF(of type: DownloadableSectionName, index: Int, downloadView: Bool, subIndex: Int! = nil) {
        guard let downData = timeLineData.durl else { debugPrint("Invalid Download URLs"); return }
        guard let viewData = timeLineData.url else { debugPrint("Invalid View URLs"); return }
        
        let docType = type
    
        switch type {
        case .PO:
            if let nameData = timeLineData.popdf?[index], let name = nameData.poPDF {
                if downloadView {
                    guard let downURL = downData.po else { debugPrint("Invalid PO Download URL"); return }
                    viewDownloadPDF(type: docType, view: false, url: downURL, name: name)
                } else {
                    guard let viewURL = viewData.po else { debugPrint("Invalid PO View URL"); return }
                    viewDownloadPDF(type: docType, view: true, url: viewURL, name: name)
                }
            } else { debugPrint("Invalid PO Name"); return }
        case .PO_Document:
            if let nameData = timeLineData.pochange?[index], let name = nameData.documentUpload {
                if downloadView {
                    guard let downURL = downData.poDocument else { debugPrint("Invalid PO_Document Download URL"); return }
                    viewDownloadPDF(type: docType, view: false, url: downURL, name: name)
                } else {
                    guard let viewURL = viewData.poDocument else { debugPrint("Invalid PO_Document View URL"); return }
                    viewDownloadPDF(type: docType, view: true, url: viewURL, name: name)
                }
            } else { debugPrint("Invalid PO_Document Name"); return }
        case .Sales_Contract:
            if let nameData = timeLineData.salescontract?[index], let name = nameData.salescontract {
                if downloadView {
                    guard let downURL = downData.salesContract else { debugPrint("Invalid Sales_Contract Download URL"); return }
                    viewDownloadPDF(type: docType, view: false, url: downURL, name: name)
                } else {
                    guard let viewURL = viewData.salesContract else { debugPrint("Invalid Sales_Contract View URL"); return }
                    viewDownloadPDF(type: docType, view: true, url: viewURL, name: name)
                }
            } else { debugPrint("Invalid Sales_Contract Name"); return }
        case .Other:
            if let nameData = timeLineData.pochange?[index], var name = nameData.otherDocument {
                //name.removeFirst()
                //name.removeFirst()
                if downloadView {
                    guard let downURL = downData.other else { debugPrint("Invalid Other Document Download URL"); return }
                    viewDownloadPDF(type: docType, view: false, url: downURL, name: name)
                } else {
                    guard let viewURL = viewData.other else { debugPrint("Invalid Other Document View URL"); return }
                    viewDownloadPDF(type: docType, view: true, url: viewURL, name: name)
                }
            } else { debugPrint("Invalid Other Document Name"); return }
        case .Invoice:
            if let nameData = timeLineData.invoice?[index], let singleAllName = nameData.attachment {
                
                var allNames = singleAllName.components(separatedBy: ",")
                let name = allNames[subIndex]
                
                if downloadView {
                    guard let downURL = downData.invoice else { debugPrint("Invalid Invoices Download URL"); return }
                    viewDownloadPDF(type: docType, view: false, url: downURL, name: name)
                } else {
                    guard let viewURL = viewData.invoice else { debugPrint("Invalid Invoices View URL"); return }
                    viewDownloadPDF(type: docType, view: true, url: viewURL, name: name)
                }
            } else { debugPrint("Invalid Invoices Name"); return }
        }
    }
    
    /// Views or Downloads PDF from passed URL
    /// - Parameter view: `TRUE` if PDF to be Viewed & `FALSE` if PDF to be Downloaded
    /// - Parameter url: Segment of URL which is passed from ``POTimelineVC.downloadViewPDF`` function of type String
    /// - Parameter name: PDF Name of type String
    private func viewDownloadPDF(type: DownloadableSectionName, view: Bool, url: String, name: String) {
        var baseURL = API_BaseURL
        baseURL.removeLast()    ///Removing last `/` from API_BaseURL
        
        let onlyURL = baseURL + url
        let finalURL = onlyURL + name
        
        guard let finalURL = finalURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { debugPrint("Invalid URL Format to pass"); appView.makeToast("Invalid URL!"); return }
        let pdfVC = allReportStoryboard.instantiateViewController(withIdentifier: "PDFViewVC") as! PDFViewVC
        pdfVC.pdfLink = URL(string: finalURL)
        pdfVC.pdfName = name
        pdfVC.isViewingPDF = view
        pdfVC.documentType = type
        navigationController?.pushViewController(pdfVC, animated: true)
    }
}

// MARK: - Document Preview Delegate
extension POTimelineVC: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
}

// MARK: - All IBAction
extension POTimelineVC {
    @IBAction func btn_Back_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - TableView Delegate Methods
extension POTimelineVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: poTimeLineTableCell.identifier, for: indexPath) as! poTimeLineTableCell
        
        cell.selectionStyle = .none
        // Default date format to be shown in every cell which has some kind of date
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        
        if poCount > 0 && indexPath.row < poCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.PurchaseOrder.rawValue, color: sectionWiseColor[.PurchaseOrder]!)
            guard let poData = timeLineData.pochange?[indexPath.row] else { return cell }
            
            cell.setFirstRowData(heading: poData.poNo, data: nil, onlyHeading: true)
            cell.setSecondRowData(heading: "Date : ",
                                  data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: poData.createdAt!)), isHidden: false)
            cell.setThirdRowData(heading: "Status : ", data: poData.statusSlug, isHidden: false)
            
            if let documentPO = poData.documentUpload, documentPO != "" {
                let index = cell.infoStackView.arrangedSubviews.firstIndex { view in
                    guard let view = view as? TimeLineAddtionalView else { return false }
                    return (view.tag == indexPath.row && view.downloadbelCategory == .PO_Document)
                }
                
                if index == nil {
                    let pdfRow = TimeLineAddtionalView()
                    pdfRow.setUpActions(downloadSection: .PO_Document, viewAction: { [weak self] in
                        self?.view_Diff_Type_PDF(type: .PO_Document, tag: indexPath.row)
                    }, downloadAction: { [weak self] in
                        self?.download_Diff_Type_PDF(type: .PO_Document, tag: indexPath.row)
                    }, index: indexPath.row)
                    pdfRow.setInfo(heading: "Document PO", viewName: documentPO)
                    cell.infoStackView.addArrangedSubview(pdfRow)
                }
            }
            
            if let pdfData = timeLineData.popdf?[indexPath.row] {
                let index = cell.infoStackView.arrangedSubviews.firstIndex { view in
                    guard let view = view as? TimeLineAddtionalView else { return false }
                    return (view.tag == indexPath.row && view.downloadbelCategory == .PO)
                }
                
                if index == nil {
                    let pdfRow = TimeLineAddtionalView()
                    pdfRow.setUpActions(downloadSection: .PO, viewAction: { [weak self] in
                        self?.view_Diff_Type_PDF(type: .PO, tag: indexPath.row)
                    }, downloadAction: { [weak self] in
                        self?.download_Diff_Type_PDF(type: .PO, tag: indexPath.row)
                    }, index: indexPath.row)
                    pdfRow.setInfo(heading: "PO", viewName: pdfData.poPDF)
                    cell.infoStackView.addArrangedSubview(pdfRow)
                }
            }
            
            if var otherDoc = poData.otherDocument, otherDoc != "" {
                let index = cell.infoStackView.arrangedSubviews.firstIndex { view in
                    guard let view = view as? TimeLineAddtionalView else { return false }
                    return (view.tag == indexPath.row && view.downloadbelCategory == .Other)
                }
                
                if index == nil {
                    let pdfRow = TimeLineAddtionalView()
                    pdfRow.setUpActions(downloadSection: .Other, viewAction: { [weak self] in
                        self?.view_Diff_Type_PDF(type: .Other, tag: indexPath.row)
                    }, downloadAction: { [weak self] in
                        self?.download_Diff_Type_PDF(type: .Other, tag: indexPath.row)
                    }, index: indexPath.row)
                    debugPrint("ORIGINAL OTHER DOC.-- \(otherDoc)")
//                    otherDoc.removeFirst()
//                    otherDoc.removeFirst()
                    pdfRow.setInfo(heading: "Other Document: ", viewName: otherDoc)
                    cell.infoStackView.addArrangedSubview(pdfRow)
                }
            } 
        } else if vscCount > 0 && (indexPath.row-poCount) < vscCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.VendorSalesContract.rawValue, color: sectionWiseColor[.VendorSalesContract]!)
            guard let vscData = timeLineData.salescontract?[indexPath.row-poCount] else { return cell }
            
            cell.setFirstRowData(heading: "\(vscData.id)", data: nil, onlyHeading: true)
            cell.setSecondRowData(heading: "Date : ",
                                  data: formatter.string(from: DateHelper.shared.getDateFromString(vscData.date, withFormat: "yyyy-MM-dd")!), isHidden: false)
            cell.setThirdRowData(heading: "Sales Contract Date : ",
                                 data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: vscData.createdAt!)), isHidden: false)
            
            if let pdfName = vscData.salescontract {
                let isAlreadAdded = cell.infoStackView.arrangedSubviews.firstIndex { view in
                    guard let view = view as? TimeLineAddtionalView else { return false }
                    return view.tag == indexPath.row-poCount
                }
                
                if isAlreadAdded == nil {
                    let pdfRow = TimeLineAddtionalView()
                    pdfRow.setUpActions(downloadSection: .Sales_Contract, viewAction: { [weak self] in
                        self?.view_Diff_Type_PDF(type: .Sales_Contract, tag: indexPath.row-self!.poCount)
                    }, downloadAction: { [weak self] in
                        self?.download_Diff_Type_PDF(type: .Sales_Contract, tag: indexPath.row-self!.poCount)
                    }, index: indexPath.row-lessVI)
                    pdfRow.setInfo(heading: "Sales Contract", viewName: pdfName)
                    cell.infoStackView.addArrangedSubview(pdfRow)
                }
            }
        } else if lcCount > 0 && (indexPath.row-lessLC) < lcCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.LetterOfCredit.rawValue, color: sectionWiseColor[.LetterOfCredit]!)
            guard let lcData = timeLineData.lc?[indexPath.row-lessLC] else { return cell }

            cell.setFirstRowData(heading: lcData.lcNo ?? "N/A", data: nil, onlyHeading: true)
            cell.setSecondRowData(heading: "LC Date : ",
                                  data: formatter.string(from: DateHelper.shared.getDateFromString(lcData.lcDate, withFormat: "yyyy-MM-dd")!), isHidden: false)
            cell.setThirdRowData(heading: "Expiry Date : ",
                                 data: formatter.string(from: DateHelper.shared.getDateFromString(lcData.lcExpiryDate, withFormat: "yyyy-MM-dd")!), isHidden: false)
            let index = cell.infoStackView.arrangedSubviews.firstIndex { view in
                guard let view = view as? TimeLineStackRow else { return false }
                return view.tag == indexPath.row-lessLC
            }
            
            if index == nil {
                let fourthRow = TimeLineStackRow()
                fourthRow.setAllInfo(heading: "Amount : ", data: lcData.lcAmount == nil ? "N/A" : "\(lcData.lcAmount!)", tag: indexPath.row-lessLC)
                cell.infoStackView.addArrangedSubview(fourthRow)
            } else {
                debugPrint("INDEX EXIST--")
            }
            
        } else if pssCount > 0 && (indexPath.row-lessPSS) < pssCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.PreShipmentSample.rawValue, color: sectionWiseColor[.PreShipmentSample]!)
            
            guard let pssData = timeLineData.pss?[indexPath.row-lessPSS] else { return cell }
            
            cell.setFirstRowData(heading: pssData.sampleNo ?? "N/A", data: nil, onlyHeading: true)
            cell.setSecondRowData(heading: "Received Date : ",
                                  data: formatter.string(from: DateHelper.shared.getDateFromString(pssData.recvdDate, withFormat: "yyyy-MM-dd")!), isHidden: false)
            cell.setThirdRowData(heading: "Created Date : ",
                                  data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: pssData.createdAt!)), isHidden: false)
            
        } else if viCount > 0 && (indexPath.row-lessVI) < viCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.VendorInvoice.rawValue, color: sectionWiseColor[.VendorInvoice]!)
            
            guard let viData = timeLineData.invoice?[indexPath.row-lessVI] else { return cell }

            cell.setFirstRowData(heading: viData.invoiceNum ?? "N/A", data: nil, onlyHeading: true)
            cell.setSecondRowData(heading: "Invoice Date : ",
                                  data: formatter.string(from: DateHelper.shared.getDateFromString(viData.invoiceDate, withFormat: "yyyy-MM-dd")!), isHidden: false)
            cell.setThirdRowData(heading: "Created Date : ",
                                 data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: viData.createdAt!)), isHidden: false)
            
            if let attachment = viData.attachment {
                let allAttachments = attachment.components(separatedBy: ",")
                
                for i in 0..<allAttachments.count {
                    let isAlreadAdded = cell.infoStackView.arrangedSubviews.firstIndex { view in
                        guard let view = view as? TimeLineAddtionalView else { return false }
                        return view.tag == i
                    }
                    
                    if isAlreadAdded == nil {
                        let pdfRow = TimeLineAddtionalView()
                        pdfRow.setUpActions(downloadSection: .Invoice, viewAction: { [weak self] in
                            self?.view_Diff_Type_PDF(type: .Invoice, tag: indexPath.row-self!.lessVI, subIndex: i)
                        }, downloadAction: { [weak self] in
                            self?.download_Diff_Type_PDF(type: .Invoice, tag: indexPath.row-self!.lessVI, subIndex: i)
                        }, index: indexPath.row-lessVI)
                        pdfRow.setInfo(heading: "Invoice Attachment", viewName: allAttachments[i])
                        cell.infoStackView.addArrangedSubview(pdfRow)
                    }
                }
            }
        } else if spCount > 0 && (indexPath.row-lessSP) < spCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.ShipmentPlanning.rawValue, color: sectionWiseColor[.ShipmentPlanning]!)
            guard let spData = timeLineData.shipment?[indexPath.row-lessSP] else { return cell }
            
            cell.setFirstRowData(heading: spData.blNo ?? "N/A", data: nil, onlyHeading: true)
            cell.setSecondRowData(heading: "Estimate Dispatch Date : ",
                                  data: formatter.string(from: DateHelper.shared.getDateFromString(spData.etd, withFormat: "yyyy-MM-dd")!), isHidden: false)
            cell.setThirdRowData(heading: "Created Date : ",
                                 data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: spData.createdAt!)), isHidden: false)
        } else if drCount > 0 && (indexPath.row-lessDR) < drCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.DocumentReceived.rawValue, color: sectionWiseColor[.DocumentReceived]!)
            guard let drData = timeLineData.docbank?[indexPath.row-lessDR] else { return cell }
            
            cell.setFirstRowData(heading: drData.intimationNumber ?? "N/A", onlyHeading: true)
            cell.setSecondRowData(heading: "Created At : ", data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: drData.createdAt!)), isHidden: false)
            cell.setThirdRowData(isHidden: true)
        
        } else if dcCount > 0 && (indexPath.row-lessDC) < dcCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.DutyClearing.rawValue, color: sectionWiseColor[.DutyClearing]!)
            guard let dcData = timeLineData.duty?[indexPath.row-lessDC] else { return cell }
            
            cell.setFirstRowData(heading: dcData.invoiceNum ?? "N/A", onlyHeading: true)
            cell.setSecondRowData(heading: "Created At : ", data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: dcData.createdAt!)), isHidden: false)
            cell.setThirdRowData(isHidden: true)
        } else if boeCount > 0 && (indexPath.row-lessBOE) < boeCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.BillOfEntry.rawValue, color: sectionWiseColor[.BillOfEntry]!)
            guard let boeData = timeLineData.boe?[indexPath.row-lessBOE] else { return cell }
            
            cell.setFirstRowData(heading: boeData.beNo ?? "N/A", onlyHeading: true)
            cell.setSecondRowData(heading: "Created At : ", data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: boeData.createdAt!)), isHidden: false)
            cell.setThirdRowData(isHidden: true)
            
            
        } else if paymentCount > 0 && (indexPath.row-lessPAY) < paymentCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.Payment.rawValue, color: sectionWiseColor[.Payment]!)
            guard let payData = timeLineData.payment?[indexPath.row-lessPAY] else { return cell }
            
            cell.setFirstRowData(heading: "\(payData.id)", onlyHeading: true)
            cell.setSecondRowData(heading: "Created Date : ",
                                 data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: payData.createdAt!)), isHidden: false)
            cell.setThirdRowData(heading: "Payment Date : ",
                                 data: formatter.string(from: DateHelper.shared.getDateFromString(payData.paymentDate, withFormat: "yyyy-MM-dd")!), isHidden: false)
            
        } else if grnCount > 0 && (indexPath.row-lessGRN) < grnCount {
            cell.setHeaderAndColor(heading: AllTimeLineSections.GoodsReceivedNote.rawValue, color: sectionWiseColor[.GoodsReceivedNote]!)
            guard let grnData = timeLineData.grn?[indexPath.row-lessGRN] else { return cell }
            cell.setFirstRowData(heading: grnData.grnReference ?? "N/A", onlyHeading: true)
            cell.setSecondRowData(heading: "GRN Date : ",
                                  data: formatter.string(from: DateHelper.shared.getDateFromString(grnData.grnDate, withFormat: "yyyy-MM-dd")!), isHidden: false)
            cell.setThirdRowData(heading: "Created Date : ",
                                 data: formatter.string(from: DateHelper.shared.decodeDashboardDate(dateString: grnData.createdAt!)), isHidden: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
