//
//  FormDispatchPlanningReportVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 26/12/22.
//

import UIKit

class FormDispatchPlanningReportVC: UIViewController {

    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    
    @IBOutlet weak var btnFromTentativeDate: UIButton!
    @IBOutlet weak var btnToTentativeDate: UIButton!
    
    @IBOutlet weak var btnFromConfirmDate: UIButton!
    @IBOutlet weak var btnToConfirmDate: UIButton!
    
    @IBOutlet weak var datePickerContentView: UIView!
    @IBOutlet weak var btnCancelPicker: UIButton!
    @IBOutlet weak var btnDonePicker: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    // MARK: - All Constants & Variables
    /// Selected From Tentative Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var fromTentativeDate: Date! = nil
    
    /// Selected To Tentative Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var toTentativeDate: Date! = nil
    
    /// Selected From Confirmation Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var fromConfDate: Date! = nil
    
    /// Selected To Confirmation Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var toConfDate: Date! = nil
    
    /// Determines if  Calendar is showing from Which Button
    private var isFromSectionCalendar: DateMode = .None
    
    /// DIfferent Types From Which DatePicker View is Called
    private enum DateMode {
        case FromTentative
        case ToTentative
        case FromConfirmation
        case ToConfirmation
        case None
    }
    
    private lazy var mainBlurr: UIVisualEffectView = {
        var blurrView = UIVisualEffectView()
        blurrView.frame = view.frame
        blurrView.alpha = 0.9
        return blurrView
    }()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.setPickerStyle_iOS14()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        [btnFromTentativeDate, btnToTentativeDate, btnFromConfirmDate, btnToConfirmDate, btnCancelPicker, btnDonePicker, btnSubmit].forEach { button in
            button?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
        datePickerContentView.layer.applyShadowAndRadius(shadowRadius: 1.5)
    }
    
    // MARK: - All Actions
    @IBAction func back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func from_date_buttons_Action(_ sender: UIButton) {
        isFromSectionCalendar = sender == btnFromTentativeDate ? .FromTentative : .FromConfirmation
        datePickerContentView.show_With_Popup_Effect()
        datePicker.setDate(Date(), animated: true)
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func to_date_buttons_Action(_ sender: UIButton) {
        isFromSectionCalendar = sender == btnToTentativeDate ? .ToTentative : .ToTentative
        datePickerContentView.show_With_Popup_Effect()
        datePicker.setDate(Date(), animated: true)
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func cancel_picker_button_Action(_ sender: UIButton) {
        datePickerContentView.dismiss_With_Popup_Effect()
        mainBlurr.hideBlurrEffect()
    }
    
    @IBAction func done_picker_button_Action(_ sender: UIButton) {
        datePickerContentView.dismiss_With_Popup_Effect()
        
        switch isFromSectionCalendar {
        case .FromTentative: fromTentativeDate = datePicker.date
        case .ToTentative: toTentativeDate = datePicker.date
        case .FromConfirmation: fromConfDate = datePicker.date
        case .ToConfirmation: toConfDate = datePicker.date
        case .None: break;
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainBlurr.hideBlurrEffect()
            
            switch self.isFromSectionCalendar {
            case .FromTentative: self.btnFromTentativeDate.setTitle(DateHelper.shared.getStringFrom(date: self.fromTentativeDate), for: .normal)
            case .ToTentative: self.btnToTentativeDate.setTitle(DateHelper.shared.getStringFrom(date: self.toTentativeDate), for: .normal)
            case .FromConfirmation: self.btnFromConfirmDate.setTitle(DateHelper.shared.getStringFrom(date: self.fromConfDate), for: .normal)
            case .ToConfirmation: self.btnToConfirmDate.setTitle(DateHelper.shared.getStringFrom(date: self.toConfDate), for: .normal)
            case .None: break
            }
        }
    }
    
    @IBAction func submit_button_Action(_ sender: UIButton) {
        view.endEditing(true)
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let param = ["td_date" : fromTentativeDate != nil ? API_parsing_dateString(date: fromTentativeDate) : "",
                     "td_to_date" : toTentativeDate != nil ? API_parsing_dateString(date: toTentativeDate) : "",
                     "C_date" : fromConfDate != nil ? API_parsing_dateString(date: fromConfDate) : "",
                     "C_to_date" : toConfDate != nil ? API_parsing_dateString(date: toConfDate) : ""]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.DISPATCH_PLANNING_REPORT.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false, senderView: btnSubmit)
        { [weak self] (isSuccess, response: Dispatch_Planning_Report_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response = response {
                if let mainData = response.data, mainData.count > 0 {    // DATA AVAILABLE
                    #warning("Data Implementation")
                    debugPrint("SUCCESS")
                    let vc = allReportStoryboard.instantiateViewController(withIdentifier: "DispatchPlanningReportVC") as! DispatchPlanningReportVC
                    vc.arrDispatchData = mainData
                    self.navigationController?.pushViewController(vc, animated: true)
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                } else {    // NO DATA AVAILABLE
                    appView.makeToast("NO DATA AVAILABLE!")
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                }
            } else if let errorMessage {
                debugPrint("Sales Report Error - \(errorMessage)")
                HapticFeedbackGenerator.notificationFeedback(type: .error)
            }
        }
    }
    
    // MARK: - All Helper Methods
}
