//
//  DispatchPlanningReportVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 26/12/22.
//

import UIKit
import PopupDialog

class DispatchPlanningReportVC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var btnExport: UIButton!
    
    // MARK: - All Constants & Variables
    var arrDispatchData = [Dispatch_Planning_Data]()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        settingThisVC()
    }
    
    // MARK: - All Helper Methods
    private func settingThisVC() {
        mainTableView.register(UINib(nibName: Dispatch_Planning_Cell.identifier, bundle: nil), forCellReuseIdentifier: Dispatch_Planning_Cell.identifier)
        mainTableView.setSectionPadding_iOS_15()
    }
    
    @objc private func View_Detail_Action(_ sender: UIButton) {
        let sectionData = arrDispatchData[sender.tag]
        
        let detailVC = allReportStoryboard.instantiateViewController(withIdentifier: "DispatchMoreDetailVC") as! DispatchMoreDetailVC
        detailVC.mainData = sectionData
        
        let pop = PopupDialog(viewController: detailVC, transitionStyle: .bounceUp, tapGestureDismissal: true, panGestureDismissal: true, completion: nil)
        pop.visibleBackground = true
        navigationController?.present(pop, animated: true)
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
    }
    
    // MARK: - All Actions
    @IBAction func Back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Export_Button_Action(_ sender: UIButton) {
        appView.makeToast("Upcoming Feature!")
    }
}

// MARK: - TableView Delegate Methods
extension DispatchPlanningReportVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrDispatchData.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == arrDispatchData.count-1 { return 0 }
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sep = UIView()
        sep.backgroundColor = .tertiarySystemGroupedBackground
        return sep
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // + 2 is for Header View, Total View & View Detail Button Rows
        guard let lineItems = arrDispatchData[section].preshipmentlineitem else { return 3 }
        return lineItems.count + 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row > (arrDispatchData[indexPath.section].preshipmentlineitem?.count ?? 0) {
            return getDynamicHeight(size: 35)
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let mainData = arrDispatchData[indexPath.section]
        let lineItemData = mainData.preshipmentlineitem
        let cell = tableView.dequeueReusableCell(withIdentifier: Dispatch_Planning_Cell.identifier, for: indexPath) as! Dispatch_Planning_Cell
        cell.separatorInset = .zero
        cell.selectionStyle = .none
        if row == 0 {
            cell.setDefaultData(isHeading: true)
            cell.separatorInset = UIEdgeInsets(top: 0, left: SCREEN_WIDTH, bottom: 0, right: 0) //To make last cell's separator insible
            return cell
        } else {
            if row <= lineItemData?.count ?? 0 {
                let data = lineItemData![row-1]
                
                var fullQuantity = data.quantity ?? "N/A"
                fullQuantity = mainData.unit == nil ? fullQuantity : (fullQuantity + " " + mainData.unit!)
                
                cell.setDefaultData(data1: data.preshipmentInvoiceNo, data2: data.materialDescription, data3: fullQuantity)
                return cell
            } else {
//                debugPrint("For section \(indexPath.section) Total View -- \(row == (lineItemData?.count ?? 0)+1)")
                cell.separatorInset = UIEdgeInsets(top: 0, left: SCREEN_WIDTH, bottom: 0, right: 0) //To make last cell's separator insible
                cell.lblTotal.text = arrDispatchData[indexPath.section].total.flatMap { String($0) }
                cell.ChangeView(isTotalView: row == (lineItemData?.count ?? 0)+1, section: indexPath.section)
                cell.btnViewDetail.addTarget(self, action: #selector(View_Detail_Action(_:)), for: .touchUpInside)
            }
        }
        return cell
    }
}
