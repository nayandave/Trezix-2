//
//  DispatchMoreDetailVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 07/01/23.
//

import UIKit

class DispatchMoreDetailVC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var headingView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var dataTableView: UITableView!
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    // MARK: - All Constants & Variable
    var mainData: Dispatch_Planning_Data! = nil
    
    private var arrHeadingStr = [String]()
    private var arrValue = [String]()
    
    private let ShowablePair: [String: String] = ["shipType": "Ship Type",
                                                  "buyer": "Buyer",
                                                  "shipToParty": "Ship To Party",
                                                  "shipNetWeight": "Ship. Net Wt",
                                                  "shipGrossWeight": "Ship. Gross Wt",
                                                  "noOfPallet": "No. of Pallet",
                                                  "loose": "Loose",
                                                  "stuffingInstruction": "Stuffing Instruction",
                                                  "cartingPoint": "Carting Point",
                                                  "blSpecificRemark": "B/L Specific Remark",
                                                  "remark": "Remark",
                                                  "containerNo": "Container No.",
                                                  "vehicalNo": "Vehicle No.",
                                                  "contactPersonName": "Contact Person Name",
                                                  "mobileNo": "Contact No."]
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let mirrorData = Mirror(reflecting: mainData!)
        for child in mirrorData.children {
            if let head = child.label, let finalHeading = ShowablePair[head] {
                if let strValue = child.value as? String {
                    arrValue.append(strValue)
                    arrHeadingStr.append(finalHeading)
                } else if let doubleValue = child.value as? Double {
                    arrValue.append(String(doubleValue))
                    arrHeadingStr.append(finalHeading)
                }
            }
        }
        dataTableView.setSectionPadding_iOS_15()
        dataTableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headingView.layer.masksToBounds = false
        dataTableView.layer.masksToBounds = false
        mainContentView.layer.borderWidth = 0.25
        mainContentView.layer.borderColor = UIColor(named: "ShadowLabelColor")!.cgColor
        mainContentView.layer.applyShadowAndRadius()
        mainContentView.layer.masksToBounds = true
    }
}

// MARK: - TableView Delegate Methods
extension DispatchMoreDetailVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHeadingStr.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Dispatch_More_Table_Cell", for: indexPath) as! Dispatch_More_Table_Cell
        cell.setHeading(heading: arrHeadingStr[indexPath.row])
        cell.setMainData(value: arrValue[indexPath.row])
        cell.separatorInset = UIEdgeInsets(top: 0, left: (indexPath.row == arrHeadingStr.count-1) ? SCREEN_WIDTH : 0, bottom: 0, right: 0)
        DispatchQueue.main.async { [weak self] in
            self?.tableHeight.constant = tableView.contentSize.height
        }
        return cell
    }
}

// MARK: - Dispatch More Detail Table Cell
class Dispatch_More_Table_Cell: UITableViewCell {
    @IBOutlet weak var shadowContentView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setHeading(heading: String) {
        lblHeading.text = heading
    }
    
    public func setMainData(value: String?) {
        lblDescription.text = value ?? "N/A"
    }
}
