//
//  FormOrderReportVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 29/07/22.
//

import UIKit

class FormOrderReportVC: UIViewController {
    
    // MARK: - ALL IBOUTLETS
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var textPlantName: UITextField!
    @IBOutlet weak var textFromPONumber: UITextField!
    @IBOutlet weak var textToPONumber: UITextField!
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    @IBOutlet weak var textVendor: UITextField!
    @IBOutlet weak var textCHA: UITextField!
    @IBOutlet weak var dropSelectMaterial: DropDown!
    @IBOutlet weak var dropSelectMaterialGroup: DropDown!
    @IBOutlet weak var dropSelectOrder: DropDown!
    @IBOutlet weak var btnCheckmarkGroupWise: UIButton!
    @IBOutlet weak var imageCheckUncheck: UIImageView!
    
    @IBOutlet weak var datePickerContentView: UIView!
    @IBOutlet weak var btnCancelPicker: UIButton!
    @IBOutlet weak var btnDonePicker: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    // MARK: - ALL CONSTANTS & VARIABLES
    
    ///*Determines if* `Group Wise Checkmark is selected or not`
    /// Default Value is `false`
    private var isCheckedGroupWise = false
    
    /// Selected From Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var fromDate: Date! = nil
    
    /// Selected To Date From Calendar
    ///  - Used in ``call_Order_Report_API()``
    private var toDate: Date! = nil
    
    /// Determines if  Calendar is showing from `From PO Number` or `To PO Number` Button
    private var isFromSectionCalendar = true
    
    /// ID of  selected Material from it's DropDown
    ///  - Used in ``call_Order_Report_API()``
    private var selMaterialID: Int! = nil
    
    /// ID of selected Material Group from it's DropDown
    ///  - Used in ``call_Order_Report_API()``
    private var selMaterialGroupID: String! = nil
    
    /// String Array of `Selected Order Dropdown` options
    /// - Default must be `ALL` which is first String element of this Array
    private let arrSelectedOrderDropdown = ["ALL", "PENDING"]
    
    /// ID of selected Order from it's DropDown
    ///  - Used in ``call_Order_Report_API()``
    private var selOrderType: String! = nil
    
    /// Array of Material Data fetched from ``APIConstants/GET_MATERIAL_DROPDOWN``
    private var arrMaterialData = [Material_Data]()
    
    /// Array of Material Group Data fetched from ``APIConstants/GET_MATERIAL_GROUP_DROPDOWN``
    private var arrMaterialGroupData = [Material_Group_Data]()
    
    private lazy var mainBlurr: UIVisualEffectView = {
        var blurrView = UIVisualEffectView()
        blurrView.frame = view.frame
        blurrView.alpha = 0.9
        return blurrView
    }()
}

// MARK: - ALL METHODS
extension FormOrderReportVC {
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let touch = touches.first {
            if !datePickerContentView.bounds.contains(touch.location(in: datePickerContentView)) {
                datePickerContentView.dismiss_With_Popup_Effect()
                self.mainBlurr.hideBlurrEffect()
            }
        }
        self.view.endEditing(true)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.viewDidLayoutSubviews()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        [btnFromDate, btnToDate, btnSubmit, btnCancelPicker, btnDonePicker].forEach { button in
            button?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
        datePickerContentView.layer.applyShadowAndRadius(shadowRadius: 1.5)
        [textPlantName, textFromPONumber, textToPONumber, textVendor, textCHA, dropSelectMaterial, dropSelectMaterialGroup, dropSelectOrder].forEach { textField in
            textField?.layer.borderWidth = 0.5
            textField?.layer.borderColor = UIColor.systemBackground.cgColor
            textField?.layer.applyShadowAndRadius(shadowRadius: 1)
        }
    }

    // MARK: - All Helper Methods
    
    /// `Only Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable { callAllInitMethods() }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    ///  - Parameter afterNetworkRefresh: Boolean value that indicates if method if called first time or called after network refresh (after being offline & online)
    private func callAllInitMethods(_ afterNetworkRefresh: Bool = false) {
        if !afterNetworkRefresh {
            if let shortcut = sceneDelegate.savedShortCutItem {
                if shortcut.type == AllQuickShortcutTypes.report.rawValue {
                    sideMenuController?.hideMenu()
                    sceneDelegate.savedShortCutItem = nil
                }
            }
            set_Up_All_Dropdowns()
            datePicker.setPickerStyle_iOS14()
            let apiDispatch = DispatchQueue.global(qos: .utility)
            apiDispatch.async { [weak self] in
                self?.get_Material_Dropdown_Data()
                self?.get_Material_Group_Dropdown_Data()
            }
        }
    }

    // MARK: - All UI Changes Methods
    
    private func set_Up_All_Dropdowns() {
        selOrderType = arrSelectedOrderDropdown[0]
        [dropSelectMaterial, dropSelectMaterialGroup, dropSelectOrder].forEach { [weak self] dropDown in
            dropDown?.arrowColor = .ourAppThemeColor
            dropDown?.hideOptionsWhenSelect = true
            dropDown?.checkMarkEnabled = false
            dropDown?.isSearchEnable = true
            dropDown?.handleKeyboard = true
            dropDown?.selectedRowColor = .ourAppThemeColor
            
            if dropDown == dropSelectOrder {
                dropDown?.listHeight = getDynamicHeight(size: 150)
                dropDown?.optionArray = arrSelectedOrderDropdown
                dropDown?.selectedIndex = 0
                selOrderType = arrSelectedOrderDropdown[0]
                dropDown?.text = arrSelectedOrderDropdown[0]  //Default Selection
            } else {
                let topHeight = dropSelectMaterial.distanceFromSafeArea(distanceType: .upperDistanceFromSafeArea)
                let bottomHeight = dropSelectMaterial.distanceFromSafeArea(distanceType: .lowerDistanceFromSafeArea)
                dropDown?.listHeight = topHeight > bottomHeight ? bottomHeight : topHeight
            }
            
            dropDown?.didSelect(completion: { [weak self] selectedText, index, id in
                guard let strongSelf = self else { return }
                HapticFeedbackGenerator.simpleFeedback(type: .light)
                dropDown?.text = selectedText
                
                if dropDown == strongSelf.dropSelectMaterial {
                    strongSelf.selMaterialID = index == 0 ? nil : strongSelf.arrMaterialData[index].id
                } else if dropDown == strongSelf.dropSelectMaterialGroup {
                    strongSelf.selMaterialGroupID = index == 0 ? nil : selectedText
                } else {
                    strongSelf.selOrderType = selectedText
                }
            })
        }
    }
    
    // MARK: - API Calling Methods
    private func call_Order_Report_API() {
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
        let param: [String: Any] = ["plant": textPlantName.text ?? "",
                                    "po_no_1": textFromPONumber.text ?? "",
                                    "po_no_2": textToPONumber.text ?? "",
                                    "date_1": fromDate != nil ? API_parsing_dateString(date: fromDate) : "",
                                    "date_2": toDate != nil ? API_parsing_dateString(date: toDate) : "",
                                    "ncha": textCHA.text ?? "",
                                    "vendor": textVendor.text ?? "",
                                    "material": selMaterialID == nil ? "" : (dropSelectMaterial.text ?? ""),
                                    "material_grp": selMaterialGroupID == nil ? "" : (dropSelectMaterialGroup.text ?? ""),
                                    "order": dropSelectOrder.text ?? "",
                                    "group": isCheckedGroupWise ? 1 : 0]
        
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.GET_ORDER_REPORT.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false, senderView: btnSubmit)
        { [weak self] (isSuccess, response: OrderReportReponse?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response = response {
                if let mainData = response.data, mainData.count > 0 {       // DATA AVAILABLE
                    let vc = allReportStoryboard.instantiateViewController(withIdentifier: "OrderReportVC") as! OrderReportVC
                    vc.arrReportData = mainData
                    self.navigationController?.pushViewController(vc, animated: true)
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                } else {        // NO DATA AVAILABLE
                    appView.makeToast("NO DATA AVAILABLE!")
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                }
            } else if let errorMessage {
                debugPrint("Order Report Error - \(errorMessage)")
                HapticFeedbackGenerator.notificationFeedback(type: .error)
            }
        }
    }
    
    /// *Fetechs Dropdown Data of* `Select Material`*Dropdown*
    private func get_Material_Dropdown_Data() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_MATERIAL_DROPDOWN.fullPath(),
                                            headers: [default_app_header],
                                            showHud: false)
        { [weak self] (isSuccess, response: Material_Dropdown_Modal?, errorMessage) in
            Loader.hideLoader()
            guard let self = self else { return }
            if isSuccess  {
                if let response = response {
                    if let data = response.data {
                        self.arrMaterialData = data
                        var allMaterialNames = data.compactMap { $0.materialDescription }
                        allMaterialNames.insert("Select Material", at: 0)
                        
                        self.dropSelectMaterial.selectedIndex = 0
                        self.dropSelectMaterial.text = allMaterialNames[0]
                        self.dropSelectMaterial.optionArray = allMaterialNames
                    } else {
                        // No Material Data Available
                    }
                }
            } else if let errorMessage = errorMessage {
                debugPrint("Error Fetching Material Dropdown Data because of \(errorMessage)!")
                appView.makeToast("Error Fetching Material Dropdown Data!")
            }
        }
    }
    
    /// *Fetechs Dropdown Data of* `Select Material Group` *Dropdown*
    private func get_Material_Group_Dropdown_Data() {
        NetworkClient.encodedNetworkRequest(method: .get,
                                            apiName: APIConstants.GET_MATERIAL_GROUP_DROPDOWN.fullPath(),
                                            headers: [default_app_header],
                                            showHud: false)
        { [weak self] (isSuccess, response: Material_Group_Dropdown_Modal?, errorMessage) in
            Loader.hideLoader()
            guard let self = self else { return }
            if isSuccess, let response = response {
                if let data = response.data {
                    self.arrMaterialGroupData = data
                    var allMaterialGroupNames = ["Select Material Group"]
                    for group in data {
                        let sep = group.value.components(separatedBy: ",")
                        allMaterialGroupNames.append(contentsOf: sep)
                    }
                    self.dropSelectMaterialGroup.selectedIndex = 0
                    self.dropSelectMaterialGroup.text = allMaterialGroupNames[0]
                    self.dropSelectMaterialGroup.optionArray = allMaterialGroupNames
                    Loader.hideLoader()
                } else {
                    // No Data Available
                }
            } else if let errorMessage = errorMessage {
                debugPrint("Error Fetching Material Dropdown Data because of \(errorMessage)!")
                appView.makeToast("Error Fetching Material Dropdown Data!")
            }
        }
    }
}



// MARK: - ALL ACTIONS
extension FormOrderReportVC {
    @IBAction func back_Action(_ sender: UIButton) {
        if navigationController != nil {
            navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func open_from_calendar(_ sender: Any) {
        self.view.endEditing(true)
        isFromSectionCalendar = true
        datePickerContentView.show_With_Popup_Effect()
//        DispatchQueue.main.async { [weak self] in
            mainBlurr.showBlurrAnimation(behind: datePickerContentView)
//        }
    }
    
    @IBAction func open_to_calendar(_ sender: Any) {
        self.view.endEditing(true)
        isFromSectionCalendar = false
//        blurrView.isHidden = false
        datePickerContentView.show_With_Popup_Effect()
        datePicker.setDate(Date(), animated: true)
        mainBlurr.showBlurrAnimation(behind: datePickerContentView)
    }
    
    @IBAction func Cancel_Picker_Action(_ sender: UIButton) {
//        blurrView.isHidden = true
        datePickerContentView.dismiss_With_Popup_Effect()
        mainBlurr.hideBlurrEffect()
    }
    
    @IBAction func Done_Picker_Action(_ sender: UIButton) {
        datePickerContentView.dismiss_With_Popup_Effect()
        
        isFromSectionCalendar ? (fromDate = datePicker.date) : (toDate = datePicker.date)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainBlurr.hideBlurrEffect()
            self.isFromSectionCalendar ? (self.btnFromDate.setTitle(DateHelper.shared.getStringFrom(date: self.fromDate), for: .normal)) : (self.btnToDate.setTitle(DateHelper.shared.getStringFrom(date: self.toDate), for: .normal))
        }
    }
    
    @IBAction func check_uncheck_group_Action(_ sender: UIButton) {
        isCheckedGroupWise = !isCheckedGroupWise
        imageCheckUncheck.image = isCheckedGroupWise ? UIImage(systemName: "checkmark.circle") :  UIImage(systemName: "circle")
        HapticFeedbackGenerator.simpleFeedback(type: .medium)
    }
    
    @IBAction func submit_Action(_ sender: Any) {
        view.endEditing(true)
        call_Order_Report_API()
    }
}


extension UIVisualEffectView {
    public func showBlurrAnimation(effect: UIBlurEffect.Style = .systemUltraThinMaterial, behind: UIView) {
        var effect = effect
        if #unavailable(iOS 14) {
            effect = .regular
        }
        
        behind.superview?.insertSubview(self, belowSubview: behind)
        UIView.animate(withDuration: 0.5) {
            self.effect = UIBlurEffect(style: effect)
        }
    }
    
    public func hideBlurrEffect() {
        UIView.animate(withDuration: 0.35) {
            self.effect = nil
        } completion: { _ in
            self.removeFromSuperview()
        }
    }
}
