//
//  OrderReportVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 02/08/22.
//

import UIKit

class OrderReportVC: UIViewController {
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    // Heading Section
    @IBOutlet weak var lblFirstSection: UILabel!
    @IBOutlet weak var imageFirstSecAscending: UIImageView!
    @IBOutlet weak var imageFirstSecDescending: UIImageView!
    
    @IBOutlet weak var lblSecondSection: UILabel!
    @IBOutlet weak var imageSecondSecAscending: UIImageView!
    @IBOutlet weak var imageSecondSecDescending: UIImageView!
    
    @IBOutlet weak var lblThirdSection: UILabel!
    @IBOutlet weak var imageThirdSecAscending: UIImageView!
    @IBOutlet weak var imageThirdSecDescending: UIImageView!
    
    @IBOutlet weak var btnFirstSecSort: UIButton!
    @IBOutlet weak var btnSecondSecSort: UIButton!
    @IBOutlet weak var btnThirdSecSort: UIButton!
    
    @IBOutlet weak var reportTable: UITableView!
    @IBOutlet weak var btnExportPDF: UIButton!
    
    // MARK: - All Variables & Constants
    private var currentSortingType: CurrentSorting! = .firstSecAscending
    
    private var isPONumberAscending = true
    private var isPODateAscending = true
    private var isVendorAscending = true
    
    private var arrShowableSections = [Int]()
    
    public var arrReportData = [mainOrderReportData]()
    
    var spreadView: SpreadsheetView! = nil
    
    var arr = CGFloat()
    
    private let firstRow = ["Sr No", "PO No", "PO Date", "Company Plant", "Vendor Name", "Vendor Group Name", "Vendor Group Description", "Material Code", "Material Name", "PO Quantity", "Unit of Measurement", "Rate", "Currency", "FC Value", "INR Value", "Shipped Quantity", "Pending Quantity", "Packing Instruction", "Material Group", "Payment Terms", "Incoterm 1"]
    
    private let columnWidth = [0.0175, 0.05, 0.045, 0.05, 0.065, 0.05, 0.05, 0.05, 0.07, 0.045, 0.06, 0.035, 0.04, 0.05, 0.055, 0.045, 0.045, 0.05, 0.05, 0.0425, 0.035]
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    // MARK: - All Actions
    @IBAction func Navigate_Back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Change_First_Section_Sorting(_ sender: UIButton) {
        Loader.showLoader()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }
            self.changeSorting(type: self.isPONumberAscending ? .firstSecAscending : .firstSecDescending)
            self.isPONumberAscending = !self.isPONumberAscending
            self.isPODateAscending = false
            self.isVendorAscending = false
        }
    }
    
    @IBAction func Change_Second_Section_Sorting(_ sender: UIButton) {
        Loader.showLoader()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }
            self.changeSorting(type: self.isPODateAscending ? .secondSecAscending : .secondSecDescending)
            self.isPODateAscending = !self.isPODateAscending
            self.isPONumberAscending = false
            self.isVendorAscending = false
        }
    }
    
    @IBAction func Change_Third_Section_Sorting(_ sender: UIButton) {
        Loader.showLoader()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }
            self.changeSorting(type: self.isVendorAscending ? .thirdSecAscending : .thirdSecDescending)
            self.isVendorAscending = !self.isVendorAscending
            self.isPONumberAscending = false
            self.isPODateAscending = false
        }
    }
    
    @IBAction func action_exportAsPDF(_ sender: UIButton) {
//        getPDFFromSpreadsheet()
        Loader.showLoader()
        DispatchQueue.main.async {
            self.tryingPDF()
        }
    }
}

// MARK: - All Helper Methods
extension OrderReportVC {
    /// `Only Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable { callAllInitMethods() }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(true)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    ///  - Parameter afterNetworkRefresh: Boolean value that indicates if method if called first time or called after network refresh (after being offline & online)
    private func callAllInitMethods(_ afterNetworkRefresh: Bool = false) {
        if !afterNetworkRefresh {
            settingUpPage()
            initSpreadSheet()
        }
        changeSorting(type: .firstSecAscending)
    }
    
    private func settingUpPage() {
//        reportTable.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: reportTable.frame.width, height: getEdgeNotchHeight(of: .bottomNotch)))
        reportTable.register(UINib(nibName: POReportExtraInfoCell.identifier, bundle: nil), forCellReuseIdentifier: POReportExtraInfoCell.identifier)
        if #available(iOS 15, *) {
            reportTable.sectionHeaderTopPadding = 0
        }
        reportTable.tableFooterView = UIView()
    }
    
    private func initSpreadSheet() {
        if spreadView == nil {
            spreadView = SpreadsheetView(frame: CGRect(x: self.view.frame.minX, y: self.view.frame.minY, width: self.view.frame.width, height: self.view.frame.height))
//            self.view.addSubview(spreadView)
            
//            spreadView.translatesAutoresizingMaskIntoConstraints = false
//            self.view.insertSubview(spreadView, aboveSubview: btnDownload)
//            NSLayoutConstraint(item: spreadView, attribute: .bottom, relatedBy: .equal, toItem: btnDownload, attribute: .top, multiplier: 1, constant: 0).isActive = true
//            NSLayoutConstraint(item: spreadView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 5).isActive = true
//            NSLayoutConstraint(item: spreadView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 5).isActive = true
//            NSLayoutConstraint(item: spreadView, attribute: .top, relatedBy: .equal, toItem: bgView, attribute: .bottom, multiplier: 1, constant: 5).isActive = true
            
            spreadView.delegate = self
            spreadView.dataSource = self
            spreadView.register(MyLabelCell.self, forCellWithReuseIdentifier: MyLabelCell.identifier)
            spreadView.showsHorizontalScrollIndicator = false
            spreadView.showsVerticalScrollIndicator = false
            spreadView.bounces = false
//            spreadView.gridStyle = .solid(width: 0.5, color: .darkGray)
            spreadView.intercellSpacing = .zero
            spreadView.gridStyle = .none
            spreadView.backgroundColor = .white
            spreadView.reloadData()
        }
    }
    
    private func tryingPDF() {
        let data = NSMutableData()
        
        let pageHeight: CGFloat = 11 * 72
        let pageWidth: CGFloat = 17 * 72
        
        let frame = CGRect(origin: .zero, size: CGSize(width: pageWidth, height: pageHeight))
        
        UIGraphicsBeginPDFContextToData(data, frame, nil)
        
        // Main Container View
        let tempContainerView = UIView(frame: CGRect(x: 0, y: 0, width: pageWidth, height: pageHeight))
        
        // HeaderView With Label
        let headerHeight = getDynamicHeight(size: 50) + 20
        let headingView = PDFHeadingView()
        headingView.backgroundColor = .clear
        headingView.frame = CGRect(x: 0, y: 10, width: pageWidth, height: getDynamicHeight(size: 50))
        headingView.lblHeading.text = "Pending Order - Trezix"
        headingView.layoutIfNeeded()
        
        // Temp SpreadSheet View
        let tempSpreadView: SpreadsheetView = self.spreadView
        tempSpreadView.contentOffset = CGPoint.zero
        tempSpreadView.backgroundColor = .clear
        
        // Row Count Calculation
        var arrPageHeight: [CGFloat] = [0]
        var allRowMaxHeight = [CGFloat]()
        var allPageLastRowHeight: [CGFloat] = [0]
        
        let spreadSheetWidth: CGFloat = (17 * 72) - 20
        
        for row in 0..<arrReportData.count {
            let mirror = Mirror(reflecting: arrReportData[row])
            var i = 0
            var maxHeight: CGFloat = 0
            for child in mirror.children {
                if let text = child.value as? String {
                    let label = UILabel(frame: CGRectMake(0, 0, spreadSheetWidth*columnWidth[i], getDynamicHeight(size: 40)))
                    label.DynamicHeight = true
                    label.numberOfLines = 0
                    label.minimumScaleFactor = 0.5
                    label.lineBreakMode = .byWordWrapping
                    label.font = .systemFont(ofSize: getDynamicHeight(size: 10))
                    label.text = text
                    label.sizeToFit()
                    maxHeight = label.frame.height > maxHeight ? label.frame.height : maxHeight
                }
                i += 1
            }
            allRowMaxHeight.append(maxHeight)
        }
        
        var pageCounter = 0
        var rowCOunt = 0
        for rowHeight in allRowMaxHeight {
            var availableHeight: CGFloat = 0
            if pageCounter == 0 {   //First Page Height Calculation
                availableHeight = pageHeight - headerHeight - 20
            } else {    //Other Page Height Calculation
                availableHeight = pageHeight - getDynamicHeight(size: 50) - 40
            }
            
            if arrPageHeight.count == pageCounter {
                arrPageHeight.append(0)
                allPageLastRowHeight.append(0)
            }
            
            print("On Page \(pageCounter+1), Available Page Height \(availableHeight), currHeight = \(arrPageHeight[pageCounter]), remaining page height = \(availableHeight-arrPageHeight[pageCounter])\n\n\n")
            
            if (arrPageHeight[pageCounter] + rowHeight) <= availableHeight {
                arrPageHeight[pageCounter] += rowHeight
                allPageLastRowHeight[pageCounter] = rowHeight
                rowCOunt += 1
//                debugPrint("Current Page = \(pageCounter+1) && Total Height Array = \(arrPageHeight)")
            } else {
                debugPrint("On Page \(pageCounter+1) == Row Count \(rowCOunt)")
                rowCOunt = 0
                pageCounter += 1
            }
        }
        
        tempContainerView.addSubview(tempSpreadView)
        tempContainerView.addSubview(headingView)
        
        var currentPage = 0
        var scrollHeight: CGFloat = 0
        for page in arrPageHeight {
            UIGraphicsBeginPDFPage()
            tempSpreadView.setContentOffset(CGPoint(x: 0, y: currentPage == 0 ? 0 : scrollHeight), animated: false)
            
            print("Page--\(page)")
            print("pageHeight--\(pageHeight)")
            print("Header Height--\(headerHeight)")
            print("Last Row Height--\(allPageLastRowHeight[currentPage])\n\n")
            
            tempSpreadView.frame = CGRect(x: 10, y: currentPage == 0 ? headerHeight : 20, width: self.spreadView.contentSize.width, height: page)
            tempContainerView.layer.render(in: UIGraphicsGetCurrentContext()!)
            if currentPage == 0 {
                headingView.removeFromSuperview()
            }
            scrollHeight += (page-getDynamicHeight(size: 50))
            currentPage += 1
        }
        UIGraphicsEndPDFContext()
        let path = allReportPath.appendingPathComponent("Pending Order - Trezix.pdf")
        
        do {
            try data.write(to: path, options: .atomic)
            debugPrint("PDF Created Successfully")
            debugPrint("With Total == \(arrReportData.count) rows..")
//            appView.makeToast("PDF Created Successfully!\nand Saved in Files")
            
            let openPath = allReportPath.absoluteString.replacingOccurrences(of: "file://", with: "shareddocuments://")
            let url = URL(string: openPath)!
            Loader.hideLoader()
            PDFCompleteAlert(url: url)
        } catch let ree {
            debugPrint("Somwthing Went Wrong Creating PDF = \(ree.localizedDescription)")
            appView.makeToast("Something Went Wrong!\nTry Again Later!")
            Loader.hideLoader()
        }
    }
    
    private func PDFCompleteAlert(url: URL) {
        let alert = UIAlertController(title: "Success", message: "Order Report is successfully exported as PDF!", preferredStyle: .alert)
        
        let cancelBtn = UIAlertAction(title: "Cancel", style: .cancel)
        let openFile = UIAlertAction(title: "Open File", style: .default) { _ in
            UIApplication.shared.open(url)
            self.dismiss(animated: true)
        }
        
        alert.addAction(cancelBtn)
        alert.addAction(openFile)
        
        present(alert, animated: true)
    }
    
    func getPDFFromSpreadsheet() {
        let data = NSMutableData()
        
        let fixedHeight: CGFloat = 11 * 72
        let fixedWidth: CGFloat = 17 * 72

//        let estiHeight = getDynamicHeight(size: 75) + (30*getDynamicHeight(size: 50)) + (32)
//        debugPrint("ESTIMATED HEIGHT IS --------- \(estiHeight)")
//        UIGraphicsBeginImageContextWithOptions(self.spreadView.contentSize, false, UIScreen.main.scale)
        let frame = CGRect(origin: .zero, size: CGSize(width: fixedWidth, height: fixedHeight))
        
        UIGraphicsBeginPDFContextToData(data, frame, nil)
        UIGraphicsBeginPDFPage()
        
        let tempSpreadView: SpreadsheetView = self.spreadView
        
        // reset offset to top left point
        tempSpreadView.contentOffset = CGPoint.zero
        // set frame to content size
        let headerHeight = getDynamicHeight(size: 50) + 20
//        tempSpreadView.frame = CGRect(x: 10, y: headerHeight, width: self.spreadView.contentSize.width, height: self.spreadView.contentSize.height)
        // remove background
        tempSpreadView.backgroundColor = UIColor.clear
        
        // make temp view with scroll view content size
        // a workaround for issue when image on ipad was drawn incorrectly
        let tempView = UIView(frame: CGRect(x: 0, y: 0, width: self.spreadView.contentSize.width, height: fixedHeight))
        
        let headingView = PDFHeadingView()
        headingView.frame = CGRect(x: 0, y: 10, width: tempView.bounds.width, height: getDynamicHeight(size: 50))
        headingView.lblHeading.text = "Pending Order - Trezix"
        headingView.layoutIfNeeded()
        
        
        tempView.addSubview(headingView)
        tempView.addSubview(tempSpreadView)
        
        var pageHeight: CGFloat = 0
        
        while pageHeight < self.spreadView.contentSize.height {
            tempSpreadView.setContentOffset(CGPoint(x: 0, y: pageHeight), animated: false)
            tempView.layer.render(in: UIGraphicsGetCurrentContext()!)
            if pageHeight == 0 {
                headingView.removeFromSuperview()
                
                let tempHeight = fixedHeight - headerHeight - getDynamicHeight(size: 50)
                
//                tempSpreadView.frame = CGRect(x: 10, y: 0, width: self.spreadView.contentSize.width, height: tempHeight)
                
                let arr = spreadView.visibleCells.filter({ ($0.frame.maxY+25) < tempHeight })
                if arr.count > 0 {
                    pageHeight = arr.last?.frame.maxY ?? tempHeight
                }
//                pageHeight += fixedHeight - headerHeight - getDynamicHeight(size: 50)
//                pageHeight += (estiHeight) - getDynamicHeight(size: 75) - getDynamicHeight(size: 50) - 2
            } else {
                let tempHeight = fixedHeight - getDynamicHeight(size: 50)
//                tempSpreadView.frame = CGRect(x: 10, y: 0, width: self.spreadView.contentSize.width, height: tempHeight)
                let arr = spreadView.visibleCells.filter({ ($0.frame.maxY+25) < (pageHeight+tempHeight) && ($0.frame.maxY+25) > pageHeight })
                if arr.count > 0 {
                    pageHeight = arr.last?.frame.maxY ?? tempHeight
                } else {
//                    pageHeight += tempHeight
//                    return
                    break
                }
//                pageHeight += fixedHeight - getDynamicHeight(size: 50)
//                pageHeight += (estiHeight) - getDynamicHeight(size: 75) - 2
            }
            debugPrint("NEw PAGE HEIGHT = \(pageHeight)")
            if pageHeight < self.spreadView.contentSize.height {
                UIGraphicsBeginPDFPage()
            }
        }
        UIGraphicsEndPDFContext()
        // and return everything back
        tempView.subviews[0].removeFromSuperview()
        tempView.removeFromSuperview()

        self.spreadView = nil
        self.initSpreadSheet()
        
//        debugPrint("SUBVIEWs===\(self.view.subviews)....isExists-- \(self.view.subviews.contains(self.spreadView))")
        
        let path = allReportPath.appendingPathComponent("Pending Order - Trezix.pdf")
        
        do {
            try data.write(to: path, options: .atomic)
            debugPrint("PDF Created Successfully")
            debugPrint("With Total == \(arrReportData.count) rows..")
            appView.makeToast("PDF Created Successfully!\nand Saved in Files")
            
            let openPath = allReportPath.absoluteString.replacingOccurrences(of: "file://", with: "shareddocuments://")
            let url = URL(string: openPath)!

//            UIApplication.shared.open(url)
        } catch let ree {
            debugPrint("Somwthing Went Wrong Creating PDF = \(ree.localizedDescription)")
            appView.makeToast("Something Went Wrong!\nTry Again Later!")
        }
        Loader.hideLoader()
    }
    
    private func changeSorting(type: CurrentSorting) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let arrAscendingImages = [imageFirstSecAscending, imageSecondSecAscending, imageThirdSecAscending]
        let arrDescendingImages = [imageFirstSecDescending, imageSecondSecDescending, imageThirdSecDescending]
        applyFilterAndRefresh(filter: type)
        DispatchQueue.main.async {
            switch type {
            case .firstSecAscending:
                for i in 0...2 {
                    arrAscendingImages[i]?.image = i == 0 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                    arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
                }
            case .firstSecDescending:
                for i in 0...2 {
                    arrDescendingImages[i]?.image = i == 0 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                    arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
                }
            case .secondSecAscending:
                for i in 0...2 {
                    arrAscendingImages[i]?.image = i == 1 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                    arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
                }
            case .secondSecDescending:
                for i in 0...2 {
                    arrDescendingImages[i]?.image = i == 1 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                    arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
                }
            case .thirdSecAscending:
                for i in 0...2 {
                    arrAscendingImages[i]?.image = i == 2 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                    arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
                }
            case .thirdSecDescending:
                for i in 0...2 {
                    arrDescendingImages[i]?.image = i == 2 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                    arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
                }
            }
        }
    }
    
    #warning("Find Solution to process sorting faster.")
    private func applyFilterAndRefresh(filter: CurrentSorting) {
        switch filter {
        case .firstSecAscending:
            self.arrReportData = self.arrReportData.sorted { $0.poNo.localizedCompare($1.poNo) == .orderedAscending }
        case .secondSecAscending:
            self.arrReportData = self.arrReportData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.createdAt) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.createdAt) else { return false }
                return date1.compare(date2) == .orderedAscending
            })
        case .thirdSecAscending:
            self.arrReportData = self.arrReportData.sorted { $0.vendorName.localizedCompare($1.vendorName) == .orderedAscending }
        case .firstSecDescending:
            self.arrReportData = self.arrReportData.sorted { $0.poNo.localizedCompare($1.poNo) == .orderedDescending }
        case .secondSecDescending:
            self.arrReportData = self.arrReportData.sorted(by: { first, second in
                guard let date1 = DateHelper.shared.getDateFromString(first.createdAt) else { return false }
                guard let date2 = DateHelper.shared.getDateFromString(second.createdAt) else { return false }
                return date1.compare(date2) == .orderedDescending
            })
        case .thirdSecDescending:
            self.arrReportData = self.arrReportData.sorted { $0.vendorName.localizedCompare($1.vendorName) == .orderedDescending }
        }
        DispatchQueue.main.async {  [weak self] in
            Loader.hideLoader()
            self?.reportTable.reloadData()
        }
    }
    
    private func stringToDate(_ stringDate: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/mm/YYYY"
        return formatter.date(from: stringDate)!
    }
    
    @objc private func tappedOnSection(_ tap: UITapGestureRecognizer) {
        guard let tappedView = tap.view as? ThreeColumnRowView else { appView.makeToast("Unrecognised Touch"); return }
        let currentIndex = tappedView.currentRow
        if arrShowableSections.contains(currentIndex) {
            arrShowableSections = arrShowableSections.filter({ $0 != currentIndex })
        } else {
            arrShowableSections.append(currentIndex)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.reportTable.beginUpdates()
            self.reportTable.reloadSections(IndexSet(integer: currentIndex), with: .automatic)
            self.reportTable.endUpdates()
//            if self.arrShowableSections.contains(currentIndex) {
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.75, execute: { [weak self] in
//                    self?.reportTable.scrollToRow(at: IndexPath(row: 0, section: currentIndex), at: .top, animated: false)
//                })
//            }
        }
    }
}

// MARK: - TableView Delegate Methods
extension OrderReportVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrReportData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getDynamicHeight(size: 50)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0 //getDynamicHeight(size: 1)
    }

//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let separator = UIView(frame: CGRect(x: 10, y: 0, width: tableView.frame.width-20, height: 1))
//        separator.backgroundColor = .secondaryLabel
//        return separator
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ThreeColumnRowView()
        headerView.isViewHeadingView(withSeparator: true, tag: section)
        let data = arrReportData[section]
        headerView.setData(firstLabel: data.poNo, secondLabel: data.createdAt, thirdLabel: data.vendorName, threeColumnView: true)
        headerView.isSelected(selected: arrShowableSections.contains(section), lastCell: section+1 == arrReportData.count)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnSection(_:)))
        headerView.addGestureRecognizer(tapGesture)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrShowableSections.contains(section) ? 18 : 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
        cell.poData = arrReportData[indexPath.section]
        cell.setPOReportInfo(forRow: indexPath.row)
        cell.isUserInteractionEnabled = false
        return cell
    }
}

//extension OrderReportVC: UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView == reportTable {
//            return arrReportData.count
//        } else {
//            return 15
//        }
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == reportTable {
//            return UITableView.automaticDimension
//        } else {
//            return getDynamicHeight(size: 40)
//        }
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if tableView == reportTable {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "POReportMainCell", for: indexPath) as! POReportMainCell
//            let showSubtable = arrShowableSections.contains(indexPath.row)
//            cell.bottomSeparatorView.isHidden = indexPath.row+1 != arrReportData.count
//            if showSubtable {
//                cell.extraTableHeight.constant = 15 * getDynamicHeight(size: 40)
//                DispatchQueue.main.async {
//                    cell.setExtraTableDatasourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
//                    if indexPath.row+1 == self.arrReportData.count {
//                        tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
//                    }
//                }
//            } else {
//                cell.extraTableHeight.constant = 0
//            }
//            let data = arrReportData[indexPath.row]
//            cell.fillCellInfo(po: data.poNo, date: data.createdAt, vendor: data.vendorName, showExtraInfo: showSubtable)
//            return cell
//        } else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
//            cell.poData = arrReportData[tableView.tag]
//            cell.setPOReportInfo(forRow: indexPath.row)
//            return cell
//        }
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        if tableView == reportTable {
//            if arrShowableSections.contains(indexPath.row) {
//                arrShowableSections.remove(at: arrShowableSections.firstIndex(of: indexPath.row)!)
//                 tableView.reloadRows(at: [indexPath], with: .bottom)
//            } else {
//                arrShowableSections.append(indexPath.row)
//                tableView.reloadRows(at: [indexPath], with: .top)
//                if indexPath.row+1 == arrReportData.count {
//                    tableView.invalidateIntrinsicContentSize()
//                    tableView.layoutIfNeeded()
//                }
//            }
//        }
//    }
//}

extension OrderReportVC: SpreadsheetViewDelegate, SpreadsheetViewDataSource {
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return firstRow.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return arrReportData.count+1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        let spreadWidth: CGFloat = (17 * 72)-20
        return spreadWidth * columnWidth[column]
//        switch column {
//        case 0: return spreadWidth*0.0175
//        case 1: return spreadWidth*0.05
//        case 2: return spreadWidth*0.05
//        case 3: return spreadWidth*0.05
//        case 4: return spreadWidth*0.075
//        case 5: return spreadWidth*0.05
//        case 6: return spreadWidth*0.05
//        case 7: return spreadWidth*0.05
//        case 8: return spreadWidth*0.075
//        case 9: return spreadWidth*0.075
//        case 10: return spreadWidth*0.05
//        case 11: return spreadWidth*0.05
//        case 12: return spreadWidth*0.05
//        case 13: return spreadWidth*0.075
//        case 14: return spreadWidth*0.04
//        case 15: return spreadWidth*0.04
//        case 16: return spreadWidth*0.05
//        case 17: return spreadWidth*0.035
//        case 18: return spreadWidth*0.04
//        case 19: return spreadWidth*0.0275
//        default:
//            return 10
//        }
    }

    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if row == 0 {
            return getDynamicHeight(size: 50)
        } else {
            
            var maxHeight: CGFloat = 0
            
            let wi: CGFloat = (17 * 72)-20
            let dd = Mirror(reflecting: arrReportData[row-1])
            
            var i = 0
            for chil in dd.children {
                if let text = chil.value as? String {
                    let label = UILabel(frame: CGRectMake(0, 0, wi*columnWidth[i], getDynamicHeight(size: 40)))
                    label.numberOfLines = 0
                    label.minimumScaleFactor = 0.5
                    label.lineBreakMode = .byTruncatingTail
                    label.font = .systemFont(ofSize: getDynamicHeight(size: 10))
                    label.text = text
                    label.sizeToFit()
                    
//                  print("Height for Row = \(row) & column = \(String(describing: chil.label)) is --> \(label.frame.height)")
                    maxHeight = label.frame.height > maxHeight ? label.frame.height : maxHeight
//                    arr.append(label.frame.height)
                }
                i += 1
            }
//            debugPrint("Heights of single row == \(maxHeight)")
            arr += maxHeight
//            debugPrint("Total Height == \(arr)")
            return maxHeight
        }
    }
    
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: MyLabelCell.identifier, for: indexPath) as! MyLabelCell
        if indexPath.row == 0 {
            cell.setup(with: firstRow[indexPath.section], isHeading: true, multiLine: true)
            return cell
        } else {
            let currentData = arrReportData[indexPath.row-1]
            cell.backgroundColor = indexPath.row % 2 == 0 ? .lightGray.withAlphaComponent(0.25) : .clear
            cell.gridlines = .all(.none)
            cell.borders = .all(.none)
            switch indexPath.section {
            case 0: cell.setup(with: "\(indexPath.row)"); return cell
            case 1: cell.setup(with: currentData.poNo); return cell
            case 2: cell.setup(with: currentData.createdAt); return cell
            case 3: cell.setup(with: currentData.companyPlant, multiLine: true); return cell
            case 4: cell.setup(with: currentData.vendorName, multiLine: true); return cell
            case 5: cell.setup(with: currentData.group1); return cell
            case 6: cell.setup(with: currentData.group2); return cell
            case 7: cell.setup(with: currentData.materialCode); return cell
            case 8: cell.setup(with: currentData.materialName, multiLine: true); return cell
            case 9: cell.setup(with: currentData.quility); return cell
            case 10: cell.setup(with: currentData.unit); return cell
            case 11: cell.setup(with: currentData.rate); return cell
            case 12: cell.setup(with: currentData.currency); return cell
            case 13: cell.setup(with: "\(currentData.fcValue)"); return cell
            case 14: cell.setup(with: "\(currentData.inrvalue)"); return cell
            case 15: cell.setup(with: "\(currentData.shippedQty)"); return cell
            case 16: cell.setup(with: "\(currentData.pendingQty)"); return cell
            case 17: cell.setup(with: currentData.packingInstruction); return cell
            case 18: cell.setup(with: currentData.materialGrpValue); return cell
            case 19: cell.setup(with: currentData.paymentTerm, multiLine: true); return cell
            case 20: cell.setup(with: currentData.incoterm); return cell
            default:
                cell.setup(with: "Default Cell")
                return cell
            }
        }
    }
}

class MyLabelCell: Cell {
    static let identifier = "MyLabelCell"
    private var label = UILabel()
    public func setup(with text: String?, isHeading: Bool = false, shouldBeLeft: Bool = false, multiLine: Bool = false, backgroundColor: UIColor = .clear) {
        label.text = text ?? "-"
        label.textAlignment = shouldBeLeft ? .left : .center
        label.DynamicHeight = true
        label.font = isHeading ? .systemFont(ofSize: getDynamicHeight(size: 12)) : .systemFont(ofSize: getDynamicHeight(size: 10))
        label.minimumScaleFactor = 0.5
        label.numberOfLines = multiLine ? 0 : 1
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = .clear
        label.textColor = isHeading ? .white : .black
//        label.sizeToFit()
        contentView.backgroundColor = isHeading ? UIColor.hexStringToUIColor(hex: "2D4154") : backgroundColor
        contentView.addSubview(label)
    }
    
    override func layoutSubviews () {
        super.layoutSubviews ()
        label.frame = contentView.bounds.inset(by: UIEdgeInsets(top: 0.5, left: 0.5, bottom: 0.5, right: 0.5))
//        label.frame = contentView.frame
    }
}
