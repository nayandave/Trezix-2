//
//  Vessel_List_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 11/04/23.
//

import UIKit

class Vessel_List_VC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var sortingHeaderView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnFirstSecSort: UIButton!
    @IBOutlet weak var btnSecondSecSort: UIButton!
    @IBOutlet weak var btnThirdSecSort: UIButton!
    
    @IBOutlet weak var imageFirstSecAscending: UIImageView!
    @IBOutlet weak var imageFirstSecDescending: UIImageView!
    
    @IBOutlet weak var imageSecondSecAscending: UIImageView!
    @IBOutlet weak var imageSecondSecDescending: UIImageView!
    
    @IBOutlet weak var imageThirdSecAscending: UIImageView!
    @IBOutlet weak var imageThirdSecDescending: UIImageView!
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var vessel_List_Table: UITableView!
    
    // MARK: - All Constants & Variables
    private var arrVesselData = [Vessel_List_Data]()
    
    private var currentSortingType: CurrentSorting! = .firstSecAscending
    
    private var isVendorAscending = true
    private var isBLAscending = true
    private var isETAAscending = true
    
    private var arr_All_Unique_Vendors = [String]()
    
    private var arrShowableSections = [Int]()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        noDataView.layer.applyShadowAndRadius()
    }
    
    // MARK: - All Outlets
    /// `Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable {
            callAllInitMethods()
        }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(isNetworkRefreshed: true)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    /// *Default Value of isNetworkRefreshed is* `TRUE`
    private func callAllInitMethods(isNetworkRefreshed: Bool = false) {
        if !isNetworkRefreshed {    // Code Should Be Called Only Once e.g. register table cell, dropdown etc.
            setUpVesselVC()
        }
        call_Vessel_List_API()
    }
    
    private func setUpVesselVC() {
        vessel_List_Table.configure_General_Table()
        vessel_List_Table.register(UINib(nibName: POReportExtraInfoCell.identifier, bundle: nil), forCellReuseIdentifier: POReportExtraInfoCell.identifier)
    }
    
    private func call_Vessel_List_API() {
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.VESSEL_LIST.fullPath(),
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Vessel_List_Response_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                Loader.hideLoader()
                if let data = response.data, !data.isEmpty {
                    self.arrVesselData = data
                    let allUnique = Array(Set(data.compactMap { $0.vendorName }))
//                    var allUnique = [String]()
//                    allVendor.compactMap {
//                        if !allUnique.contains($0) { allUnique.append($0) }
//                    }
//                    print("All Vendors = \(allVendor)")
                    print("Unique Vendors = \(allUnique) and countt =\(allUnique.count)")
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.vessel_List_Table.isHidden = false
                        self?.noDataView.isHidden = true
                        self?.sortingHeaderView.isHidden = false
                        self?.changeSorting(type: .firstSecAscending)
//                        self?.vessel_List_Table.reloadData()
                    }
                } else {    // No Data Available
                    DispatchQueue.main.async { [weak self] in
                        self?.vessel_List_Table.isHidden = true
                        self?.sortingHeaderView.isHidden = true
                        self?.noDataView.isHidden = false
                    }
                }
            } else if let errorMessage {
                debugPrint("Known Error Fetching Vessel List... \(errorMessage)")
                appView.makeToast("Something Went Wrong\nPlease Try Agauin Later!")
                Loader.hideLoader()
            } else {
                debugPrint("Unknown Error Fetching Vessel List...")
                appView.makeToast("Something Went Wrong\nPlease Try Agauin Later!")
                Loader.hideLoader()
            }
            
        }
    }
    
    private func call_Vessel_Info_API_Redirect(vesselID: Int, detailButton: UIButton, detailStackView: UIStackView) {
        let vc = vesselStoryboard.instantiateViewController(withIdentifier: "Vessel_Details_VC") as! Vessel_Details_VC
        let param = ["id" : vesselID]
        detailStackView.alpha = 0
        debugPrint("VESSEL PARAM = \(param)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.VESSEL_INFO.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: false, senderView: detailButton)
        { [weak self] (isSuccess, response: Vessel_Detail_Response_Modal?, errorMessage) in
            guard let self = self else { return }
            if isSuccess, let response {
                if let data = response.data {
                    vc.mainVesselData = data

                    // Shipment Data Handling
                    if let eventData = data.shipmentEvent {
                        vc.arrEventData = eventData
                    }

                    // Origin To Port Data Handling
                    if let originData = data.originToPort {
                        vc.vessel_Origin_Port_Data = originData
                        debugPrint("Origin Data -- \(originData)")
                    }

                    // Port To Port Data Handling
                    if let portData = data.portToPort {
                        vc.vessel_Port_ToPort_Data = portData
                        debugPrint("POrt Data -- \(portData)")
                    }
                    detailStackView.alpha = 1
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {    // No Data Available...
                    HapticFeedbackGenerator.notificationFeedback(type: .error)
                    detailStackView.alpha = 1
                    appView.makeToast("No Data Available!")
                }
            } else if let errorMessage {
                HapticFeedbackGenerator.notificationFeedback(type: .error)
                detailStackView.alpha = 1
                debugPrint("Known error while fetching vessel info --- \(errorMessage)")
                appView.makeToast("Something Went Wrong,\nPlease Try Again Later!")
            } else {
                HapticFeedbackGenerator.notificationFeedback(type: .error)
                detailStackView.alpha = 1
                debugPrint("Unknown error while fetching vessel info")
                appView.makeToast("Something Went Wrong,\nPlease Try Again Later!")
            }
        }
    }
    
    private func changeSorting(type: CurrentSorting) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        let arrAscendingImages = [imageFirstSecAscending, imageSecondSecAscending, imageThirdSecAscending]
        let arrDescendingImages = [imageFirstSecDescending, imageSecondSecDescending, imageThirdSecDescending]
        applyFilterAndRefresh(filter: type)
        switch type {
        case .firstSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 0 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .firstSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 0 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .secondSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 1 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .secondSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 1 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        case .thirdSecAscending:
            for i in 0...2 {
                arrAscendingImages[i]?.image = i == 2 ? UIImage(named: "ic-ascending_arrow(selected)") : UIImage(named: "ic-ascending_arrow(Unselected)")
                arrDescendingImages[i]?.image = UIImage(named: "ic-descending_arrow(Unselected)")
            }
        case .thirdSecDescending:
            for i in 0...2 {
                arrDescendingImages[i]?.image = i == 2 ? UIImage(named: "ic-descending_arrow(selected)") : UIImage(named: "ic-descending_arrow(Unselected)")
                arrAscendingImages[i]?.image = UIImage(named: "ic-ascending_arrow(Unselected)")
            }
        }
    }
    
    private func applyFilterAndRefresh(filter: CurrentSorting) {
        switch filter {
        case .firstSecAscending:
            arrVesselData = arrVesselData.sorted { $0.containerNumber.non_NilEmpty(true).localizedCompare($1.containerNumber.non_NilEmpty(true)) == .orderedAscending }
        case .secondSecAscending:
            arrVesselData = arrVesselData.sorted { $0.seaLine.non_NilEmpty(true).localizedCompare($1.seaLine.non_NilEmpty(true)) == .orderedAscending }
        case .thirdSecAscending:
            arrVesselData = arrVesselData.sorted { $0.etaDateAccordingTracking.non_NilEmpty(true).localizedCompare($1.etaDateAccordingTracking.non_NilEmpty(true)) == .orderedAscending }
        case .firstSecDescending:
            arrVesselData = arrVesselData.sorted { $0.containerNumber.non_NilEmpty(true).localizedCompare($1.containerNumber.non_NilEmpty(true)) == .orderedDescending }
        case .secondSecDescending:
            arrVesselData = arrVesselData.sorted { $0.seaLine.non_NilEmpty(true).localizedCompare($1.seaLine.non_NilEmpty(true)) == .orderedDescending }
        case .thirdSecDescending:
            arrVesselData = arrVesselData.sorted { $0.etaDateAccordingTracking.non_NilEmpty(true).localizedCompare($1.etaDateAccordingTracking.non_NilEmpty(true)) == .orderedDescending }
        }
        DispatchQueue.main.async {  [weak self] in
            self?.vessel_List_Table.reloadData()
        }
    }
    
    // MARK: - All Actions
    @IBAction func Back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func tappedOnSection(_ tap: UITapGestureRecognizer) {
        guard let tappedView = tap.view as? ThreeColumnRowView else { appView.makeToast("Unrecognised Touch"); return }
        let currentIndex = tappedView.currentRow
        if arrShowableSections.contains(currentIndex) {
            arrShowableSections = arrShowableSections.filter({ $0 != currentIndex })
        } else {
            arrShowableSections.append(currentIndex)
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.vessel_List_Table.beginUpdates()
            self.vessel_List_Table.reloadSections(IndexSet(integer: currentIndex), with: .automatic)
            self.vessel_List_Table.endUpdates()
        }
    }
    
    @IBAction func Change_First_Section_Sorting(_ sender: UIButton) {
        if isVendorAscending {
            changeSorting(type: .firstSecDescending)
        } else {
            changeSorting(type: .firstSecAscending)
        }
        isVendorAscending = !isVendorAscending
        isBLAscending = true
        isETAAscending = true
    }
    
    @IBAction func Change_Second_Section_Sorting(_ sender: UIButton) {
        if isBLAscending {
            changeSorting(type: .secondSecAscending)
        } else {
            changeSorting(type: .secondSecDescending)
        }
        isBLAscending = !isBLAscending
        isVendorAscending = true
        isETAAscending = true
    }
    
    @IBAction func Change_Third_Section_Sorting(_ sender: UIButton) {
        if isETAAscending {
            changeSorting(type: .thirdSecAscending)
        } else {
            changeSorting(type: .thirdSecDescending)
        }
        isETAAscending = !isETAAscending
        isVendorAscending = true
        isBLAscending = true
    }
    
    @objc private func more_Detail_Button_Action(_ sender: UIButton) {
        HapticFeedbackGenerator.simpleFeedback(type: .light)
        guard let cell = vessel_List_Table.cellForRow(at: IndexPath(row: 4, section: sender.tag)) as? POReportExtraInfoCell else { return }
        let mainData = arrVesselData[sender.tag]
        call_Vessel_Info_API_Redirect(vesselID: mainData.id, detailButton: sender, detailStackView: cell.detainButton_StackView)
    }
}

// MARK: - TableView Delegate Methods
extension Vessel_List_VC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrVesselData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getDynamicHeight(size: 50)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0 //getDynamicHeight(size: 1)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ThreeColumnRowView()
        headerView.isViewHeadingView(withSeparator: true, tag: section)
        let data = arrVesselData[section]
        headerView.setData(firstLabel: data.containerNumber, secondLabel: data.seaLine, thirdLabel: data.etaDateAccordingTracking, threeColumnView: true)
        headerView.isSelected(selected: arrShowableSections.contains(section), lastCell: section+1 == arrVesselData.count)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnSection(_:)))
        headerView.addGestureRecognizer(tapGesture)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrShowableSections.contains(section) ? 5 : 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: POReportExtraInfoCell.identifier, for: indexPath) as! POReportExtraInfoCell
        if indexPath.row == 4 {
            cell.is_Detail_Button_Only(index: indexPath.section, title: "Detail Tracking")
            cell.btnDetail.addTarget(self, action: #selector(more_Detail_Button_Action(_:)), for: .touchUpInside)
            cell.isUserInteractionEnabled = true
        } else {
            cell.isUserInteractionEnabled = false
            cell.vesselTrackingData = arrVesselData[indexPath.section]
            cell.setUpVesselDataCell(for: indexPath.row)
        }
        return cell
    }
}

extension UITableView {
    /// **Common Configuration of UITableView**
    ///
    /// * 0 section top padding if iOS version is greater or equal to 15
    /// * Making default footer View
    public func configure_General_Table() {
        if #available(iOS 15, *) {
            self.sectionHeaderTopPadding = 0
        }
        self.tableFooterView = UIView()
    }
}
