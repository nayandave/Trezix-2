//
//  Vessel_Details_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 13/04/23.
//

import UIKit
import MapKit

class Vessel_Details_VC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var mainStackView_BottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var section2_NoData_View: UIView!
    @IBOutlet weak var section3_NoData_View: UIView!
    @IBOutlet weak var section4_NoData_View: UIView!
    
    @IBOutlet weak var section1Table: UITableView!
    @IBOutlet weak var section2Table: UITableView!
    @IBOutlet weak var section3Table: UITableView!
    @IBOutlet weak var section4Table: UITableView!
    
    @IBOutlet weak var section1_TableHeight: NSLayoutConstraint!
    @IBOutlet weak var section2_TableHeight: NSLayoutConstraint!
    @IBOutlet weak var section3_TableHeight: NSLayoutConstraint!
    @IBOutlet weak var section4_TableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mapContentView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - All Constants & Variables
    
    var vessel_ID = Int()
    
    /// `Main Vessel Detail Data Array`
    var mainVesselData: Vessel_Detail_Data! = nil
    
    /// `Section 2 Data Array`
    var arrEventData = [Vessel_Shipment_Event_Data]()
    
    /// `Section 3 Data Array`
    var vessel_Origin_Port_Data: Vessel_OriginToPort_Data! = nil
    
    /// `Section 4 Data Array`
    var vessel_Port_ToPort_Data: Vessel_PortToPort_Data! = nil
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setting_Up_VC()
        set_MapView()
        mainStackView_BottomConstraint.constant = getEdgeNotchHeight(of: .bottomNotch)
    }
    
    // MARK: - All Helper Methods
    private func register_Cells_To_Tables() {
        DispatchQueue.global(qos: .userInitiated).sync { [weak self] in
            [self?.section2Table, self?.section4Table].forEach { table in
                table?.register(UINib(nibName: Four_Column_TableCell.identifier, bundle: nil), forCellReuseIdentifier: Four_Column_TableCell.identifier)
            }
            DispatchQueue.main.async { [weak self] in
                self?.load_Vessel_Data_UI()
            }
        }
    }
    
    private func setting_Up_VC() {
        [section1Table, section2Table, section3Table, section4Table].forEach { table in
            table?.configure_General_Table()
        }
        register_Cells_To_Tables()
    }
    
    private func set_MapView() {
        guard let location = mainVesselData.latestLocation,
              let lattitude = location.latitude,
              let longitude = location.longitude else {
            debugPrint("No Last Location Found!!!")
            mapContentView.isHidden = true
            return
        }
        mapContentView.isHidden = false
        mapView.region = .init(center: CLLocationCoordinate2D(latitude: CLLocationDegrees(floatLiteral: lattitude),
                                                              longitude: CLLocationDegrees(floatLiteral: longitude)),
                               span: MKCoordinateSpan(latitudeDelta: CLLocationDegrees(integerLiteral: 1.5), longitudeDelta: CLLocationDegrees(integerLiteral: 1.5)))
    }
    
    private func load_Vessel_Data_UI() {
        DispatchQueue.main.async { [weak self] in
            self?.section1Table.reloadData()
        }
        
        if arrEventData.isEmpty {
            DispatchQueue.main.async { [weak self] in
                self?.section2_TableHeight.constant = 70
                self?.section2Table.isHidden = true
                self?.section2_NoData_View.isHidden = false
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.section2Table.isHidden = false
                self?.section2_NoData_View.isHidden = true
                self?.section2Table.reloadData()
            }
        }
        
        if vessel_Origin_Port_Data == nil {
            DispatchQueue.main.async { [weak self] in
                self?.section3_TableHeight.constant = 70
                self?.section3Table.isHidden = true
                self?.section3_NoData_View.isHidden = false
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.section3Table.isHidden = false
                self?.section3_NoData_View.isHidden = true
                self?.section3Table.reloadData()
            }
        }
        
        if vessel_Port_ToPort_Data == nil {
            DispatchQueue.main.async { [weak self] in
                self?.section4_TableHeight.constant = 70
                self?.section4Table.isHidden = true
                self?.section4_NoData_View.isHidden = false
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.section4Table.isHidden = false
                self?.section4_NoData_View.isHidden = true
                self?.section4Table.reloadData()
            }
        }
    }
    
    // MARK: - All Actions
    @IBAction func Back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - TableView Delegate Methods
extension Vessel_Details_VC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == section1Table {
            if mainVesselData != nil { return 11 } else { return 0 }
        } else if tableView == section2Table {
            if arrEventData.isEmpty { return 0 } else { return arrEventData.count+1 }
        } else if tableView == section3Table {
            if vessel_Origin_Port_Data != nil { return 2 } else { return 0 }
        } else {
            if vessel_Port_ToPort_Data != nil { return 2 } else { return 0 }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == section1Table {
            let cell = tableView.dequeueReusableCell(withIdentifier: Vessel_Common_TableCell.identifier, for: indexPath) as! Vessel_Common_TableCell
            cell.vesselData = mainVesselData
            cell.set_Data_According(toIndex: indexPath.row)
            
            DispatchQueue.main.async { [weak self] in
                self?.section1_TableHeight.constant = tableView.contentSize.height
            }
            return cell
        } else if tableView == section2Table {
            let cell = tableView.dequeueReusableCell(withIdentifier: Four_Column_TableCell.identifier, for: indexPath) as! Four_Column_TableCell
            if indexPath.row == 0 { //Heading Row
                cell.configure_Cell_Row_Dynamically(isHeading: true, head1: "Event", head2: "Estimated Time", head3: "Actual Time", head4: "Location")
            } else {
                let eventData = arrEventData[indexPath.row-1]
                cell.configure_Cell_Row_Dynamically(isHeading: false)
                cell.setAllData(data1: eventData.name.non_NilEmpty(),
                                data2: eventData.estimateTime.non_NilEmpty(),
                                data3: eventData.actualTime.non_NilEmpty(),
                                data4: eventData.location.non_NilEmpty())
            }
            DispatchQueue.main.async { [weak self] in
                self?.section2_TableHeight.constant = tableView.contentSize.height
            }
            return cell
        } else if tableView == section3Table {
            let cell = UITableViewCell()
            if #available(iOS 14.0, *) {
                var config = cell.defaultContentConfiguration()
                var backConfig = UIBackgroundConfiguration.listPlainCell()
                if indexPath.row == 0 {
                    config.text = "Carrier"
                    config.textProperties.font = UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 16))!
                    config.textProperties.color = .ourAppThemeColor
                    backConfig.backgroundColor = UIColor.ourAppThemeColor.withAlphaComponent(0.15)
                } else {
                    config.text = vessel_Origin_Port_Data.carrier.non_NilEmpty()
                    config.textProperties.font = UIFont(name: AppCenturyFont.Regular.Name(), size: getDynamicHeight(size: 14))!
                    backConfig.backgroundColor = UIColor.systemBackground
                    config.textProperties.color = .label
                }
                cell.contentConfiguration = config
                cell.backgroundConfiguration = backConfig
            } else {
                if indexPath.row == 0 {
                    cell.textLabel?.font = UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 16))
                    cell.textLabel?.textColor = .ourAppThemeColor
                    cell.backgroundColor = UIColor.ourAppThemeColor.withAlphaComponent(0.15)
                    cell.textLabel?.text = "Carrier"
                } else {
                    cell.textLabel?.font = UIFont(name: AppCenturyFont.Regular.Name(), size: getDynamicHeight(size: 14))
                    cell.textLabel?.text = vessel_Origin_Port_Data.carrier.non_NilEmpty()
                    cell.backgroundColor = .systemBackground
                    cell.textLabel?.textColor = .label
                }
            }
            DispatchQueue.main.async { [weak self] in
                self?.section3_TableHeight.constant = tableView.contentSize.height
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Four_Column_TableCell.identifier, for: indexPath) as! Four_Column_TableCell
            if indexPath.row == 0 {     //Heading Row
                cell.configure_Cell_Row_Dynamically(isHeading: true, head1: "First Port", head2: "First Port Atd", head3: "Discharge Port", head4: "Discharge Port Ata")
            } else {
                cell.configure_Cell_Row_Dynamically(isHeading: false)
                cell.setAllData(data1: vessel_Port_ToPort_Data.firstPort.non_NilEmpty(),
                                data2: vessel_Port_ToPort_Data.firstPortAtd.non_NilEmpty(),
                                data3: vessel_Port_ToPort_Data.dischargePort.non_NilEmpty(),
                                data4: vessel_Port_ToPort_Data.dischargePortATA.non_NilEmpty())
            }
            DispatchQueue.main.async { [weak self] in
                self?.section4_TableHeight.constant = tableView.contentSize.height
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == section1Table {
//            if indexPath.row == 0 { return getDynamicHeight(size: 55) }
//        } else {
            return getDynamicHeight(size: 35)
        }
        return UITableView.automaticDimension
    }
}
