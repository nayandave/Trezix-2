//
//  SideMenuVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 16/07/22.
//

import UIKit

class SideMenuVC: UIViewController {
    // MARK: - All Outlets
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var sideMenuTableView: UITableView!
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var sideMenuCustomWidth: NSLayoutConstraint!
    
    @IBOutlet weak var footerLogoutView: UIView!
    @IBOutlet weak var btnFooter: UIButton!
    @IBOutlet weak var footerIconImage: UIImageView!
    @IBOutlet weak var lblFooter: UILabel!
    
    
    // MARK: - All Constants & Variables
    //private var currShowingReportSubMenu = false
    
    private var selectedCell: Int = 0
    private var selectedSubCell: IndexPath = IndexPath(row: 99, column: 99)
    
    private var arrShowableReportSubmenu = [String]()
    
    private struct SideMenuOptions: Equatable {
        let name: String
        let icon: String
        let hasSubmenu: Bool
        var subMenu: [SideMenuOptions]? = nil
    }
    
    private var allSideMenu = [SideMenuOptions]()
    
    private var rowList = [Int: Int]()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareSideMenuStructure()
        setUpTableView()
        setMenuWidth()
        setUpFooterView()
        sideMenuController?.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(reloadSelections(_:)), name: .reloadSideMenuSelectionNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(select_ReportSection(_:)), name: .select_Report_Notificaiton, object: nil)
    }
    
    private func setUpFooterView(isSelected: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lblFooter.textColor = isSelected ? .themeYellowColor : .white
            self.footerIconImage.tintColor = isSelected ? .themeYellowColor : .white
            self.lblFooter.changeFontStyle(style: isSelected ? .Bold : .Regular, fontSize: 18)
        }
    }
    
    @objc private func select_ReportSection(_ notification: NSNotification) {
        if let tappedView = sideMenuTableView.headerView(forSection: 1) as? SideMenuHeaderSectionView {
            HapticFeedbackGenerator.simpleFeedback(type: .soft)
            let currentIndex = tappedView.currentCell
            let prevRow = selectedCell
            selectedCell = currentIndex
            
            var notiObj: AllSideMenuIdentifier! = nil
            
            // currentIndex == 1 { Report Section Selected }
            if currentIndex == 0 {  /// **Selected Dashboard Section**
                notiObj = .Dashboard
            } else if currentIndex == 2 {   /// **Selected Approval Section**
                notiObj = .Approval
            } else if currentIndex == 3 {   /// **Selected EximGPT Section**
                notiObj = .eximgpt
            } else if currentIndex == 4 {   /// **Vessel Tracking Section**
                notiObj = .vesselTrack
            } else if currentIndex == 5 {   /// **Selected LogOut Section**
                notiObj = .Logout
            }
            
            if prevRow != currentIndex {
                let prevHeaderView = sideMenuTableView.headerView(forSection: prevRow) as! SideMenuHeaderSectionView
                let rowCount = allSideMenu[prevRow].hasSubmenu ? allSideMenu[prevRow].subMenu!.count : 0
                if rowList[prevRow] == rowCount {
                    for _ in 0..<rowCount {
                        sideMenuTableView.beginUpdates()
                        rowList[prevRow]! -= 1
                        sideMenuTableView.deleteRows(at: [IndexPath(row: rowList[prevRow]!, section: prevRow)], with: .fade)
                        sideMenuTableView.endUpdates()
                    }
                    prevHeaderView.animateDetailIndicator(show: false)
                }
                prevHeaderView.changeBackground(selected: false)
                
                let currHeaderView = sideMenuTableView.headerView(forSection: currentIndex) as! SideMenuHeaderSectionView
                let selRowCount = allSideMenu[currentIndex].hasSubmenu ? allSideMenu[currentIndex].subMenu!.count : 0
                if rowList[currentIndex] != selRowCount {
                    for row in 0..<selRowCount {
                        sideMenuTableView.beginUpdates()
                        rowList[currentIndex]! += 1
                        sideMenuTableView.insertRows(at: [IndexPath(row: row, section: currentIndex)], with: .fade)
                        sideMenuTableView.endUpdates()
                    }
                    currHeaderView.animateDetailIndicator(show: true)
                }
                currHeaderView.changeBackground(selected: true)
            } else {
                let headerView = sideMenuTableView.headerView(forSection: currentIndex) as! SideMenuHeaderSectionView
                let selRowCount = allSideMenu[currentIndex].hasSubmenu ? allSideMenu[currentIndex].subMenu!.count : 0
                if rowList[currentIndex] == 0 { //Was Hidden and now showable
                    for row in 0..<selRowCount {
                        sideMenuTableView.beginUpdates()
                        rowList[currentIndex]! += 1
                        sideMenuTableView.insertRows(at: [IndexPath(row: row, section: currentIndex)], with: .fade)
                        sideMenuTableView.endUpdates()
                    }
                    headerView.animateDetailIndicator(show: true)
                } else {    //Was showing and now hidden
                    for _ in 0..<selRowCount {
                        sideMenuTableView.beginUpdates()
                        rowList[currentIndex]! -= 1
                        sideMenuTableView.deleteRows(at: [IndexPath(row: rowList[currentIndex]!, section: currentIndex)], with: .fade)
                        sideMenuTableView.endUpdates()
                    }
                    headerView.animateDetailIndicator(show: false)
                }
            }
            if notiObj != nil {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    self?.sideMenuController?.hideMenu()
                }
                NotificationCenter.default.post(name: .sideMenuSelectionNotification, object: notiObj)
            }
        }
    }
    
    @objc private func reloadSelections(_ notification: NSNotification) {
        for key in rowList.keys { rowList[key] = 0 }    // Hiding All Submenus
        selectedCell = 0
        selectedSubCell = IndexPath(row: 99, column: 99)
        setUpFooterView()
        sideMenuTableView.reloadData()
    }
    
    private let sideDashboard = SideMenuOptions(name: "Dashboard", icon: "ic-dashboad", hasSubmenu: false)
    
    private let sideOrderReport = SideMenuOptions(name: "Order Report", icon: "ic-CommonReport", hasSubmenu: false)
    private let sideShipmentReport = SideMenuOptions(name: "Shipment Report", icon: "ic-CommonReport", hasSubmenu: false)
    private let sideLandingCostReport = SideMenuOptions(name: "Landing Cost Report", icon: "ic-CommonReport", hasSubmenu: false)
    private let sideLandingInvoice = SideMenuOptions(name: "Landing Cost Invoice", icon: "ic-CommonReport", hasSubmenu: false)
    private let sideSalesOrderReport = SideMenuOptions(name: "Sales Order Report", icon: "ic-CommonReport", hasSubmenu: false)
    //  Dispatch Planning Report
    private let sideDispatchPlanning = SideMenuOptions(name: "Dispatch Planning Report", icon: "ic-CommonReport", hasSubmenu: false)
    private let sideSalesReport = SideMenuOptions(name: "Sales Report", icon: "ic-CommonReport", hasSubmenu: false)
    private let sidePOTimeLine = SideMenuOptions(name: "PO Timeline", icon: "ic-CommonReport", hasSubmenu: false)
    private let sideScriptDrawback = SideMenuOptions(name: "Script Drawback Report", icon: "ic-CommonReport", hasSubmenu: false)
    
    private let sideApproval = SideMenuOptions(name: "Approval", icon: "ic-Approval_Notification", hasSubmenu: false)
    
    private let eximGPTSide = SideMenuOptions(name: "Exim GPT", icon: "icon-Chat", hasSubmenu: false)
    
    private let vesselTrackSide = SideMenuOptions(name: "Shipment Tracking", icon: "icon_Vessel", hasSubmenu: false)
    
    private let quotationListSide = SideMenuOptions(name: "Quotation", icon: "ic-quotation", hasSubmenu: false)
    
    private let sideDocUpload = SideMenuOptions(name: "Upload Documents", icon: "ic-Document_Upload", hasSubmenu: false)
    
    private var sideReport: SideMenuOptions! = nil
    
    private func prepareSideMenuStructure() {
        var mainMenus = [SideMenuOptions]()
        var subMenu = [SideMenuOptions]()
        if all_Permissions.contains(.Dashboard) || all_Permissions.contains(.ImportDashboard) || all_Permissions.contains(.ExportDashboard) {
            mainMenus.append(sideDashboard)
        }
        
        if all_Permissions.contains(.OrderReport) { subMenu.append(sideOrderReport) }
        if all_Permissions.contains(.ShipmentReport) { subMenu.append(sideShipmentReport) }
        if all_Permissions.contains(.LandingCostReport) { subMenu.append(sideLandingCostReport) }
        if all_Permissions.contains(.LandingCostInvoice) { subMenu.append(sideLandingInvoice) }
        if all_Permissions.contains(.DispatchPlanningReport) {
            dump(all_Permissions)
            debugPrint("Added Dispatch Planning")
            subMenu.append(sideDispatchPlanning)
        }
        if all_Permissions.contains(.SalesOrderReport) { subMenu.append(sideSalesOrderReport) }
        if all_Permissions.contains(.SalesReport) { subMenu.append(sideSalesReport) }
        if all_Permissions.contains(.POTimeLine) { subMenu.append(sidePOTimeLine) }
        if all_Permissions.contains(.ScriptDrawback) { subMenu.append(sideScriptDrawback) }
        
        sideReport = SideMenuOptions(name: "Reports", icon: "ic-Report", hasSubmenu: true, subMenu: subMenu)
        
        if !subMenu.isEmpty {
            mainMenus.append(sideReport)
        }
        
        if all_Permissions.contains(.Approval) {
            mainMenus.append(sideApproval)
        }
        
        if all_Permissions.contains(.eximgpt) {
            mainMenus.append(eximGPTSide)
        }
        
        if all_Permissions.contains(.vesselTrack) {
            mainMenus.append(vesselTrackSide)
        }
        
        if all_Permissions.contains(.quotationView) {
            mainMenus.append(quotationListSide)
        }
        
        mainMenus.append(sideDocUpload)
        
        allSideMenu = mainMenus
        
        for header in 0..<allSideMenu.count { rowList[header] = 0 }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setMenuWidth()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        setMenuWidth()
    }
    
    // MARK: - Helper Methods
    private func setMenuWidth() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let ipadMode = (self.traitCollection.horizontalSizeClass == .regular && self.traitCollection.verticalSizeClass == .regular)
            self.sideMenuCustomWidth.constant = SCREEN_WIDTH * (ipadMode ? 0.4 : 0.5)
            SideMenuController.preferences.basic.menuWidth = self.sideMenuCustomWidth.constant
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    /// *Initializing Side Menu Table VIew*
    private func setUpTableView() {
        sideMenuTableView.setSectionPadding_iOS_15()
        sideMenuTableView.register(UINib(nibName: "SideMenuTableCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableCell")
        sideMenuTableView.register(UINib(nibName: SideMenuHeaderSectionView.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: SideMenuHeaderSectionView.identifier)
        DispatchQueue.main.async { [weak self] in
            self?.sideMenuTableView.reloadData()
        }
    }
    
    /// *Tap On Section(Side Main Menu Click) Recognizer*
    /// Opens SubMenu of tapped sideMenu if it has sub menu avilable
    /// Hides SideMenu & Notify ``HomepageVC`` with selected side menu object
    @objc func tapOnSection(_ tapGesture: UITapGestureRecognizer) {
        if let tappedView = tapGesture.view as? SideMenuHeaderSectionView {     
            HapticFeedbackGenerator.simpleFeedback(type: .soft)
            let currentIndex = tappedView.currentCell
            let prevRow = selectedCell
            selectedCell = currentIndex
            
            var notiObj: AllSideMenuIdentifier! = nil
            
            let selectedSection = allSideMenu[currentIndex]
            
            // currentIndex == 1 { Report Section Selected }
            if selectedSection == sideDashboard {  /// **Selected Dashboard Section**
                notiObj = .Dashboard
            } else if selectedSection == sideApproval {   /// **Selected Approval Section**
                notiObj = .Approval
            } else if selectedSection == eximGPTSide {   /// **Selected EximGPT Section**
                notiObj = .eximgpt
            } else if selectedSection == vesselTrackSide {   /// **Vessel Tracking Section**
                notiObj = .vesselTrack
            } else if selectedSection == sideDocUpload {   /// ** Document Upload Section**
                notiObj = .DocumentUpload
            } else if selectedSection == quotationListSide {
                notiObj = .Quotation
            }
            
            if prevRow != currentIndex {
                let prevHeaderView = sideMenuTableView.headerView(forSection: prevRow) as! SideMenuHeaderSectionView
                let rowCount = allSideMenu[prevRow].hasSubmenu ? allSideMenu[prevRow].subMenu!.count : 0
                if rowList[prevRow] == rowCount {
                    for _ in 0..<rowCount {
                        sideMenuTableView.beginUpdates()
                        rowList[prevRow]! -= 1
                        sideMenuTableView.deleteRows(at: [IndexPath(row: rowList[prevRow]!, section: prevRow)], with: .fade)
                        sideMenuTableView.endUpdates()
                    }
                    prevHeaderView.animateDetailIndicator(show: false)
                }
                prevHeaderView.changeBackground(selected: false)
                
                let currHeaderView = sideMenuTableView.headerView(forSection: currentIndex) as! SideMenuHeaderSectionView
                let selRowCount = allSideMenu[currentIndex].hasSubmenu ? allSideMenu[currentIndex].subMenu!.count : 0
                if rowList[currentIndex] != selRowCount {
                    for row in 0..<selRowCount {
                        sideMenuTableView.beginUpdates()
                        rowList[currentIndex]! += 1
                        sideMenuTableView.insertRows(at: [IndexPath(row: row, section: currentIndex)], with: .fade)
                        sideMenuTableView.endUpdates()
                    }
                    currHeaderView.animateDetailIndicator(show: true)
                }
                currHeaderView.changeBackground(selected: true)
            } else {
                let headerView = sideMenuTableView.headerView(forSection: currentIndex) as! SideMenuHeaderSectionView
                let selRowCount = allSideMenu[currentIndex].hasSubmenu ? allSideMenu[currentIndex].subMenu!.count : 0
                if rowList[currentIndex] == 0 { //Was Hidden and now showable
                    for row in 0..<selRowCount {
                        sideMenuTableView.beginUpdates()
                        rowList[currentIndex]! += 1
                        sideMenuTableView.insertRows(at: [IndexPath(row: row, section: currentIndex)], with: .fade)
                        sideMenuTableView.endUpdates()
                    }
                    headerView.animateDetailIndicator(show: true)
                } else {    //Was showing and now hidden
                    for _ in 0..<selRowCount {
                        sideMenuTableView.beginUpdates()
                        rowList[currentIndex]! -= 1
                        sideMenuTableView.deleteRows(at: [IndexPath(row: rowList[currentIndex]!, section: currentIndex)], with: .fade)
                        sideMenuTableView.endUpdates()
                    }
                    headerView.animateDetailIndicator(show: false)
                }
            }
            if notiObj != nil {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    self?.sideMenuController?.hideMenu()
                }
                NotificationCenter.default.post(name: .sideMenuSelectionNotification, object: notiObj)
            }
        }
    }
    
    // MARK: - Action Methods
    @IBAction func logout_Action(_ sender: UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) { [weak self] in
            self?.sideMenuController?.hideMenu()
        }
        UIView.animate(withDuration: 0, delay: 0, options: [.curveEaseInOut, .transitionCrossDissolve], animations: { [weak self] in
            self?.setUpFooterView(isSelected: true)
        }) { _ in
            NotificationCenter.default.post(name: .sideMenuSelectionNotification, object: AllSideMenuIdentifier.Logout)
        }
    }
}

// MARK: - TableView Delegate Methods
extension SideMenuVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return allSideMenu.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return isRunningOnIPAD ? 70 : getDynamicHeight(size: 55)
    }
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return allSideMenu[section].hasSubmenu ? (currShowingReportSubMenu ? allSideMenu[section].subMenu!.count : 0) : 0
        return rowList[section]!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: SideMenuHeaderSectionView.identifier) as! SideMenuHeaderSectionView
        header.changeBackground(selected: section == selectedCell)
        header.insertHeaderData(title: allSideMenu[section].name, icon: allSideMenu[section].icon, showSubmenu: allSideMenu[section].hasSubmenu, index: section)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnSection(_:)))
        header.addGestureRecognizer(tap)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return getDynamicHeight(size: 0.5)
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let separator = UIView(frame: CGRect(x: 10, y: 0, width: tableView.frame.width-20, height: 1))
//        separator.backgroundColor = .white
//        return separator
//    }
//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableCell", for: indexPath) as! SideMenuTableCell
        let currArrRows = allSideMenu[indexPath.section].subMenu!
        cell.insertingData(title: currArrRows[indexPath.row].name, icon: currArrRows[indexPath.row].icon, showSubmenu: currArrRows[indexPath.row].hasSubmenu)
        cell.changeBackground(selected: indexPath == selectedSubCell)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return isRunningOnIPAD ? 50 : getDynamicHeight(size: 40)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedSubCell != IndexPath(row: 99, column: 99) {
            let prevRow = selectedSubCell
            selectedSubCell = indexPath
            let prevCell = tableView.cellForRow(at: prevRow) as! SideMenuTableCell
            let currCell = tableView.cellForRow(at: indexPath) as! SideMenuTableCell
            prevCell.changeBackground(selected: false)
            currCell.changeBackground(selected: true)
        } else {
            selectedSubCell = indexPath
            let cell = tableView.cellForRow(at: indexPath) as! SideMenuTableCell
            cell.changeBackground(selected: true)
        }
        
        var sideMenuObj: AllSideMenuIdentifier! = nil
        
        if indexPath.section == 1 {
            let selected = allSideMenu[1].subMenu![indexPath.row]
            if selected == sideOrderReport {
                sideMenuObj = .OrderReport
            } else if selected == sideShipmentReport {
                sideMenuObj = .ShipmentReport
            } else if selected == sideLandingCostReport {
                sideMenuObj = .LandingCostReport
            } else if selected == sideLandingInvoice {
                sideMenuObj = .LandingCostInvoice
            } else if selected == sideDispatchPlanning {
                sideMenuObj = .DispatchPlanningReport
            } else if selected == sideSalesOrderReport {
                sideMenuObj = .SalesOrderReport
            } else if selected == sideSalesReport {
                sideMenuObj = .SalesReport
            } else if selected == sidePOTimeLine {
                sideMenuObj = .POTimeLine
            } else if selected == sideScriptDrawback {
                sideMenuObj = .ScriptDrawback
            }
            let headerView = sideMenuTableView.headerView(forSection: 1) as! SideMenuHeaderSectionView
            headerView.animateDetailIndicator(show: false)
        }
        
        NotificationCenter.default.post(name: .sideMenuSelectionNotification, object: sideMenuObj)
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.sideMenuController?.hideMenu()
        }
    }
}
extension SideMenuVC: SideMenuControllerDelegate {
    func sideMenuController(_ sideMenuController: SideMenuController,
                            animationControllerFrom fromVC: UIViewController,
                            to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BasicTransitionAnimator(options: .transitionFlipFromRight, duration: 0.6)
    }

    func sideMenuController(_ sideMenuController: SideMenuController, willShow viewController: UIViewController, animated: Bool) {
        print("[Example] View controller will show [\(viewController)]")
    }

    func sideMenuController(_ sideMenuController: SideMenuController, didShow viewController: UIViewController, animated: Bool) {
        print("[Example] View controller did show [\(viewController)]")
    }
}
