//
//  EximGPTVC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 03/04/23.
//

import UIKit
import AVKit
import IQKeyboardManagerSwift

class EximGPTVC: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var chatTableView: UITableView!
    
    @IBOutlet weak var textMessage: UITextView!
    @IBOutlet weak var textMessage_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var textStack_BottomConstraint: NSLayoutConstraint!
    
    // MARK: - All Constants & Variables
    
    private struct ChatData {
        var message: String
        var fromMySide: Bool
        var time: String?
    }
    
    private var arrAllChat = [ChatData]()
    private let defaultPlaceHolder = "Ask Me About Exim..."
    private let defaultWaitMessage = "Please Wait Few Seconds..."
    
    private var showingLoadingAnimation = false
    
    private var textMessageHeightObserver: NSKeyValueObservation?
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        reassignReachability()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        textMessage.layer.borderWidth = 1
        textMessage.layer.borderColor = UIColor.hexStringToUIColor(hex: "C8D9DB").cgColor
        DispatchQueue.main.async { [weak self] in
            self?.adjustTextViewHeight()
        }
//        textMessage.layer.applyShadowAndRadius(cornerRadius: textMessage.frame.height/2, opacity: 0.75, shadowRadius: 2)
        btnSend.layer.applyShadowAndRadius(cornerRadius: btnSend.frame.height/2)

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        textMessage.resignFirstResponder()
    }
    
    // MARK: - All Helper Methods
    
    /// `Method to be called in` ``viewDidLoad()``
    /// Nullify current `whenReachable` & assign new one for this particular ViewController
    private func reassignReachability() {
        appDelegate.reachability.whenReachable = nil
        if appDelegate.reachability.isReachable {
            callAllInitMethods()
        }
        appDelegate.setupNetworkMonitoring { [weak self] in
            guard let self = self else { return }
            self.callAllInitMethods(false)
        }
    }
    
    /// `All Initial Required Methods which must be called in` ``viewDidLoad()``
    private func callAllInitMethods(_ calledFirstTime: Bool = true) {
        if calledFirstTime {
            setUp_Exim_VC()
            registerAllCells()
        }
    }
    
    private func registerAllCells() {
        chatTableView.register(UINib(nibName: MyMessage_TableCell.identifier, bundle: nil), forCellReuseIdentifier: MyMessage_TableCell.identifier)
        chatTableView.register(UINib(nibName: GPTMessage_TableCell.identifier, bundle: nil), forCellReuseIdentifier: GPTMessage_TableCell.identifier)
    }
    
    private func setUp_Exim_VC() {
        textMessage.delegate = self
        
        if chatTableView.transform.isIdentity {
            chatTableView.transform = chatTableView.transform.rotated(by: .pi)
        }
        if #available(iOS 15.0, *) {
            chatTableView.sectionHeaderTopPadding = 0
        }
        chatTableView.tableFooterView = UIView()
        
        IQKeyboardManager.shared.enable = false
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        let tapOntable = UITapGestureRecognizer(target: self, action: #selector(tappedOnTableView))
        tapOntable.delegate = self
        chatTableView.addGestureRecognizer(tapOntable)
        chatTableView.keyboardDismissMode = .onDrag
        
        
        textMessage.delegate = self
        textMessage.text = defaultPlaceHolder
        textMessage.textColor = UIColor.ourAppThemeColor.withAlphaComponent(0.5)
        textMessage.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        let fixedWidth = textMessage.frame.size.width
        let newSize = textMessage.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        textMessage_HeightConstraint.constant = newSize.height
        
        //self.view.layoutIfNeeded()
    }

    ///**To Dismiss Keyboard When Tapped on TableView**
    @objc func tappedOnTableView() {
        textMessage.endEditing(true)
        debugPrint("Forced Ended TextField")
    }
    
    
    
    // MARK: - All Actions
    @IBAction func back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    var genrating_Answer_Animation = false
    
    @IBAction func send_Button_Action(_ sender: UIButton) {
        if textMessage.hasText && textMessage.text != defaultPlaceHolder && textMessage.text != defaultWaitMessage {
            view.endEditing(true)
            showingLoadingAnimation = true
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.arrAllChat.insert(ChatData(message: self.textMessage.text!, fromMySide: true), at: 0)
                self.textMessage.text.removeAll()
                self.textViewDidChange(self.textMessage)
                self.chatTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .left)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { [weak self] in
                    guard let self = self else { return }
                    self.arrAllChat.insert(ChatData(message: "Answer", fromMySide: false), at: 0)
                    self.chatTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .right)
                }
            }
            GPT_Helper.ask_EximGPT(withQuestion: textMessage.text!, showHud: false, senderView: nil)
            { [weak self] isSuccess, response, errorMessage in
                guard let self = self else { return }
                if isSuccess, let response {
                    guard let answer = response.answer else { debugPrint("Wrong Answer -- \(response)"); return}
                    self.arrAllChat[0] = ChatData(message: answer, fromMySide: false)
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.showingLoadingAnimation = false
                        self.genrating_Answer_Animation = true
                        self.chatTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                    }
                } else {
                    debugPrint("Error Occured -- \(String(describing: errorMessage))")
                    self.arrAllChat[0] = ChatData(message: "Sorry, I did not get that!\nCan you please ask again ?", fromMySide: false)
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.showingLoadingAnimation = false
                        self.genrating_Answer_Animation = true
                        self.chatTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                    }
                }
            }
        } else if !textMessage.hasText {
            appView.makeToast("Please Enter Question First!")
            view.endEditing(true)
        } else if textMessage.text == defaultPlaceHolder {
            appView.makeToast("Please Enter Valid Question!")
            view.endEditing(true)
        } else if textMessage.text == defaultWaitMessage {
            appView.makeToast("Please Wait For EximGPT Answer!")
            view.endEditing(true)
        }
    }
}

extension EximGPTVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAllChat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentData = arrAllChat[indexPath.row]
        if currentData.fromMySide {
            let cell = tableView.dequeueReusableCell(withIdentifier: MyMessage_TableCell.identifier, for: indexPath) as! MyMessage_TableCell
            cell.init_MySide_Message(message: currentData.message, time: currentData.time)
            if cell.transform.isIdentity {
                cell.transform = cell.transform.rotated(by: .pi)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: GPTMessage_TableCell.identifier, for: indexPath) as! GPTMessage_TableCell
            if showingLoadingAnimation {
                textMessage.isEditable = false
                textMessage.text = defaultWaitMessage
                textMessage.textColor = UIColor.ourAppThemeColor.withAlphaComponent(0.5)
                tableView.isScrollEnabled = false
                cell.show_Loading_Dot_Animation()
            } else {
                cell.init_GPTSide_Message(index: indexPath.row, message: currentData.message, time: currentData.time, firstTimeLoading: genrating_Answer_Animation) { [weak self] in
                    guard let self = self else { return }
                    self.textMessage.isEditable = true
                    self.textMessage.text = self.defaultPlaceHolder
                    self.textMessage.textColor = UIColor.ourAppThemeColor.withAlphaComponent(0.5)
                    self.genrating_Answer_Animation = false
                    tableView.isScrollEnabled = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                        self?.textMessage.becomeFirstResponder()
                    }
                }
            }
            if cell.transform.isIdentity {
                cell.transform = cell.transform.rotated(by: .pi)
            }
            return cell
        }
    }
}

// MARK: - TEXTVIEW DELEGATE METHODS
extension EximGPTVC: UITextViewDelegate {
    func adjustTextViewHeight() {
        let fixedWidth = textMessage.frame.size.width
        let newSize = textMessage.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        if newSize.height > getDynamicHeight(size: 80) {
            textMessage.isScrollEnabled = true
            textMessage.layer.applyShadowAndRadius(cornerRadius: getDynamicHeight(size: 10), opacity: 0.5, shadowRadius: 2)
        } else {
            textMessage.isScrollEnabled = false
            textMessage_HeightConstraint.constant = newSize.height
            textMessage.layer.applyShadowAndRadius(cornerRadius: textMessage.frame.height/2, opacity: 0.5, shadowRadius: 2)
        }
        view.layoutIfNeeded()
        view.setNeedsLayout()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.adjustTextViewHeight()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if !textMessage.text!.isEmpty && textMessage.text! == defaultPlaceHolder {
            textMessage.text = ""
            textMessage.textColor = UIColor.ourAppThemeColor
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textMessage.text.isEmpty {
            textMessage.text = defaultPlaceHolder
            textMessage.textColor = UIColor.ourAppThemeColor.withAlphaComponent(0.5)
        }
    }
}

// MARK: - KEYBOARD HIDE/SHOW NOTIFICATION CENTER ACTIONS
extension EximGPTVC {
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
                self.textStack_BottomConstraint.constant = keyboardSize.height - appView.safeAreaInsets.bottom + 10
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.textStack_BottomConstraint.constant = 10
        self.view.layoutIfNeeded()
    }
}
