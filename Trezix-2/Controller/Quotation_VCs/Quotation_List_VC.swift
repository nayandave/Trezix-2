//
//  Quotation_List_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 16/08/23.
//

import UIKit

class Quotation_List_VC: UIViewController {
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var segImportExport: UISegmentedControl!
    private var bottomSegmentBar: UIView! = nil
    
    @IBOutlet weak var dropStatus: DropDown!
    @IBOutlet weak var list_TableView: UITableView!
    
    @IBOutlet weak var noDataView: UIView!
    
    // MARK: - All Constants & Variables
    
    private var quoteModule: All_Module_Sorting = .modImport
    private var quoteStatus: All_Status_Sorting = .all
    
    private var arrQuotationList = [Quotation_List_Data]()
    private var showableData = [Quotation_List_Data]()
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initing_VC()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        noDataView.layer.applyShadowAndRadius()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        let frame = bottomSegmentBar.frame.origin.x
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            UIView.transition(with: self.bottomSegmentBar, duration: 0) {
                self.bottomSegmentBar.frame.origin.x = frame
            }
        }
    }
    
    private func initing_VC() {
        list_TableView.setSectionPadding_iOS_15(withBottomSafety: true)
        list_TableView.register(UINib(nibName: approvalListTableCell.identifier, bundle: nil), forCellReuseIdentifier: approvalListTableCell.identifier)
        setUpSegment()
        init_Dropdown()
        call_All_Quotation_List_API()
    }
    
    private func init_Dropdown() {
        dropStatus.arrowColor = .ourAppThemeColor
        dropStatus.hideOptionsWhenSelect = true
        dropStatus.isSearchEnable = false
        dropStatus.handleKeyboard = true
        dropStatus.checkMarkEnabled = false
        dropStatus.selectedIndex = 0
        
        dropStatus.selectedRowColor = .ourAppThemeColor
        dropStatus.selectedIndex = 0
        dropStatus.rowHeight = getDynamicHeight(size: 30)
        dropStatus.optionArray = All_Status_Sorting.allCases.compactMap { $0.rawValue }
        dropStatus.text = All_Status_Sorting.all.rawValue
        dropStatus.selectedIndex = 0
        quoteStatus = .all
        
        dropStatus.didSelect { [weak self] (selectedText, index, id) in
            guard let self = self else { return }
            self.quoteStatus = All_Status_Sorting.allCases.first { $0.rawValue == selectedText } ?? .all
            self.change_Sorting(mod: self.quoteModule, status: self.quoteStatus)
            UIView.animate(withDuration: 0.1, delay: 0) {
                self.dropStatus.invalidateIntrinsicContentSize()
            }
        }
    }
    
    
    ///*Change UI & calls required methods, When Import or Export Section is clicked*
    private func setUpSegment() {
//        UILabel.appearanceWhenContainedInInstancesOfClasses([UISegmentedControl.self]).numberOfLines = 0
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 0

        for segmentViews in segImportExport.subviews {
            for segmentLabel in segmentViews.subviews {
                if segmentLabel is UILabel {
                    debugPrint("Label Name = \(segmentLabel)")
                    (segmentLabel as! UILabel).numberOfLines = 0
                }
            }
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            // TODO: - Make themeColor image and set it as below image to test proper transparency in segmentControl
            self.segImportExport.setBackgroundImage(UIImage(), for: .normal, barMetrics: .default)
            self.segImportExport.backgroundColor = .clear
            
            self.segImportExport.layer.backgroundColor = UIColor.ourAppThemeColor.cgColor
            self.segImportExport.tintColor = .clear
            
            self.segImportExport.setTitleTextAttributes([.font : UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 18))!,
                                                             .foregroundColor: UIColor.white], for: .selected)
            self.segImportExport.setTitleTextAttributes([.font : UIFont(name: AppCenturyFont.Bold.Name(), size: getDynamicHeight(size: 18))!,
                                                             .foregroundColor: UIColor.white.withAlphaComponent(0.75)], for: .normal)
        }

        bottomSegmentBar = UIView()
        bottomSegmentBar.translatesAutoresizingMaskIntoConstraints = false
        bottomSegmentBar.backgroundColor = .white
        
        navContentView.addSubview(bottomSegmentBar)
        bottomSegmentBar.topAnchor.constraint(equalTo: segImportExport.bottomAnchor).isActive = true
        bottomSegmentBar.heightAnchor.constraint(equalToConstant: getDynamicHeight(size: 3)).isActive = true
        bottomSegmentBar.leadingAnchor.constraint(equalTo: segImportExport.leadingAnchor).isActive = true
        bottomSegmentBar.widthAnchor.constraint(equalTo: segImportExport.widthAnchor, multiplier: 1/CGFloat(self.segImportExport.numberOfSegments), constant: 0).isActive = true
        self.view.layoutIfNeeded()
    }
    
    private func animate_Changing_Section(toImport: Bool?) {
        HapticFeedbackGenerator.simpleFeedback(type: .soft)
        if let toImport {
            quoteModule = toImport ? .modImport : .modExport
        } else {
            quoteModule = .modOtherService
        }
        change_Sorting(mod: quoteModule, status: quoteStatus)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            UIView.animate(withDuration: 0.2) {
                if let toImport {
                    self.bottomSegmentBar.frame.origin.x = toImport ? self.segImportExport.frame.minX : self.segImportExport.frame.midX-(self.bottomSegmentBar.bounds.width/2)
                } else {
                    self.bottomSegmentBar.frame.origin.x = self.segImportExport.frame.maxX-(self.bottomSegmentBar.bounds.width)
                }
                self.view.setNeedsDisplay()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    private enum All_Module_Sorting {
        case modImport
        case modExport
        case modOtherService
    }
    
    private enum All_Status_Sorting: String, CaseIterable {
        case all = "All"
        case statusOpen = "Open"
        case statusClose = "Close"
    }
    
    private func change_Sorting(mod: All_Module_Sorting, status: All_Status_Sorting) {
        showableData.removeAll()
        let ser = DispatchQueue(label: "Sorting", qos: .userInitiated, attributes: .concurrent)
        ser.sync {
            switch mod {
            case .modImport:
                showableData = arrQuotationList.filter { $0.module == .modImport }
            case .modExport:
                showableData = arrQuotationList.filter { $0.module == .modExport }
            case .modOtherService:
                showableData = arrQuotationList.filter { $0.module == .empty }
            }
            
            switch status {
            case .statusOpen:
                showableData = showableData.filter { $0.status == .statusOpen }
            case .statusClose:
                showableData = showableData.filter { $0.status == .statusClose }
            case .all:
                debugPrint("No Addition Code Change Needed!")
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.set_UI(withData: !self.showableData.isEmpty)
            }
        }
    }
    
    private func set_UI(withData: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.list_TableView.isHidden = !withData
            self.noDataView.isHidden = withData
            if withData {
                debugPrint("table reloaded!")
                self.list_TableView.reloadData() }
        }
    }
    
    
    private func call_All_Quotation_List_API(isRefreshing: Bool = false) {
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.QUOTATION_LIST.fullPath(),
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Quotation_List_Modal?, errorMessage) in
            guard let self = self else { return }
            Loader.hideLoader()
            debugPrint("Quotation List API DASH SUCCESS == \(isSuccess) REASON = \(String(describing: errorMessage))")
            if isSuccess, let response {
                guard let data = response.data else {
                    self.set_UI(withData: false)
                    return
                }
                self.arrQuotationList = data
                if isRefreshing { HapticFeedbackGenerator.notificationFeedback(type: .success) }
                self.change_Sorting(mod: .modImport, status: .all)
            } else {
                HapticFeedbackGenerator.notificationFeedback(type: .warning)
                debugPrint("Error Fetching Quotation List Data!")
                self.set_UI(withData: false)
            }
        }
    }
    
    // MARK: - All Actions
    @IBAction func back_Button_Action(_ sender: UIButton) {
        guard let nav = navigationController else {
            navigationController?.popViewController(animated: true)
            return
        }
        nav.popViewController(animated: true)
    }
    
    ///**Segment Change**
    @IBAction func changeSegmentSection(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            animate_Changing_Section(toImport: true)
        case 1:
            animate_Changing_Section(toImport: false)
        case 2:
            animate_Changing_Section(toImport: nil)
        default:
            debugPrint("Warning!!!!!!\nDefault Value Selected!\nMay Cause Some Errors")
            HapticFeedbackGenerator.simpleFeedback(type: .medium)
        }
    }
}
// MARK: - TableView Delegate Methods
extension Quotation_List_VC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        showableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: approvalListTableCell.identifier, for: indexPath) as! approvalListTableCell
        let data = showableData[indexPath.row]
        cell.isCell_Quotation_List = true
        let backColorView = cell.shadowContentView.subviews.first(where: { $0.restorationIdentifier == "TempBackShadowView" })
        if data.status == .statusOpen {
            if backColorView == nil {
                let backView = UIView()
                backView.restorationIdentifier = "TempBackShadowView"
                backView.backgroundColor = UIColor.ourAppThemeColor.withAlphaComponent(0.15)
                cell.shadowContentView.insertSubview(backView, at: 1)
                backView.translatesAutoresizingMaskIntoConstraints = false
                backView.leadingAnchor.constraint(equalTo: cell.shadowContentView.leadingAnchor).isActive = true
                backView.trailingAnchor.constraint(equalTo: cell.shadowContentView.trailingAnchor).isActive = true
                backView.topAnchor.constraint(equalTo: cell.shadowContentView.topAnchor).isActive = true
                backView.bottomAnchor.constraint(equalTo: cell.shadowContentView.bottomAnchor).isActive = true
                backView.layer.cornerRadius = 8
                cell.setNeedsDisplay()
                cell.setNeedsLayout()
            }
        } else {
            if let backColorView { backColorView.removeFromSuperview() }
        }
        
        cell.setAllHeadings(heading1: "Title",
                            heading2: "Bid Date",
                            heading3: "Due Date",
                            heading4: "Bid Count")
        cell.setAllValues(value1: data.title.non_NilEmpty(),
                          value2: data.bidDate.non_NilEmpty(),
                          value3: data.dueDate.non_NilEmpty(),
                          value4: data.bidCount.non_NilEmpty())
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    { UITableView.automaticDimension }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let detailVC = quotationStoryboard.instantiateViewController(withIdentifier: "Quotation_Detail_VC") as! Quotation_Detail_VC
        detailVC.quoteID = showableData[indexPath.row].id
        if quoteModule != .modOtherService {
            detailVC.isImportMod = quoteModule == .modImport
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}
