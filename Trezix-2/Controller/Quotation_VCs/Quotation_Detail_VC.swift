//
//  Quotation_Detail_VC.swift
//  Trezix-2
//
//  Created by Amar Panchal on 24/08/23.
//

import UIKit
import Lottie

class Quotation_Detail_VC: UIViewController {
    
    // MARK: - All Outlets
    @IBOutlet weak var navContentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    // All Section Backgroundsss
    @IBOutlet weak var poDetailSectionView: UIView!
    @IBOutlet weak var bidDetailSectionView: UIView!
    @IBOutlet weak var partenerDetailSectionView: UIView!
    @IBOutlet weak var attributeDetailSectionView: UIView!
    @IBOutlet weak var notesSectionView: UIView!
    
    @IBOutlet weak var lblPOSOHeading: UILabel!
    @IBOutlet weak var lblPOSOValue: UILabel!
    @IBOutlet weak var lblVendorInvoice: UILabel!
    @IBOutlet weak var lblVendor: UILabel!
    
    @IBOutlet weak var lblBidDate: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    
    @IBOutlet weak var tablePartners: UITableView!
    @IBOutlet weak var noPartnerDataView: UIView!
    @IBOutlet weak var tablePartner_Height: NSLayoutConstraint!
    
    @IBOutlet weak var stackView_Attributes: UIStackView!
    @IBOutlet weak var stackAttribute_Height: NSLayoutConstraint!
    @IBOutlet weak var tableAttributes: UITableView!
    @IBOutlet weak var collectionPartner_Attribute: UICollectionView!
    @IBOutlet weak var noAttributeDataView: UIView!
    
    @IBOutlet weak var lblPartnerType: UILabel!
    @IBOutlet weak var lblNotes: UILabel!
    
    // MARK: - All Constants & Variables
    public var quoteID = Int()
    public var isImportMod: Bool? = nil
    
    private var quoteData: Quotation_Detail_Data! = nil
    private var quoteAttributeData = [[Quotation_Additional_Attribute]]()
    private var isWinnerSelectable = false
    
    private var arrAllPartnerNames = [String]()
    
    private var arrAttriHeadings = [String]()
    private var dictAttributeClass = [String : [String : String]]()
    private var dictPartnerWinner = [String : (Int,Bool)]()
    
    // MARK: - View Life Cycle Method
    private var animationView: LottieAnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        init_VC()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        [noPartnerDataView, noAttributeDataView].forEach { noDataView in
            noDataView?.layer.applyShadowAndRadius()
        }
        [poDetailSectionView,
         bidDetailSectionView,
         partenerDetailSectionView,
         attributeDetailSectionView,
         notesSectionView].forEach { view in
            view?.layer.masksToBounds = true
            view?.layer.cornerRadius = 8
            if (isImportMod != nil && view == poDetailSectionView) || view == bidDetailSectionView {
                view?.add_ShadowView(cornerRadius: 8)
            }
        }
    }
    
    // MARK: - Helper Methods
    private func init_VC() {
        [tablePartners, tableAttributes].forEach { tableView in
            tableView?.register(UINib(nibName: QuotationDetailCell.identifier, bundle: nil), forCellReuseIdentifier: QuotationDetailCell.identifier)
            tableView?.setSectionPadding_iOS_15()
        }
        collectionPartner_Attribute.register(UINib(nibName: QuoteAttrubute_CollCell.identifier, bundle: nil), forCellWithReuseIdentifier: QuoteAttrubute_CollCell.identifier)
        call_Detail_Quotation_API()
    }
    
    private func setUp_UI() {
        tablePartner_Height.constant = arrAllPartnerNames.count == 0 ? (getDynamicHeight(size: 60)) : (CGFloat(arrAllPartnerNames.count+1) * getDynamicHeight(size: 45).rounded(.awayFromZero))
        stackAttribute_Height.constant = arrAttriHeadings.count == 0 ? getDynamicHeight(size: 60) : ((CGFloat(arrAttriHeadings.count+2) * getDynamicHeight(size: 45).rounded(.awayFromZero)))
        
        tablePartners.layer.cornerRadius = 8
        tablePartners.layer.masksToBounds = true
        partenerDetailSectionView.add_ShadowView(cornerRadius: 8)
        
        stackView_Attributes.layer.cornerRadius = 8
        stackView_Attributes.layer.masksToBounds = true
        attributeDetailSectionView.add_ShadowView(cornerRadius: 8)
        
        animationView = .init(name: "slideleft")
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .repeat(3)
        animationView.animationSpeed = 1.0
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            self.tablePartners.isHidden = self.arrAllPartnerNames.isEmpty
            self.noPartnerDataView.isHidden = !self.arrAllPartnerNames.isEmpty
            
            self.stackView_Attributes.isHidden = self.arrAttriHeadings.isEmpty
            self.noAttributeDataView.isHidden = !self.arrAttriHeadings.isEmpty
            
            self.tablePartners.reloadData()
            self.tableAttributes.reloadData()
            
            self.collectionPartner_Attribute.reloadData()
            
            if !self.tablePartners.isHidden {
                self.tablePartners.add_ShadowView()
            }
            
            if !self.stackView_Attributes.isHidden {
                self.stackView_Attributes.add_ShadowView()
                self.animationView.frame = self.collectionPartner_Attribute.frame
                self.stackView_Attributes.addSubview(self.animationView)
                self.animationView.play { [weak self] completed in
                    guard let self = self else { return }
                    self.animationView.stop()
                    self.animationView.removeFromSuperview()
                }
            }
        }
    }
    
    private func setUp_UI_With_Data() {
        lblTitle.text = quoteData.title.non_NilEmpty()
        // Some dynamic fields are remaining
        if let isImportMod {
            poDetailSectionView.isHidden = false
            lblPOSOHeading.text = isImportMod ? "PO No : " : "SO No : "
            lblPOSOValue.text = isImportMod ? quoteData.poNo.non_NilEmpty() : quoteData.soNo.non_NilEmpty()
        } else {
            poDetailSectionView.isHidden = true
        }
        lblVendor.text = quoteData.vendor.non_NilEmpty()
        lblVendorInvoice.text = quoteData.vendorInvoice.non_NilEmpty()
        
        lblBidDate.text = quoteData.bidDate.non_NilEmpty()
        lblDueDate.text = quoteData.dueDate.non_NilEmpty()
        lblPartnerType.text = quoteData.partnerType.non_NilEmpty()
        
        lblNotes.text = quoteData.notes.non_NilEmpty()
        notesSectionView.add_ShadowView(cornerRadius: 8)
    }
    
    private func call_Detail_Quotation_API(winnerPartnerID: Int? = nil) {
        var param = ["quotation_id": quoteID]
        if let winnerPartnerID {
            if winnerPartnerID == 0 {
                debugPrint("No Partner ID available...")
                appView.makeToast("You can not make this partner winner!")
            } else {
                param["partner_id"] = winnerPartnerID
            }
        }
        
        print("Param - \(param)")
        NetworkClient.encodedNetworkRequest(method: .post,
                                            apiName: APIConstants.QUOTATION_DETAIL.fullPath(),
                                            param: param,
                                            headers: [default_app_header],
                                            showHud: true)
        { [weak self] (isSuccess, response: Quotation_Detail_Modal? , errorMessage) in
            Loader.hideLoader()
            guard let self = self else { return }
            if isSuccess, let response {
                if let mainData = response.data {
                    HapticFeedbackGenerator.notificationFeedback(type: .success)
                    self.quoteData = mainData
                    self.isWinnerSelectable = mainData.selectWinner
                    
                    if let attributes = mainData.attributes {
                        self.quoteAttributeData = attributes.compactMap { $0.attributeClass }
                        
                        var diction = [String: [String: String]]()
                        var allHeadings = [String]()
                        
                        var temp = [String : String]()
                        var isPartnerWinner = [String : (Int,Bool)]()
                        
                        for att in attributes {
                            if let partID = att.partnerID, att.partner != "" {
                                if let extras = att.attributeClass {
                                    extras.forEach { attribute in
                                        temp[attribute.heading] = attribute.value
                                        if !allHeadings.contains(attribute.heading) { allHeadings.append(attribute.heading) }
                                    }
                                    isPartnerWinner[att.partner] = (partID, (att.winner == "1"))
                                    diction[att.partner] = temp
                                } else {
                                    diction[att.partner] = nil
                                }
                            }
                        }
                        self.dictPartnerWinner = isPartnerWinner
                        self.arrAttriHeadings = allHeadings
                        self.dictAttributeClass = diction
                    }
                    if let allPartners = mainData.allPartners {
                        self.arrAllPartnerNames = allPartners.compactMap {
                            $0.partnerName != "" ? $0.partnerName : nil
                        }
                    }
                    self.setUp_UI_With_Data()
                    self.setUp_UI()
                } else {    // API Called But Empty Data
                    HapticFeedbackGenerator.notificationFeedback(type: .warning)
                    debugPrint("Empty Data in Detail with message: \(response.message)")
                }
            } else {
                debugPrint("Error calling detail API \(errorMessage.non_NilEmpty())")
                appView.makeToast("Something Went Wrong!\nPlease Try Again Later!")
                HapticFeedbackGenerator.notificationFeedback(type: .error)
            }
        }
    }
    
    
    // MARK: - All Action Methods
    /// Back Button Action
    @IBAction func back_Button_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    /// Make Winner Button Action (Should work when `selectWinner` is `TRUE`)
    /// **Gets the partner ID from button tag and calls the quotation  detail API with that partner ID**
    @objc private func make_Winner_Action(_ sender: UIButton) {
        let partnerName = arrAllPartnerNames[sender.tag]
        guard let partDetail = dictPartnerWinner[partnerName] else {
            debugPrint("No Partner Detail Available.")
            return
        }
        call_Detail_Quotation_API(winnerPartnerID: partDetail.0)
    }
}
// MARK: - TableView Delegate & DataSource Methods
extension Quotation_Detail_VC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tablePartners {
            return arrAllPartnerNames.isEmpty ? 0 : arrAllPartnerNames.count+1
        } else {    // Attribute Table
            return arrAttriHeadings.isEmpty ? 0 : arrAttriHeadings.count+2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: QuotationDetailCell.identifier, for: indexPath) as! QuotationDetailCell
        if tableView == tablePartners {
            if indexPath.row == 0 {
                cell.isCellHeader(heading: "Partners")
            } else {
                cell.setCellValue(arrAllPartnerNames[indexPath.row-1])
                if indexPath.row == arrAllPartnerNames.count {
                    cell.separatorView.isHidden = true
                }
            }
        } else {
            if indexPath.row == 0 {
                cell.isCellHeader(heading: "Attribute Class")
            } else if indexPath.row == arrAttriHeadings.count+1 {
                cell.isCellFooter()
            } else {
                cell.setCellValue(arrAttriHeadings[indexPath.row-1].capitalized)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        getDynamicHeight(size: 45).rounded(.awayFromZero)
    }
    
}
extension Quotation_Detail_VC: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int { arrAllPartnerNames.count }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAttriHeadings.isEmpty ? 0 : arrAttriHeadings.count+2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: QuoteAttrubute_CollCell.identifier, for: indexPath) as! QuoteAttrubute_CollCell
        cell.delegate = self
        let partner = arrAllPartnerNames[indexPath.section]
        if indexPath.row == 0 {
            cell.isCellHeader(heading: partner)
            cell.configureSideButtons(currIndex: indexPath.section,
                                      totalCount: arrAllPartnerNames.count)
        } else if indexPath.row == arrAttriHeadings.count+1 {
            cell.isCellFooter(index: indexPath.section,
                              shouldSelectWinner: isWinnerSelectable,
                              isWinner: dictPartnerWinner[partner]?.1 ?? false)
            cell.btnWinner.addTarget(self, action: #selector(make_Winner_Action(_:)), for: .touchUpInside)
        } else {
            let allAttr = dictAttributeClass[partner]!
            let value = allAttr[arrAttriHeadings[indexPath.row-1]]
            cell.setAttribute(value)
        }
        return cell
    }
}
extension Quotation_Detail_VC: AttributeScrollDelegate {
    func scrollAttribute(currentIndex: Int, scrollLeft: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.collectionPartner_Attribute.scrollToItem(at: IndexPath(item: 0, section: scrollLeft ? currentIndex-1 : currentIndex+1), at: .top, animated: true)
        }
    }
}

extension Quotation_Detail_VC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionPartner_Attribute.bounds.width, height: getDynamicHeight(size: 45).rounded(.awayFromZero))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat { .zero }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat { .zero }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets { .zero }
}
