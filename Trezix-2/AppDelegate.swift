//
//  AppDelegate.swift
//  Trezix-2
//
//  Created by Amar Panchal on 20/06/22.
//

import UIKit
import Firebase
import AVKit
import IQKeyboardManagerSwift

let synth = AVSpeechSynthesizer()

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var isNetworkAvailable: Bool = false
    
    let gcmMessageIDKey = "gcm.message_id"
    
    var reachability = Reachability()!
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        ToastManager.shared.style.backgroundColor = .ourAppThemeColor
        createDownloadFolder()
        configureSideMenu()
        setupNetworkMonitoring { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.initialAppSetup(app: application)
//            if let shotcutItem = launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
//                currentShortcut = shotcutItem
//            }
        }
        return true
    }

    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}
//
//extension AppDelegate {
//    /// Handles Quick Shortcut and Redirects to respective ShortCut related View Controller
//    /// - Parameter shortcut : called from `willConnectTo` Scene Delegate Method
//    /// - Returns:Boolean value that indicates if shortcut is handled.
//    private func handleShortCutItem(shortcut: UIApplicationShortcutItem) -> Bool {
//        if let topVC = UIApplication.topViewController() {
//            if let mainNavController = topVC.children[0] as? UINavigationController {
//                if shortcut.type == AllQuickShortcutTypes.poReport.rawValue {
//                    if !mainNavController.children.last!.isKind(of: FormOrderReportVC.self) {
//                        let vc = allReportStoryboard.instantiateViewController(withIdentifier: "FormOrderReportVC")
//                        vc.modalPresentationStyle = .fullScreen
//                        mainNavController.present(vc, animated: true)
//                        return true
//                    }
//                } else if shortcut.type == AllQuickShortcutTypes.importDashboard.rawValue {
//                    if !mainNavController.children.last!.isKind(of: HomepageVC.self) {
//                        mainNavController.popToRootViewController(animated: true)
//                        return true
//                    }
//                } else if shortcut.type == AllQuickShortcutTypes.notification.rawValue {
//                    if !mainNavController.children.last!.isKind(of: Notification_List_VC.self) {
//                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "Notification_List_VC")
//                        vc.modalPresentationStyle = .fullScreen
//                        mainNavController.present(vc, animated: true)
//                        return true
//                    }
//                } else if shortcut.type == AllQuickShortcutTypes.shipmentReport.rawValue {
//
//                }
//            }
//        }
//        return false
//    }
//}

// MARK: - Initial Setup Of App.
extension AppDelegate {
    func isUpdateAvailable() throws -> (Bool, String) {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                throw VersionError.invalidBundleInfo
        }
        debugPrint("Current Version == \(currentVersion) --- identifier = \(identifier)")
        let data = try Data(contentsOf: url)
        
        do {
            let json = try JSON(data: data, options: .fragmentsAllowed)
            
            if let result = json["results"].array, let first = result.first?.dictionary, let version = first["version"]?.string {
                print("version in app store", version,currentVersion);
                return (version > currentVersion, version)
            } else {
                debugPrint("ELSE METHOD")
                throw VersionError.invalidResponse
            }
        } catch let errr {
            debugPrint("DUE TOMOK \(errr.localizedDescription)")
            throw VersionError.invalidResponse
        }
        
//        guard let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: Any] else {
//            throw VersionError.invalidResponse
//        }
//        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
//            print("ALL RESULT - \(result)")
//            print("version in app store", version,currentVersion);
//            return (version > currentVersion, version)
//        }
//        throw VersionError.invalidResponse
    }
    
    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }
    
    func initialAppSetup(app: UIApplication) {
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
    }
    
    private func configureSideMenu() {
        if UIScreen.main.traitCollection.horizontalSizeClass == .regular && UIScreen.main.traitCollection.verticalSizeClass == .regular {
            SideMenuController.preferences.basic.menuWidth = SCREEN_WIDTH*0.4
        } else {
            SideMenuController.preferences.basic.menuWidth = SCREEN_WIDTH*0.5
        }
        SideMenuController.preferences.basic.defaultCacheKey = "0"
        SideMenuController.preferences.basic.enablePanGesture = true
        SideMenuController.preferences.basic.enableRubberEffectWhenPanning = true
        SideMenuController.preferences.basic.position = .sideBySide
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.animation.hideDuration = 0.25
    }
    
    private func createDownloadFolder() {
        if !FileManager.default.fileExists(atPath: tempFolderPath.gettingPath()) {
            do {
                try FileManager.default.createDirectory(at: tempFolderPath, withIntermediateDirectories: true)
            } catch let err {
                debugPrint("Unable to create TEMP FOLDER due to \(err.localizedDescription)")
            }
        }
        
        if !FileManager.default.fileExists(atPath: allReportPath.gettingPath()) {
            do {
                try FileManager.default.createDirectory(atPath: allReportPath.gettingPath(), withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Unable to create directory \(error.debugDescription)")
            }
        }
        /// `.removingPercentEncoding` required to remove space replacement with %20 when creating folder.
        guard let poPath = poTimelineReportPath.gettingPath().removingPercentEncoding else {
            debugPrint("Caould not create PO TimeLine Path"); return
        }
        
        if !FileManager.default.fileExists(atPath: poPath) {
            do {
                try FileManager.default.createDirectory(atPath: poPath, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Unable to create directory \(error.debugDescription)")
            }
        }
    }
}

// MARK: - Network Reachability
extension AppDelegate {
    func setupNetworkMonitoring(whenReachable: (() -> Void)? = nil) {
        reachability.whenReachable = { reachability in
            DispatchQueue.main.async {
                if reachability.isReachableViaWiFi {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Cellular")
                }
                NoInternet.Manager.hideOfflinePopup()
                self.isNetworkAvailable = true
                whenReachable?()
            }
        }
        reachability.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                print("Not reachable")
                NoInternet.Manager.showNoInternetPopup()
                self.isNetworkAvailable = false
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            self.isNetworkAvailable = true
        } else {
            print("Network not reachable")
            self.isNetworkAvailable = false
        }
    }
}
// MARK: - Notification Methods
extension AppDelegate: UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let content = notification.request.content
        let message = content.title
        
        let utterance = AVSpeechUtterance(string: message)
        utterance.voice = AVSpeechSynthesisVoice(language: "hi-IN")
        utterance.rate = 0.5

        synth.speak(utterance)
        debugPrint("Message : \(message)")
        
        let userInfo = content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        if #available(iOS 14, *) {
            if message.isEmpty {
                completionHandler([.list, .banner, .badge, .sound])
            } else {
                completionHandler([.list, .banner, .badge])
            }
        } else {
            completionHandler([[.alert, .sound , .badge]])
        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        debugPrint("Recieved Remote Notification !!!!")
        completionHandler(.newData)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let content = response.notification.request.content
//        let message = content.title
//
//        let utterance = AVSpeechUtterance(string: message)
//        utterance.voice = AVSpeechSynthesisVoice(language: "en-IN")
//
//        let synth = AVSpeechSynthesizer()
//        synth.speak(utterance)
        
        
        if content.categoryIdentifier == notification_Category {
            switch response.actionIdentifier {
            case "MARK_AS_READ" : debugPrint("Notification Clicked 1")
            case "REMIND_LATER" : debugPrint("Notification Clicked 2")
            case "DISMISS" : debugPrint("Notification Clicked 3")
            default: debugPrint("\(response.actionIdentifier) Default Notification Clicked")
            }
        }
        
        if let messageID = content.userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        completionHandler()
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint("Notification is not registered due to : \(error.localizedDescription)")
    }
}

extension UNUserNotificationCenter {
    public func getPermission() {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        self.requestAuthorization(options: authOptions) { flag, error in
            debugPrint("Notification Permission Flag : \(flag)")
            if !flag {
                DispatchQueue.main.async { [weak self] in
                    guard self != nil else { return }
                    let warningAlert = UIAlertController(title: "Oops :(", message: "Looks like you haven't allowed notification access to us.\nKindly allow us to notify you important updates more quickly.\n promise we won't bother you often ;)", preferredStyle: .alert)
                    let changePerm = UIAlertAction(title: "Change Settings", style: .default) { _ in
                        if let url = URL.init(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                    warningAlert.addAction(changePerm)
                    warningAlert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
                    // show alert
                    appView.rootViewController?.present(warningAlert, animated: true)
                }
            }
            if (error != nil) {
                debugPrint("THIS IS ERROR : \(error!.localizedDescription)")
            }
        }
    }
}

extension AppDelegate: MessagingDelegate {
    // DidReceive FireBase Delegate Method
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        if let token = fcmToken {
            set_Device_Token_UD(token: token)
//            if getIsLoginUD() {
//                DispatchQueue.global(qos: .background).async { [weak self] in
//                    self?.updateDeviceToken()
//                    self?.add_Update_Admin_Device_Token()
//                }
//            }
            print("FCM:",token)
        }
    }
}
