//
//  notiTabCell.swift
//  Trezix-Demo1
//
//  Created by Amar Panchal on 16/06/22.
//

import UIKit

class notiTabCell: UITableViewCell {
    
    public static let identifier = "notiTabCell"
    
    // MARK: - ALL IB_OUTLETS
    @IBOutlet weak var shadowContentView: UIView!
    @IBOutlet weak var mainStack: UIStackView!
    
    @IBOutlet weak var lblNotificationTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Updates Notification Table Cells In Main Thread
    public func updateCellInfo(info: Notification_Modal) {
        
        let isRead = info.isRead == "1"
        
        let showableDate = DateHelper.shared.decodeDashboardDate(dateString: info.createdAt)
        let finalTime = getShowableDateString(from: showableDate)
        
        lblNotificationTitle.text = info.message
        lblTime.text = finalTime
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.shadowContentView.layer.applyShadowAndRadius(shadowRadius: 1)
            self.shadowContentView.backgroundColor = isRead ? .systemBackground : .secondarySystemBackground
        }
    }
    
    public func setApprovalNotiData(modal: Approval_Item_Data) {
        let isRead = true // modal.isRead == "1"
        lblNotificationTitle.text = modal.moduleName ?? "N/A"
        lblTime.text = modal.date ?? "N/A"
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.shadowContentView.layer.applyShadowAndRadius(shadowRadius: 1)
            self.shadowContentView.backgroundColor = isRead ? .systemBackground : .secondarySystemBackground
        }
    }
}
extension notiTabCell {
    func getShowableDateString(from: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        
        let startDay = Int(formatter.string(from: from))!
        let endDay = Int(formatter.string(from: Date()))!
        
//        let difference = Calendar.current.dateComponents([.hour, .day], from: from, to: Date())
//
//        let days = abs(difference.day ?? 0)
//        let hours = abs(difference.hour ?? 0)
        
//        debugPrint("From Date \(from) and End Date \(Date()) Difference of Day \(days), and Hours \(hours)")
//
        var partialString = "N/A"
//        if days > 0 {
//            if days == 1 { partialString = "Yesterday"}
//        } else if hours > 0 {
//            if hours <= 24 { partialString = "Today"}
//        }
        if startDay == endDay {
            partialString = "Today"
        } else if endDay-startDay == 1 || startDay-endDay == 1 {
            partialString = "Yesterday"
        }
        
        let dateFormatter = DateFormatter()
        
        if partialString == "N/A" {
            dateFormatter.dateFormat = "dd MMMM yyyy, h:mm a"
            return dateFormatter.string(from: from)
        } else {
            dateFormatter.dateFormat = "h:mm a"
            let otherHalf = dateFormatter.string(from: from)
            return "\(partialString), \(otherHalf)"
        }
    }
}
