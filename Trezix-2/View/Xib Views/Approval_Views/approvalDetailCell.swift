//
//  approvalDetailCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 13/10/22.
//

import UIKit

class approvalDetailCell: UITableViewCell {

    @IBOutlet weak var shadowContantView: UIView!
    @IBOutlet weak var lblHashIndex: UILabel!
    @IBOutlet weak var lblHeadDescription: UILabel!
    @IBOutlet weak var lblHeadRate: UILabel!
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    
    @IBOutlet weak var lblHeadQuantity: UILabel!
    @IBOutlet weak var lblHeadAmount: UILabel!
    
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var stackMarkContainer: UIStackView!
    @IBOutlet weak var lblHeadMarkContainerNum: UILabel!
    @IBOutlet weak var lblMarkContainer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        shadowContantView.layer.applyShadowAndRadius()
    }
    
    public func fillAllDetails(index: Int, name: String, desc: String, rate: String, quantity: String, amount: String, ifNotPOMarkContainerNum: (Bool, String?) = (true, nil)) {
        DispatchQueue.main.async { [weak self] in
            self?.lblIndex.text = "\(index)"
            self?.lblItemName.text = name
            self?.lblDescription.text = desc
            self?.lblRate.text = rate
            self?.lblQuantity.text = quantity
            self?.lblAmount.text = amount
            if !ifNotPOMarkContainerNum.0  {
                self?.lblMarkContainer.text = ifNotPOMarkContainerNum.1
            }
        }
    }
    
    public func showHideMarkStackView(_ show: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            self?.stackMarkContainer.isHidden = !show
        }
    }
}
