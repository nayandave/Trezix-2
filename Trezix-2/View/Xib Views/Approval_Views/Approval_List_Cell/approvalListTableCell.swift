//
//  approvalListTableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 22/10/22.
//

import UIKit

class approvalListTableCell: UITableViewCell {
    
    @IBOutlet weak var shadowContentView: UIView!
    
    @IBOutlet weak var lblHeading1: UILabel!
    @IBOutlet weak var lblValue1: UILabel!
    
    @IBOutlet weak var lblHeading2: UILabel!
    @IBOutlet weak var lblValue2: UILabel!
    
    @IBOutlet weak var lblHeading3: UILabel!
    @IBOutlet weak var lblValue3: UILabel!
    
    @IBOutlet weak var lblHeading4: UILabel!
    @IBOutlet weak var lblValue4: UILabel!

    public static var identifier = "approvalListTableCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.shadowContentView.layer.applyShadowAndRadius()
        }
    }
    
    public func setAllHeadings(heading1: String, heading2: String, heading3: String, heading4: String) {
        lblHeading1.text = heading1
        lblHeading2.text = heading2
        lblHeading3.text = heading3
        lblHeading4.text = heading4
        layoutIfNeeded()
        setNeedsDisplay()
    }
    
    public func setAllValues(value1: String, value2: String, value3: String, value4: String) {
        lblValue1.text = value1
        lblValue2.text = value2
        lblValue3.text = value3
        lblValue4.text = value4 == "" ? "-" : value4
        layoutIfNeeded()
        setNeedsDisplay()
    }
    
    public var isCell_Quotation_List : Bool = false {
        didSet {
            if isCell_Quotation_List {
                lblValue1.numberOfLines = 0
                [lblValue2, lblValue3, lblValue4].forEach { label in
                    label?.numberOfLines = 1
                }
            }
        }
    }
}
