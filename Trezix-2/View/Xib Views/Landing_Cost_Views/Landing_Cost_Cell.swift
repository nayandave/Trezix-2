//
//  Landing_Cost_Cell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 04/01/23.
//

import UIKit

class Landing_Cost_Cell: UITableViewCell {

    static let identifier = "Landing_Cost_Cell"
    
    // MARK: - All Outlets
    @IBOutlet weak var shadowContentView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var stack1SeparatorView: UIView!
    @IBOutlet weak var lblValue: UILabel!
    
    @IBOutlet weak var secondStackView: UIStackView!
    @IBOutlet weak var mainSeparatorView: UIView!
    
    @IBOutlet weak var lblHeading2: UILabel!
    @IBOutlet weak var stack2SeparatorView: UIView!
    @IBOutlet weak var lblValue2: UILabel!
    
    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSpaceConstraint: NSLayoutConstraint!
    
    
    // MARK: - All Constants & Variables
    var landingCostData: Landing_Cost_Invoice_Data! = nil
    let arrLandingCostReportHeading1 = ["BE Ex. Rate",
                                        "Bank Charges",
                                        "DEEC",
                                        "BCD",
                                        "CVD",
                                        "SAD",
                                        "IGST",
                                        "Total USD",        ///Currency will be dynamically addded at setUp Method
                                        "Total INR : ",
                                        "1 Quantity USD" ,      ///Currency will be dynamically addded at setUp Method
                                        "1 Quantity INR : "]
    let arrLandingCostReportHeading2 = ["Cess of CVD",
                                        "Edv cess",
                                        "Anti Dumping",
                                        "Penalty",
                                        "Other",
                                        "Sec Educ Cess",
                                        "Social Welfare",
                                        "Total USD (With GST)",     ///Currency will be dynamically addded at setUp Method
                                        "Total INR (With GST) : ",
                                        "1 Quantity USD (With GST)",     ///Currency will be dynamically addded at setUp Method
                                        "1 Quantity INR (With GST) : "]
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        [stack1SeparatorView, stack2SeparatorView].forEach { sep in
            sep?.isHidden = false
        }
        self.backgroundColor = UIColor.ourAppThemeColor.withAlphaComponent(0.1)
    }
    
    // MARK: - Landing Cost Report Cell Initialization
    public func setLandingCostReportInfo(forRow: Int) {
        [stack1SeparatorView, stack2SeparatorView].forEach { sep in
            sep?.isHidden = false
        }
        self.secondStackView.isHidden = false
        
        var otherCount = 0
        var mainOtherData = [Landing_Cost_OtherCharges_Data]()
        
        if let allOtherData = landingCostData.otherCharges {
            mainOtherData = allOtherData.filter { $0.pvalue != nil }
            otherCount = mainOtherData.count
        }
        
        if forRow*2 < otherCount {
            if forRow == 0 {
                topSpaceConstraint.constant = 10
                bottomSpaceConstraint.constant = 0
                shadowContentView.updateConstraints()
                DispatchQueue.main.async { [weak self] in
                    self?.shadowContentView.layer.applyShadowAndRadius(corners: [.topLeft, .topRight])
                }
            } else {
                topSpaceConstraint.constant = 0
                bottomSpaceConstraint.constant = 0
                shadowContentView.updateConstraints()
                DispatchQueue.main.async { [weak self] in
                    self?.shadowContentView.layer.applyShadowAndRadius(cornerRadius: 0)
                }
            }
            lblHeading.text = mainOtherData[forRow*2].ptype ?? "N/A" ; lblValue.text = mainOtherData[forRow*2].pvalue ?? "N/A" ;
            let evenSec = (forRow*2)+1
            if evenSec < otherCount {
                lblHeading2.text = mainOtherData[evenSec].ptype ?? "N/A" ; lblValue2.text = mainOtherData[evenSec].pvalue ?? "N/A" ;
            } else {
                secondStackView.isHidden = true
            }
        } else {
            let currRow = forRow - (otherCount % 2 == 0 ? otherCount/2 : (otherCount+1)/2)
  
            if currRow == 0 && otherCount == 0 {
                topSpaceConstraint.constant = 10
                bottomSpaceConstraint.constant = 0
                shadowContentView.updateConstraints()
                DispatchQueue.main.async { [weak self] in
                    self?.shadowContentView.layer.applyShadowAndRadius(corners: [.topLeft, .topRight])
                }
            } else {
                topSpaceConstraint.constant = 0
                bottomSpaceConstraint.constant = 0
                shadowContentView.updateConstraints()
                DispatchQueue.main.async { [weak self] in
                    self?.shadowContentView.layer.applyShadowAndRadius(cornerRadius: 0)
                }
            }
            switch currRow {
            case 0:
                lblHeading.text = arrLandingCostReportHeading1[0]; lblValue.text = landingCostData.beExchangeRate ?? "N/A";
                lblHeading2.text = arrLandingCostReportHeading2[0]; lblValue2.text = landingCostData.cessOfCvd ?? "N/A";
            case 1:
                lblHeading.text = arrLandingCostReportHeading1[1]; lblValue.text = landingCostData.bankCharges ?? "N/A";
                lblHeading2.text = arrLandingCostReportHeading2[1]; lblValue2.text = landingCostData.edvCess ?? "N/A";
            case 2:
                lblHeading.text = arrLandingCostReportHeading1[2]; lblValue.text = landingCostData.deec ?? "N/A";
                lblHeading2.text = arrLandingCostReportHeading2[2]; lblValue2.text = landingCostData.antiDumping ?? "N/A";
            case 3:
                lblHeading.text = arrLandingCostReportHeading1[3]; lblValue.text = landingCostData.bcd ?? "N/A";
                lblHeading2.text = arrLandingCostReportHeading2[3]; lblValue2.text = landingCostData.penalty ?? "N/A";
            case 4:
                lblHeading.text = arrLandingCostReportHeading1[4]; lblValue.text = landingCostData.cvd ?? "N/A"
                lblHeading2.text = arrLandingCostReportHeading2[4]; lblValue2.text = landingCostData.other ?? "N/A";
            case 5:
                lblHeading.text = arrLandingCostReportHeading1[5]; lblValue.text = landingCostData.sad ?? "N/A";
                lblHeading2.text = arrLandingCostReportHeading2[5]; lblValue2.text = landingCostData.secEducCess ?? "N/A";
            case 6:
                lblHeading.text = arrLandingCostReportHeading1[6]; lblValue.text = landingCostData.igst ?? "N/A";
                lblHeading2.text = arrLandingCostReportHeading2[6]; lblValue2.text = landingCostData.socialWelfare ?? "N/A";
            case 7:
                [stack1SeparatorView, stack2SeparatorView].forEach { sep in
                    sep?.isHidden = true
                }
                lblHeading.text = "Total \(landingCostData.currncy!) : "; lblValue.text = landingCostData.finalTotal ?? "N/A";
                lblHeading2.text = "Total \(landingCostData.currncy!) (With GST) : "; lblValue2.text = landingCostData.finalTotalWithGst ?? "N/A";
            case 8:
                [stack1SeparatorView, stack2SeparatorView].forEach { sep in
                    sep?.isHidden = true
                }
                lblHeading.text = arrLandingCostReportHeading1[8]; lblValue.text = landingCostData.totalInr ?? "N/A";
                lblHeading2.text = arrLandingCostReportHeading2[8]; lblValue2.text = landingCostData.totalInrWithGst ?? "N/A";
            case 9:
                [stack1SeparatorView, stack2SeparatorView].forEach { sep in
                    sep?.isHidden = true
                }
                lblHeading.text = "1 Quantity \(landingCostData.currncy!) : "; lblValue.text = landingCostData.quantityOther ?? "N/A";
                lblHeading2.text = "1 Quantity \(landingCostData.currncy!) (With GST) : "; lblValue2.text = landingCostData.quantityOtherGst ?? "N/A";
            case 10:
                [stack1SeparatorView, stack2SeparatorView].forEach { sep in
                    sep?.isHidden = true
                }
                
                lblHeading.text = arrLandingCostReportHeading1[10]; lblValue.text = landingCostData.quantityInr ?? "N/A";
                lblHeading2.text = arrLandingCostReportHeading2[10]; lblValue2.text = landingCostData.quantityInrGst ?? "N/A";
                
                bottomSpaceConstraint.constant = 10
                topSpaceConstraint.constant = 0
                shadowContentView.updateConstraints()
                self.separatorInset = UIEdgeInsets(top: 0, left: SCREEN_WIDTH, bottom: 0, right: 0) //To make last cell's separator insible
                DispatchQueue.main.async { [weak self] in
                    self?.shadowContentView.layer.applyShadowAndRadius(corners: [.bottomLeft, .bottomRight])
                }
            default: lblHeading.text = "Unavailable"; lblValue.text = "Unavailable"
            }
        }
    }
}
