//
//  Landing_Cost_HeaderView.swift
//  Trezix-2
//
//  Created by Amar Panchal on 26/12/22.
//

import UIKit

class Landing_Cost_HeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var shadowBackgroundView: UIView!
    @IBOutlet weak var shadowContentView: UIView!
    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblProductCodeHeading: UILabel!
    @IBOutlet weak var lblProductNameHeading: UILabel!
    
    @IBOutlet weak var lblQuantityHeading: UILabel!
    @IBOutlet weak var lblPriceHeading: UILabel!
    @IBOutlet weak var lblTotalHeading: UILabel!
    @IBOutlet weak var lblCurrencyHeading: UILabel!
    
    @IBOutlet weak var moreDetailStackView: UIStackView!
    
    @IBOutlet weak var lblProductCode: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    
    @IBOutlet weak var imageIndicator: UIImageView!
    
    @IBOutlet weak var separatorView: UIView!
    
    public var currentRow = Int()

    override func layoutSubviews() {
        super.layoutSubviews()
        shadowBackgroundView.layer.applyShadowAndRadius(cornerRadius: 0)
        if traitCollection.userInterfaceStyle == .dark {
            shadowBackgroundView.backgroundColor = UIColor.secondarySystemGroupedBackground
            shadowBackgroundView.alpha = 1
        } else {
            shadowBackgroundView.backgroundColor = .ourAppThemeColor
            shadowBackgroundView.alpha = 0.1
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.layoutSubviews()
    }
    
    public func isSectionOpenedClosed(open: Bool = false) {
        separatorView.isHidden = !open
        UIView.animate(withDuration: 0.15, delay: 0, options: [.curveLinear], animations: {
            self.bottomSpaceConstraint.constant = open ? 0 : 5
            self.moreDetailStackView.isHidden = !open
        }, completion: nil)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            UIView.transition(with: self.imageIndicator, duration: 0.1, options: [.curveEaseInOut]) {
                self.imageIndicator.transform = open ? CGAffineTransform(rotationAngle: -.pi/2) : .identity
            }
        }
    }
    
    public func setAllData(productCode: String?, productName: String?, quantity: String?, price: String?, total: String?, currency: String?, tag: Int) {
        currentRow = tag
        lblProductCode.text = productCode ?? "N/A"
        lblProductName.text = productName.non_NilEmpty()  // (productName ?? "N/A") == "" ? "N/A" : (productName ?? "N/A")
        
        lblQuantity.text = quantity ?? "N/A"
        lblPrice.text = price ?? "N/A"
        lblTotal.text = total ?? "N/A"
        lblCurrency.text = currency ?? "N/A"
    }
}
