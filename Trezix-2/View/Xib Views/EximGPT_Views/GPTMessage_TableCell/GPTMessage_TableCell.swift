//
//  GPTMessage_TableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 04/04/23.
//

import UIKit

class GPTMessage_TableCell: UITableViewCell {
    
    static let identifier = "GPTMessage_TableCell"
    
    @IBOutlet weak var msgContentView: UIView!
    @IBOutlet weak var msgContentView_Height: NSLayoutConstraint!
    @IBOutlet weak var msgContentView_Width: NSLayoutConstraint!
    
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    
    
    var updateChatLoading: (() -> Void)?
    public var index = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func show_Loading_Dot_Animation() {
        DispatchQueue.main.async { [weak self] in
            self?.mainStackView.isHidden = true
            self?.layoutIfNeeded()
            self?.setNeedsDisplay()
            self?.applyShadowTo_MessageContentView(true)
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                showAnimatingDotsInImageView(superView: self.msgContentView)
            }
        }
    }
    
    
    func applyShadowTo_MessageContentView(_ whileLoading: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if whileLoading {
                self.msgContentView.layer.applyShadowAndRadius(cornerRadius: self.msgContentView.frame.height/2, opacity: 0.75, shadowRadius: 2)
            } else {
                self.msgContentView.layer.applyShadowAndRadius(cornerRadius: min(self.frame.width, self.frame.height) >= 50 ? getDynamicHeight(size: 15) : (0.188 * min(self.frame.width, self.frame.height)), opacity: 0.75, shadowRadius: 2)
            }
        }
    }
    
    func init_GPTSide_Message(index: Int, message: String, time: String?, firstTimeLoading: Bool = false, completion: (() -> Void)?) {
        removeDotAnimation(superView: msgContentView)
        msgContentView_Height?.isActive = false
        msgContentView_Width?.isActive = false
        mainStackView.isHidden = false
        self.index = index
        if firstTimeLoading {
            //            DispatchQueue.main.async {
            self.lblMessage.alpha = 0
            self.lblMessage.text = message
            //                self.lblMessage.setTyping(text: message)
            DispatchQueue.main.async {
                self.show_Typing_Animation(fullText: message) {
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.lblMessage.alpha = 1
                        self.applyShadowTo_MessageContentView()
                        self.layoutIfNeeded()
                        self.setNeedsDisplay()
                        self.layoutSubviews()
                    }
                } completion: {
                    completion?()
                }
            }
        } else {
            lblMessage.text = message
            layoutIfNeeded()
            setNeedsDisplay()
            completion?()
        }
        lblTime.isHidden = time.isNilOrEmpty
        if !time.isNilOrEmpty { lblTime.text = time! }
    }
    
    func show_Typing_Animation(fullText: String, interval: TimeInterval = 0.01, callBackAfterCharacterInsertion:(()->())?, completion: (()->Void)? = nil) {
        
        self.lblMessage.text = ""
        
        let group = DispatchGroup()
        group.enter()
        
        DispatchQueue.global(qos: .userInteractive).async {
            
        allChar: for (_, character) in fullText.enumerated() {
            
            DispatchQueue.main.async {
                self.lblMessage.text = self.lblMessage.text! + String(character)
                callBackAfterCharacterInsertion?()
            }
            
            Thread.sleep(forTimeInterval: interval)
        }
            
            group.leave()
        }
        
        group.notify(queue: .main) {
            //do something
            completion?()
        }
    }
    
    
    
}

//extension UILabel {
//    ///`Typing Letter By Letter Animation`
//    func setTyping(text: String, characterDelay: TimeInterval = 1.0) {
//        self.text = ""
//
//        let writingTask = DispatchWorkItem { [weak self] in
//            text.forEach { char in
//                DispatchQueue.main.async {
//                    self?.text?.append(char)
//                    self?.layoutIfNeeded()
//                    self?.setNeedsDisplay()
//                }
//                Thread.sleep(forTimeInterval: characterDelay/100)
//            }
//        }
//
//        let queue: DispatchQueue = .init(label: "typespeed", qos: .userInteractive)
//        queue.asyncAfter(deadline: .now() + 0.05, execute: writingTask)
//    }
//
//    func setTextWithTypeAnimation(typedText: String, characterInterval: TimeInterval = 0.01, callBackAfterCharacterInsertion:(()->())?, completion: (()->Void)? = nil) {
//
//      text = ""
//
//      let group = DispatchGroup()
//      group.enter()
//
//      DispatchQueue.global(qos: .userInteractive).async {
//
//      allChar: for (_, character) in typedText.enumerated() {
//
//            DispatchQueue.main.async {
//                self.text = self.text! + String(character)
//                callBackAfterCharacterInsertion?()
//            }
//
//          break allChar
//            Thread.sleep(forTimeInterval: characterInterval)
//        }
//
//        group.leave()
//      }
//
//      group.notify(queue: .main) {
//        //do something
//          completion?()
//      }
//    }
//}

