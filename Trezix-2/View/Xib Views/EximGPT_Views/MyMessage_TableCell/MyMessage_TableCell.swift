//
//  MyMessage_TableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 04/04/23.
//

import UIKit

class MyMessage_TableCell: UITableViewCell {
    
    static let identifier = "MyMessage_TableCell"
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var msgContentView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            //(0.15 * min(self!.frame.width, self!.frame.height)) Dynamic Corner Radius Does Not Work With Large Texts
            self.msgContentView.layer.applyShadowAndRadius(cornerRadius: min(self.frame.width, self.frame.height) >= 50 ? getDynamicHeight(size: 15) : (0.188 * min(self.frame.width, self.frame.height)), opacity: 0.75, shadowRadius: 2)
        }
    }
    
    func init_MySide_Message(message: String, time: String?) {
        lblMessage.text = message
        lblTime.isHidden = time.isNilOrEmpty
        if !time.isNilOrEmpty { lblTime.text = time! }
        layoutIfNeeded()
        setNeedsDisplay()
        layoutSubviews()
    }
}
