//
//  Upcoming_Event_TableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 10/01/23.
//

import UIKit

class Upcoming_Event_TableCell: UITableViewCell {
    
    static let identifier = "Upcoming_Event_TableCell"
    
    @IBOutlet weak var shadowContentView: UIView!
    
    @IBOutlet weak var dayContainerView: UIView!
    @IBOutlet weak var dayContainerStackView: UIStackView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    
    @IBOutlet weak var lblEvent: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()
        dayContainerView.layer.masksToBounds = false
        dayContainerStackView.layer.masksToBounds = false
        lblDate.layer.masksToBounds = false
        lblDay.layer.masksToBounds = false
        
        lblEvent.layer.masksToBounds = false
        shadowContentView.layer.cornerRadius = 8
        shadowContentView.layer.masksToBounds = true
        dayContainerView.layer.applyShadowAndRadius(corners: [.topLeft, .bottomLeft])
        shadowContentView.add_ShadowView()
    }
    
    public func setAllData(day: String?, date: String?, event: String?) {
        lblDate.text = date
        lblDay.text = day
        lblEvent.text = event ?? "N/A"
    }
}
