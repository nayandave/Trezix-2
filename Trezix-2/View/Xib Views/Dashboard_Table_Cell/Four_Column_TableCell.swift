//
//  Four_Column_TableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 09/01/23.
//

import UIKit

class Four_Column_TableCell: UITableViewCell {
    
    static let identifier = "Four_Column_TableCell"
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var lblData1: UILabel!
    @IBOutlet weak var lblData2: UILabel!
    @IBOutlet weak var lblData3: UILabel!
    @IBOutlet weak var lblData4: UILabel!
    
    @IBOutlet weak var firstLabelWidth: NSLayoutConstraint!
    
    public enum Import_Heading_Type {
        case InTransit
        case Pending
        case Closed
        case Licence
        case ShipmentTrack
    }
    
    public enum Export_Heading_Type {
        case OpenOrder
        case ShipmentPlan
        case CompleteOrder
        case DispatchPlan
        case ShipmentTrack
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        three_ColumnView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        mainStackView.layer.masksToBounds = false
        mainContentView.layer.masksToBounds = false
        self.layer.masksToBounds = true
    }
    
    /// Configuring Cell i.e. changing UI according to if cell is heading or not
    /// - Parameter heading: `true` if cell is heading & `false` if not
    /// - Parameter importHeadingType: ``Import_Heading_Type`` enum indicating type of heading from Import Section
    /// - Parameter exportHeadingType: ``Export_Heading_Type`` enum indicating type of heading from Export Section
    public func configureCellType(heading: Bool = true, importHeadingType: Import_Heading_Type? = nil, exportHeadingType: Export_Heading_Type? = nil, isEven: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainContentView.backgroundColor = heading ? .ourAppThemeColor.withAlphaComponent(0.15) : (isEven ? .systemBackground : .secondarySystemBackground)
            if heading {
                if importHeadingType != nil {
                    switch importHeadingType! {
                    case .InTransit:
                        self.lblData1.text = "Invoice No."
                        self.lblData2.text = "Date"
                        self.lblData3.text = "Quantity"
                        self.lblData4.text = "Amount"
                    case .Licence:
                        self.lblData1.text = "Licence No."
                        self.lblData2.text = "Validity"
                        self.lblData3.text = "Utilized"
                        self.lblData4.text = "Balance"
                    case .ShipmentTrack:
                        self.lblData1.text = "Vendor"
                        self.lblData2.text = "BL Number"
                        self.lblData3.text = "ETA"
                        // Fourth Column will be always hidden
                    case .Closed, .Pending:
                        self.lblData1.text = "PO No."
                        self.lblData2.text = "Date"
                        self.lblData3.text = "Quantity"
                        self.lblData4.text = "Amount"
                    }
                } else if exportHeadingType != nil {
                    switch exportHeadingType! {
                    case .OpenOrder, .ShipmentPlan, .CompleteOrder:
                        self.lblData1.text = "Invoice No."
                        self.lblData2.text = "Date"
                        self.lblData3.text = "Quantity"
                        self.lblData4.text = "Amount"
                    case .DispatchPlan:
                        self.lblData1.text = "Planning Date"
                        self.lblData2.text = "Quantity"
                        self.lblData3.text = "No of PI"
                        self.lblData4.text = "Amount"   // Fourth Column will be always hidden
                    case .ShipmentTrack:
                        self.lblData1.text = "BL Number"
                        self.lblData2.text = "ETA"
                        self.lblData3.text = "Delayed By"
                        //self.lblData4.text = "Amount"   // Fourth Column will be always hidden
                    }
                }
            }
            
            
//            if heading && (importHeadingType != nil) {
//                self.lblData1.text = importHeadingType == .Licence ? "Licence No." : (importHeadingType == .InTransit ? "Invoice No." : "PO No.")
//                self.lblData2.text = importHeadingType == .Licence ? "Validity" : "Date"
//                self.lblData3.text = importHeadingType == .Licence ? "Utilized" : "Quantity"
//                self.lblData4.text = importHeadingType == .Licence ? "Balance" : "Amount"
//            } else if heading && (exportHeadingType != nil) {
//                self.lblData1.text = exportHeadingType == .DispatchPlan ? "Planning Date" : "Invoice No."
//                self.lblData2.text = exportHeadingType == .DispatchPlan ? "Quantity" : "Date"
//                self.lblData3.text = exportHeadingType == .DispatchPlan ? "No of PI" : "Quantity"
//                self.lblData4.text = "Amount"       // Not checking as Dispatch Planning has only three column. so, lblData4 will be always hidden for that headingType
//            }
            [self.lblData1, self.lblData2, self.lblData3, self.lblData4].forEach { label in
                label?.numberOfLines = 0
                label?.minimumScaleFactor = heading ? 0.8 : 0.65
                label?.textColor = heading ? UIColor.ourAppThemeColor : UIColor.label
                //label?.changeFontStyle(style: heading ? .Bold : .Regular, fontSize: heading ? 16 : 15)
                label?.changeFontStyle(style: .Regular, fontSize: heading ? 16 : 15)
            }
        }
    }
    
    public func configure_Cell_Row_Dynamically(isHeading: Bool = true, head1: String? = nil, head2: String? = nil, head3: String? = nil, head4: String? = nil) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainContentView.backgroundColor = isHeading ? .ourAppThemeColor.withAlphaComponent(0.15) : .systemBackground
            if isHeading {
                self.lblData1.text = head1.non_NilEmpty()
                self.lblData2.text = head2.non_NilEmpty()
                self.lblData3.text = head3.non_NilEmpty()
                self.lblData4.text = head4.non_NilEmpty()
            }
            
            [self.lblData1, self.lblData2, self.lblData3, self.lblData4].forEach { label in
                label?.numberOfLines = isHeading ? 1 : 0
                label?.textColor = isHeading ? UIColor.ourAppThemeColor : UIColor.label
                label?.changeFontStyle(style: isHeading ? .Bold : .Regular, fontSize: isHeading ? 16 : 15)
            }
        }
    }
    
    public func setAllData(data1: String?, data2: String?, data3: String?, data4: String?) {
        lblData1.text = data1.non_NilEmpty()
        lblData2.text = data2.non_NilEmpty()
        lblData3.text = data3.non_NilEmpty()
        lblData4.text = data4.non_NilEmpty()
        self.layoutIfNeeded()
    }
    
    public func three_ColumnView(required: Bool = false) {
        lblData4.isHidden = required
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if required && self.firstLabelWidth != nil {
                let newCon = NSLayoutConstraint(item: self.firstLabelWidth.firstItem!, attribute: self.firstLabelWidth.firstAttribute, relatedBy: self.firstLabelWidth.relation, toItem: self.firstLabelWidth.secondItem, attribute: self.firstLabelWidth.secondAttribute, multiplier: 0.33, constant: self.firstLabelWidth.constant)
                self.firstLabelWidth.isActive = false
                newCon.isActive = true
            }
        }
    }
}
