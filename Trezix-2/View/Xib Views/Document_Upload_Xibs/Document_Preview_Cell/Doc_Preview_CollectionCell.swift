//
//  Doc_Preview_CollectionCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 01/06/23.
//

import UIKit
import PDFKit

class Doc_Preview_CollectionCell: UICollectionViewCell {
    // MARK: - All Outlets
    @IBOutlet weak var shadowContentView: UIView!
    @IBOutlet weak var pdfFrameView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var cancelImageView: UIImageView!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var failedLoad_View: UIView!
    
    // MARK: - All Constants & Variables
    private var doc_PDF_View: PDFView! = nil
    
    // MARK: - Required Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        btnAdd.isHidden = true
        cancelImageView.isHidden = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        shadowContentView.layer.applyShadowAndRadius()
        pdfFrameView.layer.cornerRadius = 8
        imageView.layer.cornerRadius = 8
        btnAdd.layer.applyShadowAndRadius(cornerRadius: btnAdd.frame.width/2)
        failedLoad_View.layer.applyShadowAndRadius()
        cancelImageView.layer.cornerRadius = cancelImageView.frame.width/2
    }
    
    // MARK: - Helper Methods
    private func initDocPDFView() {
        doc_PDF_View = PDFView()
        configurePDF()
        pdfFrameView.addSubview(doc_PDF_View)
        doc_PDF_View.translatesAutoresizingMaskIntoConstraints = false
        doc_PDF_View.topAnchor.constraint(equalTo: pdfFrameView.topAnchor, constant: 5).isActive = true
        doc_PDF_View.bottomAnchor.constraint(equalTo: pdfFrameView.bottomAnchor, constant: -5).isActive = true
        doc_PDF_View.leadingAnchor.constraint(equalTo: pdfFrameView.leadingAnchor, constant: 5).isActive = true
        doc_PDF_View.trailingAnchor.constraint(equalTo: pdfFrameView.trailingAnchor, constant: -5).isActive = true
    }
    
    private func configurePDF() {
        doc_PDF_View.maxScaleFactor = 3
        doc_PDF_View.minScaleFactor = doc_PDF_View.scaleFactor
        doc_PDF_View.autoScales = true
        doc_PDF_View.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        doc_PDF_View.displayMode = .singlePageContinuous
        doc_PDF_View.displaysPageBreaks = true
        doc_PDF_View.displayDirection = .vertical
        doc_PDF_View.backgroundColor = .clear
        doc_PDF_View.isUserInteractionEnabled = false
    }
    
    private func categorizedCell(isPDF: Bool = false, isButton: Bool = false) {
        if isButton {
            btnAdd.isHidden = false
            cancelImageView.isHidden = true
            pdfFrameView.isHidden = true
            imageView.isHidden = true
        } else {
            btnAdd.isHidden = true
            cancelImageView.isHidden = false
            pdfFrameView.isHidden = !isPDF
            imageView.isHidden = isPDF
        }
    }
    
    public func setPDF(pdfURL: URL, fromViewMode: Bool = false) {
        initDocPDFView()
        categorizedCell(isPDF: true)
        if fromViewMode {
            DispatchQueue.main.async { [weak self] in
                self?.cancelImageView.isHidden = true
                self?.btnAdd.isHidden = true
            }
        }
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let pdfDocument = PDFDocument(url: fromViewMode ? pdfURL : pdfURL.standardizedFileURL) else {
                DispatchQueue.main.async {
                    self?.failedLoad_View.isHidden = false
                }
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.doc_PDF_View.document = pdfDocument
            }
        }
    }
    
    public func setImage(image: UIImage) {
        categorizedCell(isPDF: false)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.imageView.image = image
        }
    }
    
    public func setData(finalURL: String) {
        let isPDF = (finalURL as NSString).pathExtension.contains("pdf")
        let url = URL(string: finalURL)!
        if isPDF {
            setPDF(pdfURL: url, fromViewMode: true)
        } else {
            categorizedCell(isPDF: false)
            DispatchQueue.main.async { [weak self] in
                self?.btnAdd.isHidden = true
                self?.cancelImageView.isHidden = true
                self?.imageView.animateLoader(.brown)
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.imageView.sd_setImage(with: url) { (image, err, cache, url) in
                    self.imageView.remove_Activity_Animator {
                        if let image {
                            self.imageView.image = image
                        } else {
                            self.failedLoad_View.isHidden = false
                        }
                    }
                }
            }
        }
    }
    
    public func isButton() {
        categorizedCell(isButton: true)
    }
}
