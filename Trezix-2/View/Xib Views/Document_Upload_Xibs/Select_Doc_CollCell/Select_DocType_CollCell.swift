//
//  Select_DocType_CollCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 16/02/23.
//

import UIKit

class Select_DocType_CollCell: UICollectionViewCell {
    static let identifier = "Select_DocType_CollCell"
    
    @IBOutlet weak var shadowContentView: UIView!
    @IBOutlet weak var lblDocumentHead: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func setHeading(heading: String) {
        DispatchQueue.main.async { [weak self] in
            self?.lblDocumentHead.text = heading
        }
    }
    
    override var isSelected: Bool {
        didSet {
            lblDocumentHead.textColor = isSelected ? .white : .ourAppThemeColor
            shadowContentView.backgroundColor = isSelected ? .ourAppThemeColor : .systemBackground
        }
    }
}
