//
//  DocumentList_TableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 31/03/23.
//

import UIKit
import SDWebImage

class DocumentList_TableCell: UITableViewCell {
    static let identifier = "DocumentList_TableCell"
    
    @IBOutlet weak var shadowContentView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var imageDocument: UIImageView!
    @IBOutlet weak var lblDocumentName: UILabel!
    
    @IBOutlet weak var btnViewDocument: UIButton!
    @IBOutlet weak var btnDeleteDocument: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.imageBackView.layer.applyShadowAndRadius(cornerRadius: self.imageBackView.frame.width/2)
        }
    }
    
    public func set_Document_Details(name: String?, tag: Int) {
        guard let name else { debugPrint("Invalid Document Name"); return }
        let isDocImage = !(name as NSString).pathExtension.contains("pdf")
        [btnViewDocument, btnDeleteDocument].forEach { button in
            button?.tag = tag
        }
        imageDocument.image = UIImage(named: isDocImage ? "ic-DocImage" : "ic-DocPDF")
        lblDocumentName.text = ""
        name.components(separatedBy: ",").forEach { singleName in
            let isLabelEmpty = lblDocumentName.text.isNilOrEmpty
            if isLabelEmpty { lblDocumentName.text = singleName }
            else {
                lblDocumentName.text = lblDocumentName.text! + "\n" + singleName
            }
        }
        layoutIfNeeded()
        setNeedsDisplay()
        DispatchQueue.main.async { [weak self] in
            self?.shadowContentView.layer.applyShadowAndRadius()
        }
    }
}
