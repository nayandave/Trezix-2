//
//  poTimeLineTableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 28/11/22.
//

import UIKit

class poTimeLineTableCell: UITableViewCell {
    
    static let identifier = "poTimeLineTableCell"
    
    // MARK: - All Outlets
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var verticalBorderView: UIView!
    
    @IBOutlet weak var outerCircleView: UIView!
    @IBOutlet weak var innerCircleView: UIView!
    
    @IBOutlet weak var dataContainerView: UIView!
    @IBOutlet weak var headerCurveView: UIView!
    @IBOutlet weak var lblSectionHeading: UILabel!
    @IBOutlet weak var infoContainerView: UIView!
    @IBOutlet weak var infoContainerTrailing: NSLayoutConstraint!
    @IBOutlet weak var infoStackView: UIStackView!
    
    @IBOutlet weak var firstStackView: UIStackView!
    @IBOutlet weak var secondStackView: UIStackView!
    @IBOutlet weak var thirdStackView: UIStackView!
    
    @IBOutlet weak var lblHeading1: UILabel!
    @IBOutlet weak var lblHeading2: UILabel!
    @IBOutlet weak var lblHeading3: UILabel!
    
    @IBOutlet weak var lblData1: UILabel!
    @IBOutlet weak var lblData2: UILabel!
    @IBOutlet weak var lblData3: UILabel!
    
    // MARK: - Required Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        infoContainerTrailing.constant = getDynamicWidth(size: -25)
//        [firstStackView, secondStackView, thirdStackView, lblHeading1, lblHeading2, lblHeading3, lblData1, lblData2, lblData3].forEach { view in
//            view?.isHidden = false
//        }
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        let infoCount = infoStackView.arrangedSubviews.count
        for subView in infoStackView.arrangedSubviews {
            if subView.isKind(of: TimeLineAddtionalView.self) || subView.isKind(of: TimeLineStackRow.self) {
                subView.removeFromSuperview()
                infoStackView.layoutIfNeeded()
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.outerCircleView.layer.applyShadowAndRadius(cornerRadius: self.outerCircleView.bounds.width/2, opacity: 0.9)
            self.innerCircleView.layer.cornerRadius = self.innerCircleView.bounds.width/2
            
            self.headerCurveView.layer.applyShadowAndRadius(corners: [.topLeft, .bottomLeft], cornerRadius: self.headerCurveView.bounds.height/2)
            self.infoContainerView.layer.applyShadowAndRadius(corners: [.bottomLeft, .bottomRight], cornerRadius: 8, offset: CGSize(width: 0, height: 2))
        }
    }
    
    /// Draws the triangle at the edge of ``headerCurveView`` in UI
    /// - Parameter color: UIColor to be given as background color to the drawn triangle
    private func setDownTriangle(color: UIColor) {
        let offset = CGSize(width: 1, height: 1.5)
        let constant = getDynamicWidth(size: 25)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if let allSub = self.dataContainerView.layer.sublayers, let sub = allSub.last {
                if let lastSub = sub as? CAShapeLayer { lastSub.fillColor = color.cgColor; return }
                else {
                    let path = CGMutablePath()
                    
                    path.move(to: CGPoint(x: self.headerCurveView.frame.maxX, y: self.headerCurveView.frame.maxY))
                    path.addLine(to: CGPoint(x: self.headerCurveView.frame.maxX-constant, y: self.headerCurveView.frame.maxY))
                    path.addLine(to: CGPoint(x: self.headerCurveView.frame.maxX-constant, y: self.headerCurveView.frame.maxY+constant))
                    path.addLine(to: CGPoint(x: self.headerCurveView.frame.maxX, y: self.headerCurveView.frame.maxY))
                    
                    let shape = CAShapeLayer()
                    shape.fillColor = color.cgColor
                    shape.path = path
                    
                    shape.shadowColor = UIColor(named: "ShadowLabelColor")!.cgColor
                    shape.shadowOffset = offset
                    shape.shadowOpacity = 0.6
                    shape.shadowRadius = 2
                    shape.shadowPath = path
                    
                    self.dataContainerView.layer.addSublayer(shape)
                }
            }
        }
    }
    
    /// Setting the details of First Stack Row
    /// - Parameter heading: heading text of type optional string to be set to left most label, default value is `nil`
    /// - Parameter data: Actual data text of type optional string, default value is `nil`
    /// - Parameter onlyHeading: Boolean value that indicates to show/hide dataLabel i.e `TRUE` if dataLabel is to be hidden & `FALSE` if dataLabel is to be shown, default value is `FALSE`
    /// - Parameter isHidden: Boolean value that indicates is first stack row is to be shown/hidden i.e. `TRUE` if row is to be hidden & `FALSE` if row is to be shwon, default value is `FALSE`
    public func setFirstRowData(heading: String? = nil, data: String? = nil, onlyHeading: Bool = false, isHidden: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.firstStackView.isHidden = isHidden
            if !isHidden {
                self.lblHeading1.changeFontStyle(style: .Bold, fontSize: onlyHeading ? 18 : 16)
                self.lblHeading1.text = heading
                self.lblData1.isHidden = onlyHeading
                if !onlyHeading { self.lblData1.text = data }
            }
            self.layoutIfNeeded()
        }
    }

    /// Setting the details of Second Stack Row
    /// - Parameter heading: heading text of type optional string to be set to left most label, default value is `nil`
    /// - Parameter data: Actual data text of type optional string, default value is `nil`
    /// - Parameter onlyHeading: Boolean value that indicates to show/hide dataLabel i.e `TRUE` if dataLabel is to be hidden & `FALSE` if dataLabel is to be shown, default value is `FALSE`
    /// - Parameter isHidden: Boolean value that indicates is second stack row is to be shown/hidden i.e. `TRUE` if row is to be hidden & `FALSE` if row is to be shwon, default value is `FALSE`
    public func setSecondRowData(heading: String? = nil, data: String? = nil, onlyHeading: Bool = false, isHidden: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.secondStackView.isHidden = isHidden
            if !isHidden {
                self.lblHeading2.text = heading
                self.lblData2.isHidden = onlyHeading
                if !onlyHeading { self.lblData2.text = data }
            }
            self.layoutIfNeeded()
        }
    }
    
    /// Setting the details of Third Stack Row
    /// - Parameter heading: heading text of type optional string to be set to left most label, default value is `nil`
    /// - Parameter data: Actual data text of type optional string, default value is `nil`
    /// - Parameter onlyHeading: Boolean value that indicates to show/hide dataLabel i.e `TRUE` if dataLabel is to be hidden & `FALSE` if dataLabel is to be shown, default value is `FALSE`
    /// - Parameter isHidden: Boolean value that indicates is third stack row is to be shown/hidden i.e. `TRUE` if row is to be hidden & `FALSE` if row is to be shwon, default value is `FALSE`
    public func setThirdRowData(heading: String? = nil, data: String? = nil, onlyHeading: Bool = false, isHidden: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.thirdStackView.isHidden = isHidden
            if !isHidden {
                self.lblHeading3.text = heading
                self.lblData3.isHidden = onlyHeading
                if !onlyHeading { self.lblData3.text = data }
            }
            self.layoutIfNeeded()
        }
    }
    
    /// Sets Heading text & Background Color
    /// - Parameter heading: heading text of type String to be given to ``lblSectionHeading``
    /// - Parameter color: color of type ``POTimelineVC.POTimeLineBackColor`` to be given as background color of ``headerCurveView`` & ``outerCircleView``, also it's associatedDarkColor to be given as edgeTraingle's background color
    public func setHeaderAndColor(heading: String, color: POTimelineVC.POTimeLineBackColor) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lblSectionHeading.text = heading
            self.headerCurveView.backgroundColor = UIColor.hexStringToUIColor(hex: color.rawValue)
            self.outerCircleView.backgroundColor = UIColor.hexStringToUIColor(hex: color.rawValue)
            self.setDownTriangle(color: color.associatedDarkColor)
        }
    }
}
