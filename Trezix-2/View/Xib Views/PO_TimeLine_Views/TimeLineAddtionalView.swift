//
//  TimeLineAddtionalView.swift
//  Trezix-2
//
//  Created by Amar Panchal on 28/11/22.
//

import UIKit

class TimeLineAddtionalView: UIView {
    // MARK: - All Outlets
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var viewButtonRatio: NSLayoutConstraint!
    @IBOutlet weak var btnDownload: UIButton!
    
    /// Executable variable View Button Action which is defined in caller of this View
    private var viewButtonAction: (() -> Void)? = nil
    /// Executable variable Download Button Action which is defined in caller of this View
    private var downloadButtonAction: (() -> Void)? = nil
    
    var downloadbelCategory: POTimelineVC.DownloadableSectionName! = nil
    
    // MARK: - All Required Methods
    
    /// Customizable Initializer which is used in ``POTimelineVC``
    /// - Parameter downloadSection: 
    /// - Parameter viewAction: Executable Action which is called when View Button is Clicked
    /// - Parameter downloadAction: Executable Action which is called when Download Button is Clicked
    /// - Parameter index: Int type index which is assigned as tag to all button and View
    public func setUpActions(downloadSection: POTimelineVC.DownloadableSectionName, viewAction: (() -> Void)? = nil, downloadAction: (() -> Void)? = nil, index: Int) {
        btnView.tag = index
        btnDownload.tag = index
        tag = index
        downloadbelCategory = downloadSection
        viewButtonAction = viewAction
        downloadButtonAction = downloadAction
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("TimeLineAddtionalView", owner: self)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    /// Setting heading, tag & view button title
    /// - Parameter heading: Heading text of type String to be set in ``lblHeading`` label
    /// - Parameter tag: Index of type integer to be given as ``btnView``, ``btnDownload`` & current view's tag
    public func setInfo(heading: String, viewName: String?) {
//        if let ratio = self.viewButtonRatio {
//            ratio.isActive = viewName == nil
//        }
            lblHeading.text = heading + " :"
//            if viewName == nil {
//                self.viewButtonRatio.isActive = true
////                self.btnView.translatesAutoresizingMaskIntoConstraints = false
////                self.btnView.widthAnchor.constraint(equalTo: self.btnView.heightAnchor, multiplier: 1.5).isActive = true
////                self.layoutIfNeeded()
//            } else {
//                self.viewButtonRatio.isActive = false
//            }
            btnView.setImage((viewName != nil) ? nil : UIImage(named: "ic-ViewPO"), for: .normal)
            btnView.setTitle(viewName, for: .normal)
//            self.translatesAutoresizingMaskIntoConstraints = false
//        }
    }
    
    // MARK: - All IB-Actions
    @IBAction func download_Action(_ sender: UIButton) {
        downloadButtonAction?()
    }
    @IBAction func view_Action(_ sender: UIButton) {
        viewButtonAction?()
    }
}
