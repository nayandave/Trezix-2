//
//  TimeLineStackRow.swift
//  Trezix-2
//
//  Created by Amar Panchal on 30/11/22.
//

import UIKit

class TimeLineStackRow: UIView {
    // MARK: - All Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblData: UILabel!
    
    // MARK: - Required Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("TimeLineStackRow", owner: self)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    /// Setting up required information to each tableCell
    /// - Parameter heading: Heading to be given for cell of string type
    /// - Parameter data: Actual data to be given for cell of string type
    /// - Parameter tag: Index or tag to be given to each cell of integer type
    public func setAllInfo(heading: String, data: String, tag: Int) {
        self.tag = tag
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lblHeading.text = heading
            self.lblData.text = data
        }
    }
}
