//
//  dutyClearingDetailTableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 17/10/22.
//

import UIKit

class dutyClearingDetailTableCell: UITableViewCell {
    // MARK: - All Outlets
    @IBOutlet weak var lblLeftHeading: UILabel!
    @IBOutlet weak var lblLeftValue: UILabel!
    
    @IBOutlet weak var lblRightHeading: UILabel!
    @IBOutlet weak var lblRightValue: UILabel!
    
    @IBOutlet weak var topSeparatorView: UIView!
    
    // MARK: - All Helper Methods
    public func setHeadings(left: String, right: String) {
        DispatchQueue.main.async { [weak self] in
            self?.lblLeftHeading.text = left
            self?.lblRightHeading.text = right
        }
    }
    
    public func setValues(left: String, right: String) {
        DispatchQueue.main.async { [weak self] in
            self?.lblLeftValue.text = left
            self?.lblRightValue.text = right
        }
    }
    
    public func isFirstCell(_ showTopSep: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            self?.topSeparatorView.isHidden = true//!showTopSep
        }
    }
}
