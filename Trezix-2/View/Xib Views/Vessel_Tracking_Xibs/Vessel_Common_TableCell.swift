//
//  Vessel_Common_TableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 13/04/23.
//

import UIKit

class Vessel_Common_TableCell: UITableViewCell {
    
    static let identifier = "Vessel_Common_TableCell"
    
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblData: UILabel!
    
    public var vesselData: Vessel_Detail_Data! = nil
    
    private let arr_All_Headings = ["Shipment Number : ",
                                    "Container Number : ",
                                    "Container Seal Number : ",
                                    "Container Size : ",
                                    "Container Type : ",
                                    "Container Iso : ",
                                    "Status : ",
                                    "Sub Status 1 : ",
                                    "Sub Status 2 : ",
                                    "MBL Number : ",
                                    "Carrier Scac : "]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func set_Data_According(toIndex: Int) {
        lblHeading.text = arr_All_Headings[toIndex]
        switch toIndex {
        case 0: lblData.text = vesselData.shipmentNumber.non_NilEmpty()
        case 1: lblData.text = vesselData.containerNumber.non_NilEmpty()
        case 2: lblData.text = vesselData.containerSealNumber.non_NilEmpty()
        case 3: lblData.text = vesselData.containerSize.non_NilEmpty()
        case 4: lblData.text = vesselData.containerType.non_NilEmpty()
        case 5: lblData.text = vesselData.containerISO.non_NilEmpty()
        case 6: lblData.text = vesselData.status.non_NilEmpty()
        case 7: lblData.text = vesselData.subStatus1.non_NilEmpty()
        case 8: lblData.text = vesselData.subStatus2.non_NilEmpty()
        case 9: lblData.text = vesselData.mblNumber.non_NilEmpty()
        case 10: lblData.text = vesselData.carrierScac.non_NilEmpty()
        default: lblData.text = "Unknown Data"
        }
        layoutIfNeeded()
        setNeedsDisplay()
    }
}
