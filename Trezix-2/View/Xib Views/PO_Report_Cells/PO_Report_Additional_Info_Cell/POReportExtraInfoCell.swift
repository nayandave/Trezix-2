//
//  POReportExtraInfoCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 02/08/22.
//

import UIKit

class POReportExtraInfoCell: UITableViewCell {
    static let identifier = "POReportExtraInfoCell"
    
    @IBOutlet weak var shadowContentView: UIView!
    
    @IBOutlet weak var reportData_StackView: UIStackView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    @IBOutlet weak var secondStackView: UIStackView!
    @IBOutlet weak var lblHeading2: UILabel!
    @IBOutlet weak var lblValue2: UILabel!
    
    // Detail Button Content View
    @IBOutlet weak var detailButtonContentView: UIView!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var detainButton_StackView: UIStackView!
    @IBOutlet weak var lblDetailButtonTitle: UILabel!
    
    
    var poData: mainOrderReportData! = nil
    let arrPOReportHeading = ["Company Plant : ",
                              "Vendor Group Name : ",
                              "Vendor Group Description : ",
                              "Material Code : ",
                              "Material Name : ",
                              "PO Quantity : ",
                              "Unit of Measurement : ",
                              "Rate : ",
                              "Currency : ",
                              "FC Value : ",
                              "Advance Payment : ",
                              "INR Value : ",
                              "Shipped Qnt : ",
                              "Pending Qnt : ",
                              "Packing Instruction : ",
                              "Material Group : ",
                              "Payment terms : ",
                              "Inco term 1 : "]
    
    var shipmentData: MainShipmentData! = nil
    let arrShipmentReportHeading = ["LC No : ",
                                    "LC Date : ",
                                    "LC Expiry Date : ",
                                    "Latest Date of Shipment : ",
                                    "Company Plant : ",
                                    "Vendor Name : ",
                                    "Vendor Group Name : ",
                                    "Vendor Group Description : ",
                                    "Material Code : ",
                                    "Material Name : ",
                                    "PO Quantity : ",
                                    "Shipped Quantity : ",
                                    "Pending Quantity : ",
                                    "Unit of Measurement : ",
                                    "Rate : ",
                                    "Currency : ",
                                    "FC Value : ",
                                    "Advance Payment : ",
                                    "INR Value : ",
                                    "Packing Instruction : ",
                                    "Material Group : ",
                                    "Payment Terms : ",
                                    "Inco Term 1 : ",
                                    "Inco Term 2 : ",
                                    "Invoicing Party : ",
                                    "Invoice No : ",
                                    "Invoice Date : ",
                                    "Delivery Port : ",
                                    "B/L No. : ",
                                    "B/L Date : ",
                                    "ETA Date : ",
                                    "Bank Intimation Number : ",
                                    "Intimation Date : ",
                                    "Doc. Clearing Date : ",
                                    "Nominated CHA : ",
                                    "BE No. : ",
                                    "BE Date : ",
                                    "IGM No. : ",
                                    "IGM Date : ",
                                    "Status : "]
    
    
    var salesReportData: Sales_Report_Data! = nil
    let arrSalesReportHeading1 = ["FI Date : ",
                                  "Comm. Inv. Date : ",
                                  "Ship To Party : ",
                                  "Inco-1 : ",
                                  "Net Weight : ",
                                  "Invoice Value : ",
                                  "PI INR : ",
                                  "Scheme-1 : ",
                                  "Scheme-2 : ",
                                  "Scheme-3 : ",
                                  "Scheme-4 : ",
                                  "Ins. Policy No. : ",
                                  "Ins. Cert. Date : ",
                                  "Insurance Charge : ",
                                  "Container No. : ",
                                  "Port of Discharge : ",
                                  "Country of Final Destination : ",
                                  "Port Name : ",
                                  "FI Exc. Rate : ",
                                  "Let Export Dt. : ",
                                  "B/L Dt. : ",
                                  "S/B Dt. : ",
                                  "G.R. No. : ",
                                  "B.R.C. No. : ",
                                  "BRC Ex. Rate : ",
                                  "BRC FC : ",
                                  "Foreign Bank Charges : ",
                                  "Short Payment : ",
                                  "S.O. No. : ",
                                  "Sold To Country : ",
                                  "Plant : ",
                                  "Customer Group 2 : ",
                                  "Customer Group 4 : ",
                                  "Customer Group 6 : ",
                                  "Customer Group 8 : ",
                                  "Customer Group 10 : ",
                                  "Local Bank : ",
                                  "CHA Name : ",
                                  "Insurance Company : ",
                                  "Material Description : ",
                                  "Batch No. : ",
                                  "No. of Pack : ",
                                  "Storage Location : ",
                                  "Item Value : ",
                                  "PO No. : ",
                                  "Material Group 1 : ",
                                  "Material Group 3 : ",
                                  "Material Group 5 : ",
                                  "Material Group 7 : ",
                                  "Material Group 9 : "]
    
    
    
    let arrSalesReportHeading2 = ["Comm. Inv. No. : ",
                                  "Sold To Party : ",
                                  "Payment Term : ",
                                  "Inco-2 : ",
                                  "Doc. Cur. : ",
                                  "PI Exc. Rate : ",
                                  "Shipment Type : ",
                                  "Scheme 1 Lic. No. : ",
                                  "Scheme 2 Lic. No. : ",
                                  "Scheme 3 Lic. No. : ",
                                  "Scheme 4 Lic. No. : ",
                                  "Ins. Cert. No. : ",
                                  "Insured Amount : ",
                                  "Vessel/Flight : ",
                                  "Port of Loading : ",
                                  "Final Destination : ",
                                  "Port Code : ",
                                  "Place of Receipt : ",
                                  "FI INR : ",
                                  "B/L No. : ",
                                  "S/B No. : ",
                                  "S/B Ex. Rate : ",
                                  "G.R. Dt. : ",
                                  "B.R.C. Dt. : ",
                                  "BRC FOB : ",
                                  "BRC Payer : ",
                                  "Bank Charges : ",
                                  "Due Date : ",
                                  "Delivery No : ",
                                  "Ship To Country : ",
                                  "Customer Group 1 : ",
                                  "Customer Group 3 : ",
                                  "Customer Group 5 : ",
                                  "Customer Group 7 : ",
                                  "Customer Group 9 : ",
                                  "Notify : ",
                                  "Customer Bank : ",
                                  "Shipping Agent : ",
                                  "Material Code : ",
                                  "Customer Product Desc. : ",
                                  "Per Pack : ",
                                  "Item Qnt. : ",
                                  "Rate : ",
                                  "Item Value INR : ",
                                  "PO Dt. : ",
                                  "Material Group 2 : ",
                                  "Material Group 4 : ",
                                  "Material Group 6 : ",
                                  "Material Group 8 : ",
                                  "Material Group 10 : "]
    
    
    var salesOrderReportData: Sales_Order_Report_Data! = nil
    let arrSalesOrderReportHeading = ["Customer Order No : ",
                                      "Customer Order Date : ",
                                      "Customer Name : ",
                                      "Ship To Name : ",
                                      "MOT : ",
                                      "Incoterm1 : ",
                                      "Incoterm2 : ",
                                      "Port of Discharge : ",
                                      "LC No. : ",
                                      "LC Date : ",
                                      "Material : ",
                                      "Material Description : ",
                                      "Rate : ",
                                      "Total Value : ",
                                      "Currency : ",
                                      "Schedule Date : ",
                                      "Order Quantity : ",
                                      "Sales Quantity : ",
                                      "UOM : ",
                                      "Balance : ",
                                      "PSS : ",
                                      "MFG Plant : ",
                                      "Packing : ",
                                      "Item Status : ",
                                      "Order Status : "]
    
    var scriptDrabackData: Script_Drawback_Data! = nil
    let arrScriptDrabackHeading1 = ["PI No. : ",
                                    "Tax Inv. No. : ",
                                    "Scheme 1 : ",
                                    "Scheme 2 : ",
                                    "Scheme 3 : ",
                                    "Scheme 4 : ",
                                    "Script No. : ",
                                    "Expiry Date : ",
                                    "Received Bank : ",
                                    "FI No. : ",
                                    "Customer : ",
                                    "Payment Term : ",
                                    "Realization Date : ",
                                    "Inv. Currency : ",
                                    "B/L Date : ",
                                    "S/B Date : ",
                                    "Let Export Date : ",
                                    "Port Code : ",
                                    "Scheme 1 S/B Value : ",
                                    "Scheme 2 S/B Value : ",
                                    "Applied 1 Date : ",
                                    "Balance : "]
    
    
    let arrScriptDrabackHeading2 = ["PI Date : ",
                                    "Tax Inv. Date : ",
                                    "Licence 1 : ",
                                    "Licence 2 : ",
                                    "Licence 3 : ",
                                    "Licence 4 : ",
                                    "Script Date : ",
                                    "Received Date : ",
                                    "Amount : ",
                                    "FI Date : ",
                                    "Incoterm : ",
                                    "Due Date : ",
                                    "Inv. Total Value : ",
                                    "B/L No. : ",
                                    "S/B No. : ",
                                    "S/B Exchange Rate : ",
                                    "S/B FOB : ",
                                    "Port Name : ",
                                    "Scheme 1 Received : ",
                                    "Scheme 2 Received : ",
                                    "Applied 2 Date : "]
    
    var landingCostReportData: Landing_Cost_Report_Data! = nil
    let arrLandingCostReportHeading1 = ["Plant : ",
                                        "Vendor : ",
                                        "Product Name : ",
                                        "Quantity : ",
                                        "Total : ",
                                        "Partner Other Charges Total : ",
                                        "Bank Charges : ",
                                        "BCD : ",
                                        "SAD : ",
                                        "Cess of CVD : ",
                                        "Anti Dumping : ",
                                        "Other : ",
                                        "Social Welfare",
                                        "Total FC : ",
                                        "Total FC GST : ",
                                        "One Qty FC : ",
                                        "One Qty FC With GST : "]
    
    let arrLandingCostReportHeading2 = ["PO No. : ",
                                        "Product Code : ",
                                        "UOM : ",
                                        "Price : ",
                                        "Currency : ",
                                        "BE Ex. Rate : ",
                                        "DEEC : ",
                                        "CVD : ",
                                        "IGST : ",
                                        "Edv Cess : ",
                                        "Penalty : ",
                                        "Sec Edu Cess : ",
                                        "Total INR : ",
                                        "Total INR GST : ",
                                        "One Qty INR : ",
                                        "One Qty INR With GST : "]
    
    var vesselTrackingData: Vessel_List_Data! = nil
    let arrVesselTrackingHeading = ["Vessel Name : ",
                                    "ETA According to User : ",
                                    "Track Status : ",
                                    "Shipment Status : "]
    
    
    var importShipmentTrackingData: ShipmentTracking_Import_Data! = nil
    let arrImportShipmentTrackingHeading = ["Invoices : ",
                                            "Vessel Name : ",
                                            "Status : "]
    
    var exportShipmentTrackingData: ShipmentTracking_Export_Data! = nil
    let arrExportShipmentTrackingHeading = ["Invoices : ",
                                            "Vessel Name : ",
                                            "Status : "]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        detailButtonContentView.isHidden = true
        reportData_StackView.isHidden = false
    }
    
    // MARK: - PO Report Cell Initialization
    public func setPOReportInfo(forRow: Int) {
        secondStackView.isHidden = true
        switch forRow {
        case 0: lblHeading.text = arrPOReportHeading[0]; lblValue.text = poData.companyPlant.non_NilEmpty()
        case 1: lblHeading.text = arrPOReportHeading[1]; lblValue.text = poData.group1.non_NilEmpty()
        case 2: lblHeading.text = arrPOReportHeading[2]; lblValue.text = poData.group2.non_NilEmpty()
        case 3: lblHeading.text = arrPOReportHeading[3]; lblValue.text = poData.materialCode.non_NilEmpty()
        case 4: lblHeading.text = arrPOReportHeading[4]; lblValue.text = poData.materialName.non_NilEmpty()
        case 5: lblHeading.text = arrPOReportHeading[5]; lblValue.text = poData.quility.non_NilEmpty()
        case 6: lblHeading.text = arrPOReportHeading[6]; lblValue.text = poData.unit.non_NilEmpty()
        case 7: lblHeading.text = arrPOReportHeading[7]; lblValue.text = poData.rate.non_NilEmpty()
        case 8: lblHeading.text = arrPOReportHeading[8]; lblValue.text = poData.currency.non_NilEmpty()
        case 9: lblHeading.text = arrPOReportHeading[9]; lblValue.text = poData.fcValue.flatMap{ String($0) }.non_NilEmpty()
        case 10: lblHeading.text = arrPOReportHeading[10]; lblValue.text = poData.advancePayment.non_NilEmpty()
        case 11: lblHeading.text = arrPOReportHeading[11]; lblValue.text = poData.inrvalue.flatMap { String($0) }.non_NilEmpty()
        case 12: lblHeading.text = arrPOReportHeading[12]; lblValue.text = poData.shippedQty.flatMap(String.init).non_NilEmpty()
        case 13: lblHeading.text = arrPOReportHeading[13]; lblValue.text = poData.pendingQty.flatMap(String.init).non_NilEmpty()
        case 14: lblHeading.text = arrPOReportHeading[14]; lblValue.text = poData.packingInstruction.non_NilEmpty()
        case 15: lblHeading.text = arrPOReportHeading[15]; lblValue.text = poData.materialGrpValue.non_NilEmpty()
        case 16: lblHeading.text = arrPOReportHeading[16]; lblValue.text = poData.paymentTerm.non_NilEmpty()
        case 17: lblHeading.text = arrPOReportHeading[17]; lblValue.text = poData.incoterm.non_NilEmpty()
        default:
            self.lblHeading.text = "Unavailable"; self.lblValue.text = "Unavailable"
        }
        self.setNeedsLayout()
        self.setNeedsDisplay()
        self.layoutIfNeeded()
    }
    
    // MARK: - Shipment Planning Report Cell Initialization
    public func setShipReportInfo(forRow: Int) {
        secondStackView.isHidden = true
        switch forRow {
        case 0: lblHeading.text = arrShipmentReportHeading[0]; lblValue.text = shipmentData.lcNo.non_NilEmpty()
        case 1: lblHeading.text = arrShipmentReportHeading[1]; lblValue.text = shipmentData.lcDate.non_NilEmpty()
        case 2: lblHeading.text = arrShipmentReportHeading[2]; lblValue.text = shipmentData.lcExpiryDate.non_NilEmpty()
        case 3: lblHeading.text = arrShipmentReportHeading[3]; lblValue.text = shipmentData.lastDateOfShipment.non_NilEmpty()
        case 4: lblHeading.text = arrShipmentReportHeading[4]; lblValue.text = shipmentData.companyPlant.non_NilEmpty()
        case 5: lblHeading.text = arrShipmentReportHeading[5]; lblValue.text = shipmentData.vendorName.non_NilEmpty()
        case 6: lblHeading.text = arrShipmentReportHeading[6]; lblValue.text = shipmentData.vendorGroup1.non_NilEmpty()
        case 7: lblHeading.text = arrShipmentReportHeading[7]; lblValue.text = shipmentData.vendorGroup2.non_NilEmpty()
        case 8: lblHeading.text = arrShipmentReportHeading[8]; lblValue.text = shipmentData.materialCode.non_NilEmpty()
        case 9: lblHeading.text = arrShipmentReportHeading[9]; lblValue.text = shipmentData.materialName.non_NilEmpty()
        case 10: lblHeading.text = arrShipmentReportHeading[10]; lblValue.text = shipmentData.poQuantity.non_NilEmpty()
        case 11: lblHeading.text = arrShipmentReportHeading[11]; lblValue.text = shipmentData.shippedQuantity.flatMap(String.init).non_NilEmpty()
        case 12: lblHeading.text = arrShipmentReportHeading[12]; lblValue.text = shipmentData.pendingQuantity.flatMap(String.init).non_NilEmpty()
        case 13: lblHeading.text = arrShipmentReportHeading[13]; lblValue.text = shipmentData.unit.non_NilEmpty()
        case 14: lblHeading.text = arrShipmentReportHeading[14]; lblValue.text = shipmentData.rate.non_NilEmpty()
        case 15: lblHeading.text = arrShipmentReportHeading[15]; lblValue.text = shipmentData.currency.non_NilEmpty()
        case 16: lblHeading.text = arrShipmentReportHeading[16]; lblValue.text = shipmentData.fcValue.flatMap{ String($0) }.non_NilEmpty()
        case 17: lblHeading.text = arrShipmentReportHeading[17]; lblValue.text = "NA From Server"
        case 18: lblHeading.text = arrShipmentReportHeading[18]; lblValue.text = shipmentData.inrValue.flatMap{ String($0) }.non_NilEmpty()
        case 19: lblHeading.text = arrShipmentReportHeading[19]; lblValue.text = shipmentData.packingInstruction.non_NilEmpty()
        case 20: lblHeading.text = arrShipmentReportHeading[20]; lblValue.text = shipmentData.materialGrpValue.non_NilEmpty()
        case 21: lblHeading.text = arrShipmentReportHeading[21]; lblValue.text = shipmentData.paymentTerm.non_NilEmpty()
        case 22: lblHeading.text = arrShipmentReportHeading[22]; lblValue.text = shipmentData.incoTerm1.non_NilEmpty()
        case 23: lblHeading.text = arrShipmentReportHeading[23]; lblValue.text = shipmentData.incoTerm2.non_NilEmpty()
        case 24: lblHeading.text = arrShipmentReportHeading[24]; lblValue.text = shipmentData.invoicingParty.non_NilEmpty()
        case 25: lblHeading.text = arrShipmentReportHeading[25]; lblValue.text = shipmentData.invoiceNum.non_NilEmpty()
        case 26: lblHeading.text = arrShipmentReportHeading[26]; lblValue.text = shipmentData.invoiceDate.non_NilEmpty()
        case 27: lblHeading.text = arrShipmentReportHeading[27]; lblValue.text = shipmentData.deliveryPort.non_NilEmpty()
        case 28: lblHeading.text = arrShipmentReportHeading[28]; lblValue.text = shipmentData.blNo.non_NilEmpty()
        case 29: lblHeading.text = arrShipmentReportHeading[29]; lblValue.text = shipmentData.blDate.non_NilEmpty()
        case 30: lblHeading.text = arrShipmentReportHeading[30]; lblValue.text = shipmentData.etaDate.non_NilEmpty()
        case 31: lblHeading.text = arrShipmentReportHeading[31]; lblValue.text = shipmentData.intimationNumber.non_NilEmpty()
        case 32: lblHeading.text = arrShipmentReportHeading[32]; lblValue.text = shipmentData.intimationDate.non_NilEmpty()
        case 33: lblHeading.text = arrShipmentReportHeading[33]; lblValue.text = shipmentData.documentClearingDate.non_NilEmpty()
        case 34: lblHeading.text = arrShipmentReportHeading[34]; lblValue.text = shipmentData.nominatedCha.non_NilEmpty()
        case 35: lblHeading.text = arrShipmentReportHeading[35]; lblValue.text = shipmentData.beNo.non_NilEmpty()
        case 36: lblHeading.text = arrShipmentReportHeading[36]; lblValue.text = shipmentData.beDate.non_NilEmpty()
        case 37: lblHeading.text = arrShipmentReportHeading[37]; lblValue.text = shipmentData.igmNo.non_NilEmpty()
        case 38: lblHeading.text = arrShipmentReportHeading[38]; lblValue.text = shipmentData.igmDate.non_NilEmpty()
        case 39: lblHeading.text = arrShipmentReportHeading[39]; lblValue.text = shipmentData.status.non_NilEmpty()
        default: lblHeading.text = "Unavailable"; lblValue.text = "Unavailable"
        }
        self.setNeedsLayout()
        self.setNeedsDisplay()
        self.layoutIfNeeded()
    }
    
    // MARK: - Sales Report Cell Initialization
    public func setUpSalesReportCell(for row: Int) {
        
        switch row {
        case 0:
            lblHeading.text = arrSalesReportHeading1[0]; lblValue.text = salesReportData.fiDate.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[0]; lblValue2.text = salesReportData.commInvoiceNo.non_NilEmpty()
        case 1:
            lblHeading.text = arrSalesReportHeading1[1]; lblValue.text = salesReportData.commInvoiceDate.non_NilEmpty()
#warning("Sold To Party")
            lblHeading2.text = arrSalesReportHeading2[1]; lblValue2.text = salesReportData.soldToCountry.non_NilEmpty()
        case 2:
#warning("Ship To Party")
            lblHeading.text = arrSalesReportHeading1[2]; lblValue.text = salesReportData.shipToCountry.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[2]; lblValue2.text = salesReportData.paymentTerms.non_NilEmpty()
        case 3:
            lblHeading.text = arrSalesReportHeading1[3]; lblValue.text = salesReportData.incotermDescription.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[3]; lblValue2.text = salesReportData.incoTerm2.non_NilEmpty()
        case 4:
            lblHeading.text = arrSalesReportHeading1[4]; lblValue.text = salesReportData.netWeight.flatMap(String.init).non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[4]; lblValue2.text = salesReportData.currencyName.non_NilEmpty()
        case 5:
            lblHeading.text = arrSalesReportHeading1[5]; lblValue.text = salesReportData.valueTotal.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[5]; lblValue2.text = salesReportData.exchangeRate.non_NilEmpty()
        case 6:
            lblHeading.text = arrSalesReportHeading1[6]; lblValue.text = salesReportData.pdfPreshipmentInvoiceInr.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[6]; lblValue2.text = salesReportData.shipmentType.non_NilEmpty()
        case 7:
            lblHeading.text = arrSalesReportHeading1[7]; lblValue.text = salesReportData.scheme1.flatMap(String.init).non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[7]; lblValue2.text = salesReportData.license1.non_NilEmpty()
        case 8:
            lblHeading.text = arrSalesReportHeading1[8]; lblValue.text = salesReportData.scheme2.flatMap(String.init).non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[8]; lblValue2.text = salesReportData.license2.non_NilEmpty()
        case 9:
            lblHeading.text = arrSalesReportHeading1[9]; lblValue.text = salesReportData.scheme3.flatMap(String.init).non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[9]; lblValue2.text = salesReportData.license3.non_NilEmpty()
        case 10:
            lblHeading.text = arrSalesReportHeading1[10]; lblValue.text = salesReportData.scheme4.flatMap(String.init).non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[10]; lblValue2.text = salesReportData.license4.non_NilEmpty()
        case 11:
            lblHeading.text = arrSalesReportHeading1[11]; lblValue.text = salesReportData.insPolicyNo.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[11]; lblValue2.text = salesReportData.insCERTNo.non_NilEmpty()
        case 12:
            lblHeading.text = arrSalesReportHeading1[12]; lblValue.text = salesReportData.insCERTDate.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[12]; lblValue2.text = salesReportData.insuredAmount.non_NilEmpty()
        case 13:
            lblHeading.text = arrSalesReportHeading1[13]; lblValue.text = salesReportData.insuranceCharges.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[13]; lblValue2.text = salesReportData.vesselFlightNo.non_NilEmpty()
        case 14:
            lblHeading.text = arrSalesReportHeading1[14]; lblValue.text = salesReportData.containerNo.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[14]; lblValue2.text = salesReportData.portOfLoading.non_NilEmpty()
        case 15:
            lblHeading.text = arrSalesReportHeading1[15]; lblValue.text = salesReportData.portOfDischarge.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[15]; lblValue2.text = salesReportData.finalDestination.non_NilEmpty()
        case 16:
            lblHeading.text = arrSalesReportHeading1[16]; lblValue.text = salesReportData.countryFinalDestination.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[16]; lblValue2.text = salesReportData.portCode.non_NilEmpty()
        case 17:
            lblHeading.text = arrSalesReportHeading1[17]; lblValue.text = salesReportData.portName.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[17]; lblValue2.text = salesReportData.placeOfReceipt.non_NilEmpty()
        case 18:
            lblHeading.text = arrSalesReportHeading1[18]; lblValue.text = salesReportData.fiExchangeRate.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[18]; lblValue2.text = salesReportData.fiInr.non_NilEmpty()
        case 19:
            lblHeading.text = arrSalesReportHeading1[19]; lblValue.text = salesReportData.letExportDate.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[19]; lblValue2.text = salesReportData.blNo.non_NilEmpty()
        case 20:
            lblHeading.text = arrSalesReportHeading1[20]; lblValue.text = salesReportData.blDate.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[20]; lblValue2.text = salesReportData.sbNo.non_NilEmpty()
        case 21:
            lblHeading.text = arrSalesReportHeading1[21]; lblValue.text = salesReportData.sbDate.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[21]; lblValue2.text = salesReportData.shippingBillExchangeRate.non_NilEmpty()
        case 22:
            lblHeading.text = arrSalesReportHeading1[22]; lblValue.text = salesReportData.grNo.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[22]; lblValue2.text = salesReportData.grDate.non_NilEmpty()
        case 23:
            lblHeading.text = arrSalesReportHeading1[23]; lblValue.text = salesReportData.brcNo.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[23]; lblValue2.text = salesReportData.brcDate.non_NilEmpty()
        case 24:
            lblHeading.text = arrSalesReportHeading1[24]; lblValue.text = salesReportData.brcExchangeRate.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[24]; lblValue2.text = salesReportData.pdfFobInvoice.non_NilEmpty()
        case 25:
            lblHeading.text = arrSalesReportHeading1[25]; lblValue.text = salesReportData.currencyID.flatMap(String.init).non_NilEmpty()
            #warning("payer ID as in BRC Payer, discuss it")
            lblHeading2.text = arrSalesReportHeading2[25]; lblValue2.text = salesReportData.payerID.non_NilEmpty()
        case 26:
            lblHeading.text = arrSalesReportHeading1[26]; lblValue.text = salesReportData.foreignBankCharges.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[26]; lblValue2.text = salesReportData.bankCharges.non_NilEmpty()
        case 27:
            lblHeading.text = arrSalesReportHeading1[27]; lblValue.text = salesReportData.shortPayment.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[27]; lblValue2.text = salesReportData.dueDate.non_NilEmpty()
        case 28:
            lblHeading.text = arrSalesReportHeading1[28]; lblValue.text = salesReportData.orderNo.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[28]; lblValue2.text = salesReportData.deliveryNo.non_NilEmpty()
        case 29:
            lblHeading.text = arrSalesReportHeading1[29]; lblValue.text = salesReportData.soldToCountry.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[29]; lblValue2.text = salesReportData.shipToCountry.non_NilEmpty()
        case 30:
            lblHeading.text = arrSalesReportHeading1[30]; lblValue.text = salesReportData.plant.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[30]; lblValue2.text = salesReportData.customerName.non_NilEmpty()
        case 31:
            lblHeading.text = arrSalesReportHeading1[31]; lblValue.text = salesReportData.customerGroup2.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[31]; lblValue2.text = salesReportData.customerGroup3.non_NilEmpty()
        case 32:
            lblHeading.text = arrSalesReportHeading1[32]; lblValue.text = salesReportData.customerGroup4.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[32]; lblValue2.text = salesReportData.customerGroup5.non_NilEmpty()
        case 33:
            lblHeading.text = arrSalesReportHeading1[33]; lblValue.text = salesReportData.customerGroup6.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[33]; lblValue2.text = salesReportData.customerGroup7.non_NilEmpty()
        case 34:
            lblHeading.text = arrSalesReportHeading1[34]; lblValue.text = salesReportData.customerGroup8.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[34]; lblValue2.text = salesReportData.customerGroup9.non_NilEmpty()
        case 35:
            lblHeading.text = arrSalesReportHeading1[35]; lblValue.text = salesReportData.customerGroup10.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[35]; lblValue2.text = salesReportData.notify.non_NilEmpty()
        case 36:
            lblHeading.text = arrSalesReportHeading1[36]; lblValue.text = salesReportData.localBank.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[36]; lblValue2.text = salesReportData.customerBank.non_NilEmpty()
        case 37:
            lblHeading.text = arrSalesReportHeading1[37]; lblValue.text = salesReportData.cha.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[37]; lblValue2.text = salesReportData.shippingAgent.non_NilEmpty()
        case 38:
            lblHeading.text = arrSalesReportHeading1[38]; lblValue.text = salesReportData.insuranceCompany.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[38]; lblValue2.text = salesReportData.materialCode.non_NilEmpty()
        case 39:
            lblHeading.text = arrSalesReportHeading1[39]; lblValue.text = salesReportData.materialDescription.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[39]; lblValue2.text = salesReportData.customerProductDescription.non_NilEmpty()
        case 40:
            lblHeading.text = arrSalesReportHeading1[40]; lblValue.text = salesReportData.batchNo.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[40]; lblValue2.text = salesReportData.perPack.non_NilEmpty()
        case 41:
            lblHeading.text = arrSalesReportHeading1[41]; lblValue.text = salesReportData.noPacking.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[41]; lblValue2.text = salesReportData.itemQty.non_NilEmpty()
        case 42:
            lblHeading.text = arrSalesReportHeading1[42]; lblValue.text = salesReportData.storageLocation.non_NilEmpty()
#warning("rate")
            lblHeading2.text = arrSalesReportHeading2[42]; lblValue2.text = salesReportData.exchangeRate.non_NilEmpty()
        case 43:
            lblHeading.text = arrSalesReportHeading1[43]; lblValue.text = salesReportData.itemValue.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[43]; lblValue2.text = salesReportData.inrValue.non_NilEmpty()
        case 44:
            lblHeading.text = arrSalesReportHeading1[44]; lblValue.text = salesReportData.poNo.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[44]; lblValue2.text = salesReportData.poDate.non_NilEmpty()
        case 45:
            lblHeading.text = arrSalesReportHeading1[45]; lblValue.text = salesReportData.materialGroup1.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[45]; lblValue2.text = salesReportData.materialGroup2.non_NilEmpty()
        case 46:
            lblHeading.text = arrSalesReportHeading1[46]; lblValue.text = salesReportData.materialGroup3.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[46]; lblValue2.text = salesReportData.materialGroup4.non_NilEmpty()
        case 47:
            lblHeading.text = arrSalesReportHeading1[47]; lblValue.text = salesReportData.materialGroup5.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[47]; lblValue2.text = salesReportData.materialGroup6.non_NilEmpty()
        case 48:
            lblHeading.text = arrSalesReportHeading1[48]; lblValue.text = salesReportData.materialGroup7.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[48]; lblValue2.text = salesReportData.materialGroup8.non_NilEmpty()
        case 49:
            lblHeading.text = arrSalesReportHeading1[49]; lblValue.text = salesReportData.materialGroup9.non_NilEmpty()
            lblHeading2.text = arrSalesReportHeading2[49]; lblValue2.text = salesReportData.materialGroup10.non_NilEmpty()
        default:
            lblHeading.text = "Unavailable"; self.lblValue.text = "Unavailable";
            secondStackView.isHidden = true
        }
        self.setNeedsLayout()
        self.setNeedsDisplay()
        self.layoutIfNeeded()
    }
    
    // MARK: - Sales Order Report Cell Initialization
    public func setSalesOrderReportInfo(forRow: Int) {
        self.secondStackView.isHidden = true
        switch forRow {
        case 0: lblHeading.text = arrSalesOrderReportHeading[0]; lblValue.text = salesOrderReportData.customerOrderNo.non_NilEmpty()
        case 1: lblHeading.text = arrSalesOrderReportHeading[1]; lblValue.text = salesOrderReportData.customerOrderDate.non_NilEmpty()
        case 2: lblHeading.text = arrSalesOrderReportHeading[2]; lblValue.text = salesOrderReportData.customerName.non_NilEmpty()
        case 3: lblHeading.text = arrSalesOrderReportHeading[3]; lblValue.text = salesOrderReportData.shipToName.non_NilEmpty()
        case 4: lblHeading.text = arrSalesOrderReportHeading[4]; lblValue.text = salesOrderReportData.mot.non_NilEmpty()
        case 5: lblHeading.text = arrSalesOrderReportHeading[5]; lblValue.text = salesOrderReportData.incoTerm1.non_NilEmpty()
        case 6: lblHeading.text = arrSalesOrderReportHeading[6]; lblValue.text = salesOrderReportData.incoTerm2.non_NilEmpty()
        case 7: lblHeading.text = arrSalesOrderReportHeading[7]; lblValue.text = salesOrderReportData.portOfDischarge.non_NilEmpty()
        case 8: lblHeading.text = arrSalesOrderReportHeading[8]; lblValue.text = salesOrderReportData.lcNo.non_NilEmpty()
        case 9: lblHeading.text = arrSalesOrderReportHeading[9]; lblValue.text = salesOrderReportData.lcDate.non_NilEmpty()
        case 10: lblHeading.text = arrSalesOrderReportHeading[10]; lblValue.text = salesOrderReportData.material.non_NilEmpty()
        case 11: lblHeading.text = arrSalesOrderReportHeading[11]; lblValue.text = salesOrderReportData.materialDescription.non_NilEmpty()
        case 12: lblHeading.text = arrSalesOrderReportHeading[12]; lblValue.text = salesOrderReportData.rate.non_NilEmpty()
        case 13: lblHeading.text = arrSalesOrderReportHeading[13]; lblValue.text = salesOrderReportData.totalValue.non_NilEmpty()
        case 14: lblHeading.text = arrSalesOrderReportHeading[14]; lblValue.text = salesOrderReportData.currency.non_NilEmpty()
        case 15: lblHeading.text = arrSalesOrderReportHeading[15]; lblValue.text = salesOrderReportData.scheduleDate.non_NilEmpty()
        case 16: lblHeading.text = arrSalesOrderReportHeading[16]; lblValue.text = salesOrderReportData.orderQuantity.non_NilEmpty()
        case 17: lblHeading.text = arrSalesOrderReportHeading[17]; lblValue.text = salesOrderReportData.salesQuantity.non_NilEmpty()
        case 18: lblHeading.text = arrSalesOrderReportHeading[18]; lblValue.text = salesOrderReportData.uom.non_NilEmpty()
        case 19: lblHeading.text = arrSalesOrderReportHeading[19]; lblValue.text = salesOrderReportData.balance.non_NilEmpty()
        case 20: lblHeading.text = arrSalesOrderReportHeading[20]; lblValue.text = salesOrderReportData.pss.non_NilEmpty()
        case 21: lblHeading.text = arrSalesOrderReportHeading[21]; lblValue.text = salesOrderReportData.mfgPlant.non_NilEmpty()
        case 22: lblHeading.text = arrSalesOrderReportHeading[22]; lblValue.text = salesOrderReportData.packing.non_NilEmpty()
        case 23: lblHeading.text = arrSalesOrderReportHeading[23]; lblValue.text = salesOrderReportData.itemStatus.non_NilEmpty()
        case 24: lblHeading.text = arrSalesOrderReportHeading[24]; lblValue.text = salesOrderReportData.orderStatus.non_NilEmpty()
        default: lblHeading.text = "Unavailable"; lblValue.text = "Unavailable"
        }
        self.setNeedsLayout()
        self.setNeedsDisplay()
        self.layoutIfNeeded()
    }
    
    // MARK: - Landing Cost Report
    public func set_Landing_Report_Info(for row: Int, otherHeadings: [String]?, otherValues: [String]?) {
        self.secondStackView.isHidden = false
        
        var otherCount = 0
        var mainOtherData = [String]()
        var mainOtherHeadings = [String]()
        
        if let otherHeadings, let otherValues {
            otherCount = otherHeadings.count
            mainOtherData = otherValues
            mainOtherHeadings = otherHeadings
        }
//        if let allOtherData = landingCostReportData.keyvalue?.convertToJSONObject() {
//            mainOtherData = allOtherData.compactMap { ($0.value as? Double)?.cleanValue }
//            otherHeadings = Array(allOtherData.keys)
//            otherCount = mainOtherData.count
//        }
        
        if row < arrLandingCostReportHeading1.count {
            switch row {
            case 0 :
                lblHeading.text = arrLandingCostReportHeading1[0]; lblValue.text = landingCostReportData.plant.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[0]; lblValue2.text = landingCostReportData.poNo.non_NilEmpty()
            case 1 :
                lblHeading.text = arrLandingCostReportHeading1[1]; lblValue.text = landingCostReportData.vendor.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[1]; lblValue2.text = landingCostReportData.productCode.non_NilEmpty()
            case 2 :
                lblHeading.text = arrLandingCostReportHeading1[2]; lblValue.text = landingCostReportData.productName.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[2]; lblValue2.text = landingCostReportData.unitOM.non_NilEmpty()
            case 3 :
                lblHeading.text = arrLandingCostReportHeading1[3]; lblValue.text = landingCostReportData.quantity.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[3]; lblValue2.text = landingCostReportData.price.non_NilEmpty()
            case 4 :
                lblHeading.text = arrLandingCostReportHeading1[4]; lblValue.text = landingCostReportData.total.non_NilEmpty();
                lblHeading2.text = arrLandingCostReportHeading2[4]; lblValue2.text = landingCostReportData.currency.non_NilEmpty()
            case 5 :
                lblHeading.text = arrLandingCostReportHeading1[5]; lblValue.text = landingCostReportData.partnercharges.non_NilEmpty();
                lblHeading2.text = arrLandingCostReportHeading2[5]; lblValue2.text = landingCostReportData.beExRate.non_NilEmpty()
            case 6 :
                lblHeading.text = arrLandingCostReportHeading1[6]; lblValue.text = landingCostReportData.bankCharges.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[6]; lblValue2.text = landingCostReportData.deec.non_NilEmpty()
            case 7 :
                lblHeading.text = arrLandingCostReportHeading1[7]; lblValue.text = landingCostReportData.bcd.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[7]; lblValue2.text = landingCostReportData.cvd.non_NilEmpty()
            case 8 :
                lblHeading.text = arrLandingCostReportHeading1[8]; lblValue.text = landingCostReportData.sad.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[8]; lblValue2.text = landingCostReportData.igst.non_NilEmpty()
            case 9 :
                lblHeading.text = arrLandingCostReportHeading1[9]; lblValue.text = landingCostReportData.cessOfCVD.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[9]; lblValue2.text = landingCostReportData.edvCess.non_NilEmpty()
            case 10 :
                lblHeading.text = arrLandingCostReportHeading1[10]; lblValue.text = landingCostReportData.antiDumping.non_NilEmpty();
                lblHeading2.text = arrLandingCostReportHeading2[10]; lblValue2.text = landingCostReportData.penalty.non_NilEmpty()
            case 11 :
                lblHeading.text = arrLandingCostReportHeading1[11]; lblValue.text = landingCostReportData.other.non_NilEmpty();
                lblHeading2.text = arrLandingCostReportHeading2[11]; lblValue2.text = landingCostReportData.secEducCess.non_NilEmpty()
            case 12 :
                lblHeading.text = arrLandingCostReportHeading1[12]; lblValue.text = landingCostReportData.socialWelfare.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[12]; lblValue2.text = landingCostReportData.totalINR.non_NilEmpty()
            case 13 :
                lblHeading.text = arrLandingCostReportHeading1[13]; lblValue.text = landingCostReportData.totalForegin.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[13]; lblValue2.text = landingCostReportData.totalINRGST.non_NilEmpty()
            case 14 :
                lblHeading.text = arrLandingCostReportHeading1[14]; lblValue.text = landingCostReportData.totalForeginGST.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[14]; lblValue2.text = landingCostReportData.oneQtyINR.non_NilEmpty()
            case 15 :
                lblHeading.text = arrLandingCostReportHeading1[15]; lblValue.text = landingCostReportData.oneQtyForegin.non_NilEmpty()
                lblHeading2.text = arrLandingCostReportHeading2[15]; lblValue2.text = landingCostReportData.oneQtyINRGST.non_NilEmpty()
            case 16 :
                lblHeading.text = arrLandingCostReportHeading1[16]; lblValue.text = landingCostReportData.oneQtyForeginGST.non_NilEmpty()
                
                if otherCount > 0 {
                    lblHeading2.text = mainOtherHeadings[0]; lblValue2.text = mainOtherData[0]
                } else {
                    secondStackView.isHidden = true
                }
            default:
                lblHeading.text = "Unavailable"; self.lblValue.text = "Unavailable";
                secondStackView.isHidden = true
            }
        } else {
            let index = ((row+1)-arrLandingCostReportHeading1.count)*2
            if index <= mainOtherHeadings.count {   // OtherCharges is within Index
                lblHeading.text =  mainOtherHeadings[index-1] + " : "; lblValue.text = mainOtherData[index-1]
                if index < mainOtherHeadings.count {    // Second Stack Should Exists
                    lblHeading2.text =  mainOtherHeadings[index] + " : "; lblValue2.text = mainOtherData[index]
                } else {
                    secondStackView.isHidden = true
                }
            } else {
                lblHeading.text = "Unavailable"; self.lblValue.text = "Unavailable";
                secondStackView.isHidden = true
            }
        }
        setNeedsLayout()
        setNeedsDisplay()
        layoutIfNeeded()
    }
    
    
//    // MARK: - Landing Cost Report Cell Initialization
//    public func setLandingCostReportInfo(forRow: Int) {
//        self.secondStackView.isHidden = false
//
//        var otherCount = 0
//        var mainOtherData = [Landing_Cost_OtherCharges_Data]()
//
//        if let allOtherData = landingCostData.otherCharges {
//            mainOtherData = allOtherData.filter { $0.pvalue != nil }
//            otherCount = mainOtherData.count
//        }
//
//        if forRow*2 < otherCount {
//            lblHeading.text = mainOtherData[forRow*2].ptype ?? "N/A" ; lblValue.text = mainOtherData[forRow*2].pvalue ?? "N/A" ;
//            let evenSec = (forRow*2)+1
//            if evenSec < otherCount {
//                lblHeading2.text = mainOtherData[evenSec].ptype ?? "N/A" ; lblValue2.text = mainOtherData[evenSec].pvalue ?? "N/A" ;
//            } else {
//                secondStackView.isHidden = true
//            }
//        } else {
//            let currRow = forRow - (otherCount % 2 == 0 ? otherCount/2 : (otherCount+1)/2)
//            switch currRow {
//            case 0:
//                lblHeading.text = arrLandingCostReportHeading1[0]; lblValue.text = landingCostData.beExchangeRate ?? "N/A";
//                lblHeading2.text = arrLandingCostReportHeading2[0]; lblValue2.text = landingCostData.cessOfCvd ?? "N/A";
//            case 1:
//                lblHeading.text = arrLandingCostReportHeading1[1]; lblValue.text = landingCostData.bankCharges.flatMap { String($0) } ?? "N/A";
//                lblHeading2.text = arrLandingCostReportHeading2[1]; lblValue2.text = landingCostData.edvCess ?? "N/A";
//            case 2:
//                lblHeading.text = arrLandingCostReportHeading1[2]; lblValue.text = landingCostData.deec ?? "N/A";
//                lblHeading2.text = arrLandingCostReportHeading2[2]; lblValue2.text = landingCostData.antiDumping ?? "N/A";
//            case 3:
//                lblHeading.text = arrLandingCostReportHeading1[3]; lblValue.text = landingCostData.bcd ?? "N/A";
//                lblHeading2.text = arrLandingCostReportHeading2[3]; lblValue2.text = landingCostData.penalty ?? "N/A";
//            case 4:
//                lblHeading.text = arrLandingCostReportHeading1[4]; lblValue.text = landingCostData.cvd ?? "N/A"
//                lblHeading2.text = arrLandingCostReportHeading2[4]; lblValue2.text = landingCostData.other ?? "N/A";
//            case 5:
//                lblHeading.text = arrLandingCostReportHeading1[5]; lblValue.text = landingCostData.sad ?? "N/A";
//                lblHeading2.text = arrLandingCostReportHeading2[5]; lblValue2.text = landingCostData.secEducCess ?? "N/A";
//            case 6:
//                lblHeading.text = arrLandingCostReportHeading1[6]; lblValue.text = landingCostData.igst.flatMap { String($0) } ?? "N/A";
//                lblHeading2.text = arrLandingCostReportHeading2[6]; lblValue2.text = landingCostData.socialWelfare.flatMap { String($0) } ?? "N/A";
//            case 7:
//                lblHeading.text = "Total \(landingCostData.currncy!) : "; lblValue.text = landingCostData.total ?? "N/A";
//                lblHeading2.text = "Total \(landingCostData.currncy!) (With GST) : "; lblValue2.text = landingCostData.totalWithGst ?? "N/A";
//            case 8:
//                lblHeading.text = arrLandingCostReportHeading1[8]; lblValue.text = landingCostData.totalInr ?? "N/A";
//                lblHeading2.text = arrLandingCostReportHeading2[8]; lblValue2.text = landingCostData.totalInrWithGst ?? "N/A";
//            default: lblHeading.text = "Unavailable"; lblValue.text = "Unavailable"
//            }
//        }
//    }
    
    // MARK: - Script Drawback Report Cell Initialization
    public func setUpScriptDrawbackCell(for row: Int) {
        secondStackView.isHidden = false
        switch row {
        case 0:
            lblHeading.text = arrScriptDrabackHeading1[0]; lblValue.text = scriptDrabackData.piNo.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[0]; lblValue2.text = scriptDrabackData.piDate.non_NilEmpty()
        case 1:
            lblHeading.text = arrScriptDrabackHeading1[1]; lblValue.text = scriptDrabackData.taxInvoiceNo.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[1]; lblValue2.text = scriptDrabackData.taxInvoiceDate.non_NilEmpty()
        case 2:
            lblHeading.text = arrScriptDrabackHeading1[2]; lblValue.text = scriptDrabackData.scheme1.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[2]; lblValue2.text = scriptDrabackData.license1.non_NilEmpty()
        case 3:
            lblHeading.text = arrScriptDrabackHeading1[3]; lblValue.text = scriptDrabackData.scheme2.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[3]; lblValue2.text = scriptDrabackData.license2.non_NilEmpty()
        case 4:
            lblHeading.text = arrScriptDrabackHeading1[4]; lblValue.text = scriptDrabackData.scheme3.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[4]; lblValue2.text = scriptDrabackData.license3.non_NilEmpty()
        case 5:
            lblHeading.text = arrScriptDrabackHeading1[5]; lblValue.text = scriptDrabackData.scheme4.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[5]; lblValue2.text = scriptDrabackData.license4.non_NilEmpty()
        case 6:
            lblHeading.text = arrScriptDrabackHeading1[6]; lblValue.text = scriptDrabackData.scriptNo.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[6]; lblValue2.text = scriptDrabackData.scriptDate.non_NilEmpty()
        case 7:
            lblHeading.text = arrScriptDrabackHeading1[7]; lblValue.text = scriptDrabackData.expiryDate.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[7]; lblValue2.text = scriptDrabackData.receivedDate.non_NilEmpty()
        case 8:
            #warning("Received Bank")
            lblHeading.text = arrScriptDrabackHeading1[8]; lblValue.text = scriptDrabackData.bankID.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[8]; lblValue2.text = scriptDrabackData.amount.non_NilEmpty()
        case 9:
            lblHeading.text = arrScriptDrabackHeading1[9]; lblValue.text = scriptDrabackData.fiNo.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[9]; lblValue2.text = scriptDrabackData.fiDate.non_NilEmpty()
        case 10:
            lblHeading.text = arrScriptDrabackHeading1[10]; lblValue.text = scriptDrabackData.customer.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[10]; lblValue2.text = scriptDrabackData.incoterm.non_NilEmpty()
        case 11:
            lblHeading.text = arrScriptDrabackHeading1[11]; lblValue.text = scriptDrabackData.paymentTerm.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[11]; lblValue2.text = scriptDrabackData.dueDate.non_NilEmpty()
        case 12:
            lblHeading.text = arrScriptDrabackHeading1[12]; lblValue.text = scriptDrabackData.realizationDate.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[12]; lblValue2.text = scriptDrabackData.totalValue.non_NilEmpty()
        case 13:
            lblHeading.text = arrScriptDrabackHeading1[13]; lblValue.text = scriptDrabackData.invCurrency.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[13]; lblValue2.text = scriptDrabackData.blNo.non_NilEmpty()
        case 14:
            lblHeading.text = arrScriptDrabackHeading1[14]; lblValue.text = scriptDrabackData.blDate.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[14]; lblValue2.text = scriptDrabackData.sbNo.non_NilEmpty()
        case 15:
            lblHeading.text = arrScriptDrabackHeading1[15]; lblValue.text = scriptDrabackData.sbDate.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[15]; lblValue2.text = scriptDrabackData.sbExchangeRate.non_NilEmpty()
        case 16:
            lblHeading.text = arrScriptDrabackHeading1[16]; lblValue.text = scriptDrabackData.letExportDate.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[16]; lblValue2.text = scriptDrabackData.sbFob.non_NilEmpty()
        case 17:
            lblHeading.text = arrScriptDrabackHeading1[17]; lblValue.text = scriptDrabackData.portCode.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[17]; lblValue2.text = scriptDrabackData.portName.flatMap(String.init).non_NilEmpty()
        case 18:
            lblHeading.text = arrScriptDrabackHeading1[18]; lblValue.text = scriptDrabackData.scheme1_SbValue.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[18]; lblValue2.text = scriptDrabackData.scheme1_Received.non_NilEmpty()
        case 19:
            lblHeading.text = arrScriptDrabackHeading1[19]; lblValue.text = scriptDrabackData.scheme2_SbValue.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[19]; lblValue2.text = scriptDrabackData.scheme2_Received.non_NilEmpty()
        case 20:
            lblHeading.text = arrScriptDrabackHeading1[20]; lblValue.text = scriptDrabackData.applied1_Date.non_NilEmpty()
            lblHeading2.text = arrScriptDrabackHeading2[20]; lblValue2.text = scriptDrabackData.applied2_Date.non_NilEmpty()
        case 21:
            lblHeading.text = arrScriptDrabackHeading1[21]; lblValue.text = scriptDrabackData.balance.non_NilEmpty()
            secondStackView.isHidden = true
        default:
            lblHeading.text = "Unavailable"; self.lblValue.text = "Unavailable";
            secondStackView.isHidden = true
        }
        self.setNeedsLayout()
        self.setNeedsDisplay()
        self.layoutIfNeeded()
    }
    
    // MARK: - Vessel Tracking Data
    public func setUpVesselDataCell(for row: Int) {
        detailButtonContentView.isHidden = true
        reportData_StackView.isHidden = false
        secondStackView.isHidden = true
        switch row {
        case 0:
            lblHeading.text = arrVesselTrackingHeading[0]; lblValue.text = vesselTrackingData.vesselName.non_NilEmpty()
        case 1:
            lblHeading.text = arrVesselTrackingHeading[1]; lblValue.text = vesselTrackingData.etaDateAccordingUser.non_NilEmpty()
        case 2:
            lblHeading.text = arrVesselTrackingHeading[2]; lblValue.text = vesselTrackingData.trackStatus.non_NilEmpty()
        case 3:
            lblHeading.text = arrVesselTrackingHeading[3]; lblValue.text = vesselTrackingData.shipmentStatus.non_NilEmpty()
        default:
            lblHeading.text = "Unavailable"; self.lblValue.text = "Unavailable";
            secondStackView.isHidden = true
        }
        self.setNeedsLayout()
        self.setNeedsDisplay()
        self.layoutIfNeeded()
    }
    
    // MARK: - Import Shipment Tracking View More Data
    public func setUpImportShipmentDataCell(for row: Int) {
        detailButtonContentView.isHidden = true
        reportData_StackView.isHidden = false
        secondStackView.isHidden = true
        switch row {
        case 0:
            lblHeading.text = arrImportShipmentTrackingHeading[0]; lblValue.text = importShipmentTrackingData.invoiceNumber.non_NilEmpty()
        case 1:
            lblHeading.text = arrImportShipmentTrackingHeading[1]; lblValue.text = importShipmentTrackingData.vesselName.non_NilEmpty()
        case 2:
            lblHeading.text = arrImportShipmentTrackingHeading[2]; lblValue.text = importShipmentTrackingData.status.non_NilEmpty()
        default:
            lblHeading.text = "Unavailable"; self.lblValue.text = "Unavailable";
            secondStackView.isHidden = true
        }
        self.setNeedsLayout()
        self.setNeedsDisplay()
        self.layoutIfNeeded()
    }
    
    public func is_Detail_Button_Only(index: Int, title: String?) {
        btnDetail.tag = index
        detailButtonContentView.isHidden = false
        DispatchQueue.main.async { [weak self] in
            self?.detailButtonContentView.layer.applyShadowAndRadius()
        }
        reportData_StackView.isHidden = true
        guard let title else { return }
        lblDetailButtonTitle.text = title
        setNeedsLayout()
        setNeedsDisplay()
        layoutIfNeeded()
    }
}

