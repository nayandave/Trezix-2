//
//  CollectionViewCell_Quote.swift
//  Trezix-2
//
//  Created by Nayan Dave on 02/09/23.
//

import UIKit

class CollectionViewCell_Quote: UICollectionViewCell {
    
    static let identifier = "QuotationAttribute_CollCell"
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var lblAttribute: UILabel!
    
    @IBOutlet weak var HeaderFooterContentView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var btnWinner: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        mainContentView.isHidden = false
        lblAttribute.isHidden = false
        HeaderFooterContentView.isHidden = true
    }
    
    public func setAttribute(_ att: String?) {
        lblAttribute.isHidden = false
        HeaderFooterContentView.isHidden = true
        lblAttribute.text = att.non_NilEmpty(true)
    }
    
    public func isCellHeader(heading: String) {
//        mainContentView.isHidden = true
        lblAttribute.isHidden = true
        HeaderFooterContentView.isHidden = false
        lblHeading.isHidden = false
        lblHeading.text = heading
        btnWinner.isHidden = true
    }
    
    public func isCellFooter(index: Int, isWinner: Bool = false) {
//        mainContentView.isHidden = true
        lblAttribute.isHidden = true
        HeaderFooterContentView.isHidden = false
        btnWinner.isEnabled = !isWinner
        lblHeading.isHidden = true
        btnWinner.isHidden = false
        btnWinner.tag = index
    }
}
