//
//  QuotationDetailCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 25/08/23.
//

import UIKit

class QuotationDetailCell: UITableViewCell {
    
    static let identifier = "QuotationDetailCell"
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func setCellValue(_ value: String?) {
        separatorView.isHidden = false
        lblValue.text = value.non_NilEmpty(true)
        setNeedsDisplay()
        layoutIfNeeded()
    }
    
    public func isCellHeader(heading: String) {
        separatorView.isHidden = false
        mainContentView.backgroundColor = .ourAppThemeColor
        lblValue.textColor = .white
        lblValue.text = heading
    }
    
    public func isCellFooter() {
        separatorView.isHidden = true
        mainContentView.backgroundColor = .ourAppThemeColor
        lblValue.text = ""
    }
}
