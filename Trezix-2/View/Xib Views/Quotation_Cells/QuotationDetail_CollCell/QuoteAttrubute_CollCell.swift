//
//  QuoteAttrubute_CollCell.swift
//  Trezix-2
//
//  Created by Nayan Dave on 04/09/23.
//

import UIKit

protocol AttributeScrollDelegate {
    func scrollAttribute(currentIndex: Int, scrollLeft: Bool)
}
class QuoteAttrubute_CollCell: UICollectionViewCell {
    
    static let identifier = "QuoteAttrubute_CollCell"
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var lblAttribute: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var HeaderFooterContentView: UIView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var btnWinner: UIButton!
    
    var delegate: AttributeScrollDelegate?
    public var currentIndex = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblAttribute.isHidden = false
        HeaderFooterContentView.isHidden = true
        separatorView.isHidden = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        btnWinner.layer.applyShadowAndRadius()
    }
    
    public func setAttribute(_ att: String?) {
        lblAttribute.isHidden = false
        separatorView.isHidden = false
        HeaderFooterContentView.isHidden = true
        lblAttribute.text = att.non_NilEmpty(true)
    }
    
    public func isCellHeader(heading: String) {
        lblAttribute.isHidden = true
        separatorView.isHidden = false
        HeaderFooterContentView.isHidden = false
        lblHeading.isHidden = false
        lblHeading.text = heading
        btnWinner.isHidden = true
    }
    
    public func isCellFooter(index: Int, shouldSelectWinner: Bool, isWinner: Bool = false) {
        lblAttribute.isHidden = true
        separatorView.isHidden = true
        HeaderFooterContentView.isHidden = false
        if shouldSelectWinner || isWinner {
            btnWinner.setTitle(isWinner ? "Winner" : "Make Winner", for: .normal)
            btnWinner.isEnabled = !isWinner
            btnWinner.isHidden = false
        } else {
            btnWinner.isHidden = true
        }
        btnWinner.tag = index
        lblHeading.isHidden = true
        currentIndex = index
        btnLeft.isHidden = true
        btnRight.isHidden = true
    }
    
    public func configureSideButtons(currIndex: Int, totalCount: Int) {
        btnLeft.tag = currIndex
        btnRight.tag = currIndex
        if totalCount > 1 {
            if currIndex == 0 {
                btnLeft.isHidden = true
                btnRight.isHidden = false
            } else if currIndex == totalCount-1 {
                btnLeft.isHidden = false
                btnRight.isHidden = true
            } else {
                btnLeft.isHidden = false
                btnRight.isHidden = false
            }
        } else {
            btnLeft.isHidden = true
            btnRight.isHidden = true
        }
    }
    
    @IBAction func scroll_Left_Action(_ sender: UIButton) {
        delegate?.scrollAttribute(currentIndex: sender.tag, scrollLeft: true)
    }
    
    @IBAction func scroll_Right_Action(_ sender: UIButton) {
        delegate?.scrollAttribute(currentIndex: sender.tag, scrollLeft: false)
    }
}
