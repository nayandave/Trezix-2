//
//  SideMenuHeaderSectionView.swift
//  Trezix-2
//
//  Created by Amar Panchal on 06/10/22.
//

import UIKit

class SideMenuHeaderSectionView: UITableViewHeaderFooterView {
    
    static let identifier = "SideMenuHeaderSectionView"
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var shadowContentView: UIView!
    
    @IBOutlet weak var selectionBarView: UIView!
    
    @IBOutlet weak var logoHeaderView: UIImageView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var imageSectionHasSubmenu: UIImageView!
    
    public var currentCell: Int = 0
    public var hasSubMenu: Bool = false
    
    
    public func insertHeaderData(title: String, icon: String? = nil, showSubmenu: Bool = false, index: Int) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lblHeaderTitle.text = title
            if let strIcon = icon {
                self.logoHeaderView.image = UIImage(named: strIcon)
            }
            self.imageSectionHasSubmenu.isHidden = !showSubmenu
            self.hasSubMenu = showSubmenu
            self.currentCell = index
            self.shadowContentView.backgroundColor = .clear
        }
    }
    
    public func animateDetailIndicator(show: Bool) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            UIView.transition(with: self.imageSectionHasSubmenu, duration: 0.15, options: [.curveEaseInOut], animations: {
                self.imageSectionHasSubmenu.transform = show ? CGAffineTransform(rotationAngle: -.pi/2) : .identity
            }, completion: nil)
        }
    }
    
    public func changeBackground(selected: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            UIView.animate(withDuration: 0, delay: 0, options: [.curveEaseInOut, .transitionCrossDissolve]) {
                self.selectionBarView.isHidden = !selected
                self.logoHeaderView.tintColor = selected ? .themeYellowColor : .white
                self.imageSectionHasSubmenu.tintColor = selected ? .themeYellowColor : .white
                self.lblHeaderTitle.textColor = selected ? .themeYellowColor : .white
                self.lblHeaderTitle.changeFontStyle(style: selected ? .Bold : .Regular, fontSize: 18)
            }
        }
    }
}
