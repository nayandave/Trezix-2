//
//  SideMenuTableCell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 06/10/22.
//

import UIKit

class SideMenuTableCell: UITableViewCell {

    @IBOutlet weak var shadowContentView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var lblDash: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageHasSubmenu: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        imageLogo.tintColor = .white
        imageHasSubmenu.tintColor = .white
        lblTitle.textColor = .white
        lblDash.textColor = .white
    }
    
    public func insertingData(title: String, icon: String? = nil, showSubmenu: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.lblTitle.text = title
            if let strIcon = icon {
                self.imageLogo.image = UIImage(named: strIcon)
            }
            self.imageHasSubmenu.isHidden = !showSubmenu
        }
    }
    
    public func changeBackground(selected: Bool = false) {
//        DispatchQueue.main.async { [weak self] in
//            guard let self = self else { return }
            UIView.animate(withDuration: 0, delay: 0, options: [.curveLinear], animations: {
//                self.backgroundColor = selected ? .themeYellowColor : .clear
                self.imageLogo.tintColor = selected ? .themeYellowColor : .white
                self.imageHasSubmenu.tintColor = selected ? .themeYellowColor : .white
                self.lblTitle.textColor = selected ? .themeYellowColor : .white
                self.lblTitle.changeFontStyle(style: selected ? .Bold : .Regular, fontSize: 16)
            }, completion: nil)
//        }
    }
}
