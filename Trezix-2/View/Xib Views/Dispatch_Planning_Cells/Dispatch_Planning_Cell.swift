//
//  Dispatch_Planning_Cell.swift
//  Trezix-2
//
//  Created by Amar Panchal on 06/01/23.
//

import UIKit

class Dispatch_Planning_Cell: UITableViewCell {
    
    static let identifier = "Dispatch_Planning_Cell"
    
    // MARK: - All Outlets
    @IBOutlet weak var mainContentView: UIView!
    
    @IBOutlet weak var totalStackView: UIStackView!
    @IBOutlet weak var lblTotalHeading: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    @IBOutlet weak var viewDetailContentView: UIView!
    @IBOutlet weak var btnViewDetail: UIButton!
    
    @IBOutlet weak var defaultDataView: UIView!
    @IBOutlet weak var lblPINumber: UILabel!
    @IBOutlet weak var lblProductDetail: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    
    //MARK: - Constants & Variables
    var currentSection = Int()
    
    let defaultHeadingBGColor = UIColor.ourAppThemeColor.withAlphaComponent(0.25)
    let defaultDataCellBGColor = UIColor.tertiarySystemBackground
    
    let totalViewBGColor = UIColor.tertiarySystemGroupedBackground
    let viewDetailBGColor = UIColor.ourAppThemeColor.withAlphaComponent(0.1)
    

    override func awakeFromNib() {
        super.awakeFromNib()
        defaultDataView.isHidden = true
        totalStackView.isHidden = true
        viewDetailContentView.isHidden = true
    }
    
    // MARK: - Helper Methods
    public func ChangeView(isTotalView: Bool, section: Int) {
        btnViewDetail.tag = section
        defaultDataView.isHidden = true
        totalStackView.isHidden = !isTotalView
        viewDetailContentView.isHidden = isTotalView
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.mainContentView.backgroundColor = isTotalView ? self.totalViewBGColor : self.viewDetailBGColor
            self.defaultDataView.backgroundColor = .clear
        }
    }
    
    public func setDefaultData(isHeading: Bool = false, data1: String? = nil, data2: String? = nil, data3: String? = nil) {
        totalStackView.isHidden = true
        viewDetailContentView.isHidden = true
        defaultDataView.isHidden = false
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.defaultDataView.backgroundColor = isHeading ? self.defaultHeadingBGColor : self.defaultDataCellBGColor
            self.mainContentView.backgroundColor = .clear
            [self.lblPINumber, self.lblProductDetail, self.lblQuantity].forEach { label in
                label?.textColor = isHeading ? UIColor.ourAppThemeColor : UIColor.label
                label?.changeFontStyle(style: isHeading ? .Bold : .Regular, fontSize: isHeading ? 17 : 15)
            }
        }
        
        lblPINumber.text = isHeading ? "PI No." : data1
        lblProductDetail.text = isHeading ? "Product Detail" : data2
        lblQuantity.text = isHeading ? "Quantity" : data3
    }
}
