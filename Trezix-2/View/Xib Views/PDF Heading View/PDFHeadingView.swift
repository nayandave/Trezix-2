//
//  PDFHeadingView.swift
//  Trezix-2
//
//  Created by Amar Panchal on 27/09/22.
//

import UIKit

class PDFHeadingView: UIView {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonIniting()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonIniting()
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonIniting() {
        Bundle.main.loadNibNamed("PDFHeadingView", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}
