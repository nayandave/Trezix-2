//
//  ThreeColumnRowView.swift
//  Trezix-2
//
//  Created by Amar Panchal on 15/10/22.
//

import UIKit

class ThreeColumnRowView: UIView {

    @IBOutlet var containerView: UIView!
    
    @IBOutlet weak var shadowContentView: UIView!
    @IBOutlet weak var lblHeading1: UILabel!
    @IBOutlet weak var lblHeading2: UILabel!
    @IBOutlet weak var lblHeading3: UILabel!
    @IBOutlet weak var lblHeading4: UILabel!
    
    @IBOutlet weak var thirdColumnStackView: UIStackView!
    
    @IBOutlet weak var separator1: UIView!
    @IBOutlet weak var separator2: UIView!
    @IBOutlet weak var separator3: UIView!
    
    @IBOutlet weak var leadingSep: UIView!
    @IBOutlet weak var trailingSep: UIView!
    @IBOutlet weak var topSep: UIView!
    @IBOutlet weak var bottomSep: UIView!
    
    @IBOutlet weak var disclosureView: UIView!
    @IBOutlet weak var imageDisclosure: UIImageView!
    
    @IBOutlet weak var customButton_ContainerView: UIView!
    @IBOutlet weak var customButton: UIButton!
    
    public var currentRow = Int()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ThreeColumnRowView", owner: self)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    public func isViewHeadingView(heading: Bool = false, withSeparator: Bool = true, threeColumn: Bool = true, tag: Int) {
        currentRow = tag
        customButton.tag = tag
        DispatchQueue.main.async { [weak self] in
            self?.shadowContentView.backgroundColor = heading ? .ourAppThemeColor : .systemBackground
            [self?.lblHeading1, self?.lblHeading2, self?.lblHeading3].forEach { label in
                label?.changeFontStyle(style: heading ? .Bold : .Regular, fontSize: heading ? getDynamicHeight(size: 16) : getDynamicHeight(size: 14))
                label?.textColor = heading ? .white : .label
            }
            self?.disclosureView.isHidden = heading
            
            [self?.separator1, self?.separator2].forEach { sep in
                sep?.isHidden = !withSeparator
                sep?.backgroundColor = .label
            }
            self?.thirdColumnStackView.isHidden = threeColumn
            if threeColumn {
                self?.separator3.isHidden = true
            } else {
                self?.separator3.isHidden = !withSeparator
                self?.separator3.backgroundColor = .label
            }
        }
    }
    
    public func isSelected(selected: Bool = false, lastCell: Bool = false, isCustomButton: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            self?.disclosureView.isHidden = isCustomButton
            self?.customButton_ContainerView.isHidden = !isCustomButton
            self?.customButton.layer.applyShadowAndRadius(cornerRadius: 5)
            self?.imageDisclosure.image = UIImage(named: selected ? "ic-DetailUpImage" : "ic-DetailDownImage")
            self?.bottomSep.isHidden = selected ? true : !lastCell
        }
    }
    
    public func setData(firstLabel: String?, secondLabel: String?, thirdLabel: String?, fourthLabel: String? = nil, threeColumnView: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.lblHeading1.text = firstLabel ?? "N/A"
            self?.lblHeading2.text = secondLabel ?? "N/A"
            self?.thirdColumnStackView.isHidden = threeColumnView
            if threeColumnView {
                self?.lblHeading4.text = thirdLabel ?? "N/A"
            } else {
                self?.lblHeading3.text = thirdLabel ?? "N/A"
                self?.lblHeading4.text = fourthLabel ?? "N/A"
            }
        }
    }
}
