//
//  SortableHeaderThreeSectionView.swift
//  Trezix-2
//
//  Created by Amar Panchal on 17/11/22.
//

import UIKit

class SortableHeaderThreeSectionView: UIView {
    
    @IBOutlet weak var lblHeadingSection1: UILabel!
    @IBOutlet weak var lblHeadingSection2: UILabel!
    @IBOutlet weak var lblHeadingSection3: UILabel!
    
    @IBOutlet weak var btnSection1: UIButton!
    @IBOutlet weak var btnSection2: UIButton!
    @IBOutlet weak var btnSection3: UIButton!
    
    @IBInspectable var heading1: String {
        get {
            self.lblHeadingSection1.text = defaultHeading1
            return defaultHeading1
        }
        set(newHead) {
            self.lblHeadingSection1.text = newHead
        }
    }
    
    @IBInspectable var heading2: String {
        get {
            self.lblHeadingSection2.text = defaultHeading2
            return defaultHeading2
        }
        set(newHead) {
            self.lblHeadingSection2.text = newHead
        }
    }
    
    @IBInspectable var heading3: String {
        get {
            self.lblHeadingSection3.text = defaultHeading3
            return defaultHeading3
        }
        set(newHead) {
            self.lblHeadingSection3.text = newHead
        }
    }
    
    
    private let defaultHeading1 = "PO No."
    private let defaultHeading2 = "PO Date"
    private let defaultHeading3 = "Vendor Name"
    
    
    
    @IBAction func Change_First_Sec_Sorting(_ sender: UIButton) {
        
    }
    
    @IBAction func Change_Second_Sec_Sorting(_ sender: UIButton) {
        
    }
    
    @IBAction func Change_Third_Sec_Sorting(_ sender: UIButton) {
        
    }
}
